<!DOCTYPE html>
<html lang="en" class="login_page">

<!-- Mirrored from gebo-admin-3.tzdthemes.com/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jul 2018 09:47:31 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>IT Centre - Login Page</title>

    <!-- Bootstrap framework -->
    <link rel="stylesheet" href="{{asset('admin')}}/bootstrap/css/bootstrap.min.css" />
    <!-- theme color-->
    <link rel="stylesheet" href="{{asset('admin')}}/css/blue.css" />
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('admin')}}/lib/qtip2/jquery.qtip.min.css" />
    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('admin')}}/css/style.css" />

    <!-- favicon -->
    <link rel="shortcut icon" href="favicon.ico" />

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="{{asset('admin')}}/js/ie/html5.js"></script>
    <script src="{{asset('admin')}}/js/ie/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="login_box">

    <form method="POST" action="{{ url('/login') }}" id="login_form">
        <div class="top_b">Sign in to IT CENTRE</div>
        <div class="cnt_b">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="email" name="email" id="username" class="form-control input-sm" placeholder="Email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" id="password" name="password" class="form-control input-sm" placeholder="Password" required>
                </div>
            </div>

        </div>
        <div class="btm_b clearfix">
            <button class="btn btn-default btn-sm pull-right" type="submit">Sign In</button>
        </div>
    </form>

    <div class="links_b links_btm clearfix">
        <span class="linkform"><a href="{{ url('/password/reset') }}">Forgot password?</a></span>
    </div>

</div>

<script src="{{asset('admin')}}/js/jquery.min.js"></script>
<script src="{{asset('admin')}}/js/jquery.actual.min.js"></script>
<script src="{{asset('admin')}}/lib/validation/jquery.validate.js"></script>
<script src="{{asset('admin')}}/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){

        //* boxes animation
        form_wrapper = $('.login_box');
        function boxHeight() {
            form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);
        };
        form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
        $('.linkform a,.link_reg a').on('click',function(e){
            var target	= $(this).attr('href'),
                    target_height = $(target).actual('height');
            $(form_wrapper).css({
                'height'		: form_wrapper.height()
            });
            $(form_wrapper.find('form:visible')).fadeOut(400,function(){
                form_wrapper.stop().animate({
                    height	 : target_height,
                    marginTop: ( - (target_height/2) - 24)
                },500,function(){
                    $(target).fadeIn(400);
                    $('.links_btm .linkform').toggle();
                    $(form_wrapper).css({
                        'height'		: ''
                    });
                });
            });
            e.preventDefault();
        });

        //* validation
        $('#login_form').validate({
            onkeyup: false,
            errorClass: 'error',
            validClass: 'valid',
            rules: {
                username: { required: true, minlength: 3 },
                password: { required: true, minlength: 3 }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass("f_error");
                setTimeout(function() {
                    boxHeight()
                }, 200)
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass("f_error");
                setTimeout(function() {
                    boxHeight()
                }, 200)
            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').append(error);
            }
        });
    });
</script>

</body>

</html>

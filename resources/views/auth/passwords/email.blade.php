<!DOCTYPE html>
<html lang="en" class="login_page">
<?php $comp = \App\Company_detail::find(1)?>
<!-- Mirrored from gebo-admin-3.tzdthemes.com/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jul 2018 09:47:31 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{$comp->company_name}} - RESET</title>

    <!-- Bootstrap framework -->
    <link rel="stylesheet" href="{{asset('admin')}}/bootstrap/css/bootstrap.min.css" />
    <!-- theme color-->
    <link rel="stylesheet" href="{{asset('admin')}}/css/blue.css" />
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('admin')}}/lib/qtip2/jquery.qtip.min.css" />
    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('admin')}}/css/style.css" />

    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('company_logo/')}}/favicon.png" />

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="{{asset('admin')}}/js/ie/html5.js"></script>
    <script src="{{asset('admin')}}/js/ie/respond.min.js"></script>
    <![endif]-->

</head>
<body style="background-image: url('{{asset('uploads')}}/bglogin.jpg');background-size: cover">

<div class="login_box">
    <form role="form" method="POST" action="{{ url('/password/email') }}" id="login_form">
        {{ csrf_field() }}
        <div class="top_b" style="background-color: #3993ba!important;color: #ffffff">{{$comp->company_name}}</div>
        <div class="cnt_b">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 col-xs-offset-3 col-md-offset-3 col-sm-offset-3">
                    <img src="{{asset('company_logo/logo.png')}}">
                </div>
            </div>


            <br>
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-8 col-xs-offset-3 col-md-offset-3 col-sm-offset-3">
                    RESET PASSWORD
                </div>
            </div>
<BR>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-user"></i></span>

                    <input type="email" name="email" id="username" class="form-control input-sm" placeholder="Email" value="{{ old('email') }}" required autofocus>

                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>

        </div>
        <div class="btm_b clearfix">
            <button class="btn btn-success btn-sm" type="submit">Send Password Reset Link</button>
        </div>
    </form>

</div>

<script src="{{asset('admin')}}/js/jquery.min.js"></script>
<script src="{{asset('admin')}}/js/jquery.actual.min.js"></script>
<script src="{{asset('admin')}}/lib/validation/jquery.validate.js"></script>
<script src="{{asset('admin')}}/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){

        //* boxes animation
        form_wrapper = $('.login_box');
        function boxHeight() {
            form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);
        };
        form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
        $('.linkform a,.link_reg a').on('click',function(e){
            var target	= $(this).attr('href'),
                    target_height = $(target).actual('height');
            $(form_wrapper).css({
                'height'		: form_wrapper.height()
            });
            $(form_wrapper.find('form:visible')).fadeOut(400,function(){
                form_wrapper.stop().animate({
                    height	 : target_height,
                    marginTop: ( - (target_height/2) - 24)
                },500,function(){
                    $(target).fadeIn(400);
                    $('.links_btm .linkform').toggle();
                    $(form_wrapper).css({
                        'height'		: ''
                    });
                });
            });
            e.preventDefault();
        });

        //* validation
        $('#login_form').validate({
            onkeyup: false,
            errorClass: 'error',
            validClass: 'valid',
            rules: {
                username: { required: true, minlength: 3 },
                password: { required: true, minlength: 3 }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass("f_error");
                setTimeout(function() {
                    boxHeight()
                }, 200)
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass("f_error");
                setTimeout(function() {
                    boxHeight()
                }, 200)
            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').append(error);
            }
        });
    });
</script>

</body>

</html>

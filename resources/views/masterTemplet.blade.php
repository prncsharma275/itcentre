<!DOCTYPE html>
<html lang="en">
<?php $comp = \App\Company_detail::find(1)?>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{$comp->company_name}}</title>

    <!-- Bootstrap framework -->
    <link rel="stylesheet" href="{{asset('admin/bootstrap/css/bootstrap.min.css')}}" />
    <!-- jQuery UI theme -->
    <link rel="stylesheet" href="{{asset('admin/lib/jquery-ui/css/Aristo/Aristo.css')}}" />
    <!-- breadcrumbs -->
    <link rel="stylesheet" href="{{asset('admin/lib/jBreadcrumbs/css/BreadCrumb.css')}}" />
    <!-- tooltips-->
    <link rel="stylesheet" href="{{asset('admin/lib/qtip2/jquery.qtip.min.css')}}" />
    <!-- colorbox -->
    <link rel="stylesheet" href="{{asset('admin/lib/colorbox/colorbox.css')}}" />
    <!-- code prettify -->
    <link rel="stylesheet" href="{{asset('admin/lib/google-code-prettify/prettify.css')}}" />
    <!-- sticky notifications -->
    <link rel="stylesheet" href="{{asset('admin/lib/sticky/sticky.css')}}" />
    <!-- aditional icons -->
    <link rel="stylesheet" href="{{asset('admin/img/splashy/splashy.css')}}" />
    <!-- flags -->
    <link rel="stylesheet" href="{{asset('admin/img/flags/flags.css')}}" />
    <!-- datatables -->
    <link rel="stylesheet" href="{{asset('admin/lib/datatables/extras/TableTools/media/css/TableTools.css')}}">

    <!-- font-awesome -->
    <link rel="stylesheet" href="{{asset('admin/img/font-awesome/css/font-awesome.min.css')}}" />
    <!-- calendar -->
    <link rel="stylesheet" href="{{asset('admin/lib/fullcalendar/fullcalendar_gebo.css')}}" />

    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}" />
    <!-- theme color-->
    <link rel="stylesheet" href="{{asset('admin/css/blue.css')}}" id="link_theme" />

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('company_logo/')}}/favicon.png" />

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie.css" />
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="{{asset('admin/')}}js/ie/html5.js"></script>
    <script src="{{asset('admin/')}}js/ie/respond.min.js"></script>
    <script src="{{asset('admin/')}}lib/flot/excanvas.min.js"></script>
    <![endif]-->    </head>
<body class="full_width">

<div id="maincontainer" class="clearfix">

    <header>

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-inner">
                <div class="container-fluid">

                    <a class="brand pull-left" href="{{url('home')}}">{{$comp->company_name}}
                    </a>

                    <ul class="nav navbar-nav user_menu pull-right">


                        <li class="divider-vertical hidden-sm hidden-xs"></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('admin/img/user_avatar.png')}}" alt="" class="user_avatar">{{ Auth::user()->name }}<b class="caret"></b></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="user_profile.html">My Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="{{url('logout')}}">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>



        {{----}}

    </header>
    <div id="contentwrapper">
        <div class="main_content">
            <div id="jCrumbs" class="breadCrumb module">
                <ul>
                    <li>
                        <a href="#"><i class="glyphicon glyphicon-home"></i></a>
                    </li>

                </ul>
            </div>	<div class="row">
                <div class="col-sm-12 tac">
                    <ul class="ov_boxes">
                        <li>
                            <div class="p_bar_up p_canvas"><span>20,25,9,7,12,8,16</span></div>
                            <div class="ov_text">
                                <?php $upto_date = date('Y-m-d');
                                $from_date= date('Y-m-d', strtotime('-7 day', strtotime($upto_date)));
                                ?>
                                <?php  $qnty_sale = \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('grand_total') ?>
                                <strong><span class="fa fa-rupee"></span>{{ sprintf('%0.2f', $qnty_sale)}}</strong>
                                Weekly Sale
                            </div>
                        </li>
                        <li>
                            <div class="p_bar_down p_canvas"><span>20,15,18,14,10,13,9,7,11,2,5,1,22,1,5,10,15,1,3,9,8,11,1,5,8</span></div>
                            <div class="ov_text">

                                <?php $currentMonth = date('m');
                                $data_for_month = \App\Sale::whereRaw('MONTH(billing_date) = ?',[$currentMonth])->whereNull('status')->sum('grand_total'); ?>


                                <strong><span class="fa fa-rupee"></span>{{ sprintf('%0.2f', $data_for_month)}}</strong>
                                Monthly Sale
                            </div>
                        </li>
 {{--------------------------------------------------- purchase -------------------------------}}
                        <li>
                            <?php $upto_date = date('Y-m-d');
                            $from_date= date('Y-m-d', strtotime('-7 day', strtotime($upto_date)));
                            ?>
                            <?php  $qnty_purchase = \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('grand_total') ?>
                            @if($qnty_purchase>0)
                            <div class="p_bar_blue p_canvas"><span style="color: blue!important;">12,,6,4,9,10,8,4</span></div>
                            @else
                                    <div class="p_bar_blue p_canvas"><span style="color: blue!important;">0,0,0,0,0,0,0,0</span></div>
                            @endif
                            <div class="ov_text">

                                <strong><span class="fa fa-rupee"></span>{{ sprintf('%0.2f', $qnty_purchase)}}</strong>
                                Weekly Purchase
                            </div>
                        </li>

                        <li>

                            <?php $currentMonth = date('m');
                            $pur_for_month = \App\Purchase::whereRaw('MONTH(billing_date) = ?',[$currentMonth])->whereNull('status')->sum('grand_total'); ?>

                                @if($pur_for_month>0)
                                    <div class="p_bar_yellow p_canvas"><span style="color: blue!important;">8,8,9,12,12,24,16,1,12,1,2,5,6,4,9,10,1,2,8,4,3,2,1</span></div>
                                @else
                                    <div class="p_bar_yellow p_canvas"><span style="color: blue!important;">0,0,0,0,0,0,0,0</span></div>
                                @endif
                            <div class="ov_text">
                                <strong><span class="fa fa-rupee"></span>{{ sprintf('%0.2f', $pur_for_month)}}</strong>
                                Monthly Purchase
                            </div>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <ul class="dshb_icoNav clearfix">
                        <li><a href="{{url('users')}}" style="background-image: url(admin/img/gCons/multi-agents.png)"><span class="label label-info"><span class="fa fa-eye"></span> {{\App\User::count()}}</span> Users</a></li>
                        <li><a href="{{url('product')}}" style="background-image: url(admin/img/gCons/product.png)"><span class="label label-danger"><span class="fa fa-list"></span> {{\App\Product::count()}}</span>Products</a></li>
                        <li><a href="{{url('customers')}}" style="background-image: url(admin/img/gCons/ledger.png)"><span class="label label-primary"><span class="fa fa-user"></span>  {{\App\Customer::count()}}</span> Ledgers</a>
                            <?php $start_date = date("Y").'-04-01' ?>
                            <?php $end_date = date('Y', strtotime('+1 year')).'-03-31'; ?>
                        </li><li><a href="{{url('sale')}}" style="background-image: url(admin/img/gCons/sales.png)"><span class="label label-success"><span class="fa fa-rupee"></span> {{ sprintf('%0.2f', \App\Sale::whereBetween('billing_date', array($start_date, $end_date))->whereNull('status')->sum('grand_total'))}}</span> Sale</a></li>
                        <li><a href="{{url('purchase')}}" style="background-image: url(admin/img/gCons/sales.png)"><span class="label label-warning"><span class="fa fa-rupee"></span> {{ sprintf('%0.2f', \App\Purchase::whereBetween('billing_date', array($start_date, $end_date))->whereNull('status')->sum('grand_total'))}}</span>Purchase</a></li>
                        <li><a href="{{url('sale_type')}}" style="background-image: url(admin/img/gCons/newsale.png)">New Sale</a></li>
                        <li><a href="{{url('purchase_type')}}" style="background-image: url(admin/img/gCons/purchase.png)">New Purchase</a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <h3 class="heading">Overall  Sales Details <small>Current Year</small></h3>
                    {{--<div id="fl_2" style="height:200px;width:80%;margin:50px auto 0"></div>--}}
                    <div id="fl_2" style="height:270px;width:90%;margin:15px auto 0"></div>
                </div>
                <div class="col-sm-6">
                    <div class="heading clearfix">
                        <h3 class="pull-left">Comparision Data</h3>
                        <span class="pull-right label label-info bs_ttip" data-placement="left" data-container="body" title="Here is a sample info tooltip">Info</span>
                    </div>
                    <div id="fl_1" style="height:270px;width:100%;margin:15px auto 0"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h3 class="heading">Calendar</h3>
                    <div id="calendar"></div>
                </div>


            </div>                </div>
    </div>

</div>

<a href="javascript:void(0)" class="sidebar_switch on_switch bs_ttip" data-placement="auto right" data-viewport="body" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">

    <div class="sidebar_inner_scroll">
        <div class="sidebar_inner">
            <form action="{{url('home')}}" class="input-group input-group-sm" method="post">
                <input autocomplete="off" name="query" class="search_query form-control input-sm" size="16" placeholder="Search..." type="text">
                <span class="input-group-btn"><a href="#"  class="btn btn-default"><i class="glyphicon glyphicon-search"></i></a></span>
            </form>
            <div id="side_accordion" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-list-alt"></i> Transaction
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseOne">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('sale')}}">Sale</a></li>
                                <li><a href="{{url('purchase')}}">Purchase</a></li>
                                <li><a href="{{url('saleReturn')}}">Sale Return</a></li>
                                <li><a href="{{url('purchaseReturn')}}">Purchase Return</a></li>
                                <li><a href="{{url('sale_format_index')}}">Other Sale Bill</a></li>
                                <li><a href="{{url('challan')}}">Challan</a></li>
                                <li><a href="{{url('challan_return')}}">Challan Return</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-qrcode"></i> Inventory
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseTwo">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('product')}}">Products</a></li>
                                <li><a href="{{url('product_type')}}">Categories</a></li>
                                <li><a href="{{url('brand')}}">Brands</a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-user"></i> Ledgers
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThree">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('customers')}}">Ledgers</a></li>
                                <li><a href="{{url('ledger_type_route')}}">Ledgers Types</a></li>
                                <li><a href="{{url('ledger_group_route')}}">Ledger Group</a></li>
                            </ul>

                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThreeOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-tasks"></i> Accounts
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThreeOne">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('payment')}}">Payments</a></li>
                                <li><a href="{{url('receipt')}}">Receipts</a></li>
                                <li><a href="{{url('otherCharge')}}">Contro Entry</a></li>
                            </ul>

                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThreeTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-certificate"></i> Allowence
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThreeTwo">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('creditnote')}}">Credit Note</a></li>
                                <li><a href="{{url('debitnote')}}">Debit Note</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThreeThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-file"></i> Reports
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThreeThree">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('day_report')}}">Day Reports</a></li>
                                <li><a href="{{url('report')}}">All Reports</a></li>

                            </ul>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseFour" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-cog"></i> Settings
                        </a>
                    </div>
                    <div class="accordion-body collapse in" id="collapseFour">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('users')}}">Employee</a></li>
                                <li><a href="{{url('#')}}">Profile</a></li>
                                <li><a href="{{url('company')}}">Company Settings</a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapse7" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="fa fa-calculator"></i> Calculator
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapse7">
                        <div class="panel-body">
                            <form name="Calc" id="calc">
                                <div class="formSep input-group input-group-sm">
                                    <input class="form-control" name="Input" type="text"/>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" name="clear" value="c" onclick="Calc.Input.value = ''">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </span>
                                </div>
                                <div class="form-group">
                                    <input class="btn form-control btn-default input-sm" name="seven" value="7" onclick="Calc.Input.value += '7'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="eight" value="8" onclick="Calc.Input.value += '8'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="nine" value="9" onclick="Calc.Input.value += '9'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="div" value="/" onclick="Calc.Input.value += ' / '" type="button">
                                </div>
                                <div class="form-group">
                                    <input class="btn form-control btn-default input-sm" name="four" value="4" onclick="Calc.Input.value += '4'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="five" value="5" onclick="Calc.Input.value += '5'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="six" value="6" onclick="Calc.Input.value += '6'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="times" value="x" onclick="Calc.Input.value += ' * '" type="button">
                                </div>
                                <div class="form-group">
                                    <input class="btn form-control btn-default input-sm" name="one" value="1" onclick="Calc.Input.value += '1'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="two" value="2" onclick="Calc.Input.value += '2'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="three" value="3" onclick="Calc.Input.value += '3'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="minus" value="-" onclick="Calc.Input.value += ' - '" type="button">
                                </div>
                                <div class="formSep form-group">
                                    <input class="btn form-control btn-default input-sm" name="dot" value="." onclick="Calc.Input.value += '.'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="zero" value="0" onclick="Calc.Input.value += '0'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="DoIt" value="=" onclick="Calc.Input.value = Math.round( eval(Calc.Input.value) * 1000)/1000" type="button">
                                    <input class="btn form-control btn-default input-sm" name="plus" value="+" onclick="Calc.Input.value += ' + '" type="button">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="push"></div>
        </div>

    </div>

</div>
<script data-cfasync="false" src="{{asset('admin/cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.min.js')}}"></script>
<script src="{{asset('admin/js/jquery-migrate.min.js')}}"></script>
<script src="{{asset('admin/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js')}}"></script>
<!-- touch events for jquery ui-->
<script src="{{asset('admin/js/forms/jquery.ui.touch-punch.min.js')}}"></script>
<!-- easing plugin -->
<script src="{{asset('admin/js/jquery.easing.1.3.min.js')}}"></script>
<!-- smart resize event -->
<script src="{{asset('admin/js/jquery.debouncedresize.min.js')}}"></script>
<!-- js cookie plugin -->
<script src="{{asset('admin/js/jquery_cookie_min.js')}}"></script>
<!-- main bootstrap js -->
<script src="{{asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- bootstrap plugins -->
<script src="{{asset('admin/js/bootstrap.plugins.min.js')}}"></script>
<!-- typeahead -->
<script src="{{asset('admin/lib/typeahead/typeahead.min.js')}}"></script>
<!-- code prettifier -->
<script src="{{asset('admin/lib/google-code-prettify/prettify.min.js')}}"></script>
<!-- sticky messages -->
<script src="{{asset('admin/lib/sticky/sticky.min.js')}}"></script>
<!-- lightbox -->
<script src="{{asset('admin/lib/colorbox/jquery.colorbox.min.js')}}"></script>
<!-- jBreadcrumbs -->
<script src="{{asset('admin/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js')}}"></script>
<!-- hidden elements width/height -->
<script src="{{asset('admin/js/jquery.actual.min.js')}}"></script>
<!-- custom scrollbar -->
<script src="{{asset('admin/lib/slimScroll/jquery.slimscroll.js')}}"></script>
<!-- fix for ios orientation change -->
<script src="{{asset('admin/js/ios-orientationchange-fix.js')}}"></script>
<!-- to top -->
<script src="{{asset('admin/lib/UItoTop/jquery.ui.totop.min.js')}}"></script>
<!-- mobile nav -->
<script src="{{asset('admin/js/selectNav.js')}}"></script>
<!-- moment.js date library -->
<script src="{{asset('admin/lib/moment/moment.min.js')}}"></script>

<!-- common functions -->
<script src="{{asset('admin/js/pages/gebo_common.js')}}"></script>

<!-- multi-column layout -->
<script src="{{asset('admin/js/jquery.imagesloaded.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.wookmark.js')}}"></script>
<!-- responsive table -->
<script src="{{asset('admin/js/jquery.mediaTable.min.js')}}"></script>
<!-- small charts -->
<script src="{{asset('admin/js/jquery.peity.min.js')}}"></script>
<!-- charts -->
<script src="{{asset('admin/lib/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('admin/lib/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('admin/lib/flot/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('admin/lib/flot.tooltip/jquery.flot.tooltip.min.js')}}"></script>
<!-- calendar -->
<script src="{{asset('admin/lib/fullcalendar/fullcalendar.min.js')}}"></script>
<!-- sortable/filterable list -->
<script src="{{asset('admin/lib/list_js/list.min.js')}}"></script>
<script src="{{asset('admin/lib/list_js/plugins/paging/list.paging.min.js')}}"></script>
<!-- dashboard functions -->
<script src="{{asset('admin/js/pages/gebo_dashboard.js')}}"></script>
<script>
    $(document).ready(function() {

        // Setup the placeholder reference
        var elem = $('#fl_2');

        var data = [
                @for($i=1;$i<=12;$i++)
                           <?php  $qnty_sale = \App\Sale::whereNull('status')->whereRaw('MONTH(billing_date) = ?', [$i])->sum('grand_total') ?>
           @if($qnty_sale)
            {
                <?php $m= \App\Month::find($i) ?>
               label: "{{$m->month_in_word}}",
                data: {{$qnty_sale}}
            },
                @endif
            @endfor
                                 {{--{{$m->month_in_word}}    --}}
        ];

        // Setup the flot chart using our data


        $.plot(elem, data,
                {
                    label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%s ( %p.1% )",
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    },
                    colors: ["#8064A2", "#4BACC6", "#F79646", "#2C4D75", "#0CC162", "#EFC31A", "#174356", "#C0504D" , "#FC0D1B" , "#35D6AF" , "#1480D3" , "pink"]
                }
        );



    });




</script>


<script>
    $(document).ready(function() {
        //* jQuery.browser.mobile (http://detectmobilebrowser.com/)
        //* jQuery.browser.mobile will be true if the browser is a mobile device
        (function(a){jQuery.browser.mobile=/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
        //replace themeforest iframe
        if(jQuery.browser.mobile) {
            if (top !== self) top.location.href = self.location.href;
        }
    });
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-65389904-1', 'auto');
    ga('send', 'pageview');

</script>

</body>

<!-- Mirrored from gebo-admin-3.tzdthemes.com/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jul 2018 09:45:03 GMT -->
</html>

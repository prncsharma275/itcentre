@extends('layouts.adminPanel')
@section('title')
    Company:Settings
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }

.well{
    background-color: #ffffff!important;
    -webkit-box-shadow: 3px 3px 18px 2px ;  /* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
    -moz-box-shadow:    3px 3px 18px 2px  ; /* Firefox 3.5 - 3.6 */
    box-shadow:         3px 3px 18px 0px ;

}
/*input{*/
    /*text-transform: uppercase!important;*/
/*}*/

    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')

    <li>
        <a href="{{url('#')}}">Company Settings</a>
    </li>


@endsection

@section('content')

            <h3 class="heading">Company Details</h3>


            {{--<form class="well form-horizontal" action="storeCustomer" method="POST"  id="contact_form">--}}
            <?php echo Form::open(array('url' =>['storeCompany',1],'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
               <div class="well">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class=" has-success has-feedback">
                        <label>Company Name<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text" class="form-control" name="company_name" value="{{$company->company_name}}" placeholder="Company Name"required>


                        </div>

                    </div>

                    <div class="col-md-6 col-sm-6 ">
                        <div class=" has-success has-feedback">
                            <label>Street-1<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text" class="form-control" name="street1" value="{{$company->street_1}}" placeholder="street-1"required>


                        </div>

                    </div>

                </div> <!-- row end here -->

                   <div class="row">
                       <div class="col-md-6 col-sm-6">
                           <div class=" has-success has-feedback">
                               <label>Street-2</label>

                               <input  type="text" class="form-control" name="street2" value="{{$company->street_2}}" placeholder="Street-2">



                           </div>

                       </div>

                       <div class="col-md-6 col-sm-6 ">
                           <div class=" has-success has-feedback">
                               <label>City<span style="color: red; font-size: 15px">*</span></label>

                               <input  type="text" class="form-control" name="city" value="{{$company->city}}" placeholder="Address"required>


                           </div>

                       </div>

                   </div> <!-- row end here -->



                   <div class="row">
                       <div class="col-md-6 col-sm-6">
                           <div class=" has-success has-feedback">
                               <label>State<span style="color: red; font-size: 15px">*</span></label>

                               <input  type="text" class="form-control" name="state" value="{{$company->state}}" placeholder="State" required>



                           </div>

                       </div>

                       <div class="col-md-6 col-sm-6 ">
                           <div class=" has-success has-feedback">
                               <label>Zip</label>

                               <input  type="text" class="form-control" name="zip" value="{{$company->zip}}" placeholder="Zip">


                           </div>

                       </div>

                   </div> <!-- row end here -->

            <div class="row">
                <div class="col-md-6 col-sm-6 ">
                    <div class=" has-success has-feedback">
                        <label>Phone</label>

                        <input  type="text" class="form-control" name="phone" value="{{$company->phone}}" placeholder="Phone">


                    </div>

                </div>

                <div class="col-md-6 col-sm-6 ">
                    <div class=" has-success has-feedback">
                        <label>Mobile<span style="color: red; font-size: 15px">*</span></label>

                        <input  type="text" class="form-control" name="mobile" value="{{$company->mobile}}" placeholder="Mobile No."required>


                    </div>

                </div>

            </div>



            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class=" has-success has-feedback">
                        <label>Email<span style="color: red; font-size: 15px">*</span></label>

                        <input  type="text" class="form-control" name="email" value="{{$company->email}}" placeholder="Email Id"required>


                    </div>

                </div>

                <div class="col-md-6 col-sm-6 ">
                    <div class=" has-success has-feedback">
                        <label>Website</label>

                        <input  type="text" class="form-control" name="website" value="{{$company->website}}" placeholder="website">


                    </div>

                </div>

            </div>




                   <div class="row">
                       <div class="col-md-6 col-sm-6 ">
                           <div class=" has-success has-feedback">
                               <label>GST No.</label>

                               <input  type="text" class="form-control" name="gst" value="{{$company->gst_no}}" placeholder="GST NO.">


                           </div>

                       </div>

                       <div class="col-md-6 col-sm-6 ">
                           <div class=" has-success has-feedback">
                               <label>Company Logo</label>

                               <input  type="file" class="form-control" name="logo" value="{{$company->logo}}" placeholder="Company Logo">


                           </div>

                       </div>

                   </div>

               </div>

            <h3 class="heading">Bill No. Format</h3>
            <div class="well">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Sale Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="sale_bill" value="{{$company->billing_sale}}" placeholder="CMC/SALE/"required>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Purchase Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="purchase_bill" value="{{$company->billing_purchase}}" placeholder="CMC/PUR/"required>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Sale Return Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="sale_return_bill" value="{{$company->billing_sale_return}}" placeholder="CMC/SR/"required>

                        </div>
                    </div>
                  </div>

                    <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Purchase Return Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="purchase_return_bill" value="{{$company->billing_purchase_return}}" placeholder="CMC/PR/"required>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Receipt Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="receipt_bill" value="{{$company->billing_receipt}}" placeholder="CMC/REC/"required>
                            {{ csrf_field() }}
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Payment Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="payment_bill" value="{{$company->billing_payment}}" placeholder="CMC/PAY/"required>

                        </div>
                    </div>

                    </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Debit Note Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="debit_note_bill" value="{{$company->billing_debit_note}}" placeholder="CMC/DN/"required>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Credit Note Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="credit_note_bill" value="{{$company->billing_credit_note}}" placeholder="CMC/CN/"required>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Contra Note Bill Format<span style="color: red; font-size: 15px">*</span></label>

                            <input  type="text"  class="form-control" name="contra_note_bill" value="{{$company->billing_contra}}" placeholder="CMC/CON/"required>

                        </div>
                    </div>

                </div> <!-- row end here -->

            </div>

                <br>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-6 col-sm-6">
                        <button type="submit" class="btn btn-warning" >Update <span class="glyphicon glyphicon-hdd"></span></button>
                        <a class="btn btn-success" href="{{url('home')}}">Back</a>
                    </div>
                </div>
                   <br>

            {{--</Form>--}}
            {{form::close()}}
                    <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}


            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            <br>
            @endsection


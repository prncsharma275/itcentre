@extends('layouts.adminPanelTable')
@section('title')
    Customer Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('users')}}">User Control</a>
    </li>
@endsection

@section('content')

  @if(Auth::user()->role != 'admin')

      <script type="text/javascript">
          window.location = "{{url('/')}}";//here double curly bracket
      </script>
      @endif
            <h3 class="heading">Users Control</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createUsers')}}"><i class="splashy-document_letter_add"></i> User</a>
  <table class="table table-striped table-bordered dTableR" id="dt_a">
                <thead>
                <tr>
                    <th class="text-center">#&nbsp;&nbsp;&nbsp;</th>

                    <th class="text-center">Name</th>

                    <th class="text-center">Email</th>

                    <th class="text-center">Mobile</th>
                    <th class="text-center">Nature&nbsp;&nbsp;</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)

                    <tr class="item{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->mobile}}</td>
                        <td>{{$item->role}}</td>


                        <td>

                            <a href="editUser/{{$item->id}}" class="btn btn-default btn-sm"title="EDIT">
                                <i class="splashy-document_letter_edit"></i>
                            </a>
                            <a href="deleteUser/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()" title="DELETE">
                                <i class="splashy-document_letter_remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <script>
                $(document).ready(function() {
                    $('#table').DataTable();
                } );

                //    delete commande
                function ConfirmDelete()
                {
                    var x = confirm("Are you sure you want to Cancel This Bill?");
                    if (x)
                        return true;
                    else
                        return false;
                }


            </script>
            {{--inner content here ------------------------------------}}

     @endsection
@extends('layouts.adminPanel')

{{--Page title --}}
@section('title')
    Add New User
@endsection

{{--New css link here --}}
@section('custom_css')
@endsection

{{--inline css code--}}
@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
            border-color: #3c763d!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('users')}}">User Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Edit Users</a>
    </li>
@endsection

@section('content')
    @if(Auth::user()->role != 'admin')

        <script type="text/javascript">
            window.location = "{{url('/')}}";//here double curly bracket
        </script>
    @endif

    <h3 class="heading">Update User</h3>
    <?php echo Form::open(array('url' =>['updateUser',$viewUser->id],'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
    <div class="row">
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="col-md-4 col-sm-4 form-group">
            <label>Name<span style="color: red; font-size: 15px">*</span></label>
            <input  type="text" class="form-control"  value="{{$viewUser->name}}" name="name" placeholder="Name"required>

        </div>



        <div class="col-md-4 col-sm-4 form-group">
            <label>Designation<span style="color: red">*</span></label>
            <select  class="form-control" style="width:100%" name="role"required>
                <option value="{{$viewUser->role}}">{{$viewUser->role}}</option>
                <option value="admin">Admin</option>
                <option value="developer">Developer</option>
                <option value="receptionist">Receptionist</option>
                <option value="marketing">Marketing</option>
                <option value="office boy">Office Boy</option>
                <option value="accountant">Accountant</option>

            </select>

        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <label>Employee Code<span style="color: red; font-size: 11px">*</span></label>
            <input  type="text" class="form-control" value="{{$viewUser->employee_code}}" name="employee_code" placeholder="Employee code"readonly>

        </div>

    </div> <!-- row end here -->



    <div class="row">


        <div class="col-md-4 col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>Address Line-1 <span style="color: red">*</span></label>
                <input  type="text" class="form-control" value="{{$viewUser->add1}}" name="add1" placeholder="Address Line-1"required>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>Address Line-2</label>
                <input  type="text" value="{{$viewUser->add2}}" class="form-control" name="add2" placeholder="Address Line-1">
            </div>
        </div>

        <div class="col-md-4 col-sm-4 form-group">
            <label>City<span style="color: red; font-size: 11px">*</span></label>
            <input  type="text" class="form-control" value="{{$viewUser->city}}" name="city" placeholder="City"required>

        </div>
    </div> <!-- end row here -->

    <div class="row">


        <div class="col-md-4 col-sm-4 form-group">
            <label>State<span style="color: red; font-size: 11px">*</span></label>
            <input  type="text" class="form-control" value="{{$viewUser->state}}" name="state" placeholder="State"required>
        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <label>Zip</label>
            <input  type="text" class="form-control" value="{{$viewUser->zip}}"  name="zip" placeholder="Zip">

        </div>

        <div class="col-md-4 col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>Mobile-1 <span style="color: red">*</span></label>
                <input  type="text" class="form-control" value="{{$viewUser->mobile_1}}" name="mobile_1" placeholder="mobile-1"required>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>Mobile-2</label>
                <input  type="text" class="form-control" value="{{$viewUser->mobile_2}}" name="mobile_2" placeholder="Mobile-2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>

        <div class="col-md-4 col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>Email <span style="color: red">*</span></label>
                <input  type="email" class="form-control" value="{{$viewUser->email}}" name="email" placeholder="Email"required>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>PAN Card</label>
                <input  type="text" class="form-control" value="{{$viewUser->pan_no}}" name="pan_no" placeholder="PAN card">
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4  col-sm-4 form-group">
            <div class=" has-success has-feedback">
                <label>Date of Birth <span style="color: red">*</span></label>
                <input  type="text" class="form-control datepicker" value="{{$viewUser->dob}}" name="dob" placeholder="DOB" required>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <label>Date of Joining<span style="color: red; font-size: 11px">*</span></label>
            <input  type="text" class="form-control datepicker" value="{{$viewUser->doj}}" name="doj" placeholder="Date of Joining"required>
        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <label>Aadhar Card</label>
            <input  type="text" class="form-control" value="{{$viewUser->adhar_no}}" name="adhar_no" placeholder="Adhar card">

        </div>
    </div>

    <div class="row">

        <div class="col-md-4 col-sm-4 form-group">
            <label>Bank Account No.</label>
            <input style="" type="text" class="form-control" value="{{$viewUser->account_no}}" name="account_no" placeholder="Account No.">

        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <label>Bank Name</label>
            <input  type="text" class="form-control" value="{{$viewUser->bank_name}}"  name="bank_name" placeholder="Bank Name">

        </div>

        <div class="col-md-4 col-sm-4 form-group">
            <label>Branch Name</label>
            <input style="" type="text" class="form-control" value="{{$viewUser->branch_name}}" name="branch_name" placeholder="Branch name">

        </div>

    </div>

    <div class="row">

        <div class="col-md-4 col-sm-4 form-group">
            <label>IFSC Code</label>
            <input  type="text" class="form-control" value="{{$viewUser->ifsc_code}}" name="ifsc_code" placeholder="IFSC code">

        </div>

        <div class="col-md-4 col-sm-4 form-group">
            <label>Account Password<span style="color: red; font-size: 11px;">*</span></label>
            <input style="" type="password" class="form-control" value="{{$viewUser->password}}"  name="password" placeholder="Enter Password"required>

        </div>
        <div class="col-md-4 col-sm-4 form-group">
            <label>Confirm Password<span style="color: red; font-size: 11px;">*</span></label>
            <input  type="password" class="form-control" name="password_confirmation" value="{{$viewUser->password}}" placeholder="Enter Password"required>

        </div>

    </div>
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-4 col-md-offset-3 col-sm-offset-3">
            <button type="submit" class="btn btn-warning" >Update <span class="glyphicon glyphicon-hdd"></span></button>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4">
            <button type="reset" class="btn btn-info" >Reset <span class="glyphicon glyphicon-refresh"></span></button>
        </div>

        <div class="col-md-2 col-sm-2 col-xs-4">
            <a class="btn btn-success" href="{{url('users')}}">Back</a>
        </div>
    </div>

    {{form::close()}}
            <!-- form row end here -->
    {{---------------------------------------------------------------------------------}}

    <br>
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <br><br>
@endsection
<?php use Illuminate\Auth\SessionGuard; ?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">


    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->

    <link href="{{ asset('masterTemplet/dist/css/dashboard.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<div class="row1">
    <div class="col-2">
        <div class="company">
            <span id="logo">SNS INFOTECH</span>
        </div>

    </div>
    <div class="col-10">
        <div class="menubar">
            <div class="menu_left"><a href="#">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                    <img src="{{ asset('salesAndPurchaseStyle/images/notification.png')}}" width="24">
                    <span class="noti">5</span>
                </a>


                <div class="menu_right">
                    @if(Auth::guard('customer')->user())
                        <a class="menu_link" href="{{url('CustomerHome')}}"><i class="fa fa-user-secret" style="font-size:16px"> {{Auth::guard('customer')->user()->customer_name}}</i> </a>
                    @endif

                    <a class="menu_link" href="{{url('editCustomerFront')}}"><i class="fa fa-server" style="font-size:16px"> Settings </i> </a>
                    <a class="menu_link" href=""> </a>
                    <?php if(Auth::guard('customer')->user()) {?>
                    <a class="menu_link" href="logout"><i class="fa fa-sign-out" style="font-size:16px"> Logout </i></a>
                    <?php } ?>

                </div>
            </div>
        </div><!-- menubar end here -->
    </div>

    <div class="row1">
        <div class="col-2">
            <div class="master_sidebar">
                <ul class="sidebar_ul">
                    <li class="sidebar_li active"><i class="fa fa-address-card-o" style="font-size:16px"></i> <a class="menu_link" href="#">Overview <span class="sr-only">(current)</span></a></li>
                    <li class="sidebar_li"><i class="fa fa-archive" style="font-size:16px"></i> <a class="menu_link" href="#">My Account Details</a></li>
                    <li class="sidebar_li"><i class="fa fa-area-chart" style="font-size:16px"></i> <a class="menu_link" href="#">My Orders</a></li>
                    <li class="sidebar_li"><i class="fa fa-balance-scale" style="font-size:16px"></i> <a class="menu_link" href="#">Gift Card</a></li>
                    <li class="sidebar_li"><i class="fa fa-bar-chart" style="font-size:16px"></i> <a class="menu_link" href="#"> My Wishlist</a></li>
                    <li class="sidebar_li"><i class="fa fa-binoculars" style="font-size:16px"></i> <a class="menu_link" href="{{ url('CustomerServiceIndex') }}">My Service Details</a></li>
                    <li class="sidebar_li"><i class="fa fa-book" style="font-size:16px"></i> <a class="menu_link" href="#">Download Order Invoice</a></li>
                    <li class="sidebar_li"><i class="fa fa-briefcase" style="font-size:16px"></i> <a class="menu_link" href="#">My Saved Cards</a></li>
                    <li class="sidebar_li"><i class="fa fa-building" style="font-size:16px"></i> <a class="menu_link" href="#">24x7 Customer Care</a></li>
                    <li class="sidebar_li"><i class="fa fa-calculator" style="font-size:16px"></i> <a class="menu_link" href="#">Terms & Conditions</a></li>
                    <li class="sidebar_li"><i class="fa fa-chain" style="font-size:16px"></i> <a class="menu_link" href="{{url('editCustomerFront')}}">Profile Settings</a></li>
                    <li class="sidebar_li"><i class="fa fa-cloud-download" style="font-size:16px"></i> <a class="menu_link" href="#">Deactivate Account</a></li>
                </ul>
            </div>
        </div>
        <div class="col-10">
            <div class="mainContent">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                @yield('content')


            </div>
        </div>
    </div>



</body>
</html>

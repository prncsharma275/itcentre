<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SNS:INVOICE</title>
    <link rel="stylesheet" href="{{ asset('salesAndPurchaseStyle/css/printInvoice.css')}}" media="all" />
    <style type="text/css" media="print">
    @page {
    size: auto;   /* auto is the initial value */
    margin-left: 25px;  /* this affects the margin in the printer settings */
    margin-right: 25px;  /* this affects the margin in the printer settings */
    margin-top:0;  /* this affects the margin in the printer settings */
    }
        @media print{
            .below_notice{
                width: 600px!important;
                margin-right: 10px!important;
            }
            .bottom_box{
               position: relative;
                bottom: 0px;
            }
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img src="{{asset('salesAndPurchaseStyle/images/logo.png')}}">
    </div>



    <?php $sales = DB::table('sales')->where([
            ['customer_id', '=',$customer_id ],
            ['sale_id', '=', $sale_id],
    ])->get();


    ?>

    <div id="company">
        <h1 class="name">SNS INFOTECH</h1>
        <div>L.L.R ROAD,ASHRAMPARA, SILIGURI</div>
        <div>(602) 519-0450</div>
        <div><a href="mailto:company@example.com">company@example.com</a></div>
    </div>
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <?php $customer = DB::table('customers')->where([
                    ['id', '=',$customer_id ],
                     ])->get();

            ?>
            @foreach($customer as $customer)
            <div class="to">SALE TO:</div>
            <h2 class="name">{{$customer->customer_name}}</h2>
            <div class="address">{{$customer->address}} {{$customer->city}}, {{$customer->state}}, {{$customer->zip}}</div>
            <div class="email">{{$customer->mobile}}</div>
            <div class="email"><a href="mailto:{{$customer->email}}">{{$customer->email}}</a></div>
        </div>
        @endforeach
        <div id="invoice">
            <h1>{{$sale_id}}</h1>
            <div class="date">Date of Invoice:{{$billing_date}}</div>
            <div class="date">Due Date: 12/01/2017</div>
        </div>
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc">DESC.</th>
            <th class="unit">UNIT<br>PRICE</th>
            <th class="qty">QNTY</th>
            <th class="unit">DISC.<br>RATE.</th>
            <th class="qty">DISC.<br>AMT.</th>
            <th class="unit">AMT.</th>
            <th class="qty">TAX<br>RATE</th>
            <th class="unit">TAX<br>AMT.</th>
            <th class="total">TOTAL</th>
        </tr>
        </thead>
        <tbody>
        <?php $count = $sales->count(); ?>


       <?php $i=1 ?>

        @foreach($sales as $sale)
        <tr>

            <td class="no">{{$i}}</td>
             <td class="desc"><h3>
                     <?php $product = DB::table('products')->where([
                             ['id', '=',$sale->product_id ],
                     ])->get(); ?>
                     @foreach($product as $product)
                         {{$product->product_name}}
                         @endforeach
                 </h3>
                 <?php if($sale->description) {?>
                 ({{$sale->description}})
                 <?php }?>
                 <br>Serial No:-{{$sale->serial_number}}</td>
            <td class="unit">${{$sale->rate}}</td>
            <td class="qty">{{$sale->quantity}}Pcs.</td>
            <td class="unit">
                <?php if($sale->discount_percentage) {?>
                {{$sale->discount_percentage}}%
                <?php }?></td>
            <td class="qty">{{$sale->discount_amount}}</td>
            <td class="unit">${{$sale->amount}}</td>
            <td class="qty">{{$sale->vat_rate}}%</td>
            <td class="unit">${{$sale->vat_amount}}</td>
            <td class="total">${{$sale->sub_total}}</td>
        </tr>
       <?php $i++ ; ?>
       @endforeach

        </tbody>
    </table>
    <div class="bottom_box">
        <div class="below_notice" style="float: left; margin-right: 36px">
            <div id="thanks">Thank you!</div>
            <div id="notices">
                <div>NOTICE:</div>
                <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
            </div>
        </div>
        <table class="grand_total">
            <tfoot>
            <tr>
                <td colspan="5" style=""></td>
                <td colspan="3">SUBTOTAL</td>
                <td>${{$total_amount_withoutVat}}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td colspan="3">TOTAL TAX</td>
                <td>${{$total_vat}}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td colspan="3">SPCL. DISCOUNT</td>
                <td>(-){{$total_spcl_discount}}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td colspan="3">CARRIAGE OUTWARD</td>
                <td>${{$fright}}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td colspan="3">TOTAL</td>
                <td>${{$total_after_vat}}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td colspan="3">GRAND TOTAL</td>
                <td>${{$grand_total}}</td>
            </tr>
            </tfoot>
        </table>
        <footer>
            Invoice was created on a computer and is valid without the signature and seal.
        </footer>
    </div>


</main>

</body>
</html>
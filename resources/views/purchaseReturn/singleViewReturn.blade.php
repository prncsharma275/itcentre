@extends('layouts.adminPanel')
@section('title')
    Purchase Return:Create
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('purchaseReturn')}}">Purchase Return Panel</a>
    </li>

    <li>
        <a href="{{url('#')}}">Create Purchase Return</a>
    </li>
@endsection
@section('content')
    {{-- ----------------------------------------inner content here --------------------------------------------------------}}
    <h3 class="heading">Purchase Return(View Mode)</h3>

    <div class="row"> <!-- row start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Supplier Name</label>
                <input type="text" name="order_no" class="form-control" id="inputSuccess2"value="{{$editSale->belongsToCustomer->ledger_name}}" placeholder="Order No."readonly>

            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">P. Return No.<span style="color:#EB3E28;">*</span></label>

                <?php
                $orderdate_entry = explode('-', $editSale->billing_date);
                $year_entry = $orderdate_entry[0];
                $month_entry   = $orderdate_entry[1];
                $day_entry  = $orderdate_entry[2];

                ?>

                <input type="text" class="form-control" name="invoice_no" value="<?php echo $editSale->invoice_no ?>" id="inputSuccess2"readonly>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Invoice Date<span style="color:#EB3E28;">*</span></label>
                <input type="text" name="invoice_date" class="form-control datepicker" value="{{$day_entry}}/{{$month_entry}}/{{$year_entry}}"readonly>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Against Purchase No.</label>
                <input type="text" name="order_no" class="form-control" id="inputSuccess2" value="{{$editSale->against_purchase_no}}" placeholder="Order No."readonly>
            </div>
        </div>
    </div> <!-- row ends here -->

    <br>


    <table id="items"class="table table-striped table-bordered table-condensed" style="font-size: 13px!important;">

        <tr style="background-color: #e9f3f8;font-size: 12px">
            <th>Item</th>
            <th>Desc.</th>
            <th>MRP</th>
            <th>Qnty.</th>
            <th>Rate</th>
            <th>Amt.</th>
            <th>Disc.</th>
            <th>Txbl Amt.</th>
            <th>GST %</th>
            <th>Txt.Amt</th>
            <th>Total</th>

        </tr>
        <?php $sale_invoice = DB::table('purchase_returns_invoice')->where([
                ['purchase_return_id', '=',$editSale->id ],

        ])->get();  ?>

        @foreach($sale_invoice as $sale)
            <tr class="item-row" style="border-bottom: solid 1px black">
                <td style="width: 25%;position: relative" class="item-name">

                    <select  name="rows[0][product]" class="form-control" id="product_name" style="width:99%;padding: 9px!important;" >
                        <?php $product_name = DB::table('products')->select('id', 'product_name')->where([
                                ['id', '=',$sale->product_id ],  ])->first();  ?>
                        <option value="{{$product_name->id}}" selected="selected">{{$product_name->product_name}}</option>


                    </select>
                </td>
                <td class="main_td"><input name="rows[0][desc]" value="{{$sale->desc}}" class="form-control" readonly></td>
                <td class="main_td"><input name="rows[0][mrp]" value="{{$sale->mrp}}" class="mrp form-control"  readonly></td>
                <td class="main_td"><input name="rows[0][qty]" value="{{$sale->quantity}}" class="qty form-control"  readonly></td>
                <td><input name="rows[0][rate]" class="cost form-control" value="{{$sale->rate}}" readonly></td>
                <td class="main_td"><input name="rows[0][amount]" value="{{$sale->amount_before_tax}}" class="amt_before_tax form-control" readonly></td>
                <td class="main_td"><input name="rows[0][disc]" value="{{$sale->discount_amt}}" class="disc form-control"  readonly></td>
                <td class="main_td"><input name="rows[0][taxbl_amount]" value="{{$sale->taxable_amount}}" class="taxbl_amount form-control"  readonly></td>
                @if($editSale->tax_type=="CGST&SGST")
                    <?php
                    $total_tax_percentage=$sale->sgst_tax+$sale->cgst_tax; ?>
                @else
                    <?php    $total_tax_percentage=$sale->igst_tax;  ?>
                @endif
                <td class="main_td"><input name="rows[0][gst]" value="{{$total_tax_percentage}}" class="igst form-control" style="width: 40px;text-align: center"readonly ></td>
                <td class="main_td"><input name="rows[0][text_amount]" value="{{$sale->tax_amount}}" class="text_amount form-control" style="width:75px;text-align: center" readonly></td>
                <td class="main_td"><input name="rows[0][price]" value="{{$sale->total_amount}}" class="price form-control" style="width: 100px" readonly></td>
            </tr>
        @endforeach




    </table>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{url('printPurchaseReturn')}}/{{$editSale->id}}" class="btn btn-success btn-sm" name="submit"><span class="fa fa-print"></span> Print</a>
                </div>

                <div class="col-md-3">
                    <a href="{{url('purchaseReturn')}}" class="btn btn-danger btn-sm"><span class="fa fa-backward"></span> Back</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered table-striped">
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
                <tr>
                    <td width="50%">Total Amount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_amount_text">{{$editSale->amount_without_anything}}</span>
                        <input type="hidden" name="total_amount_input" value="{{$editSale->amount_without_anything}}" id="total_amount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Discount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_discount_text">{{$editSale->total_discount}}</span>
                        <input type="hidden" name="total_discount_input" value="{{$editSale->total_discount}}" id="total_discount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Taxble Amt.</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_taxble_text">{{$editSale->total_taxble_value}}</span>
                        <input type="hidden" name="total_taxble_input" value="{{$editSale->total_taxble_value}}" id="total_taxble_input"></td>
                </tr>



                <tr>
                    <td width="50%">Total GST</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_gst_text">{{$editSale->total_tax_amount}}</span>
                        <input type="hidden" name="total_gst_input" value="{{$editSale->total_tax_amount}}" id="total_gst_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Gross Amt.</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_sub_text">{{$editSale->gross_total}}</span>
                        <input type="hidden" name="total_sub_input" value="{{$editSale->gross_total}}" id="total_sub_input">
                        <input type="hidden"   name="postage_charge" id="postage_charge">
                    </td>
                </tr>

                <tr>
                    <td width="50%">Grand Total</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="grand_total_text">{{$editSale->grand_total}}</span>
                        <input type="hidden" name="grand_total_input" value="{{$editSale->grand_total}}" id="grand_total_input"></td>
                </tr>
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
            </table>


        </div>


    </div>

    {{-- ----------------------------------------inner content here --------------------------------------------------------}}

    <br>
@endsection
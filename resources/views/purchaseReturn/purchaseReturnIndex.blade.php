@extends('layouts.ExtraadminPanel')
@section('title')
    Purchase Return:Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }


        .well{
            background-color: #ffffff;
        }

    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('purchaseReturn')}}">Purchase Return Panel</a>
    </li>
@endsection
@section('content')

    <h3 class="heading">Purchase Return</h3>
    <a class="btn btn-default" style="margin-bottom: 10px;"data-toggle="modal" data-target="#myModal"><i class="splashy-document_letter_add"></i> Purchase Return</a>
    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
        <?php echo Form::open(array('route' => 'searchPurchaseToReturn', 'method'=>'get')); ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Select Bill To Purchase Return</h4>
                </div>
                <div class="modal-body">
                    <select id="saleno" name="purchase_id" style="width:100%;" required>
                        <?php   $product_type = \App\Purchase::where('status', '=', null)->get(); ?>
                        <?php // $product_type=\App\Sale::All();?>
                        <option value="">Please select Invoice</option>
                        @foreach($product_type as $product_type)
                            <option value="{{$product_type->id}}">{{$product_type->invoice_no}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="searchButton" class="btn btn-primary">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <table class="table table-striped table-bordered dTableR" id="dt_a">
        <thead>
        <tr style="font-size: 13px!important;">

            <th class="text-center">P.R No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">P.R. Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center"> Against Purchase Bill&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">Supplier Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">P.R AMT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">Actions&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($saleIndex as $item)

            <tr class="">
                <td>{{$item->invoice_no}}</td>

                <td><?php echo date( 'd/m/y', strtotime($item->billing_date)) ?></td>
                <td>{{$item->against_purchase_no}}</td>
                <td>{{$item->belongsToCustomer->ledger_name}}</td>

                <td>{{$item->grand_total}}</td>



                <td>
                    <a href="singleViewPurchaseReturn/{{$item->id}}" class="btn btn-default btn-sm" title="Preview"> <i class="splashy-document_letter_edit"></i>
                    </a>

                    <a href="printPurchaseReturn/{{$item->id}}" class="btn btn-default btn-sm" title="Print">
                        <i class="splashy-printer"></i>
                    </a>
                    <a href="deletePurchaseReturn/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()" title="Remove">
                        <i class="splashy-document_letter_remove"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <script>

        var product_type =  [/* states array*/];
        $("#saleno").select2({
            data: product_type
        });

        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
        $(document).ready( function () {
            $('#table').dataTable( {
                "aoColumns": [
                    null,
                    { "sType": "date-uk" },
                    null,
                    null,
                    null,
                    null,

                ]
            });

        } );

        var saleno =  [/* states array*/];
        $("#saleno").select2({
            data: saleno
        });

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    <br>
    {{--inner content here ------------------------------------}}
@endsection
@extends('layouts.adminPanelTable')
@section('title')
    Purchase Return
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('purchaseReturn')}}">Purchase Return Panel</a>
    </li>

    <li>
        <a href="{{url('#')}}">Purchase Return:searched</a>
    </li>
    @endsection
@section('content')
    {{--inner content here ------------------------------------}}

    <h3 class="heading">Purchase Return (Searched Result)</h3>
    <hr>

    <table class="table table-striped table-bordered dTableR">
        <tr style="background-color: #e9f3f8">
            <th>Bill No.</th>
            <th>Bill Date</th>
            <th>Supplier</th>
            <th>Bill Amount.</th>
        </tr>
        <tr>
            <td>{{$temp1Sale->invoice_no}}</td>
            <td><?php echo date( 'd/m/y', strtotime($temp1Sale->purchase_date)) ?></td>
            <td>{{$temp1Sale->belongsToCustomer->ledger_name}}</td>
            <td>{{$temp1Sale->grand_total}}</td>
        </tr>
    </table>
    <br>
    <div class="row">

        <a href="purchaseDetailsToReturn/{{$temp1Sale->id}}" class="btn btn-success">Purchase Return</a>
        <a href="{{url('purchaseReturn')}}" class="btn btn-danger">Cancel</a>
        <a href="{{url('home')}}" class="btn btn-info">Back To Dashboard</a>
    </div>

    {{--inner content here ------------------------------------}}
    <br>
@endsection
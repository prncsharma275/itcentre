@extends('layouts.adminPanelTable')
@section('title')
    Sales Return:Search
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('saleReturn')}}">Sale Return Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Sale Return:Searched </a>
    </li>
@endsection
@section('content')

    <h3 class="heading">Sale Return (Searched Result)</h3>
    <hr>

    <table class="table table-striped table-bordered dTableR">
        <tr style="background-color: #e9f3f8">

        <th>Bill No.</th>
            <th>Bill Date</th>
            <th>Customer</th>
            <th>Bill Amount.</th>
        </tr>
        <tr>
            <td>{{$temp1Sale->unique_id}}</td>
            <td><?php echo date( 'd/m/y', strtotime($temp1Sale->sale_date)) ?></td>
            <td>{{$temp1Sale->belongsToCustomer->ledger_name}}</td>
            <td>{{$temp1Sale->grand_total}}</td>
        </tr>
    </table>
    <br>
    <div class="row">

        <a href="saleDetailsToReturn/{{$temp1Sale->id}}" class="btn btn-success btn-sm"><i class="fa fa-backward"></i> Sale Return</a>
        <a href="{{url('saleReturn')}}" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Cancel</a>
        <a href="{{url('/')}}" class="btn btn-info btn-sm"><i class="fa fa-home"></i> Back To Dashboard</a>
    </div>

    {{--inner content here ------------------------------------}}
    <br>
@endsection
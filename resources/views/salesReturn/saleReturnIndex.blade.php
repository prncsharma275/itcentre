@extends('layouts.ExtraadminPanel')
@section('title')
    Sales Return Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }


        .well{
            background-color: #ffffff;
        }

    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('saleReturn')}}">Sale Return Panel</a>
    </li>
@endsection
@section('content')

    <h3 class="heading">Sales Return</h3>
    <a class="btn btn-default" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModal"><i class="splashy-document_letter_add"></i> Sale Return</a>

    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
        <?php echo Form::open(array('route' => 'searchSaleToReturn', 'method'=>'get')); ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Select Bill To Sale Return</h4>
                </div>
                <div class="modal-body">
                    <select id="saleno" name="sale_id" class="form-control" style="width:100%;color: #000" required>
                        <?php   $product_type = \App\Sale::where('status', '=', null)->get(); ?>
                        <option value="">Please select Invoice</option>
                        @foreach($product_type as $product_type)
                            <option value="{{$product_type->id}}">{{$product_type->unique_id}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="searchButton" class="btn btn-primary">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <br>
    <table class="table table-striped table-bordered dTableR" id="dt_a">
        <thead>
        <tr style="font-size: 13px!important;">

            <th class="text-center">S.R No</th>
            <th class="text-center">S.R. Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">Against Sale Bill &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">Customer Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">S.R AMT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($saleIndex as $item)

            <tr class="">
                <td>{{$item->sale_return_no}}</td>

                <td><?php echo date( 'd/m/y', strtotime($item->sale_return_date)) ?></td>
                <td>{{$item->against_sale_no}}</td>
                <td>{{$item->belongsToCustomer->ledger_name}}</td>

                <td>{{$item->grand_total}}</td>



                <td>
                    <a href="singleViewSaleReturn/{{$item->id}}" class="btn btn-default btn-sm" title="Preview">
                        <i class="splashy-document_letter_edit"></i>
                    </a>

                    <a href="printSaleReturn/{{$item->id}}" class="btn btn-default btn-sm" title="Print">
                        <i class="splashy-printer"></i>
                    </a>
                    <a href="deleteSaleReturn/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()" title="Remove">
                        <i class="splashy-document_letter_remove"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <script>

        var product_type =  [/* states array*/];
        $("#saleno").select2({
            data: product_type
        });

        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
        $(document).ready( function () {
            $('#table').dataTable( {
                "aoColumns": [
                    null,
                    { "sType": "date-uk" },
                    null,
                    null,
                    null,
                    null,

                ]
            });

        } );
        var saleno =  [/* states array*/];
        $("#saleno").select2({
            data: saleno
        });

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    {{--inner content here ------------------------------------}}
    <br>
@endsection
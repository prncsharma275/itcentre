<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>SALE RETURN</title>
    <link rel="stylesheet" href="style.css" media="all" />
    {{--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="{{asset('css/bootstrap/boot4.1.css')}}" rel="stylesheet">
    <style type="text/css" media="print">
        .rupees {}


        @page {
            size: auto;
            /* auto is the initial value */
            margin-left: 15px!important;
            /* this affects the margin in the printer settings */
            margin-right: 5px!important;
            /* this affects the margin in the printer settings */
            margin-top: 10px;
            /* this affects the margin in the printer settings */
            margin-bottom: 5px;
            background-color: gainsboro;
            font-size: 15px;
            /*margin: px;*/

        }



        /*@media print{@page {size: landscape}}*/

        @media print {
            table.report-container {
                page-break-after:always;
            }
            thead.report-header {
                display:table-header-group;
            }
            tfoot.report-footer {
                display:table-footer-group;
            }

            tfoot.report-footer:after {
                counter-increment: page 1;
                /*counter-reset: pages 1;*/
                content: "Page " counter(page) " of " counter(pages);
            }
            .hidden-print {
                display: none !important;
            }
        }

    </style>
</head>

<body style="">

<?php $comp = \App\Company_detail::find(1)?>
<table class="report-container">
    <thead class="report-header">
    <tr>
        <th class="report-header-cell">
            <div class="header-info">
                <p class="text-center" id="pageno">SALE RETURN</p>
                <table width="100%" border="1">
                    <tr>
                        <td style="width: 600px"><h4 class="text-left">&nbsp;{{$comp->company_name}}</h4>
                            <h6 class="text-left">&nbsp;{{$comp->street_1}}
                                <br>&nbsp;{{$comp->street_2}} {{$comp->city}}, Pin &#45; {{$comp->zip}}, {{$comp->state}}</br>
                                &nbsp;Email- {{$comp->email}}, Cell-{{$comp->mobile}}
                                <br>&nbsp;{{$comp->gst_no}}</h6></td>
                        <td style="width: 500px">
                            <p class="text-left" style="font-size: 14px;margin-bottom: -1px">&nbsp;Return From</p>
                           {{--//retail customer condtion--}}
                            @if($printBill->customer_id==1)
                                <h4 class="text-left" style="margin-bottom: -1px">&nbsp;{{$printBill->retail_name}}({{$printBill->belongsToCustomer->ledger_name}})</h4>
                                <h6 class="text-left">&nbsp;{{$printBill->city}}
                                    <br>&nbsp;{{$printBill->mobile}}
                                </h6>
                             @else
                                <h4 class="text-left" style="margin-bottom: -1px">&nbsp;{{$printBill->belongsToCustomer->ledger_name}}</h4>
                                <h6 class="text-left">&nbsp;{{$printBill->belongsToCustomer->address}}
                                    <br>&nbsp;{{$printBill->belongsToCustomer->city}}&nbsp; {{$printBill->belongsToCustomer->state}}
                                    <br>&nbsp;{{$printBill->belongsToCustomer->email}} &nbsp; {{$printBill->belongsToCustomer->mobile}}
                                    @if($printBill->belongsToCustomer->vatNo !="")
                                        <br>&nbsp;GSTIN-{{$printBill->belongsToCustomer->vatNo}}
                                    @endif
                                </h6>
                             @endif

                        </td>
                        <td style="width: 400px">
                            <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Invoice No. - {{$printBill->sale_return_no}}</h5>
                            <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Invoice Date - <?php echo date( 'd/m/Y', strtotime($printBill->sale_return_date)) ?></h5>

                        </td>
                    </tr>
                </table>
                <table width="100%" border="1" style="text-align: center">
                    <tr>
                        <td>Description</td>
                        <td style="width: 65px!important;">HSN</td>
                        <td style="width: 70px!important;">Qty.</td>
                        <td style="width: 100px!important;">Rate</td>
                        <td style="width: 100px!important;">Amount</td>
                        <td style="width: 60px!important;">Disc.%</td>
                        <td style="width: 60px!important;">GST %</td>
                        <td style="width: 110px!important;">GST Amt.</td>
                        <td style="width: 110px!important;">Total Amt.</td>
                    </tr>


                </table>
            </div>
        </th>
    </tr>
    </thead>
    <tfoot class="report-footer">
    <tr>
        <td class="report-footer-cell">
            <div class="footer-info">
                <table width="100%" border="1" style="text-align: center">
                    <tr>
                        <?php $sale_invoice = DB::table('sale_returns_invoice')->where([
                                ['sale_return_id', '=',$printBill->id ],

                        ])->get();
                        $qnty=0;
                        ?>
                        @foreach($sale_invoice as $qnt)
                                <?php $qnty =$qnty+$qnt->quantity; ?>
                         @endforeach
                        <td style="width: 135px!important">Total Qnty.</td>
                        <td style="width: 135px!important">Total Amt.</td>
                        <td style="width: 135px!important">Total Disc.</td>
                        <td  style="width: 135px!important">Total Txb.Amt.</td>
                        <td style="width: 135px!important">Total Tax</td>
                        <td style="width: 135px!important">Postage</td>
                        <td>Grand Total</td>
                    </tr>
                </table>

                <table width="100%" border="1" style="text-align: center">
                    <tr>
                        <td style="width: 135px!important">{{$qnty}}</td>
                        <td style="width: 135px!important">{{$printBill->total_amount_without_anything}}</td>
                        @if($printBill->total_discount=="")
                            <td style="width:135px!important">00</td>
                        @else
                            <td style="width:135px!important">{{$printBill->total_discount}}</td>
                        @endif
                        <td style="width: 135px!important">{{$printBill->total_taxble_value}}</td>
                        <td style="width: 135px!important">{{$printBill->total_tax}}</td>
                        <td style="width: 135px!important   ">{{$printBill->postage_charge}}</td>
                        <td>{{$printBill->grand_total}}</td>
                    </tr>
                </table>
                <table width="100%" border="1" style="text-align: center">
                    <tr>
                        <?php
                        $number = $printBill->grand_total;
                        $no = round($number);
                        $point = round($number - $no, 2) * 100;
                        $hundred = null;
                        $digits_1 = strlen($no);
                        $i = 0;
                        $str = array();
                        $words = array('0' => '', '1' => 'One', '2' => 'Two',
                                '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
                                '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
                                '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
                                '13' => 'Thirteen', '14' => 'Fourteen',
                                '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
                                '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
                                '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
                                '60' => 'Sixty', '70' => 'Seventy',
                                '80' => 'Eighty', '90' => 'Ninety');
                        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
                        while ($i < $digits_1) {
                            $divider = ($i == 2) ? 10 : 100;
                            $number = floor($no % $divider);
                            $no = floor($no / $divider);
                            $i += ($divider == 10) ? 1 : 2;
                            if ($number) {
                                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                                $str [] = ($number < 21) ? $words[$number] .
                                        " " . $digits[$counter] . $plural . " " . $hundred
                                        :
                                        $words[floor($number / 10) * 10]
                                        . " " . $words[$number % 10] . " "
                                        . $digits[$counter] . $plural . " " . $hundred;
                            } else $str[] = null;
                        }
                        $str = array_reverse($str);
                        $result = implode('', $str);
                        $points = ($point) ?
                                "." . $words[$point / 10] . " " .
                                $words[$point = $point % 10] : '';?>

                        <td style="text-align: left;padding: 7px!important;"> <?php echo $result . "Rupees  " . $points . "Only"; ?></td>
                    </tr>
                </table>
                <table width="100%" border="1" style="text-align: center">
                    <tr>
                        <td style="text-align: left;padding:7px!important;"><br>
                            <br><br><br>Customer's Seal and Signature
                        </td>
                        <td style="text-align: right;padding: 7px!important;">
                            <br>
                            <br><br><br>For,IT Centre Seal and Signature
                        </td>


                    </tr>

                </table>
                <p class="hidden-print" style="padding-top:5px!important;">&nbsp;<button class="btn btn-success hidden-print" onclick="myFunction()">Print</button>
                    <a href="{{url('saleReturn')}}" class="btn btn-info hidden-print">Create New Invoice</a>
                    <a href="{{url('saleReturn')}}" class="btn btn-primary hidden-print">Back</a></p>
                <p class="text-center" style="font-size: 18px;margin-bottom: 0;text-transform: uppercase!important;">SUBJECT TO {{$comp->city}} JURISDICATION</p>
                <p class="text-center" style="font-size: 14px">This is Computer Generated Invoice</p>

        </div>
        </td>
    </tr>
    </tfoot>
    <tbody class="report-content">
    <tr>
        <td class="report-content-cell">

            <div class="main">

                <?php $i=01 ; ?>
                <?php foreach($sale_invoice as $sale){ ?>
                <?php  $product=\App\Product::find($sale->product_id); ?>
                <table width="100%" style="text-align: center;">
                    <tr>
                        <td  style="border-right: solid 1px;border-left: solid 1px;padding: 10px;text-align: left"><span style="border: solid 1px;border-radius: 5px;font-size: 12px">{{sprintf("%02d", $i)}}</span>&nbsp;<span style="text-transform: capitalize!important;font-weight: 600">{{$product->product_name}}</span>
                            <br><span>{{$product->product_desc}}</span>
                        <br><span style="font-style: italic;">{{$sale->description}}</span>
                        </td>
                        <td style="width: 65px!important;border-right: solid 1px">{{$product->product_code}}</td>
                        <td style="width: 70px!important;border-right: solid 1px">{{$sale->quantity}}.<span style="font-weight: 500;font-size: 12px">{{$product->unit_type}}</span></td>

                        <td style="width: 100px!important;border-right: solid 1px">{{$sale->rate}}</td>
                        <td style="width: 100px!important;border-right: solid 1px">{{$sale->amount_before_tax}}</td>
                        <td style="width: 60px!important;border-right: solid 1px">{{$sale->discount}}</td>
                        @if($printBill->tax_type=="CGST&SGST")
                            <td style="width: 60px!important;border-right: solid 1px">{{$sale->cgst_tax+$sale->cgst_tax}}</td>
                        @else
                            <td style="width: 60px!important;border-right: solid 1px">{{$sale->igst_tax}}</td>
                        @endif
                        {{--<td style="width: 60px!important;border-right: solid 1px">{{$sale->cgst_tax}}</td>--}}
                        <td style="width: 110px!important;border-right: solid 1px">{{$sale->tax_amount}}</td>
                        <td style="width: 110px!important;border-right: solid 1px">{{$sale->total_amount}}</td>
                    </tr>
                </table>
                    <?php $i++ ; ?>
                <?php } ?>
            </div>

        </td>
    </tr>
    </tbody>
</table>

<script>
    function myFunction() {
        window.print();
    }
</script>
</body>

</html>

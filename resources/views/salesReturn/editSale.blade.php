<!DOCTYPE html>
<html>

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    <title>Aurthosurgical Invoice</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('salesAndPurchaseStyle/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('salesAndPurchaseStyle/css/print.css')}}" media="print" />
    <script type='text/javascript' src='{{ asset('salesAndPurchaseStyle/js/min.js')}}'></script>
    <script src="{{ asset('js/saleJquery.min.js')}}"></script>
    <link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js')}}"></script>

    <script>

    </script>
</head>

<body id="body" style="margin-left: 20px;margin-right: 10px;">



<textarea id="header" style="resize: none; height: 40px; margin-top:5px " readonly>SALE INVOICE</textarea>

<div id="identity">





</div>

<div style="clear:both"></div>
<?php echo Form::open(array('url' =>['updateSale',$editSale->id])); ?>
<label>Customer Name</label>
<div id="details">

    {{--<textarea id="customer-title">Widget Corp.c/o Steve Widget</textarea>--}}

    <div>
        <select id="customer" name="customer" style="width:300px;" required>

            <?php  $product_type=\App\Customer::All();?>
            <option value="{{$editSale->customer_id}}" selected="selected">{{$editSale->belongsToCustomer->customer_name}}</option>
            @foreach($product_type as $product_type)
                <option value="{{$product_type->id}}">{{$product_type->customer_name}}</option>
            @endforeach
        </select>
    </div>

    <div class="row" style="margin-top: 10px">
        <div class="col-md-3">
            <label>Address</label><br>
            <textarea name="address" style="width: 200px!important; resize: none;  padding: 5px; background-color: #eeeeee; border: none;color: blueviolet;font-weight: 300"  readonly>{{$editSale->belongsToCustomer->address}}</textarea>
        </div>
        <div class="col-sm-3"style="margin-top: 15px">
            <label>City</label><br>
            <input name="city" type="text"  value="{{$editSale->belongsToCustomer->city}}" style="background-color: #eeeeee; border: none; padding: 5px; color: blueviolet;font-weight: 600" readonly >
        </div>
        <div class="col-sm-3"style="margin-top: 15px">
            <label>Mobile</label><br>
            <input name="phone" type="text" value="{{$editSale->belongsToCustomer->phone}}" style="background-color: #eeeeee; border: none; padding: 5px; color: blueviolet;font-weight: 600" readonly>
        </div>
        <div class="col-sm-3"style="margin-top: 15px">
            <label>Email</label><br>
            <input name="email" type="text"  value="{{$editSale->belongsToCustomer->email}}" style="background-color: #eeeeee; border: none; padding: 5px; color: blueviolet;font-weight: 600" readonly>
        </div>


    </div>

    {{--invoice date calculation--}}






    <table id="meta" style="margin-bottom: 10px">
        <tr>
            <td class="meta-head" style="background-color: #EEEEEE">Invoice #</td>
            <td style="background-color: #1f648b;color: #ffffff">{{$editSale->unique_id}}</td>

        </tr>
        <tr>

            <td class="meta-head" style="background-color: #EEEEEE">Date</td>
            <td style="background-color: #1f648b;color: #ffffff"><div class="date"><?php echo date( 'd/m/y', strtotime($editSale->billing_date)) ?></div></td>

        </tr>
        <tr>

            <td class="meta-head" style="background-color: #EEEEEE">Challan NO</td>
            <td style="background-color: #1f648b;"><input name="challan_no" value="{{$editSale->challan_no}}"> </td>

        </tr>
        <tr>

            <td class="meta-head" style="background-color: #EEEEEE">Challan Date (dd/mm/yyyy)</td>
            <td style="background-color: #1f648b;"><input name="challan_date" value="{{$editSale->challan_date}}"> </td>

        </tr>
        <tr>

            <td class="meta-head" style="background-color: #EEEEEE">Order NO</td>
            <td style="background-color: #1f648b;"><input name="challan_no" value="{{$editSale->order_no}}"> </td>

        </tr>
        <tr>

            <td class="meta-head" style="background-color: #EEEEEE">Order Date (dd/mm/yyyy)</td>
            <td style="background-color: #1f648b;"><input name="challan_date" value="{{$editSale->order_date}}"> </td>

        </tr>

    </table>


</div>

<table id="items">

    <tr>
        <th style="background-color: #ccc">Item</th>
        <th style="background-color: #ccc">Batch No</th>
        <th style="background-color: #ccc">Mfg.Date <span style="color: red">(yyyy-mm-dd)</span></th>
        <th style="background-color: #ccc">Exp.Date <span style="color: red">(yyyy-mm-dd)</span></th>
        <th style="background-color: #ccc">Qnty.</th>
        <th style="background-color: #ccc">Rate</th>
        <th style="background-color: #ccc">Amt.</th>
    </tr>
    <?php $sale_invoice = DB::table('sale_invoice')->where([
            ['sale_id', '=',$editSale->id ],

    ])->get();


    ?>
        {{--count total number of row where sale id = this--}}
    <?php $total_sale_count = \App\Sale_invoice::where(['sale_id' => $editSale->id])->count(); ?>

    <?php   $count=0; ?>
@foreach($sale_invoice as $sale)

    <tr class="item-row">
        <td style="width: 31%" class="item-name"><div class="delete-wpr">
                <?php  ?>
                <div>
                    <select id="monti{{$sale->id}}" name="rows[{{$count}}][product]" style="width:99%;">
                        <?php $product_name = DB::table('products')->select('id', 'product_name')->where([
                                ['id', '=',$sale->product_id ],  ])->first();  ?>
                            <option value="{{$product_name->id}}" selected="selected">{{$product_name->product_name}}</option>

                        <?php  $product_type=\App\Product::All();?>
                           @foreach($product_type as $product_type)
                            <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>
                        @endforeach
                    </select>
                </div>
                <a class="deletee" onclick="deleteArticle({{$sale->id}})" title="Remove row">X</a></div></td>


            <input type="hidden" name="rows[{{$count}}][sale_invoice]" value="{{ $sale->id }}">
            <input type="hidden" name="rows[{{$count}}][product_old]" value="{{$sale->product_id}}">


        <td><select name="rows[{{$count}}][batch_no]" class="form-control"style="width: 200px;height: 38px" required>

                {{--getting selected batch no with id --}}
                <?php $Batch = DB::table('expiry_product_table')->select('id', 'batch_no')->where([
                ['batch_no', '=',$sale->batch_no ],  ])->first();  ?>
                <option value="{{$Batch->id}}|{{$Batch->batch_no}}">{{$Batch->batch_no}}</option>

                {{------------------------------}}

                 {{--getting all expiry date list    --}}
                <?php $allBatch = DB::table('expiry_product_table')->select('id', 'batch_no')->where([
                ['product_id', '=',$sale->product_id ],  ])->get();  ?>

                @foreach($allBatch as $batch)
                    <option value="{{$batch->id}}|{{$batch->batch_no}}">{{$batch->batch_no}}</option>
                @endforeach
            </select>
        </td>
            <input type="hidden" name="rows[{{$count}}][batch_no_old]" class="description" value="{{$sale->batch_no}}" required>
            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        </td>
        <td class="serialNumber"><input name="rows[{{$count}}][mfgDate]" type="date" class="mfgDate"  value="{{$sale->mfgDate}}" style="height: 38px">
            <input name="rows[{{$count}}][mfgDate_old]" type="hidden" class="mfgDate"   value="{{$sale->mfgDate}}">
        </td>

        <td class="serialNumber"><input name="rows[{{$count}}][expDate]" type="date" class="expDate"  value="{{$sale->expDate}}" style="height: 38px">
            <input name="rows[{{$count}}][expDate_old]" type="hidden" class="mfgDate" value="{{$sale->expDate}}">
        </td>
        <td><input name="rows[{{$count}}][qty]" class="qty" value="{{$sale->quantity}}" required>
            <input type="hidden" name="rows[{{$count}}][old_qnty]" class="qty" value="{{$sale->quantity}}">
        </td>
        <td><input name="rows[{{$count}}][cost]" class="cost" value="{{$sale->rate}}" required></td>
        <td><input name="rows[{{$count}}][price]" class="price" value="{{$sale->amount}}" readonly></td>
    </tr>
    <script>
        var name='<?php echo $sale->id; ?>';
        var country =  [/* states array*/];
        $("#monti"+name).select2({
            data: country
        });

        $('select[name="rows[{{$count}}][product]"]').on('change', function() {

            var stateID = $(this).val();

            if(stateID) {

                $.ajax({

                    url: '{{ url('/') }}/AjaxBatch/'+stateID,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {

                        $('select[name="rows[{{$count}}][batch_no]"]').empty();
                        $('select[name="rows[{{$count}}][batch_no]"]').append('' +
                                '<option value="">Please Select Value</option>');

                        $.each(data, function(key, value) {

                            $('select[name="rows[{{$count}}][batch_no]"]').append('' +
                                    '<option value="'+ key +'|'+value+'">'+ value +'</option>');

                        });


                    }

                });

            }else{

                $('select[name="rows[{{$count}}][batch_no]"]').empty();

            }

        });

        //       get mfgdate and exp date of given batch number
        $('select[name="rows[{{$count}}][batch_no]"]').on('change', function() {

            var stateID = $(this).val();

            if(stateID) {

                $.ajax({

                    url: '{{ url('/') }}/AjaxMfgdate/'+stateID,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {


                        var Vals    =  data;
                        $("input[name='rows[{{$count}}][mfgDate]']").val(Vals.Mfg);
                        $("input[name='rows[{{$count}}][expDate]']").val(Vals.Exp);


                    }

                });

            }else{

                $("input[name='rows[{{$count}}][mfgDate]']").empty();
                $("input[name='rows[{{$count}}][expDate]']").empty();

            }

        });


    </script>
         <?php   $count=$count+1; ?>

@endforeach


    <tr id="hiderow">
        <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row">Add New Product</a></td>
    </tr>


</table> <br>
<table class="total_table">
    <tr>
        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line">Total Amount(without Tax)</td>
        <td class="total-value"><div id="subtotal">{{$editSale->subtotal_without_vat}}</div>
            <input id="subtotal1" type="hidden"  name="subtotal_without_vat">
        </td>
    </tr>
    <tr>

        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line">Total VAT@ <input style="width: 70px;float: right" value="{{$editSale->vat_percentage}}" name="vat_percentage" id="vat_percentage" placeholder="% here"> </td>
        <td class="total-value">
            <div id="total" style="display: none"></div>
            <div id="total_vat">{{$editSale->total_vat}}</div>
            <input id="total_vat1"  type="hidden" name="total_vat">
        </td>
    </tr>
    <tr>

        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line">Total CST@ <input style="width: 70px;float: right" name="cst_percentage" value="{{$editSale->cst_percentage}}" id="cst_percentage"placeholder="% here"> </td>
        <td class="total-value">
            <div id="total" style="display: none"></div>
            <div id="total_cst">{{$editSale->total_cst}}</div>
            <input id="total_cst1"  type="hidden" name="total_cst">
        </td>
    </tr>

    <tr>
        <td colspan="2" class="blank"></td>
        <td colspan="2" class="total-line" style="background-color: #1f648b;color: white">Sub Total</td>
        <td class="total-value" style="background-color: #1f648b;color: white">

            <div id="subtotalGrand">{{$editSale->subtotalGrand}}</div>
            <input id="subtotalGrand1"  type="hidden" name="subtotalGrand">
        </td>
    </tr>

    <tr>
        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line">Discount @<input style="width: 70px;float: right" name="discount" value="{{$editSale->discount_percentage}}" id="discount"placeholder="% here"></td>


        <td class="total-value">
            <textarea style="display: none" id="paid">$0.00</textarea>
            <div id="total_spcl_discount"></div>
            <input id="total_spcl_discount1" value="{{$editSale->discount}}" name="total_spcl_discount">
        </td>
    </tr>
    <tr>
        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line balance">Frieght & Courier Charge </td>
        <td class="total-value balance">
            <input class="courierCharge" name="fright" value="{{$editSale->fright}}">
        </td>
    </tr>
    <tr>
        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line balance">Total</td>
        <td class="total-value balance">
            <div class="total1">{{$editSale->gross_total}}</div>
            <input id="total2" type="hidden" name="total_after_vat">
        </td>
    </tr>
    <tr>
        <td colspan="2" class="blank"> </td>
        <td colspan="2" class="total-line balance">Grand Total</td>
        <td class="total-value balance">
            <div class="grand_total" style="background-color: #269abc;color:white">{{$editSale->grand_total}}</div>
            <input id="grand_total1" type="hidden" name="grand_total">
        </td>
    </tr>
</table>
<br><br><br><br>
<input type="submit"  id ='one' onclick="change()"  class="btn btn-success" name="submit" value="Save">


<input type="reset" id="reset" class="btn btn-danger" value="Clear">
<a href="{{url('sale')}}" class="btn btn-info">Back</a>

{{form::close()}}

<script>
    function deleteArticle(id) {
        if (confirm('This Data is saved ! Are you sure you want to delete this?')) {
//        if (confirm("Press a button!") == true) {
            $.ajax({
                url: '{{ url('/') }}/sale_invoiceAjax/' + id,
                data: {"_token": "{{ csrf_token() }}"},
                type: 'DELETE',
                success: function (result) {
                    console.log(result);
                    location.reload();
                }
            });


        }
    }
    function print_today() {
        // ***********************************************

        var now = new Date();
        var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();
        function fourdigits(number) {
            return (number < 1000) ? number + 1900 : number;
        }
        var today =  months[now.getMonth()] + " " + date + ", " + (fourdigits(now.getYear()));
        return today;
    }

    // from http://www.mediacollege.com/internet/javascript/number/round.html
    function roundNumber(number,decimals) {
        var newString;// The new rounded number
        decimals = Number(decimals);
        if (decimals < 1) {
            newString = (Math.round(number)).toString();
        } else {
            var numString = number.toString();
            if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
                numString += ".";// give it one at the end
            }
            var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
            var d1 = Number(numString.substring(cutoff,cutoff+1));// The value of the last decimal place that we'll end up with
            var d2 = Number(numString.substring(cutoff+1,cutoff+2));// The next decimal, after the last one we want
            if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
                if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
                    while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
                        if (d1 != ".") {
                            cutoff -= 1;
                            d1 = Number(numString.substring(cutoff,cutoff+1));
                        } else {
                            cutoff -= 1;
                        }
                    }
                }
                d1 += 1;
            }
            if (d1 == 10) {
                numString = numString.substring(0, numString.lastIndexOf("."));
                var roundedNum = Number(numString) + 1;
                newString = roundedNum.toString() + '.';
            } else {
                newString = numString.substring(0,cutoff) + d1.toString();
            }
        }
        if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
            newString += ".";
        }
        var decs = (newString.substring(newString.lastIndexOf(".")+1)).length;
        for(var i=0;i<decimals-decs;i++) newString += "0";
        //var newNumber = Number(newString);// make it a number if you like
        return newString; // Output the result to the form field (change for your purposes)
    }

    function update_balance() {

    }


    // amount calculation it working
    function update_price() {
        var row = $(this).parents('.item-row');
        var price = row.find('.cost').val().replace("$","") * row.find('.qty').val();
        price = roundNumber(price,2);
        //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
        isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').val(""+price);

        update_total();
    }


    //original function it's working
    function update_total() {
        var total = 0;
        $('.price').each(function(i){
            price = $(this).val().replace("$","");
            if (!isNaN(price)) total += Number(price);
        });

        total = roundNumber(total,2);

        $('#subtotal1').val(""+total);
        $('#subtotal').html(""+total);
        $('#total').html(""+total);

        update_balance();
    }





    function totalvat() {
        var totalvat = $('#subtotal1').val()*$('#vat_percentage').val()/100;
//        var totalvat = $('#subtotal1').val()+200;

//            if (!isNaN(vat_amount)) totalvat += Number(vat_amount);


        var totalvat1 = roundNumber(totalvat,2);
        $('#total_vat1').val(""+totalvat1);
        $('#total_vat').html(""+totalvat1)


    }


    function totalcst() {
        var totalvat = $('#subtotal1').val()*$('#cst_percentage').val()/100;
//        var totalvat = $('#subtotal1').val()+200;

//            if (!isNaN(vat_amount)) totalvat += Number(vat_amount);


        var totalvat1 = roundNumber(totalvat,2);
        $('#total_cst1').val(""+totalvat1);
        $('#total_cst').html(""+totalvat1)


    }

    function subtotalgrand(){

        var subtotal=$('#subtotal1').val();
        var vat=$('#total_vat1').val();
        var cst=$('#total_cst1').val();

        var total =Number(subtotal)+Number(vat)+Number(cst);
        var totalvat1 = roundNumber(total,2);
        $('#subtotalGrand1').val(""+totalvat1);
        $('#subtotalGrand').html(""+totalvat1)
    }

    //total special disount
    function totalspclDiscount(){
        var totaldiscount = $('#subtotalGrand1').val()*$('#discount').val()/100;

        totaldiscount = roundNumber(totaldiscount,2);
        $('#total_spcl_discount1').val(""+totaldiscount);


    }


    function grandtotal(){
        var subtotal=$('#subtotalGrand1').val();
        var friegh=$('.courierCharge').val();
        var discount=$('#total_spcl_discount1').val();
        var grandtotaladd=(subtotal-Number(discount))+Number(friegh);
        grandtotaladd = roundNumber(grandtotaladd,2);
        var new_num = Math.round(grandtotaladd).toFixed(2);

        $('.total1').html("$"+grandtotaladd);
        $('#total2').val(""+grandtotaladd);
        $('.grand_total').html("$"+new_num);
        $('#grand_total1').val(""+new_num);
    }








    function bind() {
        $(".qty").blur(update_price);
        $(".cost").blur(update_price);

//        vat calculation

        $(".qty").blur(totalvat);
        $(".cost").blur(totalvat);
        $("#subtotal1").blur(totalvat);
        $("#vat_percentage").blur(totalvat);


//        cst calculation

        $(".qty").blur(totalcst);
        $(".cost").blur(totalcst);
        $("#subtotal1").blur(totalcst);
        $("#cst_percentage").blur(totalcst);

//        subtotal grand

        $(".qty").blur(subtotalgrand);
        $(".cost").blur(subtotalgrand);
        $("#subtotal1").blur(subtotalgrand);
        $("#vat_percentage").blur(subtotalgrand);
        $("#cst_percentage").blur(subtotalgrand);

//        discount

        $(".qty").blur(totalspclDiscount);
        $(".cost").blur(totalspclDiscount);
        $("#subtotal1").blur(totalspclDiscount);
        $("#vat_percentage").blur(totalspclDiscount);
        $("#cst_percentage").blur(totalspclDiscount);
        $("#discount").blur(totalspclDiscount);




//         grand total
        $(".qty").blur(grandtotal);
        $(".cost").blur(grandtotal);
        $("#subtotal1").blur(grandtotal);
        $("#vat_percentage").blur(grandtotal);
        $("#cst_percentage").blur(grandtotal);
        $("#discount").blur(grandtotal);
        $("#total_spcl_discount1").blur(grandtotal);
        $(".courierCharge").blur(grandtotal);
        $(".delete").blur(grandtotal);


    }
    $('#one').click(function(){
        update_total();
        totalvat();
        totalcst();
        subtotalgrand();
        totalspclDiscount();
        grandtotal();


    });

    $(document).ready(function() {
        var i=1;
        var count= <?php echo $total_sale_count; ?>+1;

        var customer =  [/* states array*/];
        $("#customer").select2({
            data: customer
        });
        $('input').click(function(){
            $(this).select();
        });

        $("#paid").blur(update_balance);

        $("#addrow").click(function(){
            i++;
            count++;
            $(".item-row:last").after('<tr class="item-row"><td style="width:31%" class="item-name"><div class="delete-wpr">' +
                    '<div>'+
                    '<select id=\"country' + (++i) + '\" name="rows[' + count + '][product]" style="width:99%;">'+
                    <?php  $product_type=\App\Product::All();?>
                    @foreach($product_type as $product_type)
                    '<option value="{{$product_type->id}}">{{$product_type->product_name}}</option>'+
                    @endforeach
                    '</select>'+
                    '</div>'+
                    '<a class="delete" href="javascript:;" title="Remove row">X</a></div></td><td class="description"><select name="rows[' + count + '][batch_no]"  class="form-control"style="width: 200px;height: 38px"required> <option value="">Select Product First.</option></select></td><td><input style="width: 99%;height: 38px" name="rows['+ count +'][mfgDate]" class="mfgDate"><input type="hidden"  name="rows[' + count + '][sale_invoice]" value="0"></td><td><input style="width: 99%;height: 38px" name="rows['+ count +'][expDate]" class="expDate"></td>' +
                    '<td><input class="qty" name="rows[' + count + '][qty]" required></td><td><input class="cost" name="rows[' + count + '][cost]" required></td><td><input class="price"  name="rows[' + count + '][price]"readonly></td></tr>');
            if($(".delete").length > 0) $(".delete").show();
            bind();
            $("#country"+i).select2({
                source: country
            });

//            automatic batch and mfg ajax
            $('select[name="rows['+ count +'][product]"]').on('change', function() {

                var stateID = $(this).val();

                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/AjaxBatch/'+stateID,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {

                            $('select[name="rows['+ count +'][batch_no]"]').empty();
                            $('select[name="rows['+ count +'][batch_no]"]').append('' +
                                    '<option value="">Please Select Value</option>');

                            $.each(data, function(key, value) {

                                $('select[name="rows['+ count +'][batch_no]"]').append('<option value="'+ key +'|'+value+'">'+ value +'</option>');

                            });


                        }

                    });

                }else{

                    $('select[name="rows['+ count +'][batch_no]"]').empty();

                }

            });

//       get mfgdate and exp date of given batch number
            $('select[name="rows['+ count +'][batch_no]"]').on('change', function() {

                var stateID = $(this).val();

                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/AjaxMfgdate/'+stateID,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {


                            var Vals    =  data;
                            $("input[name='rows["+ count +"][mfgDate]']").val(Vals.Mfg);
                            $("input[name='rows["+ count +"][expDate]']").val(Vals.Exp);


                        }

                    });

                }else{

                    $("input[name='rows["+ count +"][mfgDate]']").empty();
                    $("input[name='rows["+ count +"][expDate]']").empty();

                }

            });
        });

        bind();
        $(document).on('click', '.delete', function(){
        $(".delete").on('click',function(){
            $(this).parents('.item-row').remove();
            update_total();
            if ($(".delete").length < 1) $(".delete").hide();
        });
        });

        $("#cancel-logo").click(function(){
            $("#logo").removeClass('edit');
        });
        $("#delete-logo").click(function(){
            $("#logo").remove();
        });
        $("#change-logo").click(function(){
            $("#logo").addClass('edit');
            $("#imageloc").val($("#image").attr('src'));
            $("#image").select();
        });
        $("#save-logo").click(function(){
            $("#image").attr('src',$("#imageloc").val());
            $("#logo").removeClass('edit');
        });

        $("#date").val(print_today());

        $('select[name="customer"]').on('change', function() {

            var stateID = $(this).val();

            if(stateID) {

                $.ajax({

                    url: '{{ url('/') }}/ajax/'+stateID,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {


                        var Vals    =  data;
                        $("input[name='phone']").val(Vals.phone);
                        $("input[name='email']").val(Vals.email);
                        $("input[name='city']").val(Vals.city);
                        $("textarea[name='address']").val(Vals.address);



                    }

                });

            }else{

                $('select[name="city"]').empty();

            }

        });





//        $(document).ajaxStop(function(){
//            window.location.reload();
//        });
    });
</script>
</body>

</html>
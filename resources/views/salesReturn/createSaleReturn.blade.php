@extends('layouts.adminPanel')
@section('title')
    Sale Return:Create
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('saleReturn')}}">Sale Return Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Create Sale Return</a>
    </li>

@endsection
@section('content')

    <h3 class="heading">Create Sale Return</h3>
    <?php echo Form::open(array('route' => 'storeSaleReturn')); ?>
    <div class="row"> <!-- row start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Selected Customer</label>
                <select id="customer1" name="customer" class="form-control" style="width: 99%!important;"   required>
                    <?php  $product_type=\App\Customer::find($tempSale->customer_id);?>
                    <option value="{{$product_type->id}}">{{$product_type->ledger_name}}</option>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Sale Return No.<span style="color:#EB3E28;">*</span></label>
                <?php  $currentDate= date("Y/m/d");

                ?>
                <?php  $dateNew = DB::table('sale_return_bill_session')->whereDate('startDate', '<=', $currentDate)->whereDate('closeDate', '>=', $currentDate)->first(); ?>

                <?php $sale_format = \App\Company_detail::find(1) ?>

                    @if($dateNew->bill==0)
                        <?php $number=1;
                        $bilNum = sprintf("%04d", $number);


                        $invoiceNo=$sale_format->billing_sale_return."$dateNew->session"."/".$bilNum."";
                        $session_id= $dateNew->id;

                        ?>
                    @else
                        <?php $number=$dateNew->bill+1;
                        $bilNum = sprintf("%04d", $number);

                        $invoiceNo=$sale_format->billing_sale_return."$dateNew->session"."/".$bilNum."";
                        $session_id= $dateNew->id;
                        ?>

                    @endif


                <input type="text" class="form-control" tabindex="0" name="sale_return_no" value="<?php echo $invoiceNo ?>" id="inputSuccess2"readonly>
                <input type="hidden" class="form-control" name="session_id" value="<?php echo $session_id ?>" id="inputSuccess2">
                <input type="hidden" class="form-control" name="sale_id" value="<?php echo $tempSale->sale_id ?>">
                <input type="hidden" class="form-control" name="tax_type" value="<?php echo $tempSale->tax_type ?>">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Sale Return Date<span style="color:#EB3E28;">*</span></label>
                <input type="text" name="sale_return_date" class="form-control datepicker" value="{{date("d/m/Y")}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Against Sale No</label>
                <input type="text" name="against_sale_no" class="form-control" value="{{$tempSale->unique_id}}" id="inputSuccess2" placeholder="Order No." readonly>
            </div>
        </div>
    </div> <!-- row ends here -->


    <br>

    <div class="table-responsive">
        <table id="items"  class="table table-striped table-bordered table-condensed" style="font-size: 13px!important;">

            <tr style="background-color: #e9f3f8">
                <th>Selected Item</th>
                <th>Desc.</th>
                <th>Qnty.</th>
                <th>Rate</th>
                <th>Amt.</th>
                <th>Disc.</th>
                <th>Txbl Amt.</th>
                <th>GST %</th>
                <th>Txt.Amt</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
            <?php $sale_invoice = DB::table('sale_returns_invoice_temp')->where([
                    ['temp_id', '=',$tempSale->id ],

            ])->get();

            ?>
            {{-------------count total number of row where sale id = this--}}
            <?php $total_sale_count = \App\Sale_return_invoice_temp::where(['temp_id' => $tempSale->id])->count(); ?>

            <?php   $count=0; ?>
            @foreach($sale_invoice as $sale)
                <tr class="item-row" style="border-bottom: solid 1px black">
                    <td style="width: 25%;position: relative"  class="item-name">
                        <input name="rows[{{$count}}][sale_invoice_id]" type="hidden" value="{{$sale->sale_invoice_no}}" class="form-control">
                        <select  name="rows[{{$count}}][product]" id="product_name" class="form-control" style="width:99%;padding: 9px!important;"  required>
                            <?php $product_name =\Illuminate\Support\Facades\DB::table('products')->select('id', 'product_name')->where([
                                    ['id', '=',$sale->product_id ],  ])->first();  ?>
                            <option value="{{$product_name->id}}" selected="selected">{{$product_name->product_name}}</option>
                        </select>
                    </td>
                    <td style="width: 5%" class="main_td"><textarea name="rows[{{$count}}][desc]" id="desc{{$count}}"  class="form-control" style="height:34px!important">{{$sale->description}}</textarea></td>
                    <td class="main_td"><input name="rows[{{$count}}][qty]" value="{{$sale->quantity}}" class="qty form-control" required></td>
                    <td><input name="rows[{{$count}}][rate]" value="{{$sale->rate}}" class="cost form-control"  required></td>
                    <td class="main_td"><input name="rows[{{$count}}][amount]" value="{{$sale->amount_before_tax}}" class="amt_before_tax form-control" required></td>
                    <td style="width: 5%;position: relative" class="main_td"><input name="rows[{{$count}}][disc]" value="{{$sale->discount}}" class="disc form-control"  ></td>
                    <td class="main_td"><input name="rows[{{$count}}][taxbl_amount]" value="{{$sale->taxable_amount}}" class="taxbl_amount form-control"  readonly></td>
                    @if($tempSale->tax_type=="CGST&SGST")
                        <?php
                        $total_tax_percentage=$sale->sgst_tax+$sale->cgst_tax; ?>
                    @else
                        <?php    $total_tax_percentage=$sale->igst_tax;  ?>
                    @endif

                    <td class="main_td"><input name="rows[{{$count}}][gst]" value="{{$total_tax_percentage}}" class="igst form-control" ></td>
                    <td class="main_td"><input name="rows[{{$count}}][text_amount]" value="{{$sale->tax_amount}}" class="text_amount form-control"  readonly></td>
                    <td class="main_td"><input name="rows[{{$count}}][price]" value="{{$sale->total_amount}}" class="price form-control"  readonly></td>
                    <td>
                        <a class="btn btn-info btn-sm del"  href="javascript:;">DEL</a> </td>
                </tr>

                <script>
                    var mycount='<?php echo $count ?>';
                    $('#desc'+mycount).focus(function()
                    {
                        $(this).animate({ height: '+=100',width:'+=80' }, 'slow');
                    }).blur(function()
                    {
                        $(this).animate({ height: '-=100',width:'-=80' }, 'slow');
                    });
                </script>

                <?php   $count=$count+1; ?>


            @endforeach

        </table>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">
                    <button type="submit" class="btn btn-success btn-sm" name="submit"><i class="splashy-document_a4_add"></i> Save</button>
                </div>
                <div class="col-md-3">
                    <button type="reset" class="btn btn-info btn-sm" name="submit"><i class="splashy-refresh_backwards"></i> Reset</button>
                </div>
                <div class="col-md-3">
                    <a href="{{url('saleReturn')}}" class="btn btn-danger btn-sm"><i class="splashy-gem_cancel_1"></i> Cancel</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered table-striped">
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
                <tr>
                    <td width="50%">Total Amount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_amount_text">{{$tempSale->total_amount_without_anything}}</span>
                        <input type="hidden" name="total_amount_input" value="{{$tempSale->total_amount_without_anything}}" id="total_amount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Discount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_discount_text">{{$tempSale->total_discount}}</span>
                        <input type="hidden" name="total_discount_input" value="{{$tempSale->total_discount}}" id="total_discount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Taxble Amt.</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_taxble_text">{{$tempSale->total_taxable_value}}</span>
                        <input type="hidden" name="total_taxble_input" value="{{$tempSale->total_taxable_value}}" id="total_taxble_input"></td>
                </tr>



                <tr>
                    <td width="50%">Total GST</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_gst_text">{{$tempSale->total_tax_amount}}</span>
                        <input type="hidden" name="total_gst_input" value="{{$tempSale->total_tax_amount}}" id="total_gst_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Gross Amt.</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_sub_text">{{$tempSale->gross_total}}</span>
                        <input type="hidden" name="total_sub_input" value="{{$tempSale->gross_total}}" id="total_sub_input">
                        <input type="hidden"   name="postage_charge" id="postage_charge">
                    </td>
                </tr>

                <tr>
                    <td width="50%">Grand Total</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="grand_total_text">{{$tempSale->grand_total}}</span>
                        <input type="hidden" name="grand_total_input" value="{{$tempSale->grand_total}}" id="grand_total_input"></td>
                </tr>
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
            </table>


        </div>

    </div>
    {{form::close()}}

    <script>

        // from http://www.mediacollege.com/internet/javascript/number/round.html
        function roundNumber(number,decimals) {
            var newString;// The new rounded number
            decimals = Number(decimals);
            if (decimals < 1) {
                newString = (Math.round(number)).toString();
            } else {
                var numString = number.toString();
                if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
                    numString += ".";// give it one at the end
                }
                var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
                var d1 = Number(numString.substring(cutoff,cutoff+1));// The value of the last decimal place that we'll end up with
                var d2 = Number(numString.substring(cutoff+1,cutoff+2));// The next decimal, after the last one we want
                if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
                    if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
                        while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
                            if (d1 != ".") {
                                cutoff -= 1;
                                d1 = Number(numString.substring(cutoff,cutoff+1));
                            } else {
                                cutoff -= 1;
                            }
                        }
                    }
                    d1 += 1;
                }
                if (d1 == 10) {
                    numString = numString.substring(0, numString.lastIndexOf("."));
                    var roundedNum = Number(numString) + 1;
                    newString = roundedNum.toString() + '.';
                } else {
                    newString = numString.substring(0,cutoff) + d1.toString();
                }
            }
            if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
                newString += ".";
            }
            var decs = (newString.substring(newString.lastIndexOf(".")+1)).length;
            for(var i=0;i<decimals-decs;i++) newString += "0";
            //var newNumber = Number(newString);// make it a number if you like
            return newString; // Output the result to the form field (change for your purposes)
        }


        //original function
        // it's also working
        function update_balance() {

        }


        // amount calculation it working without discount without tax value for individual row
        function update_price() {
            var row = $(this).parents('.item-row');
            var price = row.find('.cost').val().replace("$","") * row.find('.qty').val();
            price = roundNumber(price,2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(price) ? row.find('.amt_before_tax').html("N/A") : row.find('.amt_before_tax').val(""+price);

            update_total();
        }

        //    taxable value  calculation for individual row
        function taxble_value(){
            var row = $(this).parents('.item-row');
            var discount_amount=row.find('.disc').val();
            var amount_before_tax=row.find('.amt_before_tax').val()
            var price1 = Number(amount_before_tax)-Number(discount_amount);
            price1 = roundNumber(price1,2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(price1) ? row.find('.taxbl_amount').html("N/A") : row.find('.taxbl_amount').val(""+price1);

//        update_total();
        }



        //    each row tax calculation

        function update_tax() {
            var row = $(this).parents('.item-row');



            var $igst=row.find('.igst').val();
            var total_tax_percentage=Number($igst)

//-----------------------------
            var sellingPrice=row.find('.taxbl_amount').val();

            var tax_amount=Number(sellingPrice)*total_tax_percentage/100;
            var total_amount=Number(sellingPrice)+tax_amount;
            tax1 =roundNumber(tax_amount,2);
            amount1=roundNumber(total_amount,2)
//        isNaN(amount1) ? row.find('.price').html("N/A") : row.find('.price').html("$"+amount1);
            isNaN(tax1) ? row.find('.text_amount').html("N/A") : row.find('.text_amount').val(""+tax1);
            isNaN(amount1) ? row.find('.price ').html("N/A") : row.find('.price ').val(""+amount1);

            update_total();
        }




        // without disc without tax amount calculation for total
        function update_total() {
            var total = 0;
            $('.amt_before_tax').each(function(i){
                price = $(this).val().replace("$","");
                if (!isNaN(price)) total += Number(price);
            });

            total = roundNumber(total,2);

            $('#total_amount_input').val(""+total);
            $('#total_amount_text').html(""+total);
//                    $('#total').html(""+total);

            update_balance();
        }

        function total_discount(){
            var total_discount = 0;
            $('.disc').each(function(i){
                price1 = $(this).val().replace("$","");
                if (!isNaN(price1)) total_discount += Number(price1);
            });

            total_discount = roundNumber(total_discount,2);

            $('#total_discount_input').val(""+total_discount);
            $('#total_discount_text').html(""+total_discount);

            update_balance();
        }
        //                taxbl_amount

        function total_taxble_amount(){
            var total_taxable = 0;
            $('.taxbl_amount').each(function(i){
                price2 = $(this).val().replace("$","");
                if (!isNaN(price2)) total_taxable += Number(price2);
            });

            total_taxable = roundNumber(total_taxable,2);

            $('#total_taxble_input').val(""+total_taxable);
            $('#total_taxble_text').html(""+total_taxable);

            update_balance();
        }

        function total_gst() {
            var total_tax = 0;
            $('.text_amount').each(function(i){
                igst = $(this).val().replace("$","");
                if (!isNaN(igst)) total_tax += Number(igst);
            });

            total_tax = roundNumber(total_tax,2);

            $('#total_gst_input').val(""+total_tax);
            $('#total_gst_text').html(""+total_tax);
            update_balance();
        }







        function grandtotal(){
            var grandtotal = 0;
            $('.price').each(function(i){
                price = $(this).val().replace("$","");
                if (!isNaN(price)) grandtotal += Number(price);
            });
            grandtotal = roundNumber(grandtotal,2);
            $('#total_sub_input').val(""+grandtotal);
            $('#total_sub_text').html(""+grandtotal)
            var new_num = Math.round(grandtotal).toFixed(2);
            $('#grand_total_text').html(""+new_num);
            $('#grand_total_input').val(""+new_num);
        }





        function bind() {
            $(".qty").blur(update_price);
            $(".cost").blur(update_price);

            //        taxable value blur
            $(".qty").blur(taxble_value);
            $(".cost").blur(taxble_value);
            $(".disc").blur(taxble_value);
            $(".amt_before_tax").blur(taxble_value);
            $(".delete").blur(taxble_value);

            //        vat calculation

            $(".qty").blur(update_tax);
            $(".cost").blur(update_tax);
            $(".disc").blur(update_tax);
            $(".taxbl_amount").blur(update_tax);
            $(".amt_before_tax").blur(update_tax);
            $(".sgst").blur(update_tax);
            $(".cgst").blur(update_tax);
            $(".igst").blur(update_tax);


            // discount

            $(".qty").blur(total_discount);
            $(".cost").blur(total_discount);
            $(".amt_before_tax").blur(total_discount);
            $(".disc").blur(total_discount);
            $(".taxbl_amount").blur(total_discount);
//                    $("#discount").blur(total_discount);


// total_taxble_amount

            $(".qty").blur(total_taxble_amount);
            $(".cost").blur(total_taxble_amount);
            $(".amt_before_tax").blur(total_taxble_amount);
            $(".disc").blur(total_taxble_amount);
            $(".taxbl_amount").blur(total_taxble_amount);





//        gst calculation
//                    total_gst

            $(".qty").blur(total_gst);
            $(".cost").blur(total_gst);
            $("#subtotal1").blur(total_gst);
            $(".disc").blur(total_gst);
            $(".taxbl_amount").blur(total_gst);
            $(".igst").blur(total_gst);
            $(".delete").blur(total_gst);



//         grand total


            $(".qty").blur(grandtotal);
            $(".cost").blur(grandtotal);
            $("#subtotal1").blur(grandtotal);
            $(".disc").blur(grandtotal);
            $(".taxbl_amount").blur(grandtotal);
            $(".igst").blur(grandtotal);
            $(".delete").blur(grandtotal);


        }
        $('#two').click(function(){
            taxble_value();
            total_igst();
            update_tax();
            total_discount();
            update_total();
            subtotalgrand();
            grandtotal();


        });


        //-----------------------------------------------------update new product ----------------------------------

        bind();
        $(document).on('click', '.del', function(){
            if (confirm('This Data is saved ! Are you sure you want to delete this?')) {
                $(this).parent().parent().remove();
                update_total();
                total_discount();
                total_taxble_amount();
                total_gst();
                grandtotal();

            }

        });







    </script>
    <br>
@endsection
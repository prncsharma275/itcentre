@extends('layouts.adminPanelTableOld')
@section('title')
    Purchase Panel
@endsection

@section('custom_css')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('css/bootstrap/js/dataTables.bootstrap.min.js')}}"></script>
@endsection

@section('manual_style_code')

    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('purchase')}}">Purchase Panel</a>
    </li>
@endsection
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Purchase Panel</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('purchase_type')}}"><i class="splashy-document_letter_add"></i> New Purchase</a>

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <table class="table table-striped table-bordered table-condensed dTableR" id="table" style="font-size: 12px!important;">
                <thead>
                <tr style="font-size: 11px!important;">
                    <th>Party Bill No.</th>
                    <th>Party Bill Date</th>
                    <th>SL. No.&nbsp; &nbsp;&nbsp;&nbsp;</th>
                    <th>Supplier Name&nbsp;&nbsp;</th>
                    <th>Bill Amount&nbsp;</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    <h4 class="text-center"></h4>

    <script>
        //                $(document).ready(function() {
        //                    $('#table').DataTable();
        //                } );
//        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
//            "date-uk-pre": function ( a ) {
//                var ukDatea = a.split('/');
//                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
//            },
//
//            "date-uk-asc": function ( a, b ) {
//                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
//            },
//
//            "date-uk-desc": function ( a, b ) {
//                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
//            }
//        } );
        $(document).ready(function () {
            $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "aaSorting": [[ 1, 'desc' ]],
                "ajax":{
                    "url": "{{ url('allpurchase') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [

                    { "data": "party_invoice" },
                    { "data": "party_invoice_date_correct" },
                    { "data": "invoice_no" },
                    { "data": "supplier_id" },
                    { "data": "grand_total" },
                    { "data": "action" }
                ]


            });
        });
        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }

        $(".alert").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });
    </script>
    <br>
@endsection

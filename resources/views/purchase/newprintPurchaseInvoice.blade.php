<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ITC:INVOICE</title>
    <link rel="stylesheet" href="{{ asset('salesAndPurchaseStyle/css/printInvoice.css')}}" media="all" />

    <style type="text/css" media="print">

        .rupees{

        }


        @page {
            size: landscape;   /* auto is the initial value */
            margin-left: 10px;  /* this affects the margin in the printer settings */
            margin-right:20px;  /* this affects the margin in the printer settings */
            margin-top:10px;  /* this affects the margin in the printer settings */

            margin-bottom: 5px;
            background-color: gainsboro;
            font-size: 15px;
            /*margin: px;*/
        }

        /*@media print{@page {size: landscape}}*/
        @media print{
            /*/!*.below_notice{*!/*/
            /*/!*width: 600px!important;*!/*/
            /*/!*margin-right: 10px!important;*!/*/
            /*/!*}*!/*/
            .bottom_box{
                position: relative;
                bottom: 0px;
            }
            button{

                display: none!important;
            }
            a{
                display: none!important;
            }
            .command{
                display: none!important;
            }
            .command1,.command2{
                display: none;!important;
            }
            footer{
                margin-top: 10px;
            }
        }

        .total_amount> li {
            border: solid 1px!important;
        }
        Head::first-letter {
            font-weight: bold;
            color: red;
        }
    </style>
</head>

{{-----------------------------------------------------------}}

<body style="">

<div class="invoice"><h3>TAX PURCHASE INVOICE</h3></div>
<div id="details" class="clearfix">
    <div id="client" style="height: 100px;">
        <h2 class="name" style="margin-top: -2px">M/S I.T. CENTRE</h2>
        <div class="address"style="margin-top:-21px;">1st Floor, OS Complex, Opposite RK Mission Road</div>
        <div class="email">Ganga Market, Itanagar, Pin &#45; 791111,Arunachal Pradesh</div>
        {{--<div class="email"><a href="mailto:{{$printBill->belongsToCustomer->email}}">{{$printBill->belongsToCustomer->email}}</a></div>--}}
        <div class="email">Email- itcentre.ita@gmail.com, Cell-8963032009 </div>
        <div class="email">GSTIN-12EMRPS6621A1Z3</div>
    </div>

    <div id="client2"style="height: 100px;">
        <div class="to" style="font-size: 14px">PURCHASE FROM:</div>
        <?php if($printBill->payment_type=="cash") {?>
        <h3 class="name" style="margin-top: -2px">{{$printBill->belongsToCustomer->ledger_name}} ({{$printBill->retail_name}})</h3>
        <div class="address"style="margin-top:-21px">{{$printBill->address}} {{$printBill->city}}, {{$printBill->state}}, {{$printBill->zip}}</div>
        <div class="email">Mobile-{{$printBill->mobile}} email- {{$printBill->email}}</div>
        {{--<div class="email"><a href="mailto:{{$printBill->belongsToCustomer->email}}">{{$printBill->belongsToCustomer->email}}</a></div>--}}

        <?php }else{ ?>
        <h3 class="name" style="margin-top: -2px">{{$printBill->belongsToCustomer->ledger_name}}</h3>
        <div class="address"style="margin-top:-21px">{{$printBill->belongsToCustomer->address}} {{$printBill->belongsToCustomer->city}}, {{$printBill->belongsToCustomer->state}}, {{$printBill->belongsToCustomer->zip}}</div>
        <div class="email">Mobile-{{$printBill->belongsToCustomer->mobile}} email- {{$printBill->belongsToCustomer->email}}</div>
        <div style="font-size: 12px">GST NO- {{$printBill->belongsToCustomer->vatNo}}</div>
        <?php } ?>

    </div>
    <div id="invoice"style="height: 100px;">
        <h1 style="color: #000000;font-size: 1.1em;"><span style="font-size: 1.1em;color: #777777">Invoice No - </span>{{$printBill->unique_id}}</h1>
        <div class="date">Date of Invoice:<span style="color: #000"> <?php echo date( 'd/m/y', strtotime($printBill->billing_date)) ?></span></div>
        <div class="date" style="margin-top: 6px"></div>
        <div class="date" style="margin-top: 6px"></div>
        <div class="date" style="margin-top: 6px">Order No: {{$printBill->order_no}},  Challan No: {{$printBill->challan_no}} </div>
        <div class="date" style="margin-top: 6px">Order Date: {{$printBill->order_date}}, Challan Date: {{$printBill->challan_date}}</div>
    </div>
</div>
<div class="billcontainer">
    <div class="head" style="border-bottom: solid 1px #000;">
        <ul>
            @if($printBill->tax_type=="CGST&SGST")
                <li style="padding:2px;width:534px;">Description</li>
            @else
                <li style="padding:2px;width:665px;">Description</li>
            @endif
            {{--<li style="padding:2px;width:69px; color: #ffffff">Size</li>--}}
            <li style="padding:2px;width:79px;">MRP</li>
            <li style="padding:2px;width: 69px;">Qnty</li>
            <li style="padding:2px;width: 69px;">Rate</li>
            <li style="padding:2px;width:82px;">Amt.</li>
            <li style="padding:2px;width: 56px;">Disc %</li>
            <li style="padding:2px;width: 56px;">Disc.</li>
            <li style="padding:2px;width: 89px;">Txbl Amt.</li>
            @if($printBill->tax_type=="CGST&SGST")
                <li style="padding:2px;width: 49px;">CGST</li>
                <li style="padding:2px;width: 79px;font-size: 14px">CGST AMT.</li>
                <li style="padding:2px;width: 49px;">SGST</li>
                <li style="padding:2px;width: 79px;font-size: 14px">SGST AMT.</li>
            @else
                <li style="padding:2px;width: 50px;">IGST</li>
                <li style="padding:2px;width: 81px;">IGST Amt.</li>
            @endif

            <li style="padding:2px;width: 98px;">Total.</li>
        </ul>
    </div>

    <?php $sale_invoice = DB::table('purchase_invoice')->where([
            ['purchase_id', '=',$printBill->id ],

    ])->get();  ?>


    <?php $i=1 ;

    $qnty=0;
    ?>

    @foreach($sale_invoice as $sale)

        <div class="head" style="text-align:left;font-size:16px;margin-top: 4px;text-transform: capitalize;">
            <ul>
                <?php  $product=\App\Product::find($sale->product_id); ?>

                @if($printBill->tax_type=="CGST&SGST")
                    <li style="padding:2px;text-align:left;width:534px;"> ({{$i}})&nbsp;{{$product->product_name}} (<span style="text-transform: uppercase;font-size: 12px!important;"> HSN </span>-{{$product->product_code}})<br> <span style="font-style: italic"></span> </li>
                @else
                    <li style="padding:2px;text-align:left;width:665px;"> ({{$i}})&nbsp;{{$product->product_name}} (<span style="text-transform: uppercase;font-size: 12px!important;"> HSN -{{$product->product_code}})</span><br> <span style="font-style: italic;font-size: 12px;"></span></li>
                @endif
                <li style="padding:2px;width:79px;">{{$product->mrp}}</li>
                {{--<li style="padding:2px;width: 69px;background-color: green">{{$sale->quantity}}{{$sale->unit}}</li>--}}
                <li style="padding:2px;width: 69px;">{{$sale->quantity}}<span style="font-size: 11px!important">{{$product->unit_type}}</span></li>
                <?php $qnty =$qnty+$sale->quantity; ?>
                <li class="text-center" style="padding:2px;width:67px;font-size:18px;">{{$sale->rate}}</li>
                <li style="padding:2px;width: 81px;font-size:18px;">{{$sale->amount_before_tax}}</li>

                <li style="padding:2px;width: 55px;text-align: center;">{{$sale->discount_per}}%</li>
                <li style="padding:2px;width: 55px;text-align: center;">{{$sale->discount_amt}}</li>
                {{--<li style="padding:2px;width: 111px; text-align: right;background-color: green">{{$sale->amount_before_tax}}</li>--}}
                <li style="padding:2px;width: 89px; text-align: center;">{{$sale->taxable_amount}}</li>
                @if($printBill->tax_type=="CGST&SGST")
                    <li style="padding:2px;width: 47px; text-align: center;">{{$sale->cgst_tax}}%</li>
                    <?php $tax_half= ((double) $sale->tax_amount)/2; ?>
                    <li style="padding:2px;width: 79px; text-align: center;">{{$tax_half}}</li>
                    <li style="padding:2px;width: 47px; text-align: center;">{{$sale->sgst_tax}}%</li>
                    <li style="padding:2px;width: 79px; text-align: center;">{{$tax_half}}</li>
                @else
                    <li style="padding:2px;width: 47px; text-align: right;">{{$sale->igst_tax}}%</li>
                    <li style="padding:2px;width: 79px; text-align: right;">{{$sale->tax_amount}}</li>
                @endif

                <li style="padding:2px;width: 102px; text-align: center;">{{$sale->total_amount}}</li>
            </ul>
        </div>

        <?php $i++ ; ?>
    @endforeach

    <div class="slno"></div>
    @if($printBill->tax_type=="CGST&SGST")
        {{--<div class="floatline description" style="left: 712px"></div>--}}
        <div class="floatline size"style="left: 519px"></div>
        <div class="floatline batch"style="left: 612px"></div>
        <div class="floatline mfg"style="left: 678px"></div>
        <div class="floatline exp"style="left: 750px"></div>
        <div class="floatline qnty"style="left: 836px"></div>
        <div class="floatline qnty"style="left: 896px"></div>
        <div class="floatline qnty"style="left: 952px"></div>
        <div class="floatline qnty"style="left: 1042px"></div>
        <div class="floatline rate"style="left: 1103px"></div>
        <div class="floatline rate"style="left: 1184px"></div>
        <div class="floatline rate"style="left: 1241px"></div>
        <div class="floatline rate"style="left: 1322px"></div>
    @else
        <div class="floatline description" style="left: 660px"></div>
        <div class="floatline description" style="left: 726px"></div>
        <div class="floatline size"style="left: 796px"></div>
        <div class="floatline batch"style="left: 878px"></div>
        <div class="floatline mfg"style="left: 962px"></div>
        <div class="floatline exp"style="left: 1028px"></div>
        <div class="floatline qnty"style="left: 1088px"></div>
        <div class="floatline rate"style="left: 1182px"></div>
        <div class="floatline rate"style="left: 1234px"></div>
        <div class="floatline rate"style="left: 1318px"></div>
        {{--<div class="floatline rate"style="left: 1201px"></div>--}}
    @endif
</div>



<div class="billcontainer1">
    <div class="head" style="font-size: 13px">
        <ul class="total_amount">
            <li style="padding:2px;width: 110px;border: solid 1px">TOTAL QNTY</li>
            <li style="padding:2px;width:140px;border: solid 1px">TOTAL AMT.</li>
            <li style="padding:2px;width: 100px;border: solid 1px">TOTAL DIS.</li>
            <li style="padding:2px;width: 140px;border: solid 1px">TOTAL TAXBLE AMT.</li>
            {{--<li style="padding:2px;width:56px;border: solid 1px">CGST %</li>--}}
            <li style="padding:2px;width:130px;border: solid 1px">TOTAL CGST AMT.</li>
            {{--<li style="padding:2px;width: 56px;border: solid 1px">SGST %</li>--}}
            <li style="padding:2px;width: 130px;border: solid 1px">TOTAL SGST AMT.</li>
            {{--<li style="padding:2px;width:56px;border: solid 1px">IGST %</li>--}}
            <li style="padding:2px;width: 130px;border: solid 1px">TOTAL IGST AMT.</li>
            <li style="padding:2px;width: 115px;border: solid 1px">TOTAL TAX AMT.</li>
            <li style="padding:2px;width: 130px;border: solid 1px">TOTAL GROSS AMT.</li>
            <li style="padding:2px;width: 120px;border: solid 1px">Postage.</li>
            <li style="padding:2px;width: 150px;border: solid 1px">GRAND TOTAL</li>
        </ul>
    </div>
    <div class="head" style="font-size: 13px">
        <ul class="total_amount">
            <li style="padding:2px;width: 110px;border: solid 1px">{{$qnty}}</li>
            <li style="padding:2px;width:140px;border: solid 1px">{{$printBill->amount_without_anything}}</li>
            @if($printBill->total_discount=="")
                <li style="padding:2px;width: 100px;border: solid 1px">00</li>
            @else
                <li style="padding:2px;width: 100px;border: solid 1px">{{$printBill->total_discount}}</li>
            @endif


            <li style="padding:2px;width: 140px;border: solid 1px">{{$printBill->total_taxable_amount}}</li>
            @if($printBill->tax_type=="CGST&SGST")
                <li style="padding:2px;width:130px;border: solid 1px">{{$printBill->total_cgst}}</li>
                <li style="padding:2px;width: 130px;border: solid 1px">{{$printBill->total_sgst}}</li>
                <li style="padding:2px;width: 130px;border: solid 1px">NA</li>
            @else
                <li style="padding:2px;width:130px;border: solid 1px">NA</li>
                <li style="padding:2px;width: 130px;border: solid 1px">NA</li>
                <li style="padding:2px;width: 130px;border: solid 1px">{{$printBill->total_igst}}</li>
            @endif
            <li style="padding:2px;width: 115px;border: solid 1px">{{$printBill->total_tax_amount}}</li>
            <li style="padding:2px;width: 130px;border: solid 1px">{{$printBill->gross_total}}</li>
            @if($printBill->postage_charge=="")
                <li style="padding:2px;width: 120px;border: solid 1px">00</li>
            @else
                <li style="padding:2px;width: 120px;border: solid 1px">{{$printBill->postage_charge}}</li>
            @endif
            <li style="padding:2px;width: 150px;border: solid 1px">{{$printBill->grand_total}}</li>
        </ul>
    </div>
</div>
{{--<div class="clearfix:after"></div>--}}
{{--Money Number to Word--}}
<?php
$number = $printBill->grand_total;
$no = round($number);
$point = round($number - $no, 2) * 100;
$hundred = null;
$digits_1 = strlen($no);
$i = 0;
$str = array();
$words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
$digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
while ($i < $digits_1) {
    $divider = ($i == 2) ? 10 : 100;
    $number = floor($no % $divider);
    $no = floor($no / $divider);
    $i += ($divider == 10) ? 1 : 2;
    if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
    } else $str[] = null;
}
$str = array_reverse($str);
$result = implode('', $str);
$points = ($point) ?
        "." . $words[$point / 10] . " " .
        $words[$point = $point % 10] : '';?>

<div class="money" style="border-top: 1px solid;border-bottom: 1px solid; font-size: 17px; margin-top: 3px;padding-left: 30px;padding-top: 1px;">
    All disputes subjects to Itanagar Jurisdication only
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;




    <?php
    echo $result . "Rupees  " . $points . "Only";
    ?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Authorised Signatory
</div>


<section id="command">
    <button class="command" onclick="myFunction()">Print</button>
    <a href="{{url('purchase_type')}}" class="command command2">Create New Invoice</a>
    <a href="{{url('/purchase')}}" class="command command1">Back</a>
    <a href="{{url('/home')}}" class="command command4">Home</a>
</section>

<script>
    function myFunction() {
        window.print();
    }
    //    history.pushState(null, null, 'no-back-button');
    //    window.addEventListener('popstate', function(event) {
    //        history.pushState(null, null, 'no-back-button');
    //    });
</script>




</body>
</html>

@extends('layouts.adminPanelTable')
@section('title')
    Purchase Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
        }
        /*input[type='search']{*/
        /*width: 200px!important;*/
        /*}*/
        .panel{
            box-shadow:3px 3px 5px 3px  #ccc;
        }
        .panel-info > .panel-heading {
            background-color: #14c1d7!important;
            color: #ffffff;
        }
        .panel-info > .panel-footer{
            background-color: #14c1d7!important;
            color: #ffffff!important;
        }
        .panel-body{
            background-color: #fff!important;
        }


    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('purchase')}}">Purchase Panel</a>
    </li>
    <li>
        <a href="{{url('purchase_type')}}">Tax Type</a>
    </li>
@endsection
@section('content')

    <h3 class="heading">Tax Type Selection</h3>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-info">
                <div class="panel-heading">SELECT GST TYPE</div>

                <div class="panel-body" style="padding-top: 40px">
                    <?php echo Form::open(array('route' => 'store_purchase_tax_type', 'class'=>"form-inline")); ?>
                    <label>GST TYPE :</label>
                    <select name="tax_type" id="customer" class="form-control" style="width: 100%" required>
                        <option value="CGST&SGST">CGST&SGST</option>
                        <option value="IGST">IGST</option>
                    </select>
                    <br> <br>
                    <button type="submit"  class="btn btn-default btn-sm"><i class="splashy-bullet_blue_arrow"></i> Select</button>
                    {{form::close()}}
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h6 style="color: #EB3E28;">* CGST&SGST- For Inside State Transaction</h6>
            <h6 style="color: #EB3E28">* IGST- For Inter State Transaction</h6>
        </div>


    </div>

    {{--inner content here ------------------------------------}}
    <br>
@endsection
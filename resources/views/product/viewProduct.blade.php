@extends('layouts.adminPanelTable')
@section('title')
    Product Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('product')}}">Product Panel</a>
    </li>
@endsection
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif
            <h3 class="heading">Stocks</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createProduct')}}"><i class="splashy-document_letter_add"></i> New Product</a>
            <table class="table table-striped table-bordered dTableR" id="dt_a">
                <thead>
                <tr>

                    <th class="text-center">Product Name</th>
                    <th class="text-center">Product Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">HSN Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Quantity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">GST %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product as $item)

                    <tr>
                        <td style="width: 25%" class="text-center">{{$item->product_name}}</td>
                        <td class="text-center">{{$item->product_type_name}}</td>
                        <td class="text-center">{{$item->product_code}}</td>

                        <?php  $qnty_sale = \App\Sale_invoice::where('product_id',$item->id)->whereNull('ischallansale')->whereNull('status')->sum('quantity') ?>
                        <?php  $qnty_sale_return = \App\Sale_return_invoice::where('product_id',$item->id)->sum('quantity') ?>
                        <?php  $qnty_purchase = \App\Purchase_invoice::where('product_id',$item->id)->whereNull('status')->sum('quantity') ?>
                        <?php  $qnty_purchase_return = \App\Purchase_return_invoice::where('product_id',$item->id)->sum('quantity') ?>
                        <?php  $qnty_challan = \App\Challan_child::where('product_id',$item->id)->where('status','!=','cancel')->sum('quantity'); ?>
                        <?php  $qnty_challan_return = \App\chalan_return_child::where('product_id',$item->id)->sum('quantity'); ?>
                          <?php $opening_stock = (float)$item->opening_stock ?>
                        <?php $total_qun = ($opening_stock+$qnty_purchase+$qnty_sale_return+$qnty_challan_return)-($qnty_sale+$qnty_purchase_return+$qnty_challan) ?>
                        <td class="text-center">{{$total_qun}}</td>
                        <td class="text-center">{{$item->igst}}</td>

                        <td>
                            <a href="editProduct/{{$item->id}}" class="btn btn-default btn-sm" title="Edit">
                                <i class="splashy-document_letter_edit"></i>
                            </a>
                            <a href="deleteProduct/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()">
                                <i class="splashy-document_letter_remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <script>
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });

                $(document).ready(function() {
                    $('#table').DataTable();
                } );

                //    delete commande
                function ConfirmDelete()
                {
                    var x = confirm("Are you sure you want to Cancel This Bill?");
                    if (x)
                        return true;
                    else
                        return false;
                }


            </script>
<br>
@endsection
            {{--inner content here ------------------------------------}}

@extends('layouts.adminPanel')
@section('title')
    Product:Create
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('product')}}">Product Panel</a>
    </li>
    <li>
        <a href="{{url('createProduct')}}">New Product</a>
    </li>
@endsection
@section('content')

            <h3 class="heading">Add New Product</h3>
            <hr>
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif

            <form  action="storeProduct" method="get"  id="contact_form">

                <div class="row">

                    <div class="col-md-6 col-sm-6">

                        <label>Product Category</label>
                        <select name="product_type_id" class="has-success has-feedback" onchange = "myFunction1()" tabindex="0" id="product_type" style="width: 100%"required>
                            <option value="">Select one</option>
                            <option value="create">+ Create New</option>
                            <?php  $product_type=\App\Product_type::All();?>
                            @foreach($product_type as $product_type)
                                <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>

                            @endforeach
                        </select>


                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group has-success has-feedback">
                            <label>Product Brand</label>
                            <select name="product_brand_id" class="has-success has-feedback" onchange = "myFunction()" id="brand_name"  style="width: 100%" required>
                                <option value="">Select one</option>
                                <option value="create">+ Create New</option>
                                <?php  $product_type=\App\Brand::All();?>
                                @foreach($product_type as $product_type)
                                    <option value="{{$product_type->id}}">{{$product_type->brand_name}}</option>

                                @endforeach
                            </select>
                        </div>


                    </div>



                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-6">
                        <div class="form-group has-success has-feedback">
                        <label>New Product Category</label>
                        <input type="text" name="new_product_type" id="new_product_type" class="form-control" placeholder="New Product Category"disabled>
                       </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group has-success has-feedback">
                            <label>New Product Brand</label>
                            <input type="text" name="new_brand_name" id="new_brand_type" class="form-control" placeholder="New Brand"disabled>
                        </div>


                    </div>



                </div>

                <br>
                <!-- row end here -->
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group has-success has-feedback">
                            <label>Product Name<span style="color: red; font-size: 15px">*</span></label>
                                <input  type="text" class="form-control " name="product_name"  placeholder="Enter Model Name Here" required>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Product HSN Code<span style="color: red; font-size: 15px">*</span></label>
                                <input  type="text" class="form-control" name="product_hsn_code" placeholder="Enter Product Code Here">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Unit Type<span style="color: red; font-size: 15px"> *</span></label>
                                <select class="form-control" id="unit" name="unit_type" required>
                                    <option value="">Select Unit Type</option>
                                    <?php $units = \App\Unit::all() ?>
                                    @foreach($units as $unit)
                                    <option value="{{$unit->unit_short_name}}">{{$unit->unit_name}} - ({{$unit->unit_short_name}})</option>
                                  @endforeach

                                </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class=" has-success has-feedback">
                        <label>Product Description</label>
                        <textarea class="form-control" name="product_desc"></textarea>
                            </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>IGST<span style="color: red; font-size: 12px">* </span><span style="color: green; font-size: 12px"> (Don't use % sign)</span></label>
                                <input  type="number" id="igst" class="form-control" onchange="copyTextValue(this);" name="igst"   placeholder="Enter IGST Rate"required>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>CGST<span style="color: red; font-size: 15px"> *</span></label>
                                <input  type="text" id="cgst" class="form-control" name="cgst"  placeholder="Enter CGST Rate"readonly>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>SGST<span style="color: red; font-size: 15px"> *</span></label>
                                <input  type="text" id="sgst" class="form-control" name="sgst"  placeholder="Enter SGST Rate"readonly>
                        </div>
                    </div>


                </div> <!-- row end here -->



                <div class="row">



                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label style="font-size: 13px">Selling Price<span style="color: green; font-size: 12px"></span></label>
                                <input  type="number" step="0.01" id="selling_price" class="form-control" name="selling_price"  placeholder="Selling Price">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>M.R.P<span style="color: red; font-size: 15px"></span></label>
                                <input  type="number" step="0.01" id="mrp" class="form-control" name="mrp" placeholder="Maximum Retail Price">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Opening Stock<span style="color: red; font-size: 15px"></span></label>
                                <input  type="number" id="opening_stock" class="form-control" name="opening_stock" placeholder="Opening Stock">
                        </div>
                    </div>


                </div> <!-- row end here -->
<br>
                <div class="row">
                    <div class="col-md-2 col-sm-4 col-xs-4 col-md-offset-3"><button type="submit" class="btn btn-success" >Save <span class="glyphicon glyphicon-hdd"></span></button></div>
                    <div class="col-md-2 col-sm-4 col-xs-4 "> <button type="reset" class="btn btn-info" >Reset <span class="glyphicon glyphicon-refresh"></span></button></div>
                    <div class="col-md-2 col-sm-4 col-xs-4 "><a href="{{url('product')}}" class="btn btn-danger" >Cancel <span class="glyphicon glyphicon-remove"></span></a></div>
                </div>





            </Form>

            <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}



            <script>
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });

                var product_type =  [/* states array*/];
                $("#product_type").select2({
                    data: product_type
                });

                var brand =  [/* states array*/];
                $("#brand_name").select2({
                    data: brand
                });

                var unit =  [/* states array*/];
                $("#unit").select2({
                    data: unit
                });

                function copyTextValue() {
                    var text1 = document.getElementById("igst").value;
                    document.getElementById("cgst").value = text1/2;
                    document.getElementById("sgst").value = text1/2;
                }
                function basicPrice() {
                    var CostPrice = document.getElementById("cost_price").value;
                    var tax = document.getElementById("igst").value;
                    var basicPrice=Number(CostPrice)-(Number(tax)*100/(100+Number(tax)));
//        var basicPrice=Number(CostPrice)-100;
                    document.getElementById("basic_price").value = Math.round(basicPrice * 100) / 100 ;

                }

                function myFunction() {
                    var option_value = document.getElementById("brand_name").value;
                    if (option_value == "create") {
                        $("#new_brand_type").removeAttr('disabled');
                        $("#new_brand_type").attr('required',true);
                    }else{
                        $("#new_brand_type").attr('disabled',true);
                    }
                }

                function myFunction1() {
                    var option_value = document.getElementById("product_type").value;
                    if (option_value == "create") {
                        $("#new_product_type").removeAttr('disabled');
                        $("#new_product_type").attr('required', true);
                    } else {
                        $("#new_product_type").attr('disabled', true);
                    }
                }
            </script>
            <br>
            @endsection

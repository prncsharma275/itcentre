@extends('layouts.adminPanel')
@section('title')
    Product:View
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        input{
            font-size: 13px!important;
        }
        .deletee {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: -29px!important;
            left: -5px;
            font-family: Verdana;
            font-size: 12px;
            width: 17px;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: -29px!important;
            left: -5px!important;
            font-family: Verdana;
            font-size: 12px;
            width: 17px;
        }
        .panel-default > .panel-heading{
            background-color: #14c1d7!important;
            color: #ffffff;
        }
        .panel-info > .panel-footer{
            background-color: #14c1d7!important;
            color: #ffffff!important;
        }
        .panel-body{
            background-color: #fff!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }
        /*.select1-container{*/
        /*height: 34px!important;*/

        /*}*/
        .table .select1-container--default .select1-selection--single {
            margin-top: -24px!important;
            margin-left: -10px!important;
            margin-right: -7px!important;
            height: 38px!important;
        }
        .select1-selection__arrow{
            top: 6px!important;
        }
        .select1-container--default .select1-selection--single .select1-selection__rendered{
            line-height: 18px!important;
        }

        .select1.select1-container.select1-container--default{
            height: 38px!important;
        }
        .ui-datepicker-week-end a {
            color: #EB3E28!important;
        }
        .ui-datepicker-header{
            background-color: #0E8F9F!important;
            color: #ffffff;!important;
        }
        th{
            /*background-color: #eb3e28!important;*/
            background-color:#1F648B!important;
            font-size: 12px!important;
            color: #fff!important;
        }

        select{
            padding: 9px!important;
        }
        .modal-dialog{
            width: 810px!important;
        }
        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
    </style>
@endsection

@section('content')

            <h3 class="text-center">View Product</h3>
<hr>

            {!! Form::open(['method'=>'get', 'class'=>' form-horizontal', 'url' =>['updateProduct',$editProduct->id]]) !!}
            <div class="row">

                <div class="col-md-6">

                    <label>Product Category</label> <br>
                    <select name="product_type_id" class="has-success has-feedback" id="product_type" style="width: 100%">
                        <option selected="selected" value="{{$editProduct->product_type_id}}">{{$editProduct->belongsToProduct_type->product_name}}</option>

                    </select>


                </div>
                <div class="col-md-6">
                    <div class=" has-success has-feedback">
                        <label>Product Brand</label> <br>
                        <select name="product_brand_id" class="has-success has-feedback" id="brand_name"  style="width: 100%">
                            <option selected="selected" value="{{$editProduct->brand_name_id}}">{{$editProduct->belongsToBrand->brand_name}}</option>

                        </select>
                    </div>


                </div>



            </div>
            <br>
            <!-- row end here -->
            <div class="row">
                <div class="col-md-6">
                    <div class=" has-success has-feedback">
                        <label>Product Name<span style="color: red; font-size: 15px">*</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" value="{{$editProduct->product_name}}" name="product_name"  placeholder="Enter Model Name Here"readonly>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class=" has-success has-feedback">
                        <label>Product HSN Code<span style="color: red; font-size: 15px">*</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" value="{{$editProduct->product_code}}" name="product_hsn_code" style="width: 100%"  placeholder="Enter Product Code Here"readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4 ">
                    <div class=" has-success has-feedback">
                        <label>IGST<span style="color: red; font-size: 12px">* </span><span style="color: green; font-size: 12px"> (Don't use % sign)</span></label>
                        <div class="input-group">
                            <input  type="text" id="igst" value="{{$editProduct->igst}}" class="form-control" onchange="copyTextValue(this);" name="igst"   placeholder="Enter IGST Rate"readonly>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 ">
                    <div class=" has-success has-feedback">
                        <label>CGST<span style="color: red; font-size: 15px"> *</span></label>
                        <div class="input-group">
                            <input  type="text" id="cgst" value="{{$editProduct->cgst}}" class="form-control" name="cgst"  placeholder="Enter CGST Rate"readonly>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 ">
                    <div class=" has-success has-feedback">
                        <label>SGST<span style="color: red; font-size: 15px"> *</span></label>
                        <div class="input-group">
                            <input  type="text" id="sgst"  value="{{$editProduct->sgst}}" class="form-control" name="sgst"  placeholder="Enter SGST Rate"readonly>
                        </div>
                    </div>
                </div>


            </div> <!-- row end here -->



            <div class="row">



                <div class="col-md-3 ">
                    <div class=" has-success has-feedback">
                        <label style="font-size: 13px">Selling Price<span style="color: green; font-size: 12px"></span></label>
                        <div class="input-group">
                            <input  type="text" id="selling_price" value="{{$editProduct->selling_price}}" class="form-control" name="selling_price"  placeholder="Selling Price"readonly>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 ">
                    <div class=" has-success has-feedback">
                        <label>M.R.P<span style="color: red; font-size: 15px"></span></label>
                        <div class="input-group">
                            <input  type="text" id="mrp" class="form-control" value="{{$editProduct->mrp}}" name="mrp" placeholder="Maximum Retail Price"readonly>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 ">
                    <div class=" has-success has-feedback">
                        <label>Opening Stock<span style="color: red; font-size: 15px"></span></label>
                        <div class="input-group">
                            <input  type="text" id="opening_stock" value="{{$editProduct->opening_stock}}" class="form-control" name="opening_stock" placeholder="Opening Stock"readonly>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 ">
                    <div class=" has-success has-feedback">
                        <label>Unit Type<span style="color: red; font-size: 15px"> *</span></label>
                        <div class="input-group">
                            <select class="form-control" name="unit-type">
                                <option value="{{$editProduct->unit_type}}"selected="selected">{{$editProduct->unit_type}}</option>

                            </select>
                        </div>
                    </div>
                </div>
            </div> <!-- row end here -->
            <br>
            <div class="row">
                <div class="col-md-4"><a href="{{url('product')}}" class="btn btn-danger" >Back <span class="glyphicon glyphicon-backward"></span></a></div>
            </div>





            </Form>

            <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}


            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <script>
                var product_type =  [/* states array*/];
                $("#product_type").select2({
                    data: product_type
                });

                var brand =  [/* states array*/];
                $("#brand_name").select2({
                    data: brand
                });
                function copyTextValue() {
                    var text1 = document.getElementById("igst").value;
                    document.getElementById("cgst").value = text1/2;
                    document.getElementById("sgst").value = text1/2;
                }
                function basicPrice() {
                    var CostPrice = document.getElementById("cost_price").value;
                    var tax = document.getElementById("igst").value;
                    var basicPrice=Number(CostPrice)-(Number(tax)*100/(100+Number(tax)));
//        var basicPrice=Number(CostPrice)-100;
                    document.getElementById("basic_price").value = Math.round(basicPrice * 100) / 100 ;

                }



            </script>
            <br>
@endsection


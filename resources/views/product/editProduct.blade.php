@extends('layouts.adminPanel')
@section('title')
    Product:Edit
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('product')}}">Product Panel</a>
    </li>
    <li>
        <a href="{{url('createProduct')}}">New Product</a>
    </li>
    <li>
        <a href="{{url('#')}}">Edit Product</a>
    </li>

@endsection
@section('content')

            <h3 class="heading">Edit Product</h3>
<hr>

            {!! Form::open(['method'=>'get', 'class'=>'form-horizontal', 'url' =>['updateProduct',$editProduct->id]]) !!}
                <div class="row">

                    <div class="col-md-6">

                        <label>Product Category</label>
                        <select name="product_type_id" class="has-success has-feedback" id="product_type" style="width: 100%">
                            <option  value="{{$editProduct->product_type_id}}">{{$editProduct->belongsToProduct_type->product_name}}</option>
                            <?php  $product_type=\App\Product_type::All();?>
                            @foreach($product_type as $product_type)
                                <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>

                            @endforeach
                        </select>


                    </div>
                    <div class="col-md-6">
                        <div class="has-success has-feedback">
                            <label>Product Brand</label>
                            <select name="product_brand_id" class="has-success has-feedback" id="brand_name"  style="width: 100%">
                                <option  value="{{$editProduct->brand_name_id}}">{{$editProduct->belongsToBrand->brand_name}}</option>
                                <?php  $product_type=\App\Brand::All();?>
                                @foreach($product_type as $product_type)
                                    <option value="{{$product_type->id}}">{{$product_type->brand_name}}</option>

                                @endforeach
                            </select>
                        </div>


                    </div>



                </div>
                <!-- row end here -->
            <br>
                <div class="row">
                    <div class="col-md-4">
                        <div class="has-success has-feedback">
                            <label>Product Name<span style="color: red; font-size: 15px">*</span></label>
                                <input  type="text" class="form-control" value="{{$editProduct->product_name}}" name="product_name"  placeholder="Enter Model Name Here">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class=" has-success has-feedback">
                            <label>Product HSN Code<span style="color: red; font-size: 15px">*</span></label>
                                <input  type="text" class="form-control" value="{{$editProduct->product_code}}" name="product_hsn_code" style="width: 100%"  placeholder="Enter Product Code Here">
                        </div>
                    </div>

                    <div class="col-md-4 ">
                        <div class=" has-success has-feedback">
                            <label>Unit Type<span style="color: red; font-size: 15px"> *</span></label>
                                <select class="form-control" name="unit_type">
                                    <option value="{{$editProduct->unit_type}}"selected="selected">{{$editProduct->unit_type}}</option>
                                    <?php $units = \App\Unit::all() ?>
                                    @foreach($units as $unit)
                                        <option value="{{$unit->unit_short_name}}">{{$unit->unit_name}} - ({{$unit->unit_short_name}})</option>
                                    @endforeach

                                </select>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class=" has-success has-feedback">
                        <label>Product Description</label>
                        <textarea class="form-control" name="product_desc">{{$editProduct->product_desc}}</textarea>
                    </div>
                </div>
            </div>

                <div class="row">

                    <div class="col-md-4 ">
                        <div class=" has-success has-feedback">
                            <label>IGST<span style="color: red; font-size: 12px">* </span><span style="color: green; font-size: 12px"> (Don't use % sign)</span></label>
                                <input  type="number" id="igst" value="{{$editProduct->igst}}" class="form-control" onchange="copyTextValue(this);" name="igst"   placeholder="Enter IGST Rate"required>
                        </div>
                    </div>

                    <div class="col-md-4 ">
                        <div class=" has-success has-feedback">
                            <label>CGST<span style="color: red; font-size: 15px"> *</span></label>
                                <input  type="text" id="cgst" value="{{$editProduct->cgst}}" class="form-control" name="cgst"  placeholder="Enter CGST Rate"readonly>
                        </div>
                    </div>

                    <div class="col-md-4 ">
                        <div class=" has-success has-feedback">
                            <label>SGST<span style="color: red; font-size: 15px"> *</span></label>
                                <input  type="text" id="sgst"  value="{{$editProduct->sgst}}" class="form-control" name="sgst"  placeholder="Enter SGST Rate"readonly>
                        </div>
                    </div>


                </div> <!-- row end here -->



                <div class="row">



                    <div class="col-md-3 ">
                        <div class=" has-success has-feedback">
                            <label style="font-size: 13px">Selling Price<span style="color: green; font-size: 12px"></span></label>
                                <input  type="number" step="0.01" id="selling_price" value="{{$editProduct->selling_price}}" class="form-control" name="selling_price"  placeholder="Selling Price">
                        </div>
                    </div>

                    <div class="col-md-3 ">
                        <div class=" has-success has-feedback">
                            <label>M.R.P<span style="color: red; font-size: 15px"></span></label>
                                <input  type="number" step="0.01" id="mrp" class="form-control" value="{{$editProduct->mrp}}" name="mrp" placeholder="Maximum Retail Price">
                        </div>
                    </div>

                    <div class="col-md-3 ">
                        <div class=" has-success has-feedback">
                            <label>Opening Stock<span style="color: red; font-size: 15px"></span></label>
                                <input  type="number"  id="opening_stock" value="{{$editProduct->opening_stock}}" class="form-control" name="opening_stock" placeholder="Opening Stock">
                        </div>
                    </div>

                    <div class="col-md-3 ">
                        <?php  $qnty_sale = \App\Sale_invoice::where('product_id',$editProduct->id)->where('ischallansale','=','')->whereNull('status')->sum('quantity') ?>
                        <?php  $qnty_sale_return = \App\Sale_return_invoice::where('product_id',$editProduct->id)->sum('quantity') ?>
                        <?php  $qnty_purchase = \App\Purchase_invoice::where('product_id',$editProduct->id)->whereNull('status')->sum('quantity') ?>
                        <?php  $qnty_purchase_return = \App\Purchase_return_invoice::where('product_id',$editProduct->id)->sum('quantity') ?>
                        <?php  $qnty_challan = \App\Challan_child::where('product_id',$editProduct->id)->sum('quantity'); ?>
                        <?php  $qnty_challan_return = \App\chalan_return_child::where('product_id',$editProduct->id)->sum('quantity'); ?>
                        <?php $opening_stock = (float)$editProduct->opening_stock ?>
                        <?php $total_qun = ($opening_stock+$qnty_purchase+$qnty_sale_return+$qnty_challan_return)-($qnty_sale+$qnty_purchase_return+$qnty_challan) ?>

                        
                        <div class=" has-success has-feedback">
                            <label>Current Stock<span style="color: red; font-size: 15px"></span></label>
                            <input  type="text" id="opening_stock" value="{{$total_qun}}" class="form-control" name="current_stock" placeholder="Opening Stock" readonly>
                        </div>
                    </div>

                </div> <!-- row end here -->
                <br>
                <div class="row">
                    <div class="col-md-2 col-md-offset-4"><button type="submit" class="btn btn-success" >Save <span class="glyphicon glyphicon-hdd"></span></button></div>
                    <div class="col-md-2"><a href="{{url('product')}}" class="btn btn-danger" >Cancel <span class="glyphicon glyphicon-remove"></span></a></div>
                </div>





            </Form>

            <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}


            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <script>
                var product_type =  [/* states array*/];
                $("#product_type").select2({
                    data: product_type
                });

                var brand =  [/* states array*/];
                $("#brand_name").select2({
                    data: brand
                });
                function copyTextValue() {
                    var text1 = document.getElementById("igst").value;
                    document.getElementById("cgst").value = text1/2;
                    document.getElementById("sgst").value = text1/2;
                }
                function basicPrice() {
                    var CostPrice = document.getElementById("cost_price").value;
                    var tax = document.getElementById("igst").value;
                    var basicPrice=Number(CostPrice)-(Number(tax)*100/(100+Number(tax)));
//        var basicPrice=Number(CostPrice)-100;
                    document.getElementById("basic_price").value = Math.round(basicPrice * 100) / 100 ;

                }



            </script>

            <br>
@endsection

@extends(Auth::user()->role=="admin" ? 'masterTemplet' : (Auth::user()->role=="engineer" ? 'masterTempletEngineer' : 'CustomerTemplet'))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome {{Auth::user()->name}} ({{Auth::user()->role}})</div>
               <hr>
                <div class="panel-body">
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.adminPanelTable')
@section('title')
    Gst Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:0;  /* this affects the margin in the printer settings */
            margin-bottom:5px;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">GST Book</a>
    </li>
@endsection
@section('content')
        <div style="background-color: #ffffff;padding:10px">
            <?php
            $orderdate = explode('-', $from_date);
            $year_from = $orderdate[0];
            $month_from   = $orderdate[1];
            $day_from  = $orderdate[2];

            $orderdate1 = explode('-', $upto_date);
            $year_upto = $orderdate1[0];
            $month_upto = $orderdate1[1];
            $day_upto = $orderdate1[2];
            ?>
            <h3 class="heading">GST Report<br>
                {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}
            </h3>


            {{--purchase and sales--}}
            <div class="row">


  {{---------------------------------------------------------- PURCHASE DETAILS ---------------------------------------------------}}
                <div class="col-md-6 col-xs-6">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th>Purchase Details</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td style="width: 75%">IGST Purchase (Without Tax)</td>
                            <?php  $purchase_igst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->whereNull('status')->sum('total_taxable_amount'); ?>
                            <td>{{sprintf('%0.2f', $purchase_igst)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">IGST Tax </td>
                            <?php  $purchase_igst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $igst_pur_value=0; ?>
                            @foreach($purchase_igst as $igst_pur)
                                <?php $igst_pur_value=$igst_pur_value+$igst_pur->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_pur_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">CGST & SGST Purchase (Without Tax)</td>
                            <?php  $purchase_cgst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->whereNull('status')->select('grand_total','total_taxable_amount')->get(); ?>
                            <?php $cgst_pur_value=0; ?>
                            @foreach($purchase_cgst as $cgst_pur)
                                <?php $cgst_pur_value=$cgst_pur_value+$cgst_pur->total_taxable_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_pur_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">CGST & SGST Tax</td>
                            <?php  $purchase_cgst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $cgst_pur_value=0; ?>
                            @foreach($purchase_cgst as $cgst_pur)
                                <?php $cgst_pur_value=$cgst_pur_value+$cgst_pur->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_pur_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Courier/Other Charges</td>
                            <?php  $purchase_cgst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->select('grand_total','postage_charge')->get(); ?>
                            <?php $cgst_pur_value=0; ?>
                            @foreach($purchase_cgst as $cgst_pur)
                                <?php $cgst_pur_value=$cgst_pur_value+$cgst_pur->postage_charge ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_pur_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">Total Purchase</td>
                            <?php  $purchase_cgst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->select('gross_total','total_tax_amount')->get(); ?>
                            <?php $cgst_pur_value=0; ?>
                            @foreach($purchase_cgst as $cgst_pur)
                                <?php $cgst_pur_value=$cgst_pur_value+$cgst_pur->gross_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_pur_value)}}</td>
                        </tr>
                    </table>
                </div>


 {{--------------------------------------------------------------- SALE DETAILS -------------------------------------------}}
                <div class="col-md-6 col-xs-6 custome-margin">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th>Sale Details</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td style="width: 75%">IGST Sale (Without Tax)</td>
                            <?php  $sale_igst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->whereNull('status')->select('grand_total','total_taxable_value')->get(); ?>
                            <?php $igst_sale_value=0; ?>
                            @foreach($sale_igst as $igst_sale)
                                <?php $igst_sale_value=$igst_sale_value+$igst_sale->total_taxable_value ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_sale_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">IGST Tax</td>
                            <?php  $sale_igst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $igst_sale_value=0; ?>
                            @foreach($sale_igst as $igst_sale)
                                <?php $igst_sale_value=$igst_sale_value+$igst_sale->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_sale_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">CGST & SGST Sale ( Without Tax )</td>
                            <?php  $sale_cgst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->whereNull('status')->select('grand_total','total_taxable_value')->get(); ?>
                            <?php $cgst_sale_value=0; ?>

                            @foreach($sale_cgst as $cgst_sale)
                                <?php $cgst_sale_value=$cgst_sale_value+$cgst_sale->total_taxable_value ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_sale_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">CGST & SGST Tax </td>
                            <?php  $sale_cgst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $cgst_sale_value=0; ?>

                            @foreach($sale_cgst as $cgst_sale)
                                <?php $cgst_sale_value=$cgst_sale_value+$cgst_sale->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_sale_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">Courier/Other Charges</td>
                            <?php  $sale_cgst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->select('grand_total','postage_charge')->get(); ?>
                            <?php $cgst_sale_value=0; ?>

                            @foreach($sale_cgst as $cgst_sale)
                                <?php $cgst_sale_value=$cgst_sale_value+$cgst_sale->postage_charge ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_sale_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Total Sale</td>
                            <?php  $sale_cgst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->select('gross_total','total_tax_amount')->get(); ?>
                            <?php $cgst_sale_value=0; ?>

                            @foreach($sale_cgst as $cgst_sale)
                                <?php $cgst_sale_value=$cgst_sale_value+$cgst_sale->gross_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_sale_value)}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <br>


 {{--------------------------------------------------Purchase RETURN DETAILS--------------------------}}
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th>Purchase Return Details</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td style="width: 75%">IGST Purchase Return</td>
                            <?php  $purchase_return_igst= \App\Purchase_return::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->select('grand_total')->get(); ?>
                            <?php $igst_pur_return_value=0; ?>
                            @foreach($purchase_return_igst as $igst_pur_return)
                                <?php $igst_pur_return_value=$igst_pur_return_value+$igst_pur_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_pur_return_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">CGST&SGST Purchase Return</td>
                            <?php  $purchase_cgst_return= \App\Purchase_return::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->select('grand_total')->get(); ?>
                            <?php $cgst_pur_return_value=0; ?>
                            @foreach($purchase_cgst_return as $cgst_pur_return)
                                <?php $cgst_pur_return_value=$cgst_pur_return_value+$cgst_pur_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_pur_return_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Total Purchase Return</td>
                            <?php $total_purchase_return=$igst_pur_return_value+$cgst_pur_return_value ?>
                            <td>{{sprintf('%0.2f', $total_purchase_return)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Purchase Return IGST Tax</td>
                            <?php  $purchase_return_input_igst= \App\Purchase_return_input_tax::whereBetween('billing_date', array($from_date, $upto_date))->where('type','IGST')->select('input_tax')->get(); ?>
                            <?php $igst_tax_pur_return_value=0; ?>
                            @foreach($purchase_return_input_igst as $igst_tax_pur_return)
                                <?php $igst_tax_pur_return_value=$igst_tax_pur_return_value+$igst_tax_pur_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_tax_pur_return_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Purchase Return CGST&SGST Tax</td>
                            <?php  $purchase_return_input_cgst= \App\Purchase_return_input_tax::whereBetween('billing_date', array($from_date, $upto_date))->where('type','CGST&SGST')->select('input_tax')->get(); ?>
                            <?php $cgst_tax_pur_return_value=0; ?>
                            @foreach($purchase_return_input_cgst as $cgst_tax_pur_return)
                                <?php $cgst_tax_pur_return_value=$cgst_tax_pur_return_value+$cgst_tax_pur_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_tax_pur_return_value)}}</td>
                        </tr>
                    </table>
                </div>


{{------------------------------------------------------- SALE RETURN DETAILS --------------------------------------}}

                <div class="col-md-6 col-xs-6 custome-margin">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th>Sale Return Details</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td style="width: 75%">IGST Sale Return</td>
                            <?php  $sale_return_igst= \App\Sale_return::whereBetween('sale_return_date', array($from_date, $upto_date))->where('tax_type','IGST')->select('grand_total')->get(); ?>
                            <?php $igst_sale_return_value=0; ?>
                            @foreach($sale_return_igst as $igst_sale_return)
                                <?php $igst_sale_return_value=$igst_sale_return_value+$igst_sale_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_sale_return_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">CGST&SGST Sale Return</td>
                            <?php  $sale_cgst_return= \App\Sale_return::whereBetween('sale_return_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->select('grand_total')->get(); ?>
                            <?php $cgst_sale_return_value=0; ?>
                            @foreach($sale_cgst_return as $cgst_sale_return)
                                <?php $cgst_sale_return_value=$cgst_sale_return_value+$cgst_sale_return->grand_total ?>

                            @endforeach
                            <td style="width: 75%">{{sprintf('%0.2f', $cgst_sale_return_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Total Sale Return</td>
                            <?php $total_sale_return=$igst_sale_return_value+$cgst_sale_return_value ?>
                            <td>{{sprintf('%0.2f', $total_sale_return)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Sale Return IGST Tax</td>
                            <?php  $sale_return_output_igst= \App\Sale_return_output_tax::whereBetween('billing_date', array($from_date, $upto_date))->where('type','IGST')->select('output_tax')->get(); ?>
                            <?php $igst_tax_sale_return_value=0; ?>
                            @foreach($sale_return_output_igst as $igst_tax_sale_return)
                                <?php $igst_tax_sale_return_value=$igst_tax_sale_return_value+$igst_tax_sale_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_tax_sale_return_value)}}</td>
                        </tr>
                        <tr>
                            <td style="width: 75%">Sale Return CGST&SGST Tax</td>
                            <?php  $sale_return_output_cgst= \App\Sale_return_output_tax::whereBetween('billing_date', array($from_date, $upto_date))->where('type','CGST&SGST')->select('output_tax')->get(); ?>
                            <?php $cgst_tax_sale_return_value=0; ?>
                            @foreach($sale_return_output_cgst as $cgst_tax_sale_return)
                                <?php $cgst_tax_sale_return_value=$cgst_tax_sale_return_value+$cgst_tax_sale_return->grand_total ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_tax_sale_return_value)}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <br>


 {{---------------------------------------------- INPUT TAX DETAILS      -------------------------}}
            {{-- taxes --}}
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th>Input Tax Details (without- P.Return Tax)</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td style="width: 75%">IGST Input Tax </td>
                            <?php  $purchase_igst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $igst_pur_value=0; ?>
                            @foreach($purchase_igst as $igst_pur)
                                <?php $igst_pur_value=$igst_pur_value+$igst_pur->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_pur_value)}}</td>
                        </tr>

                        <tr>
                            <td style="width: 75%">CGST Input Tax</td>
                            <?php  $purchase_cgst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $cgst_pur_value=0; ?>
                            @foreach($purchase_cgst as $cgst_pur)
                                <?php $cgst_pur_value=$cgst_pur_value+$cgst_pur->total_tax_amount ?>
                                <?php $newValue = ($cgst_pur_value/2)  ?>
                            @endforeach
                            @if(!empty($newValue))
                                <td>{{sprintf('%0.2f', $newValue)}}</td>
                            @else
                                <td>0.00</td>
                            @endif
                        </tr>
                        <tr>
                            <td style="width: 75%">SGST Input Tax</td>
                            @if(!empty($newValue))
                                <td>{{sprintf('%0.2f', $newValue)}}</td>
                            @else
                                <td>0.00</td>
                            @endif
                        </tr>


                        <tr>
                            <td style="width: 75%">Total Input Tax</td>
                            <?php  $purchase_cgst= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->select('gross_total','total_tax_amount')->get(); ?>
                            <?php $cgst_pur_value=0; ?>
                            @foreach($purchase_cgst as $cgst_pur)
                                <?php $cgst_pur_value=$cgst_pur_value+$cgst_pur->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_pur_value)}}</td>
                        </tr>
                    </table>
                </div>

 {{--------------------------------------- OUTPUT TAX DETAILS-------------------------------------- --}}

                <div class="col-md-6 col-xs-6 custome-margin">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th>Output Tax Details (without- S.Return Tax)</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td style="width: 75%">IGST Output Tax</td>
                            <?php  $sale_igst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','IGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $igst_sale_value=0; ?>
                            @foreach($sale_igst as $igst_sale)
                                <?php $igst_sale_value=$igst_sale_value+$igst_sale->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $igst_sale_value)}}</td>
                        </tr>


                        <tr>
                            <td style="width: 75%">CGST Output Tax </td>
                            <?php  $sale_cgst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->where('tax_type','CGST&SGST')->whereNull('status')->select('grand_total','total_tax_amount')->get(); ?>
                            <?php $cgst_sale_value=0; ?>

                            @foreach($sale_cgst as $cgst_sale)
                                <?php $cgst_sale_value=$cgst_sale_value+$cgst_sale->total_tax_amount ?>
                                <?php $newValue2 = ($cgst_sale_value/2)  ?>
                            @endforeach
                            @if(!empty($newValue2))
                                <td>{{sprintf('%0.2f', $newValue2)}}</td>
                            @else
                                <td>0.00</td>
                            @endif
                        </tr>

                        <tr>
                            <td style="width: 75%">SGST Output Tax</td>

                            @if(!empty($newValue2))
                                <td>{{sprintf('%0.2f', $newValue2)}}</td>
                            @else
                                <td>0.00</td>
                            @endif
                        </tr>

                        <tr>
                            <td style="width: 75%">Total Output Tax</td>
                            <?php  $sale_cgst2= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->select('gross_total','total_tax_amount')->get(); ?>
                            <?php $cgst_sale_value2=0; ?>

                            @foreach($sale_cgst2 as $cgst_sale22)
                                <?php $cgst_sale_value2=$cgst_sale_value2+$cgst_sale22->total_tax_amount ?>

                            @endforeach
                            <td>{{sprintf('%0.2f', $cgst_sale_value2)}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <br>
                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success hidden-print"><span class="glyphicon glyphicon-backward"></span> Back</a>
                </div>
            </div>
            <script>
                function myFunction() {
                    window.print();
                }

            </script>

        </div>

        @endsection

<!DOCTYPE HTML>
<html>
<head>
    <title>IT CENTRE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Pooled Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    {{--<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>--}}
    <link href="{{asset('adminDesign/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('adminDesign/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{asset('adminDesign/css/morris.css')}}" type="text/css"/>
    <link href="{{asset('adminDesign/css/font-awesome.css')}}" rel="stylesheet">
    <script src="{{asset('adminDesign/js/jquery-2.1.4.min.js')}}"></script>
    <link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'/>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('adminDesign/css/icon-font.min.css')}}" type='text/css' />
    <script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('css/bootstrap/js/dataTables.bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/dataTables.bootstrap.min.css')}}">
</head>
<style>
    td{
        color: #222222!important;
    }
    th{
        color: #FFFFFF!important;
    }
    .glyphicon{
        color: #FFFFFF!important;
    }
    input[type='search']{
        width: 200px!important;
    }
    @page {
        size:auto;   /* auto is the initial value */
        margin-left: 5px;  /* this affects the margin in the printer settings */
        margin-right: 5px;  /* this affects the margin in the printer settings */
        margin-top:0;  /* this affects the margin in the printer settings */
        margin-bottom:5px;  /* this affects the margin in the printer settings */
    }
    @media print{
        .sidebar-menu{
            display: none;
        }
        .header-main{
            display: none;
        }
        .left-content{
            width: 100%!important;
        }
        .my_th{
            color: #000000!important;
            background-color: #ffffff!important;
        }
        button{
            display: none;
        }
        a{
            display: none!important;
        }
    }
</style>
<body>
<div class="page-container">
    <!--/content-inner-->
    <div class="left-content"> <!-- left corner start here -->
        <div class="mother-grid-inner">
            <!--header start here-->
            <div class="header-main">
                <div class="logo-w3-agile">

                    <h1> <img src="{{ asset('masterTemplet/dist/img/logo.png')}}" width="40" /><a href="www.mustechnologies.com" style="font-size: 15px;color: #000">IT Centre</a></h1>
                </div>
                <div class="profile_details w3l">
                    <ul>
                        <li class="dropdown profile_details_drop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile_img">
                                    <span class="prfil-img"><img src="{{asset('adminDesign/images/usericon.png')}}" alt=""> </span>
                                    <div class="user-name" style="margin-left: 6px;margin-top: 14px">
                                        <p style="color: green;font-size: 12px;margin-top: 10px;margin-left: 1px;">{{ Auth::user()->name }}</p>
                                    </div>
                                    <i class="fa fa-angle-down" style="background-color: #22BEEF;font-size: 1.0em;padding: 5px;border-radius: 10px"></i>
                                    <i class="fa fa-angle-up"style="background-color: #22BEEF;font-size: 1.0em;padding: 5px;border-radius: 10px"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                            <ul class="dropdown-menu drp-mnu">
                                <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
                                <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
                                <?php if( Auth::check()) {?>
                                <li> <a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> Logout</a> </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"> </div>
            </div> <!-- menubar ends here -------------------------------------------------------- -->
            <!--heder end here-->

            {{--inner content here ------------------------------------}}
            <div style="background-color: #ffffff;padding:10px">

                <?php $find_name=\App\Customer::find($customer_id) ?>
                <h3>{{$find_name->ledger_name}}</h3>
                <h4>Supplier Ledger</h4>
                <?php
                $orderdate = explode('-', $from_date);
                $year_from = $orderdate[0];
                $month_from   = $orderdate[1];
                $day_from  = $orderdate[2];

                $orderdate1 = explode('-', $upto_date);
                $year_upto = $orderdate1[0];
                $month_upto = $orderdate1[1];
                $day_upto = $orderdate1[2];
                ?>
                <h5>{{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}} </h5>
                <hr>

                {{--purchase and sales--}}
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-bordered" style="width: 100%!important;">
                            <tr  style="background-color: #1f648b;color: #FFFFFF">
                                <th class="my_th">Description</th>
                                <th class="my_th">Date</th>
                                <th class="my_th">Terms</th>
                                <th class="my_th">Dr Amount</th>
                                <th class="my_th">Cr Amount</th>
                                <th class="my_th">Balance</th>
                            </tr>
                            {{--------------previouse balance code----------}}

                            <?php $new_date= date('Y-m-d',(strtotime ( '-1 day' , strtotime ($from_date) ) ));
                            $time = strtotime('01/01/2016');

                            $newformat = date('Y-m-d',$time);
                            ?>

                            <?php  $old_data= \App\Ledgerdr::whereBetween('billing_date', array($newformat, $new_date))->where('customer_id',$customer_id)->whereNull('status')->select('unique_id','billing_date','customer_id','type','amount','extra')->get(); ?>
                            <?php
                            $old_amount=0;
                            ?>
                            @foreach($old_data as $old)
                                <tr>

                                    <?php  if ($old->extra =="+") {
                                    $old_amount = $old_amount + $old->amount;
                                    ?>
                                     <?php   }else{
                                    $old_amount = $old_amount - $old->amount;
                                    }?>
                            @endforeach

                            {{--------------previouse balance code----------}}





                            <?php  $customer_ledger= \App\Ledgerdr::whereBetween('billing_date', array($from_date, $upto_date))->where('customer_id',$customer_id)->whereNull('status')->select('unique_id','billing_date','customer_id','type','amount','extra')->get(); ?>
                            <?php
                            $total_amount=0;
                            $total_dr=0;
                            $total_cr=0;
                            ?>
                            <tr>
                                <td>Opening Balance b/d</td>
                                <td><?php echo date( 'd/m/y', strtotime($new_date)) ?></td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>{{sprintf('%0.2f', $old_amount)}}</td>
                            </tr>
                            @foreach($customer_ledger as $sale)
                                <tr>
                                    <td>{{$sale->unique_id}}</td>
                                    <td><?php echo date( 'd/m/y', strtotime($sale->billing_date)) ?></td>
                                    <td>{{$sale->type}}</td>

                                    <?php  if ($sale->extra =="+") {
                                    $total_amount = $total_amount + $sale->amount;
                                    $total_dr = $total_dr + $sale->amount;
                                    ?>
                                    <td>{{sprintf('%0.2f', $sale->amount)}}</td>
                                    <td>-</td>
                                    <?php   }else{
                                    $total_amount = $total_amount - $sale->amount;
                                    $total_cr = $total_cr + $sale->amount;
                                    ?>
                                    <td>-</td>
                                    <td>{{sprintf('%0.2f', $sale->amount)}}</td>
                                    <?php   } ?>
                                    <td>
                                        <?php if($old_amount>0){
                                            $final_balance=$total_amount+$old_amount;
                                        }else{
                                            $final_balance=$total_amount-$old_amount;
                                        } ?>
                                        {{sprintf('%0.2f', $final_balance)}}
                                    </td>
                                </tr>
                            @endforeach
                            <tr  style="background-color: #1f648b;color: #FFFFFF">
                                <th colspan="3" class="text-center my_th">Total</th>
                                <th  class="my_th">{{sprintf('%0.2f', $total_dr)}}</th>
                                <th  class="my_th">{{sprintf('%0.2f', $total_cr)}}</th>

                                <?php if($old_amount>0){
                                    $final_balance=$total_amount+$old_amount;
                                }else{
                                    $final_balance=$total_amount-$old_amount;
                                } ?>
                                <th class="my_th">{{sprintf('%0.2f', $final_balance)}}</th>
                            </tr>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                        &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}

            <div class="inner-block">

            </div>

        </div>
    </div>  <!-- left content templet ends here --------------------------------------- -->
    <!--/sidebar-menu-->
    <div class="sidebar-menu">
        <header class="logo1">
            <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
        </header>
        <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
        <div class="menu">
            <ul id="menu" >
                <li><a href="{{url('home')}}"><i class="fa fa-tachometer"></i> <span>Dashboard</span><div class="clearfix"></div></a></li>


                <li id="menu-academico" ><a href="#"><i class="fa fa-file-text-o"></i>  <span>Transaction</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="{{url('sale')}}"><i class="fa fa-mail-forward (alias)"></i> &nbsp;  Sale</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('purchase')}}"><i class="fa fa-mail-reply (alias)"></i> &nbsp; Purchase</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('saleReturn')}}"><i class="fa fa-arrow-left"></i> &nbsp;  Sale Return</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('purchaseReturn')}}"><i class="fa fa-arrow-right"></i> &nbsp;  Purchase Return&nbsp;</a></li>

                    </ul>
                </li>
                <li id="menu-academico" ><a href="#"><i class="fa fa-gift"></i>  <span>Inventory</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="{{url('product')}}"><i class="fa fa-film"></i> &nbsp;&nbsp;  Products</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('product_type')}}"><i class="fa  fa-edit (alias)"></i> &nbsp;&nbsp; Category</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('brand')}}"><i class="fa fa-flag"></i> &nbsp;&nbsp;  Brand</a></li>
                    </ul>
                </li>

                <li id="menu-academico" ><a href="{{url('ledgerPanel')}}"><i class="fa fa-group (alias)"></i>  <span>Ledger</span><div class="clearfix"></div></a>

                </li>

                <li id="menu-academico" ><a href="#"><i class="fa fa-rupee (alias)"></i>  <span>Accounts</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="{{url('payment')}}"><i class="fa   fa-minus"></i> &nbsp;&nbsp;  Payments</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('receipt')}}"><i class="fa  fa-plus"></i> &nbsp;&nbsp; Receipts</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('otherCharge')}}"><i class="fa fa-random"></i> &nbsp; Contra Entry.&nbsp;</a></li>
                    </ul>
                </li>

                <li id="menu-academico" ><a href="#"><i class="fa fa-bar-chart"></i>  <span>Allowence</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="{{url('creditnote')}}"><i class="fa  fa-minus-square"></i> &nbsp;&nbsp;  Credit Notes</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('debitnote')}}"><i class="fa  fa-plus-square"></i> &nbsp;&nbsp; Debit Notes</a></li>
                    </ul>
                </li>

                <li><a href="{{url('report')}}"><i class="fa fa-files-o" aria-hidden="true"></i><span>Reports</span><div class="clearfix"></div></a></li>

                <li id="menu-academico" ><a href="#"><i class="fa fa-cog"></i>  <span>Settings</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="{{url('users')}}"><i class="fa  fa-group (alias)"></i> &nbsp;&nbsp;  Employees</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="{{url('#')}}"><i class="fa  fa-camera"></i> &nbsp;&nbsp; Profile</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    var toggle = true;

    $(".sidebar-icon").click(function() {
        if (toggle)
        {
            $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
            $("#menu span").css({"position":"absolute"});
        }
        else
        {
            $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
            setTimeout(function() {
                $("#menu span").css({"position":"relative"});
            }, 400);
        }

        toggle = !toggle;
    });
</script>
<!--js -->
<script src="{{asset('adminDesign/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('adminDesign/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('adminDesign/js/bootstrap.min.js')}}"></script>
<!-- /Bootstrap Core JavaScript -->
<!-- morris JavaScript -->
<script src="{{asset('adminDesign/js/raphael-min.js')}}"></script>
<script src="{{asset('adminDesign/js/morris.js')}}"></script>
<script>
    $(document).ready(function() {
        //BOX BUTTON SHOW AND CLOSE
        jQuery('.small-graph-box').hover(function() {
            jQuery(this).find('.box-button').fadeIn('fast');
        }, function() {
            jQuery(this).find('.box-button').fadeOut('fast');
        });
        jQuery('.small-graph-box .box-close').click(function() {
            jQuery(this).closest('.small-graph-box').fadeOut(200);
            return false;
        });

        //CHARTS



    });
</script>
</body>
</html>
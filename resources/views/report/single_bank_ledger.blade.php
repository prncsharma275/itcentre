@extends('layouts.adminPanelTable')
@section('title')
    Bank  Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">All Bank Ledger</a>
    </li>
@endsection
@section('content')
    <div style="background-color: #ffffff;padding:10px">
        <?php $find_name=\App\Customer::find($customer_id) ?>
        <?php
        $orderdate = explode('-', $from_date);
        $year_from = $orderdate[0];
        $month_from   = $orderdate[1];
        $day_from  = $orderdate[2];

        $orderdate1 = explode('-', $upto_date);
        $year_upto = $orderdate1[0];
        $month_upto = $orderdate1[1];
        $day_upto = $orderdate1[2];
        ?>
            <h3 class="heading" style="text-transform: capitalize">{{$find_name->ledger_name}}<br>

            {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}</h3>


        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
        &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
        {{--purchase and sales--}}




        <div class="row myrow">
            <div class="col-xs-12">
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                        <th class="my_th">Description</th>
                        <th class="my_th">Date</th>
                        <th class="my_th">Terms</th>
                        <th class="my_th">Debit (Inward)</th>
                        <th class="my_th">Credit (Outward)</th>
                        <th class="my_th">Balance</th>
                    </tr>
                    {{--------------previouse balance code----------}}



                    <?php $new_date= date('Y-m-d',(strtotime ( '-1 day' , strtotime ($from_date) ) ));
                    $time = strtotime('01/01/2016');

                    $newformat = date('Y-m-d',$time);
                    ?>

                    <?php  $old_data= \App\Bank::whereBetween('date', array($newformat, $new_date))->where('bank_id',$customer_id)->whereNull('status')->orderBy('date', 'asc')->select('unique_id','date','customer_id','type','amount','extra','bank_id')->get(); ?>
                    <?php
                    $old_amount=0;
                    ?>



                    @foreach($old_data as $old)
                        <tr>

                        <?php  if ($old->extra =="+") {
                            $old_amount = $old_amount + $old->amount;
                            ?>
                                     <?php   }else{
                            $old_amount = $old_amount - $old->amount;
                        }?>
                    @endforeach

                    {{--getting opening balance of all bank --}}
                    @if($find_name->balance_type=='dr')
                        <?php $final_old_amount = $old_amount+$find_name->opening_balance ?>
                    @else
                        <?php $final_old_amount = $old_amount-$find_name->opening_balance ?>
                    @endif


                    {{--------------previouse balance code----------}}


                    <tr>
                        <td  style="text-align: left;text-transform: uppercase!important;">Opening Balance</td>
                        <td><?php echo date( 'd/m/y', strtotime($from_date)) ?></td>

                        <td>C/F</td>

                        @if($final_old_amount>0)
                            <td>{{sprintf('%0.2f', $final_old_amount)}}</td>
                            <?php $forward_dr_amount = $final_old_amount ?>
                            <?php $forward_cr_amount = 0 ?>
                            <td>-</td>
                            <td><span style="font-size: 11px;font-style: italic">Dr.</span> {{sprintf('%0.2f', $final_old_amount)}} </td>
                        @else
                            <td>-</td>
                            <td>{{sprintf('%0.2f', -($final_old_amount))}}</td>
                            <?php $forward_dr_amount = 0; ?>
                            <?php $forward_cr_amount = -($final_old_amount) ?>
                            <td><span style="font-size: 11px;font-style: italic">Cr.</span> {{sprintf('%0.2f',-($final_old_amount))}} </td>

                        @endif
                    </tr>





                    <?php  $customer_ledger= \App\Bank::whereBetween('date', array($from_date, $upto_date))->where('bank_id',$customer_id)->whereNull('status')->get(); ?>
                    <?php
                    $total_amount=0;
                    $total_dr=$forward_dr_amount;
                    $total_cr=$forward_cr_amount;
                    ?>

                    @foreach($customer_ledger as $sale)
                        <tr>
                            @if($sale->type=='CreditSale')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewSale')}}/{{$sale->sale_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='Receipt')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewReceipt')}}/{{$sale->receipt_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='Payment')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewPayment')}}/{{$sale->payment_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='purchase')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewPurchase')}}/{{$sale->purchase_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='DebitNote')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewDebitnote')}}/{{$sale->debit_not_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='CreditNote')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewCreditnote')}}/{{$sale->credit_not_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='sale Return')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewSaleReturn')}}/{{$sale->sale_return_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='purchase Return')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewPurchaseReturn')}}/{{$sale->purchase_return_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @elseif($sale->type=='CashSale')
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewSale')}}/{{$sale->sale_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @else
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewOtherCharge')}}/{{$sale->contra_id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            @endif

                            <td><?php echo date( 'd/m/y', strtotime($sale->date)) ?></td>

                            <td>{{$sale->type}}</td>

                            <?php  if ($sale->extra =="+") {
                            $total_amount = $total_amount + $sale->amount;
                            $total_dr = $total_dr + $sale->amount;
                            ?>
                            <td>{{sprintf('%0.2f', $sale->amount)}}</td>
                            <td>-</td>
                            <?php   }else{
                            $total_amount = $total_amount - $sale->amount;
                            $total_cr = $total_cr + $sale->amount;
                            ?>
                            <td>-</td>
                            <td>{{sprintf('%0.2f', $sale->amount)}}</td>
                            <?php   } ?>
                            <td>
                                @if($total_amount>0)
                                    <span style="font-size: 11px;font-style: italic">Dr.</span> {{sprintf('%0.2f', $total_amount)}}
                                @else
                                    <span style="font-size: 11px;font-style: italic">Cr.</span> {{sprintf('%0.2f', -($total_amount))}}
                                @endif

                            </td>
                        </tr>
                    @endforeach

                    <tr  style="background-color: #ebf2f6;">
                        <th colspan="3" class="text-center my_th">Closing Balance</th>
                        <th  class="my_th">{{sprintf('%0.2f', $total_dr)}}</th>
                        <th  class="my_th">{{sprintf('%0.2f', $total_cr)}}</th>
                        <th class="my_th">
                            @if($total_amount>0)
                                <span style="font-size: 11px;font-style: italic">Dr.</span> {{sprintf('%0.2f', $total_amount)}}
                            @else
                                <span style="font-size: 11px;font-style: italic">Cr.</span> {{sprintf('%0.2f', -($total_amount))}}
                            @endif

                        </th>
                    </tr>
                </table>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <br>
                <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
            </div>
        </div>
        <script>
            function myFunction() {
                window.print();
            }

        </script>

    </div>

    {{--inner content here ------------------------------------}}
@endsection
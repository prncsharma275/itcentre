@extends('layouts.adminPanelTable')
@section('title')
    Custom Day:Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Custom Ledger</a>
    </li>
@endsection
@section('content')
            <div style="background-color: #ffffff;padding:10px">
                <?php
                $orderdate = explode('-', $from_date);
                $year_from = $orderdate[0];
                $month_from   = $orderdate[1];
                $day_from  = $orderdate[2];

                $orderdate1 = explode('-', $upto_date);
                $year_upto = $orderdate1[0];
                $month_upto = $orderdate1[1];
                $day_upto = $orderdate1[2];
                ?>
                <h3 class="heading">{{$msg}}<br>

                    {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}</h3>


                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('day_report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                {{--purchase and sales--}}


                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Description</th>
                                <th class="my_th">Dr.</th>
                                <th class="my_th">Cr.</th>

                            </tr>
                            {{--------------previouse balance code----------}}


                            <?php  $qnty_sale = \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('gross_total') ?>

                           <tr>
                               <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_sale_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Sales</td>
                               <td>{{sprintf('%0.2f', $qnty_sale)}}</td>
                               <td>-</td>
                           </tr>
                            <?php  $qnty_purchase = \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('grand_total') ?>
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_purchase_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Purchase</td>
                                <td>-</td>
                                <td>{{sprintf('%0.2f', $qnty_purchase)}}</td>
                            </tr>


                            <?php  $qnty_sale_return = \App\Sale_return::whereBetween('sale_return_date', array($from_date, $upto_date))->sum('grand_total') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_saleReturn_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Sale Return</td>
                                <td>-</td>
                                <td>{{sprintf('%0.2f', $qnty_sale_return)}}</td>
                            </tr>



                        <?php  $qnty_purchase_return = \App\Purchase_return::whereBetween('billing_date', array($from_date, $upto_date))->sum('grand_total') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_purchaseReturn_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Purchase Return</td>
                                <td>{{sprintf('%0.2f', $qnty_purchase_return)}}</td>
                                <td>-</td>
                            </tr>



                            <?php  $receipt = \App\Receipt::whereBetween('billing_date', array($from_date, $upto_date))->sum('amount') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_receipt_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Receipt</td>
                                <td>-</td>
                                <td>{{sprintf('%0.2f', $receipt)}}</td>
                            </tr>


                            <?php  $payment = \App\Payment::whereBetween('billing_date', array($from_date, $upto_date))->sum('amount') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_payment_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Payment</td>
                                <td>{{sprintf('%0.2f', $payment)}}</td>
                                <td>-</td>
                            </tr>

                            <?php  $creditNote = \App\Creditnote::whereBetween('billing_date', array($from_date, $upto_date))->sum('amount') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_creditNote_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Credit Note</td>
                                <td>-</td>
                                <td>{{sprintf('%0.2f', $creditNote)}}</td>
                            </tr>


                            <?php  $debitNote = \App\Debitnote::whereBetween('billing_date', array($from_date, $upto_date))->sum('amount') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_debitNote_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Debit Note</td>
                                <td>{{sprintf('%0.2f', $debitNote)}}</td>
                                <td>-</td>
                            </tr>

                            <?php  $contra = \App\Other_charge::whereBetween('journal_date', array($from_date, $upto_date))->sum('total_amount') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_contra_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Contra</td>
                                <td>{{sprintf('%0.2f', $contra)}}</td>
                                <td>{{sprintf('%0.2f', $contra)}}</td>
                            </tr>

                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                        &nbsp;<a href="{{url('day_report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}
@endsection
@extends('layouts.adminPanelTable')
@section('title')
    Custom Day:Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Custom Ledger</a>
    </li>
@endsection
@section('content')
            <div style="background-color: #ffffff;padding:10px">

                <h3 class="heading">Monthly Report<br>

                    </h3>


                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('day_report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                {{--purchase and sales--}}
                <?php $month = \App\Month::count(); ?>
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Sales</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}

                            @for($i=1;$i<=$month;$i++)
                            <?php  $qnty_sale = \App\Sale::whereNull('status')->whereRaw('MONTH(billing_date) = ?', [$i])->sum('grand_total') ?>
                            @if($qnty_sale)
                            <tr>
                                <?php $m= \App\Month::find($i) ?>
                                <td  style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_sale_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                <td>{{sprintf('%0.2f', $qnty_sale)}}</td>
                            </tr>
                            @endif
                       @endfor
                        </table>
                    </div>
                </div>

<!-- Purchase Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Purchase</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_purchase = \App\Purchase::whereNull('status')->whereRaw('MONTH(billing_date) = ?', [$i])->sum('grand_total') ?>
                                @if($qnty_purchase)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td  style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_purchase_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_purchase)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>

                <!-- Sale Return  Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Sale Return</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_sale_return = \App\Sale_return::whereRaw('MONTH(sale_return_date) = ?', [$i])->sum('grand_total') ?>
                                @if($qnty_sale_return)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td  style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_saleReturn_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_sale_return)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>



                <!-- Purchase Return  Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Purchase Return</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_Purchase_return = \App\Purchase_return::whereRaw('MONTH(billing_date) = ?', [$i])->sum('grand_total') ?>
                                @if($qnty_Purchase_return)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_purchaseReturn_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_Purchase_return)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>


                <!-- Receipt  Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Receipts</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_receipt = \App\Receipt::whereRaw('MONTH(billing_date) = ?', [$i])->sum('amount') ?>
                                @if($qnty_receipt)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_receipt_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_receipt)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>


                <!-- Payments  Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Payments</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_payment = \App\Payment::whereRaw('MONTH(billing_date) = ?', [$i])->sum('amount') ?>
                                @if($qnty_payment)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_payment_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_payment)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>


                <!-- Debit Note  Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Debit Notes</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_debitNote = \App\Debitnote::whereRaw('MONTH(billing_date) = ?', [$i])->sum('amount') ?>
                                @if($qnty_debitNote)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_debitNote_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_debitNote)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>


                <!-- Credit Note  Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Credit Notes</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_creditNote = \App\Creditnote::whereRaw('MONTH(billing_date) = ?', [$i])->sum('amount') ?>
                                @if($qnty_creditNote)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_creditNote_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_creditNote)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>

                <!-- Contra Entry   Details is here -->
                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Contra</th>
                                <th class="my_th">Value</th>

                            </tr>
                            {{--------------previouse balance code----------}}
                            @for($i=1;$i<=$month;$i++)
                                <?php  $qnty_contra = \App\Other_charge::whereRaw('MONTH(journal_date) = ?', [$i])->sum('total_amount') ?>
                                @if($qnty_contra)
                                    <tr>
                                        <?php $m= \App\Month::find($i) ?>
                                        <td style="width: 50%"><a style="color: green;font-weight: 600" href="{{url('monthly_contra_report')}}/{{$i}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$m->month_in_word}}</td>
                                        <td>{{sprintf('%0.2f', $qnty_contra)}}</td>
                                    </tr>
                                @endif
                            @endfor
                        </table>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                        &nbsp;<a href="{{url('day_report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}
@endsection
@extends('layouts.adminPanel')
@section('title')
    Create Sale
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Bank Ledger</a>
    </li>
@endsection
@section('content')
    <h3 class="heading">Day Report</h3>
    <?php echo Form::open(array('route' => 'showDayReport')); ?>
    <div class="container-fluid ">

        <div class="well">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-group">
                    <div class=" has-success has-feedback">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <label>Report Type<span style="color: red; font-size: 15px">*</span></label>
                        <select class="form-control" name="report_type" id="report_type" required>
                           <option value="1">Daily</option>
                           <option value="2">Monthly</option>
                           <option value="3">Custom</option>
                        </select>

                    </div>
                </div>

            </div> <!-- row end here -->

            <div class="row" id="custom">
                <div class="col-sm-4 col-sm-offset-2 text-right">
                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label>
                    <input type="number"  name="from_day" class="form-control" value="{{date('d')}}" style="width: 60px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                    <input type="number" name="from_month" class="form-control" value="{{date('m')}}" style="width: 60px;display: inline"> <span style="font-size: 25px">-</span>
                    <input type="number" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 70px;display: inline">
                </div>
                <div class="col-sm-4">
                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label>
                    <input type="number" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 60px;display: inline"> <span style="font-size: 25px">-</span>
                    <input type="number" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 60px;display: inline"> <span style="font-size: 25px">-</span>
                    <input type="number" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 70px;display: inline">
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6 col-sm-offset-5">
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning" >Show <span class="glyphicon glyphicon-list"></span></button>
                        <a class="btn btn-success" href="{{url('customers')}}">Back</a>

                    </div>
                </div>

            </div>


        </div>
    </div>
    {{--</Form>--}}
    {{form::close()}}

    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    <br>

    <script>


        $(document).ready(function(){
            $('#custom').hide();
        });
        $('#report_type').bind('change', function(event) {

            var i= $('#report_type').val();
            if(i!=3) //Retail Customer ID
            {
                $('#custom').hide(); // hide the first one

            }
            else{
                $('#custom').show();
                $('#custom').find('input').attr('required','true');
            }

        });


    </script>
@endsection

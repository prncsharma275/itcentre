@extends('layouts.adminPanelTable')
@section('title')
    Sale Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        /*input[type='search']{*/
            /*width: 200px!important;*/
        /*}*/

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{

            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }


        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Sale Book</a>
    </li>
@endsection
@section('content')
        <div style="background-color: #ffffff;padding:10px">
            <?php $m= \App\Month::find($i) ?>
            <h3 class="heading">Sale Day Book Report<br>
                {{$m->month_in_word}}
            </h3>
                <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
            {{--purchase and sales--}}
            <div class="row myrow">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th class="my_th">Invoice No.</th>
                            <th class="my_th">Invoice Date</th>
                            <th class="my_th">Customer Name</th>
                            <th class="my_th">Amt.</th>
                            <th class="my_th">Discount</th>
                            <th class="my_th">Txble. Amt.</th>
                            <th class="my_th">Gst</th>
                            <th class="my_th">Invoice Amount</th>
                        </tr>
                        <?php  $purchase_igst= \App\Sale::whereRaw('MONTH(billing_date) = ?', [$i])->whereNull('status')->select('gross_total','id','total_amount_without_anything','total_discount','total_taxable_value',
                                'total_tax_amount','unique_id','billing_date','customer_id')->get(); ?>
                       <?php
                        $total_amount=0;
                        $total_without_tax=0;
                        $total_discount=0;
                        $total_taxable_amount=0;
                        $total_tax=0;
                        ?>
                        @foreach($purchase_igst as $sale)
                        <tr>
                            <td>{{$sale->unique_id}}&nbsp;<a style="color: green;font-weight: 600" href="{{url('singleViewSale')}}/{{$sale->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a></td>
                            <td><?php echo date( 'd/m/y', strtotime($sale->billing_date)) ?></td>
                            <td>{{$sale->belongsToCustomer->ledger_name}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_amount_without_anything)}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_discount)}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_taxable_value)}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_tax_amount)}}</td>
                            <td>{{sprintf('%0.2f', $sale->gross_total)}}</td>
                            <?php $total_without_tax=$total_without_tax+$sale->total_amount_without_anything ?>
                            <?php $total_discount=$total_discount+$sale->total_discount ?>
                            <?php $total_taxable_amount=$total_taxable_amount+$sale->total_taxable_value ?>
                            <?php $total_tax=$total_tax+$sale->total_tax_amount ?>
                            <?php $total_amount=$total_amount+$sale->gross_total ?>

                        </tr>
                        @endforeach
<tr  style="background-color: #ebf2f6;color: #FFFFFF">
    <th colspan="3" class="text-center my_th">Grand Total</th>
    <th class="my_th">{{sprintf('%0.2f', $total_without_tax)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_discount)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_taxable_amount)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_tax)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_amount)}}</th>
</tr>
                    </table>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <br>
                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                </div>
            </div>
            <script>
                function myFunction() {
                    window.print();
                }

            </script>

        </div>

@endsection
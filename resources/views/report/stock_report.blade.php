@extends('layouts.adminPanelTable')
@section('title')
    Detailed Stock  Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Detailed Stock Report</a>
    </li>
@endsection
@section('content')
    <div style="background-color: #ffffff;padding:10px">
        <?php $item = \App\Product::find($product_id) ?>
        <?php
        $orderdate = explode('-', $from_date);
        $year_from = $orderdate[0];
        $month_from   = $orderdate[1];
        $day_from  = $orderdate[2];
        ?>
        <h3 class="heading">Stock Report - {{$item->product_name}}
        <br>
            {{$day_from}}-{{$month_from}}-{{$year_from}}
        </h3>

                {{--purchase and sales--}}
        <div class="row">
            <div class="col-sm-12">
                <h4>Purchase</h4>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Supplier</th>
                        <th>Quantity</th>
                        <th>Value</th>
                    </tr>
                    <?php $purchase = \App\Purchase_invoice::where('product_id',$product_id)->select('purchase_id','quantity','total_amount','rate')->get(); ?>
                    <?php  $balance_input=0?>
                    <?php  $purchase_value=0?>
                    @foreach($purchase as $item1)
                        <tr>
                            <?php $purchase_invoice = \App\Purchase::where('id',$item1->purchase_id)->select('supplier_id', 'id','party_invoice_date','party_invoice','status')->first(); ?>
                            @if($purchase_invoice->status==null)
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewPurchase')}}/{{$purchase_invoice->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$purchase_invoice->party_invoice}}</td>
                                <td>{{$purchase_invoice->party_invoice_date}}</td>
                                <td>{{$purchase_invoice->belongsToCustomer->ledger_name}}</td>
                                <td>{{$item1->quantity}}</td>
                                <td>{{$item1->total_amount}}</td>
                                <?php $balance_input+=$item1->quantity ?>
                                <?php $purchase_value+=$item1->total_amount ?>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" class="text-center">Total</th>
                        <th>{{$balance_input}}</th>
                        <th>{{$purchase_value}}</th>
                    </tr>
                </table>
            </div>

            <div class="col-sm-12">
                <h4>Sale</h4>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Quantity</th>
                        <th>value</th>
                    </tr>
                    <?php $sale = \App\Sale_invoice::where('product_id',$product_id)->select('sale_id','quantity','total_amount')->get(); ?>
                    <?php  $balance_output=0?>
                    <?php  $sale_value=0?>
                    @foreach($sale as $item)
                        <tr>
                            <?php $sale_invoice = \App\Sale::where('id',$item->sale_id)->select('unique_id','billing_date','id', 'customer_id','status')->first(); ?>
                            @if($sale_invoice->status==null)
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewSale')}}/{{$sale_invoice->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale_invoice->unique_id}}</td>
                                <td>{{$sale_invoice->billing_date}}</td>
                                <td>{{$sale_invoice->belongsToCustomer->ledger_name}}</td>
                                <td>{{$item->quantity}}</td>
                                <td>{{$item->total_amount}}</td>
                                <?php $balance_output+=$item->quantity ?>
                                <?php $sale_value+=$item->total_amount ?>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" class="text-center">Total</th>
                        <th>{{$balance_output}}</th>
                        <th>{{$sale_value}}</th>
                    </tr>
                </table>
            </div>
            {{---------------------------------- stock input------------------}}

            <div class="col-sm-12">
                <h4>Challan</h4>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Quantity</th>
                    </tr>
                    <?php $sale = \App\Challan_child::where('product_id',$product_id)->where('status','!=','cancel')->select('challan_id','quantity')->get(); ?>
                    <?php  $balance_output1=0?>
                    @foreach($sale as $item)
                        <tr>
                            <?php $sale_invoice = \App\Challan::where('id',$item->challan_id)->select('challan_no','challan_date','id', 'customer_id','status')->first(); ?>
                            @if($sale_invoice->status==null)
                                <td class="text-left">{{$sale_invoice->challan_no}}</td>
                                <td>{{$sale_invoice->challan_date}}</td>
                                <td>{{$sale_invoice->belongsToCustomer->ledger_name}}</td>
                                <td>{{$item->quantity}}</td>
                                <?php $balance_output1+=$item->quantity ?>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" class="text-center">Total</th>
                        <th>{{$balance_output1}}</th>
                    </tr>
                </table>
            </div>

        </div>


        {{------------------------------------ sale return and purchase return report here ---------------------------   --}}
        <div class="row">
            <div class="col-sm-12">
                <h4>Sale Return</h4>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Quantity</th>
                    </tr>
                    <?php $sale_return = \App\Sale_return_invoice::where('product_id',$product_id)->select('sale_return_id','quantity')->get(); ?>
                    <?php  $sale_return_balance=0?>
                    @foreach($sale_return as $item2)
                        <tr>
                            <?php $sale_return_invoice = \App\Sale_return::where('id',$item2->sale_return_id)->select('sale_return_no','id','sale_return_date','customer_id')->first(); ?>

                            <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewSaleReturn')}}/{{$sale_return_invoice->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale_return_invoice->sale_return_no}}</td>
                            <td>{{$sale_return_invoice->sale_return_date}}</td>
                            <td>{{$sale_return_invoice->belongsToCustomer->ledger_name}}</td>
                            <td>{{$item2->quantity}}</td>
                            <?php $sale_return_balance+=$item2->quantity ?>

                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" class="text-center">Total</th>
                        <th>{{$sale_return_balance}}</th>
                    </tr>
                </table>
            </div>
            {{---------------------------------- stock input------------------}}
            <div class="col-sm-12">
                <h4>Purchase Return</h4>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Quantity</th>
                    </tr>
                    <?php $purchase_return = \App\Purchase_return_invoice::where('product_id',$product_id)->select('purchase_return_id','quantity')->get(); ?>
                    <?php  $purchase_return_balance=0?>
                    @foreach($purchase_return as $item3)
                        <tr>
                            <?php $purchase_return_invoice = \App\Purchase_return::where('id',$item3->purchase_return_id)->select('invoice_no','id', 'billing_date','supplier_id')->first(); ?>
                            <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewPurchaseReturn')}}/{{$purchase_return_invoice->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$purchase_return_invoice->invoice_no}}</td>
                            <td>{{$purchase_return_invoice->billing_date}}</td>
                            <td>{{$purchase_return_invoice->belongsToCustomer->ledger_name}}</td>
                            <td>{{$item3->quantity}}</td>
                            <?php $purchase_return_balance+=$item3->quantity ?>

                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" class="text-center">Total</th>
                        <th>{{$purchase_return_balance}}</th>
                    </tr>
                </table>
            </div>
            <div class="col-sm-12">
                <h4>Challan Return</h4>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Quantity</th>
                    </tr>
                    <?php $sale_return = \App\chalan_return_child::where('product_id',$product_id)->select('challan_return_id','quantity')->get(); ?>
                    <?php  $challan_return_balance=0?>
                    @foreach($sale_return as $item2)
                        <tr>
                            <?php $sale_return_invoice = \App\chalan_return::where('id',$item2->challan_return_id)->select('invoice_no','id','challan_date','customer_id')->first(); ?>

                            <td class="text-left">{{$sale_return_invoice->invoice_no}}</td>
                            <td>{{$sale_return_invoice->challan_date}}</td>
                            <td>{{$sale_return_invoice->belongsToCustomer->ledger_name}}</td>
                            <td>{{$item2->quantity}}</td>
                            <?php $challan_return_balance+=$item2->quantity ?>

                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="3" class="text-center">Total</th>
                        <th>{{$challan_return_balance}}</th>
                    </tr>
                </table>
            </div>

        </div>
        <?php $current_stock = \App\Product::find($product_id);?>

        <div class="row">
            <?php $newbalance=0; ?>
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;">

                <th>Description</th>
                <th class="text-center">IN</th>
                <th class="text-center">OUT</th>
                <th>Balance</th>
                <th>Value</th>
                    </tr>
                <tr>
                    <td>Opening Stock</td>
                    <td>{{$current_stock->opening_stock}}</td>
                    <td>--</td>
                    <?php $newbalance+=$current_stock->opening_stock ?>
                    <td>{{$newbalance}}</td>
                    <td>-</td>
                </tr>

                <tr>
                    <td>Total Purchase</td>
                    <td>{{$balance_input}}</td>
                    <td>--</td>
                    <?php $newbalance+=$balance_input ?>
                    <td>{{$newbalance}}</td>
                    <td>{{$purchase_value}}</td>
                </tr>

                <tr>
                    <td>Total Sale Return</td>
                    <td>{{$sale_return_balance}}</td>
                    <td>--</td>
                    <?php $newbalance+=$sale_return_balance ?>
                    <td>{{$newbalance}}</td>
                    <td>-</td>
                </tr>

                <tr>
                    <td>Total Sale</td>
                    <td>--</td>
                    <td>{{$balance_output}}</td>
                    <?php $newbalance-=$balance_output ?>
                    <td>{{$newbalance}}</td>
                    <td>{{$sale_value}}</td>
                </tr>
                    <tr>
                    <td>Total Challan</td>
                    <td>--</td>
                    <td>{{$balance_output1}}</td>
                    <?php $newbalance-=$balance_output1 ?>
                    <td>{{$newbalance}}</td>
                    <td>-</td>
                    </tr>

                <tr>
                    <td>Total Purchase Return</td>
                    <td>--</td>
                    <td>{{$purchase_return_balance}}</td>
                    <?php $newbalance-=$purchase_return_balance ?>
                    <td>{{$newbalance}}</td>
                    <td>-</td>
                </tr>
                    <tr>
                    <td>Total Challan Return</td>
                    <td>{{$challan_return_balance}}</td>
                    <td>0</td>
                    <?php $newbalance+=$challan_return_balance ?>
                    <td>{{$newbalance}}</td>
                    <td>-</td>
                </tr>
                <tr>
                    <th>Total</th>
                    <th><?php echo $balance_input+$sale_return_balance ?></th>
                    <th><?php echo $balance_output+$purchase_return_balance ?></th>
                    <th>{{$newbalance}}</th>
                    @if($newbalance>0 && $purchase_value>0)
                    <?php $total_value = $newbalance*($purchase_value/$balance_input) ?>
                    <th>{{ sprintf('%0.2f', $total_value)}}</th>
                        @else
                        <th>0.00</th>
                    @endif
                </tr>
            </table>
        </div>

        <br>

        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <br>
                <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
            </div>
        </div>
        <script>
            function myFunction() {
                window.print();
            }

        </script>

    </div>

@endsection
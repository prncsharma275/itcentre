@extends('layouts.adminPanelTable')
@section('title')
    Payment
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }
    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('payment')}}">Payment Panel</a>
    </li>
@endsection

@section('content')
        <div style="background-color: #ffffff;padding:10px">
            <?php
            $orderdate = explode('-', $from_date);
            $year_from = $orderdate[0];
            $month_from   = $orderdate[1];
            $day_from  = $orderdate[2];
            ?>
            <h3 class="heading">Stock Report
            <br>{{$day_from}}-{{$month_from}}-{{$year_from}}
            </h3>


            <table class="table table-striped table-bordered dTableR" id="dt_a">
                <thead>
                <tr>

                    <th class="my_th">SL No.</th>
                    <th class="my_th">Product Name.</th>
                    <th class="my_th">Stock As On {{$day_from}}-{{$month_from}}-{{$year_from}}</th>
                    <th class="my_th">Stock Value As On {{$day_from}}-{{$month_from}}-{{$year_from}}</th>
                    <th class="my_th">Current Stock</th>
                </tr>
                </thead>
                <tbody>


{{----------------------------------------------- as on stock -------------------------------}}

                <?php  $purchase_igst= \App\Product::select('product_name','qnty','id','opening_stock')->get(); ?>
                <?php $total_amount=0;
                $slno=1;
                ?>
                @foreach($purchase_igst as $sale)

                    <tr>
                        {{-------------------------------------old purchase stock-------------------------------------------}}

                        <?php  $purchase= \App\Purchase_invoice::whereBetween('track_date', array('2016-01-01', $from_date))->where('product_id',$sale->id)->whereNull('status')->select('quantity','purchase_id')->get(); ?>

                        <?php $purchase_stock=0 ?>
                        @foreach($purchase as $stock_pur)
                            <?php $purchase_parent = \App\Purchase::where('id',$stock_pur->purchase_id)->select('status')->first(); ?>
                                @if($purchase_parent->status==null)
                            <?php $purchase_stock=$purchase_stock+$stock_pur->quantity ?>
                                @endif
                        @endforeach

                {{-------------------------------------current purchase stock-------------------------------------------}}

                        <?php  $highest_rate= \App\Purchase_invoice::where('product_id',$sale->id)->where('product_id',$sale->id)->select('rate','purchase_id')->max('rate'); ?>
                        <?php  $current_purchase= \App\Purchase_invoice::where('product_id',$sale->id)->where('product_id',$sale->id)->select('quantity','purchase_id')->get(); ?>

                        <?php $current_purchase_stock=0 ?>
                        @foreach($current_purchase as $current_pur)

                            <?php $purchase_parent2 = \App\Purchase::where('id',$current_pur->purchase_id)->select('status')->first(); ?>
                                @if($purchase_parent2->status==null)
                            <?php $current_purchase_stock+=$current_pur->quantity ?>
                                @endif
                        @endforeach

  {{-------------------------------------------------------------------------------------------}}




                        {{-------------------------------------old sale stock-------------------------------------------}}

                        <?php  $sale_st= \App\Sale_invoice::whereBetween('track_date', array('2016-01-01', $from_date))->where('product_id',$sale->id)->whereNull('ischallansale')->whereNull('status')->select('quantity','sale_id')->get(); ?>
                        {{--array_sum($a)--}}
                        <?php $sale_stock=0; ?>
                        @foreach($sale_st as $stock_sale)

                            <?php $sale_parent = \App\Sale::where('id',$stock_sale->sale_id)->select('status')->first(); ?>
                                @if($sale_parent->status==null)
                                 <?php $sale_stock=$sale_stock+$stock_sale->quantity ?>
                                @endif

                        @endforeach


                    {{-------------------------------------current  sale stock-------------------------------------------}}

                        <?php  $current_sale= \App\Sale_invoice::where('product_id',$sale->id)->whereNull('ischallansale')->whereNull('status')->select('quantity','sale_id')->get(); ?>
                        {{--array_sum($a)--}}
                        <?php $cuurent_sale_stock=0; ?>
                        @foreach($current_sale as $current_sal)

                            <?php $sale_parent2 = \App\Sale::where('id',$current_sal->sale_id)->select('status')->first(); ?>
                            @if($sale_parent2->status==null)
                                <?php $cuurent_sale_stock+=$current_sal->quantity ?>
                            @endif

                        @endforeach



    {{-------------------------------------------------------------------------------------------}}



                        {{-------------------------------------old purchase return stock-------------------------------------------}}

                        <?php  $purchase_return= \App\Purchase_return_invoice::whereBetween('track_date', array('2016-01-01', $from_date))->where('product_id',$sale->id)->select('quantity')->get(); ?>

                        <?php $purchase_return_stock=0 ?>
                        @foreach($purchase_return as $stock_pur_return)
                            <?php $purchase_return_stock=$purchase_return_stock+$stock_pur_return->quantity ?>
                        @endforeach



                        {{-------------------------------------current  purchase return stock-------------------------------------------}}

                        <?php  $current_purchase_return= \App\Purchase_return_invoice::where('product_id',$sale->id)->select('quantity')->get(); ?>

                        <?php $current_purchase_return_stock=0 ?>
                        @foreach($current_purchase_return as $current_stock_pur_return)
                            <?php $current_purchase_return_stock+=$current_stock_pur_return->quantity ?>
                        @endforeach



 {{--------------------------------------------------------------------------------------}}


                        {{-------------------------------------old sale return stock -------------------------------------------}}


                        <?php  $sale_return= \App\Sale_return_invoice::whereBetween('track_date', array('2016-01-01', $from_date))->where('product_id',$sale->id)->select('quantity')->get(); ?>
                        {{--array_sum($a)--}}
                        <?php $sale_retutn_stock=0 ?>
                        @foreach($sale_return as $stock_sale_return)
                            <?php $sale_retutn_stock=$sale_retutn_stock+$stock_sale_return->quantity ?>
                        @endforeach


                        {{-------------------------------------current sale return stock -------------------------------------------}}


                        <?php  $current_sale_return= \App\Sale_return_invoice::where('product_id',$sale->id)->select('quantity')->get(); ?>
                        {{--array_sum($a)--}}
                        <?php $current_sale_retutn_stock=0 ?>
                        @foreach($current_sale_return as $current_stock_sale_return)
                            <?php $current_sale_retutn_stock+=$current_stock_sale_return->quantity ?>
                        @endforeach



                        <?php $challan_stock=0; ?>
                        <?php  $challan_sale= \App\Challan_child::whereBetween('track_date', array('2016-01-01', $from_date))->where('product_id',$sale->id)->where('status','!=','cancel')->get(); ?>
                        @foreach($challan_sale as $challan_sale)
                            <?php $challan_stock+=$challan_sale->quantity ?>
                        @endforeach


                        <?php $challan_return_stock=0; ?>
                        <?php  $challan_return_sale= \App\chalan_return_child::whereBetween('track_date', array('2016-01-01', $from_date))->where('product_id',$sale->id)->get(); ?>
                        @foreach($challan_return_sale as $challan_return_sale)
                            <?php $challan_return_stock+=$challan_return_sale->quantity ?>
                        @endforeach


                        <?php $current_challan_stock=0; ?>
                        <?php  $current_challan= \App\Challan_child::where('product_id',$sale->id)->where('status','!=','cancel')->select('quantity')->get(); ?>
                        @foreach($current_challan as $current_challan)
                            <?php $current_challan_stock+=$current_challan->quantity ?>
                        @endforeach

                        <?php $current_challan_return_stock=0; ?>
                        <?php  $current_challan_return= \App\chalan_return_child::where('product_id',$sale->id)->select('quantity')->get(); ?>
                        @foreach($current_challan_return as $current_challan_return)
                            <?php $current_challan_return_stock+=$current_challan_return->quantity ?>
                        @endforeach


                        {{----------------------------------------------------------}}

                        <?php $total_filter_qnty=($purchase_stock+(float)$sale->opening_stock+$sale_retutn_stock+$challan_return_stock)-($sale_stock+$purchase_return_stock+$challan_stock) ?>
                        <?php $cuurent_total_filter_qnty=($current_purchase_stock+(float)$sale->opening_stock+$current_sale_retutn_stock+$current_challan_return_stock)-($cuurent_sale_stock+$current_purchase_return_stock+$current_challan_stock) ?>
                        <td>{{$sale->id}} </td>
                        <td>{{$sale->product_name}}</td>
                        <td>{{$total_filter_qnty}}</td>
                        <td>{{$total_filter_qnty*$highest_rate}}</td>
                        <td>{{$cuurent_total_filter_qnty}}</td>
                        <?php $total_amount=$total_amount+(float)$sale->amount ?>
                        <?php $slno=$slno+1 ?>
                    </tr>
                @endforeach


                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <br>
                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                </div>
            </div>
            </div>
            <script>
                function myFunction() {
                    window.print();
                }

            </script>
<br>
      @endsection
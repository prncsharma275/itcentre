@extends('layouts.adminPanelTable')
@section('title')
    Receipt  Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Receipt Report</a>
    </li>
@endsection
@section('content')
        <div style="background-color: #ffffff;padding:10px">
            <?php
            $orderdate = explode('-', $from_date);
            $year_from = $orderdate[0];
            $month_from   = $orderdate[1];
            $day_from  = $orderdate[2];

            $orderdate1 = explode('-', $upto_date);
            $year_upto = $orderdate1[0];
            $month_upto = $orderdate1[1];
            $day_upto = $orderdate1[2];
            ?>
            <h3 class="heading">Receipt Report <br>
                {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}
            </h3>
                <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>


            <div class="row myrow">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;">
                            <th class="my_th">Voucher No.</th>
                            <th class="my_th">Voucher Date</th>
                            <th class="my_th">Customer Name</th>
                            <th class="my_th">Payment Amount</th>
                        </tr>
                        <?php  $purchase_igst= \App\Receipt::whereBetween('billing_date', array($from_date, $upto_date))->select('amount','id','unique_id','billing_date','customer_id')->get(); ?>
                       <?php $total_amount=0; ?>
                        @foreach($purchase_igst as $sale)
                        <tr>
                            <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('singleViewReceipt')}}/{{$sale->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$sale->unique_id}}</td>
                            <td><?php echo date( 'd/m/y', strtotime($sale->billing_date)) ?></td>
                            <td>{{$sale->belongsToCustomer->ledger_name}}</td>
                            <td>{{sprintf('%0.2f', $sale->amount)}}</td>
                            <?php $total_amount=$total_amount+$sale->amount ?>
                        </tr>
                        @endforeach
                        <tr  style="background-color: #ebf2f6;">
    <th colspan="3" class="text-center my_th">Grand Total</th>
    <th class="my_th">{{sprintf('%0.2f', $total_amount)}}</th>
</tr>
                    </table>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <br>
                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                </div>
            </div>
            <script>
                function myFunction() {
                    window.print();
                }

            </script>

        </div>

          @endsection
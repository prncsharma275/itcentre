@extends('layouts.adminPanelTable')
@section('title')
    Sale Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        /*input[type='search']{*/
            /*width: 200px!important;*/
        /*}*/

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{

            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }


        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Sale Book</a>
    </li>
@endsection
@section('content')
        <div style="background-color: #ffffff;padding:10px">
            <?php
            $orderdate = explode('-', $from_date);
            $year_from = $orderdate[0];
            $month_from   = $orderdate[1];
            $day_from  = $orderdate[2];

            $orderdate1 = explode('-', $upto_date);
            $year_upto = $orderdate1[0];
            $month_upto = $orderdate1[1];
            $day_upto = $orderdate1[2];
            ?>
            <h3 class="heading">Sale Day Book Report<br>
                {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}
            </h3>
                <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
            {{--purchase and sales--}}
            <div class="row myrow">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered dTableR" id="dt_a">
                        <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                            <th class="my_th">Invoice No.</th>
                            <th class="my_th">Invoice Date</th>
                            <th class="my_th">Customer Name</th>
                            <th class="my_th">Amt.</th>
                            <th class="my_th">Discount</th>
                            <th class="my_th">Txble. Amt.</th>
                            <th class="my_th">Gst Type</th>
                            <th class="my_th">Gst 5%</th>
                            <th class="my_th">Gst 12%</th>
                            <th class="my_th">Gst 18%</th>
                            <th class="my_th">Gst 28%</th>

                            <th class="my_th">Total Gst</th>
                            <th class="my_th">Invoice Amount</th>
                            <th class="my_th">A</th>
                        </tr>
                        <?php  $purchase_igst= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->orderBy('billing_date', 'desc')->get(); ?>
                       <?php
                        $total_amount=0;
                        $total_without_tax=0;
                        $total_discount=0;
                        $total_taxable_amount=0;
                        $total_tax=0;
                        $total_tax12=0;
                        $total_tax18=0;
                        $total_tax28=0;
                        $total_tax5=0;
                        ?>
                        @foreach($purchase_igst as $sale)
                            <?php
                            $total_tax_for_12=0;
                            $total_tax_for_18=0;
                            $total_tax_for_28=0;
                            $total_tax_for_5=0;
                            $total_tax_for_thisrow=0;
                            ?>
                        <tr>
                            <td>{{$sale->unique_id}}&nbsp;<a style="color: green;font-weight: 600" href="{{url('singleViewSale')}}/{{$sale->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a></td>
                            <td><?php echo date( 'd/m/y', strtotime($sale->billing_date)) ?></td>
                            <td>{{$sale->belongsToCustomer->ledger_name}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_amount_without_anything)}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_discount)}}</td>
                            <td>{{sprintf('%0.2f', $sale->total_taxable_value)}}</td>

                            <td>{{$sale->tax_type}}</td>
                            @if($sale->tax_type == "CGST&SGST")
                                <?php
                                $sale_invoice_datas = DB::table('sale_invoice')
                                        ->select(DB::raw("SUM(tax_amount) as tax_amounts"))->addSelect('cgst_tax','sgst_tax','sale_id')
                                        ->where('sale_id',$sale->id )
                                        ->groupBy('sgst_tax')
                                        ->get();
                                foreach($sale_invoice_datas as $sale_invoice_data){
                                    if(($sale_invoice_data->cgst_tax + $sale_invoice_data->sgst_tax) == 12){
                                        $total_tax_for_12 += $sale_invoice_data->tax_amounts;
                                    }elseif(($sale_invoice_data->cgst_tax + $sale_invoice_data->sgst_tax) == 18){
                                        $total_tax_for_18 += $sale_invoice_data->tax_amounts;
                                    }
                                    elseif(($sale_invoice_data->cgst_tax + $sale_invoice_data->sgst_tax) == 28){
                                        $total_tax_for_28 += $sale_invoice_data->tax_amounts;
                                    }else{
                                        $total_tax_for_5 += $sale_invoice_data->tax_amounts;
                                    }
                                }
                                ?>
                                <td>{{sprintf('%0.2f', $total_tax_for_5)}}</td>
                                <td>{{sprintf('%0.2f', $total_tax_for_12)}}</td>
                                <td>{{sprintf('%0.2f', $total_tax_for_18)}}</td>
                                <td>{{sprintf('%0.2f', $total_tax_for_28)}}</td>

                            @elseif($sale->tax_type == "IGST")
                                <?php
                                $sale_invoice_datas = DB::table('sale_invoice')
                                        ->select(DB::raw("SUM(tax_amount) as tax_amounts"))->addSelect('igst_tax','sale_id')
                                        ->where('sale_id',$sale->id )
                                        ->groupBy('sgst_tax')
                                        ->get();
                                foreach($sale_invoice_datas as $sale_invoice_data){
                                    if($sale_invoice_data->igst_tax == 28){
                                        $total_tax_for_28 += $sale_invoice_data->tax_amounts;
                                    }elseif($sale_invoice_data->igst_tax == 12){
                                        $total_tax_for_12 += $sale_invoice_data->tax_amounts;
                                    }elseif($sale_invoice_data->igst_tax == 18){
                                        $total_tax_for_18 += $sale_invoice_data->tax_amounts;
                                    }else{
                                        $total_tax_for_5 += $sale_invoice_data->tax_amounts;
                                    }
                                }
                                ?>
                                <td>{{sprintf('%0.2f', $total_tax_for_5)}}</td>
                                <td>{{sprintf('%0.2f', $total_tax_for_12)}}</td>
                                <td>{{sprintf('%0.2f', $total_tax_for_18)}}</td>
                                <td>{{sprintf('%0.2f', $total_tax_for_28)}}</td>

                            @endif

                            <td>{{sprintf('%0.2f', $sale->total_tax_amount)}}</td>
                            <td>{{sprintf('%0.2f', $sale->gross_total)}}</td>
                            <?php $total_without_tax=$total_without_tax+$sale->total_amount_without_anything ?>
                            <?php $total_discount=$total_discount+$sale->total_discount ?>
                            <?php $total_taxable_amount=$total_taxable_amount+$sale->total_taxable_value ?>
                            <?php $total_tax=$total_tax+$sale->total_tax_amount ?>
                            <?php $total_tax12 += $total_tax_for_12;?>
                            <?php $total_tax18 += $total_tax_for_18;?>
                            <?php $total_tax28 += $total_tax_for_28;?>
                            <?php $total_tax5 += $total_tax_for_5;?>
                            <?php $total_amount=$total_amount+$sale->gross_total ?>
                            <?php $total_tax_for_thisrow=$total_tax_for_12+$total_tax_for_18+$total_tax_for_28+$total_tax_for_5;?>

                            @if(sprintf('%0.2f', $sale->total_tax_amount) == sprintf('%0.2f', $total_tax_for_thisrow))
                            <td>-</td>

                            @else
                            <td style="background-color: red;">Not Match</td>
                            @endif
                        </tr>
                        @endforeach
<tr  style="background-color: #ebf2f6;color: #FFFFFF">
    <th colspan="3" class="text-center my_th">Grand Total</th>
    <th class="my_th">{{sprintf('%0.2f', $total_without_tax)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_discount)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_taxable_amount)}}</th>
    <th class="my_th"></th>
    <th class="my_th">{{sprintf('%0.2f', $total_tax5)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_tax12)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_tax18)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_tax28)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_tax)}}</th>
    <th class="my_th">{{sprintf('%0.2f', $total_amount)}}</th>
</tr>
                    </table>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <br>
                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                </div>
            </div>
            <script>
                function myFunction() {
                    window.print();
                }

            </script>

        </div>

@endsection
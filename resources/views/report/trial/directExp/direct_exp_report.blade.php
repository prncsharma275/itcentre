@extends('layouts.adminPanelTable')
@section('title')
    Direct Exp:Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Custom Ledger</a>
    </li>
@endsection
@section('content')
            <div style="background-color: #ffffff;padding:10px">
                <?php
                $orderdate = explode('-', $from_date);
                $year_from = $orderdate[0];
                $month_from   = $orderdate[1];
                $day_from  = $orderdate[2];

                $orderdate1 = explode('-', $upto_date);
                $year_upto = $orderdate1[0];
                $month_upto = $orderdate1[1];
                $day_upto = $orderdate1[2];
                ?>
                <h3 class="heading">Direct Expenses<br>

                    {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}</h3>


                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>


                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Description</th>
                                <th class="my_th">Dr.</th>
                                <th class="my_th">Cr.</th>

                            </tr>
                            {{--------------previouse balance code----------}}


                            <?php  $indirectExp = \App\Customer::where('ledger_type2',5)->get() ?>
                            <?php $total=0; ?>
                            @foreach($indirectExp as $indirectExp)
                                <?php $indirectLed = \App\Ledgerdr::where('customer_id',$indirectExp->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                    <tr>
                                        <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('detail_indirect_exp')}}/{{$from_date}}/{{$upto_date}}/{{$indirectExp->id}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$indirectExp->ledger_name}}</td>
                                        <?php $total +=$indirectLed ?>
                                        <td>{{sprintf('%0.2f', $indirectLed)}}</td>
                                        <td>-</td>
                                    </tr>
                            @endforeach
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Total</th>
                                <th class="my_th">{{$total}}</th>
                                <th class="my_th">-</th>

                            </tr>

                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}
@endsection
@extends('layouts.adminPanelTable')
@section('title')
    Current Liablities:Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Custom Ledger</a>
    </li>
@endsection
@section('content')
            <div style="background-color: #ffffff;padding:10px">
                <?php
                $orderdate = explode('-', $from_date);
                $year_from = $orderdate[0];
                $month_from   = $orderdate[1];
                $day_from  = $orderdate[2];

                $orderdate1 = explode('-', $upto_date);
                $year_upto = $orderdate1[0];
                $month_upto = $orderdate1[1];
                $day_upto = $orderdate1[2];
                ?>
                <h3 class="heading">Current Liablities<br>

                    {{$day_from}}-{{$month_from}}-{{$year_from}} To {{$day_upto}}-{{$month_upto}}-{{$year_upto}}</h3>


                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                {{--purchase and sales--}}


                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Description</th>
                                <th class="my_th">Dr.</th>
                                <th class="my_th">Cr.</th>

                            </tr>
                            {{--------------previouse balance code----------}}

                            <?php $currentAssetsDr=0; ?>
                            <?php $currentAssetsCr=0; ?>











                                {{---------------------------------------- Current Liablities Loop ------------------------------------------}}
                                <?php  $ledger_type = \App\Ledger_type::where('ledger_group_id',2)->get() ?>

                                @foreach($ledger_type as $ledger_type)
                                     <?php $customer = \App\Customer::where('ledger_type2',$ledger_type->id)->get()?>

                                <?php $group_total_dr=0; ?>
                                <?php $group_total_cr=0; ?>
                                       @foreach($customer as $cust)
                                             <?php $total=0; ?>

                                {{------------------------------------------------------------- Old Value ------------------------------}}
                                             <?php $new_date= date('Y-m-d',(strtotime ( '-1 day' , strtotime ($from_date) ) ));
                                             $time = strtotime('01/01/2017');

                                             $newformat = date('Y-m-d',$time);
                                             ?>


                                             <?php $ledger_plus_old = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($newformat, $new_date))->where('extra','=','+')->whereNull('status')->sum('amount'); ?>
                                             <?php $ledger_minus_old = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($newformat, $new_date))->where('extra','=','-')->whereNull('status')->sum('amount'); ?>


                                        {{--------------------------------------------------- old value ends here ---------------------------}}

                                             <?php $ledger_plus = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','+')->whereNull('status')->sum('amount'); ?>
                                    <?php $ledger_minus = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','-')->whereNull('status')->sum('amount'); ?>

                                    <?php $total = $ledger_plus-$ledger_minus ?>
                                         {{--check if individual customer value is negative den add to negative group total--}}
                                        @if($total<1)
                                         <?php $group_total_cr += -($total) ?>
                                        @else
                                         <?php $group_total_dr += $total ?>
                                        @endif

                                      @endforeach

                                         @if($group_total_dr>0 or $group_total_cr>0 )
                                         <tr>
                                            <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('#')}}/{{$from_date}}/{{$upto_date}}"  title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$ledger_type->ledger_type_name}}</td>


                                                <?php $currentAssetsCr += $group_total_cr ?>
                                                <?php $currentAssetsDr += $group_total_dr ?>
                                                    <td>{{sprintf('%0.2f', $group_total_dr)}}</td>
                                                    <td>{{sprintf('%0.2f', $group_total_cr)}}</td>
                                        </tr>
                                        @endif

                            @endforeach


                            <?php  $input_tax_cr= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('total_tax_amount'); ?>
                            <?php  $output_tax_dr= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('total_tax_amount'); ?>

                            <?php $currentAssetsCr += $input_tax_cr ?>

                            <?php $currentAssetsDr +=$output_tax_dr ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('#')}}/{{$from_date}}/{{$upto_date}}"  title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Taxes And Duties</td>
                                <td>{{sprintf('%0.2f',$output_tax_dr)}}</td>
                                <td>{{sprintf('%0.2f', $input_tax_cr)}}</td>
                            </tr>





                            <tr  style="background-color: #ebf2f6;">
                                <th  class="text-center my_th">Total</th>
                                <th  class="my_th">{{sprintf('%0.2f',$currentAssetsDr)}}</th>
                                <th  class="my_th">{{sprintf('%0.2f', $currentAssetsCr)}}</th>
                            </tr>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}
@endsection
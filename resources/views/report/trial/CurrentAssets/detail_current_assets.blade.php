@extends('layouts.adminPanelTable')
@section('title')
  Detailed Current Assets
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Payment Report</a>
    </li>
@endsection
@section('content')
    <div style="background-color: #ffffff;padding:10px">
      <?php $ledgerType = \App\Ledger_type::find($customer) ?>
        <h3 class="heading">{{$ledgerType->ledger_type_name}}

        </h3>
        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
        <div class="row myrow">
            <div class="col-xs-12">
                <table class="table table-striped table-bordered dTableR" id="dt_a">
                    <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                        <th class="my_th">Description</th>
                        <th class="my_th">Dr.</th>
                        <th class="my_th">Cr.</th>

                    </tr>

                    <?php $customer = \App\Customer::where('ledger_type2',$customer)->where('id','!=',2)->where('id','!=',1)->get()?>
                    <?php $group_total_cr=0; ?>
                    <?php $group_total_dr=0; ?>

                    @foreach($customer as $singleCustomer)
                        <?php $total=0; ?>
                        <?php $ledger_plus = \App\Ledgerdr::where('customer_id',$singleCustomer->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','+')->whereNull('status')->sum('amount'); ?>
                        <?php $ledger_minus = \App\Ledgerdr::where('customer_id',$singleCustomer->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','-')->whereNull('status')->sum('amount'); ?>

                        <?php $total = $ledger_plus-$ledger_minus ?>
                        {{--check if individual customer value is negative den add to negative group total--}}
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('#')}}/{{$singleCustomer->id}}"  title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;{{$singleCustomer->ledger_name}}</td>

                        @if($total<1)
                                <td>-</td>
                                <td>{{sprintf('%0.2f', -($total) )}}</td>
                            <?php $group_total_cr += -($total) ?>
                        @else
                                <td>{{sprintf('%0.2f', $total)}}</td>
                                <td>-</td>
                            <?php $group_total_dr += $total ?>
                            @endif
                        </tr>
                    @endforeach
                    <tr  style="background-color: #ebf2f6;">
                        <th colspan="1" class="text-center my_th">Total</th>
                        <th class="my_th">{{sprintf('%0.2f', $group_total_dr)}}</th>
                        <th class="my_th">{{sprintf('%0.2f', $group_total_cr)}}</th>
                    </tr>
                </table>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <br>
                <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
            </div>
        </div>
        <script>
            function myFunction() {
                window.print();
            }

        </script>

    </div>

@endsection
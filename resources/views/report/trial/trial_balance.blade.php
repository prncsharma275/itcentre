@extends('layouts.adminPanelTable')
@section('title')
    Trial Balance :Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Trial Balance</a>
    </li>
@endsection
@section('content')
            <div style="background-color: #ffffff;padding:10px">

                <h3 class="heading">Trial Balance<br>

                    {{date('d-m-Y',strtotime($from_date))}} To {{date('d-m-Y',strtotime($upto_date))}}</h3>


                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                {{--purchase and sales--}}


                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th">Description</th>
                                <th class="my_th">Dr.</th>
                                <th class="my_th">Cr.</th>

                            </tr>

                            <?php $final_total_dr=0; ?>
                            <?php $final_total_cr=0; ?>
 {{---------------------------------------- Purchases ------------------------------------------}}

                            <?php  $qnty_purchase = \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('gross_total') ?>
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_purchase_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Purchase</td>
                               <?php $final_total_dr +=$qnty_purchase ?>
                                <td>{{sprintf('%0.2f', $qnty_purchase)}}</td>
                                <td>-</td>
                            </tr>

  {{---------------------------------------- Sales Return  ------------------------------------------}}
                            <?php  $qnty_sale_return = \App\Sale_return::whereBetween('sale_return_date', array($from_date, $upto_date))->sum('grand_total') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_saleReturn_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Sale Return</td>
                                <?php $final_total_dr +=$qnty_sale_return ?>
                                <td>{{sprintf('%0.2f', $qnty_sale_return)}}</td>
                                <td>-</td>

                            </tr>


  {{---------------------------------------- Indirect Expenses ------------------------------------------}}

                            <?php  $indirectExp = \App\Customer::where('ledger_type2',6)->get() ?>
                            <?php $total=0; ?>
                            @foreach($indirectExp as $indirectExp)
                                <?php $indirectLed = \App\Ledgerdr::where('customer_id',$indirectExp->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $total +=$indirectLed ?>
                            @endforeach
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('indirect_exp_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Indirect Expenses</td>
                                <?php $final_total_dr +=$total ?>
                                <td>{{sprintf('%0.2f', $total)}}</td>
                                <td>-</td>
                            </tr>


{{---------------------------------------- Direct Expenses ------------------------------------------}}

                            <?php  $directExp = \App\Customer::where('ledger_type2',5)->get() ?>
                            <?php $total=0; ?>
                            @foreach($directExp as $directExp)
                                <?php $directLed = \App\Ledgerdr::where('customer_id',$directExp->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $total +=$directLed ?>
                            @endforeach
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('direct_exp_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Direct Expenses</td>
                                <?php $final_total_dr +=$total ?>
                                <td>{{sprintf('%0.2f', $total)}}</td>
                                <td>-</td>
                            </tr>


{{-------------------------------------------------- Current Assets ----------------------------------------------------}}

                            <?php $currentAssetsDr=0; ?>
                            <?php $currentAssetsCr=0; ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('current_assets_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Current Assets</td>





                            {{---------------------------------------- Cash In Hand ------------------------------------------}}

                            <?php  $total_plus_cash = \App\Cash::whereBetween('date', array($from_date, $upto_date))->where('extra','=','+')->whereNull('status')->sum('amount') ?>
                            <?php  $total_minus_cash = \App\Cash::whereBetween('date', array($from_date, $upto_date))->where('extra','=','-')->whereNull('status')->sum('amount') ?>
                            <?php $total_cash = $total_plus_cash-$total_minus_cash ?>

                                @if($total_cash<1)
                                    <?php $currentAssetsCr += -($total_cash) ?>


                                @else
                                    <?php $currentAssetsDr +=$total_cash?>

                                @endif


                            {{---------------------------------------- Cash At Bank ------------------------------------------}}
                            <?php  $total_plus_bank= \App\Bank::whereBetween('date', array($from_date, $upto_date))->where('extra','=','+')->whereNull('status')->sum('amount') ?>
                            <?php  $total_minus_bank = \App\Bank::whereBetween('date', array($from_date, $upto_date))->where('extra','=','-')->whereNull('status')->sum('amount') ?>
                            <?php $total_bank = $total_plus_bank-$total_minus_bank ?>

                                @if($total_bank<1)
                                    <?php $currentAssetsCr += -($total_bank) ?>
                                @else
                                    <?php $currentAssetsDr +=$total_bank?>
                                @endif




                            {{---------------------------------------- Sundary Debtors------------------------------------------}}
                            <?php  $customers = \App\Customer::where('ledger_group',1)->where('ledger_type2','!=',4)->where('id','!=',2)->where('id','!=',1)->get() ?>
                            @foreach($customers as $cust)
                                <?php $ledger_plus = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','+')->whereNull('status')->sum('amount'); ?>
                                <?php $ledger_minus = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','-')->whereNull('status')->sum('amount'); ?>
                                <?php $total=0; ?>
                                <?php $total = $ledger_plus-$ledger_minus ?>
                                @if($total<1)
                                    <?php $currentAssetsCr += -($total) ?>
                                @else
                                    <?php $currentAssetsDr +=$total ?>
                                @endif
                            @endforeach
                                <?php $final_total_dr +=$currentAssetsDr ?>
                                <?php $final_total_cr +=$currentAssetsCr ?>
                                <td>{{sprintf('%0.2f', $currentAssetsDr)}}</td>
                                <td>{{sprintf('%0.2f', $currentAssetsCr)}}</td>
                            </tr>


{{------------------------------------------------------------------- End of current Assets ----------------------------------------------}}



                            {{----------------------------------------Fixed Assets ------------------------------------------}}

                            <?php  $fxAsst = \App\Customer::where('ledger_group',3)->get() ?>

                            <?php $total=0; ?>
                            @foreach($fxAsst as $fxAsst)
                                <?php$fxAsstLed = \App\Ledgerdr::where('customer_id',$fxAsst->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $total +=$fxAsstLed ?>
                            @endforeach
                            @if($total>0)
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_receipt_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Fixed Assets</td>
                                <?php $final_total_dr +=$total ?>
                                <td>{{sprintf('%0.2f', $total)}}</td>
                                <td>-</td>
                            </tr>
                             @endif



                            {{---------------------------------------- Sales  ------------------------------------------}}



                            <?php  $total_sale_grand = \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('gross_total') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_sale_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Sales</td>
                                <td>-</td>
                                <?php $final_total_cr +=$total_sale_grand ?>
                                <td>{{sprintf('%0.2f', $total_sale_grand)}}</td>

                            </tr>



                            {{---------------------------------------- Purchase Return ------------------------------------------}}
                            <?php  $qnty_purchase_return = \App\Purchase_return::whereBetween('billing_date', array($from_date, $upto_date))->sum('grand_total') ?>

                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_purchaseReturn_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Purchase Return</td>
                                <td>-</td>
                                <?php $final_total_cr +=$qnty_purchase_return ?>
                                <td>{{sprintf('%0.2f', $qnty_purchase_return)}}</td>

                            </tr>

{{----------------------------------------------  Current Liablities --------------------------------------------------------}}
                            <?php $currentLiabDr=0; ?>
                            <?php $currentLiabCr=0; ?>
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('current_liablities')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Current Liablities</td>



                                <?php  $customers1 = \App\Customer::where('ledger_group',2)->get() ?>
                                @foreach($customers1 as $cust)
                                    <?php $ledger_plus = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','+')->whereNull('status')->sum('amount'); ?>
                                    <?php $ledger_minus = \App\Ledgerdr::where('customer_id',$cust->id)->whereBetween('billing_date', array($from_date, $upto_date))->where('extra','=','-')->whereNull('status')->sum('amount'); ?>
                                    <?php $total=0; ?>
                                    <?php $total = $ledger_plus-$ledger_minus ?>
                                    @if($total<1)
                                        <?php $currentLiabCr += -($total) ?>
                                    @else
                                        <?php $currentLiabDr +=$total ?>
                                    @endif
                                @endforeach

                                {{---------------------------------------- total input tax ---------------}}

                                <?php  $input_tax_cr= \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('total_tax_amount'); ?>
                                <?php  $output_tax_dr= \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('total_tax_amount'); ?>


                                <?php $currentLiabCr += $input_tax_cr ?>

                                <?php $currentLiabDr +=$output_tax_dr ?>




                                <?php $final_total_cr +=$currentLiabCr ?>
                                <?php $final_total_dr +=$currentLiabDr ?>
                                <td>{{sprintf('%0.2f', $currentLiabDr)}}</td>
                                <td>{{sprintf('%0.2f', $currentLiabCr)}}</td>
                            </tr>










                            {{---------------------------------------- Direct Income  ------------------------------------------}}

                            <?php  $directIncom = \App\Customer::where('ledger_type2',15)->get() ?>
                            <?php $total=0; ?>
                            @foreach($directIncom as $directIncom)
                                <?php $directIncomLed = \App\Ledgerdr::where('customer_id',$directIncom->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $total +=$directIncomLed ?>
                            @endforeach
                            @if($total>0)
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_receipt_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Direct Income</td>
                                <td>-</td>
                                <?php $final_total_cr +=$total ?>
                                <td>{{sprintf('%0.2f', $total)}}</td>

                            </tr>
                            @endif
                            {{---------------------------------------- InDirect Income  ------------------------------------------}}

                            <?php  $IndirectIncom = \App\Customer::where('ledger_type2',16)->get() ?>
                            <?php $total=0; ?>
                            @foreach($IndirectIncom as $IndirectIncom)
                                <?php $IndirectIncomLed = \App\Ledgerdr::where('customer_id',$IndirectIncom->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $total +=$IndirectIncomLed ?>
                            @endforeach
                            @if($total>0)
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_receipt_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Indirect Income</td>
                                <td>-</td>
                                <?php $final_total_cr +=$total ?>
                                <td>{{sprintf('%0.2f', $total)}}</td>

                            </tr>
                          @endif


                            {{----------------------------------------Fixed Laiblities  ------------------------------------------}}

                            <?php  $fxLiab = \App\Customer::where('ledger_group',4)->get() ?>
                            <?php $total=0; ?>
                            @foreach($fxLiab as $fxLiab)
                                <?php $fxLiabLed = \App\Ledgerdr::where('customer_id',$fxLiab->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $total +=$fxLiabLed ?>
                            @endforeach
                            @if($total>0)
                            <tr>
                                <td class="text-left"><a style="color: green;font-weight: 600" href="{{url('custom_receipt_report')}}/{{$from_date}}/{{$upto_date}}" target="_blank" title="View"> <span class="glyphicon glyphicon-new-window"></span></a>&nbsp;&nbsp;&nbsp;Fixed Liablities</td>
                                <td>-</td>
                                <?php $final_total_cr +=$total ?>
                                <td>{{sprintf('%0.2f', $total)}}</td>

                            </tr>
                           @endif


 {{------------------------------------ Final total --------------------------}}
                            @if($final_total_dr>$final_total_cr)


                            <tr>
                                <td>Balance Adjusted </td>
                                <td></td>
                                <td>{{sprintf('%0.2f',$final_total_dr-$final_total_cr)}}</td>
                            </tr>
                            <tr  style="background-color: #ebf2f6;">
                                <th  class="text-center my_th">Total</th>
                                <th  class="my_th">{{sprintf('%0.2f',$final_total_dr)}}</th>
                                <th  class="my_th">{{sprintf('%0.2f', $final_total_dr)}}</th>
                           </tr>
                                @else
                                <tr>
                                    <td>Balance Adjusted </td>
                                    <td>{{ sprintf('%0.2f', $final_total_cr-$final_total_dr)}}</td>
                                    <td></td>
                                </tr>
                                <tr  style="background-color: #ebf2f6;">
                                    <th  class="text-center my_th">Total</th>
                                    <th  class="my_th">{{sprintf('%0.2f',$final_total_cr)}}</th>
                                    <th  class="my_th">{{sprintf('%0.2f', $final_total_cr)}}</th>
                                </tr>
                            @endif
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                        &nbsp;<a href="{{url('report')}}" style="margin-top:-19px!important" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}
@endsection
@extends('layouts.adminPanel')
@section('title')
    Reports
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('report')}}">Report Panel</a>
    </li>

@endsection

@section('content')

            {{--inner content starts here ------------------------------------}}
            <h3 class="heading">Reports</h3>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="dshb_icoNav clearfix">
                        <li><a href="" data-toggle="modal" data-target="#myModal_sale_book" style="background-image: url(admin/img/gCons/multi-agents.png)">Sale  Book</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_purchase_book" style="background-image: url(admin/img/gCons/world.png)" >Purchase  Book</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal" style="background-image: url(admin/img/gCons/configuration.png)">Ledgers</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal4" style="background-image: url(admin/img/gCons/lab.png)">GST Report</a>
                        </li><li><a href="" data-toggle="modal" data-target="#myModal_cash_ledger" style="background-image: url(admin/img/gCons/van.png)"> Cash Ledger</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_bank_ledger" style="background-image: url(admin/img/gCons/pie-chart.png)">Bank Ledger</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_payment" style="background-image: url(admin/img/gCons/edit.png)">Payment Report</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_receipt" style="background-image: url(admin/img/gCons/add-item.png)">Receipt Report</a></li>


                        <li><a href="" data-toggle="modal" data-target="#myModal_sale_return" style="background-image: url(admin/img/gCons/multi-agents.png)">Sale  Return</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_purchase_return" style="background-image: url(admin/img/gCons/world.png)" >Purchase  Return</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_creditnote" style="background-image: url(admin/img/gCons/configuration.png)">Credit Note</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_debitnote" style="background-image: url(admin/img/gCons/lab.png)">Debit Note</a>
                        </li><li><a href="" data-toggle="modal" data-target="#myModal_stock_report" style="background-image: url(admin/img/gCons/van.png)"> Details Stock</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_stock_old_report" style="background-image: url(admin/img/gCons/pie-chart.png)">Current Stock</a></li>
                        <li><a href="" data-toggle="modal" data-target="#myModal_contra" style="background-image: url(admin/img/gCons/edit.png)">Contra Report</a></li>
                     {{--Trial Balance Report--}}
                        <?php $start_date = date("Y").'-04-01' ?>
                        <?php $end_date = date('Y', strtotime('+1 year')).'-03-31'; ?>
                        <li><a href="{{url('trial_balnace')}}/{{$start_date}}/{{$end_date}}" style="background-image: url(admin/img/gCons/multi-agents.png)">Trial Balance</a></li>
                       {{--Profit & Loss A/c--}}
                        <?php $start_date = date("Y").'-04-01' ?>
                        <?php $end_date = date('Y', strtotime('+1 year')).'-03-31'; ?>
                        <li><a href="{{url('pl_account')}}/{{$start_date}}/{{$end_date}}" style="background-image: url(admin/img/gCons/configuration.png)">P/L Account</a></li>
                         {{--Balance Sheet  --}}
                        <?php $start_date = date("Y").'-04-01' ?>
                        <?php $end_date = date('Y', strtotime('+1 year')).'-03-31'; ?>
                        <li><a href="{{url('balancesheet')}}/{{$start_date}}/{{$end_date}}" style="background-image: url(admin/img/gCons/pie-chart.png)">Balance Sheet</a></li>

                    </ul>
                </div>
            </div>


            {{----------------------model for customer ledger-------------------------------------------------}}
            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'customer_ledger', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Customer Ledger</h4>
                        </div>
                        <div class="modal-body">
                            <select id="saleno" name="customer_id" style="width:100%;height: 38px" required>
                                <option value="">Select Customer</option>
                                <?php  $product_type = DB::table('customers')->orderBy('ledger_name', 'asc')->get(); ?>
                                @foreach($product_type as $product_type)
                                    <option value="{{$product_type->id}}">{{$product_type->ledger_name}}</option>
                                @endforeach
                            </select>
                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------model for Supplier ledger-------------------------------------------------}}
            <div class="modal fade" id="myModal1"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'supplier_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Supplier Ledger</h4>
                        </div>
                        <div class="modal-body">
                            <select id="supplier" name="customer_id" style="width:100%;height: 38px" required>

                                <?php  $product_type1 = DB::table('customers')->orderBy('ledger_name', 'asc')->get(); ?>
                                @foreach($product_type1 as $product_type2)
                                    <option value="{{$product_type2->id}}">{{$product_type2->ledger_name}}</option>
                                @endforeach

                            </select>
                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

            {{----------------------model for Cash ledger-------------------------------------------------}}
            <div class="modal fade" id="myModal_cash_ledger"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'cash_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Cash Ledger</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

            {{----------------------model for Bank ledger-------------------------------------------------}}
            <div class="modal fade" id="myModal_bank_ledger"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'bank_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Bank Ledger</h4>
                        </div>
                        <div class="modal-body">

                            <select id="supplier" name="customer_id" class="form-control" style="width:100%;height: 38px" required>

                                <?php  $product_type1 = DB::table('customers')->where('ledger_type2',4)->orderBy('ledger_name', 'asc')->get(); ?>
                                <option value="all">All Bank</option>
                                @foreach($product_type1 as $product_type2)
                                    <option value="{{$product_type2->id}}">{{$product_type2->ledger_name}}</option>
                                @endforeach

                            </select>

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>


            {{-------------------------------------------------- modal for sale day book report-----------------------------------------}}
            <div class="modal fade" id="myModal_sale_book"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'sale_day_book', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Sale Day Book</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------model for Purchase day books -------------------------------------------------}}
            <div class="modal fade" id="myModal_purchase_book"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'purchase_day_book', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Purchase Day Book Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for gst report --------------------------       --}}
            <div class="modal fade" id="myModal4"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'gst_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">GST Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for payment report --------------------------       --}}
            <div class="modal fade" id="myModal_payment"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'payment_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Payment Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for receipt report --------------------------       --}}
            <div class="modal fade" id="myModal_receipt"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'receipt_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Receipt Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for sale Return report --------------------------       --}}
            <div class="modal fade" id="myModal_sale_return"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'sale_return_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Sale Return Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for Purchase Return report --------------------------       --}}
            <div class="modal fade" id="myModal_purchase_return"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'purchase_return_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Purchase Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for Credit note report --------------------------       --}}
            <div class="modal fade" id="myModal_creditnote"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'creditnote_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Credit Note</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

            {{----------------------------------------------------------------- modal for Debit note report --------------------------       --}}
            <div class="modal fade" id="myModal_debitnote"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'debitnote_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Debit Note</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{----------------------------------------------------------------- modal for Details Product wise report  --------------------------       --}}
            <div class="modal fade" id="myModal_stock_report"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'stock_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detailed Product Wise Report</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <select id="product" name="product_id" style="width:100%;height: 38px">
                                        <option value="">Select Product</option>
                                        <?php  $product_type = DB::table('products')->orderBy('product_name', 'asc')->get(); ?>
                                        @foreach($product_type as $product_type)
                                            <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <label for="startDate" style="font-size: small">Select Date (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

            {{----------------------------------------------------------------- modal for Current stock report  --------------------------       --}}
            <div class="modal fade" id="myModal_stock_old_report"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'current_stock_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Current Stock Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <label for="startDate" style="font-size: small">Select Date (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>


            {{----------------------------------------------------------------- modal for Contra report --------------------------       --}}
            <div class="modal fade" id="myModal_contra"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('route' => 'contra_report', 'method'=>'get')); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Contra Report</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" style="margin: 20px">
                                <div class="col-sm-6" >
                                    <label for="startDate" style="font-size: small">Date From (DD - MM - YYYY)</label><br>
                                    <input type="text" name="from_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline" tabindex="0"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="from_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                                <div class="col-sm-6">
                                    <label for="closeDate" style="font-size: small">Date Upto (DD - MM - YYYY)</label><br>
                                    <input type="text" name="upto_day" class="form-control" value="{{date('d')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_month" class="form-control" value="{{date('m')}}" style="width: 50px;display: inline"> <span style="font-size: 25px">-</span>
                                    <input type="text" name="upto_year" class="form-control" value="{{date('Y')}}" style="width: 60px;display: inline">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cutstom-modal-btn-bhaskar" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary cutstom-modal-btn-bhaskar">View</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

           {{------------------------------------------------------------------------------ ends here --------------------------------------- --}}
            <script>
                var customer =  [/* states array*/];
                $("#saleno").select2({
                    data: customer
                });


                var product =  [/* states array*/];
                $("#product").select2({
                    data: product
                });
                $("#supplier").select2({
                    data: customer
                });
//                $(document).ready(function() {
//                    $('#table').DataTable();
//                } );
            </script>
            {{--inner content here ------------------------------------}}
@endsection
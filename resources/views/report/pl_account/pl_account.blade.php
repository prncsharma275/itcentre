@extends('layouts.adminPanelTable')
@section('title')
    Profit&Loss:Report
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }


        @page {
            size:auto;   /* auto is the initial value */
            margin-left: 5px;  /* this affects the margin in the printer settings */
            margin-right: 5px;  /* this affects the margin in the printer settings */
            margin-top:5px !important;    /* this affects the margin in the printer settings */
            margin-bottom:5px!important;  /* this affects the margin in the printer settings */
        }
        @media print{
            .sidebar{
                display: none;
            }
            #jCrumbs{
                display: none;
            }
            .header-main{
                display: none;
            }
            .left-content{
                width: 100%!important;
            }
            .my_th{
                color: #000000!important;
                background-color: #ffffff!important;
            }
            button{
                display: none;
            }
            a{
                display: none!important;
            }
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('report')}}">Reports Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Custom Ledger</a>
    </li>
@endsection
@section('content')
            <div style="background-color: #ffffff;padding:10px">

                <h3 class="heading">Profit & Loss A/c<br>

                    {{date('d-m-Y',strtotime($from_date))}} To {{date('d-m-Y',strtotime($upto_date))}}</h3>


                    <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                {{--purchase and sales--}}


                <div class="row myrow">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered dTableR" id="dt_a">
                            <tr  style="background-color: #ebf2f6;color: #FFFFFF!important;">
                                <th class="my_th" style="width: 35%">Particulars</th>
                                <th class="my_th">Amount</th>
                                <th class="my_th"style="width: 35%">Particulars</th>
                                <th class="my_th">Amount</th>
                            </tr>


                            <?php $old_upto_date= date('Y-m-d',(strtotime ( '-1 day' , strtotime ($from_date) ) ));
                            $time = strtotime('01/01/2016');
                            $old_from_date = date('Y-m-d',$time);
                            ?>


                            <?php
                               $all_product = \App\Product::select('id','opening_stock')->get();
                               ?>
                               <?php $opening_stock=0 ?>
                            <?php $closing_stock=0 ?>
                              @foreach($all_product as $product)

<!-- ----------------------------------- OPENING STOCK WITH OLD DATE VALUE ---------------------------------->


                                     <?php $row_total_opening =0; ?>

                                 <?php $total_sale_opening = \App\Sale_invoice::query()
                                    ->select(['quantity'])
                                    ->leftJoin('sales', 'sales.id', '=', 'sale_invoice.sale_id')
                                    ->where('sale_invoice.product_id',$product->id)
                                    ->whereNull('sale_invoice.status')
                                        ->whereBetween('sales.billing_date', array($old_from_date, $old_upto_date))->sum("sale_invoice.quantity"); ?>

                                 <?php $total_purchase_opening = \App\Purchase_invoice::query()
                                         ->select(['quantity'])
                                         ->leftJoin('purchases', 'purchases.id', '=', 'purchase_invoice.purchase_id')
                                         ->where('purchase_invoice.product_id',$product->id)
                                         ->whereNull('purchase_invoice.status')
                                         ->whereBetween('purchases.billing_date', array($old_from_date, $old_upto_date))->sum("purchase_invoice.quantity"); ?>


                                 <?php $total_sale_return_opening = \App\Sale_return_invoice::query()
                                         ->select(['quantity'])
                                         ->leftJoin('sale_returns', 'sale_returns.id', '=', 'sale_returns_invoice.sale_return_id')
                                         ->where('sale_returns_invoice.product_id',$product->id)
                                         ->whereBetween('sale_returns.sale_return_date', array($old_from_date, $old_upto_date))->sum("sale_returns_invoice.quantity"); ?>


                                 <?php $total_purchase_return_opening = \App\Purchase_return_invoice::query()
                                         ->select(['quantity'])
                                         ->leftJoin('purchase_returns', 'purchase_returns.id', '=', 'purchase_returns_invoice.purchase_return_id')
                                         ->where('purchase_returns_invoice.product_id',$product->id)
                                         ->whereBetween('purchase_returns.billing_date', array($old_from_date, $old_upto_date))->sum("purchase_returns_invoice.quantity"); ?>


                                    <?php $row_total_opening = ($total_purchase_opening+$total_sale_return_opening+$product->opening_stock)-($total_sale_opening+$total_purchase_return_opening)?>

                                   @if($row_total_opening>0)
                                     <?php $last_purchase_cost_opening=\App\Purchase_invoice::where('product_id',$product->id)->orderBy('rate', 'desc')->first() ?>
                                      @if($last_purchase_cost_opening)
                                            <?php $opening_stock+=$last_purchase_cost_opening->rate  ?>
                                      @endif
                                    @endif


 <!-- ----------------------------------- END OF OPENING STOCK WITH OLD DATE VALUE ---------------------------------->


  <!-- ----------------------------------- Closing  STOCK WITH OLD DATE VALUE ---------------------------------->

                            <?php $row_total =0; ?>

                            <?php $total_sale = \App\Sale_invoice::query()
                                    ->select(['quantity'])
                                    ->leftJoin('sales', 'sales.id', '=', 'sale_invoice.sale_id')
                                    ->where('sale_invoice.product_id',$product->id)
                                    ->whereNull('sale_invoice.status')
                                    ->whereBetween('sales.billing_date', array($from_date, $upto_date))->sum("sale_invoice.quantity"); ?>

                            <?php $total_purchase = \App\Purchase_invoice::query()
                                    ->select(['quantity'])
                                    ->leftJoin('purchases', 'purchases.id', '=', 'purchase_invoice.purchase_id')
                                    ->where('purchase_invoice.product_id',$product->id)
                                    ->whereNull('purchase_invoice.status')
                                    ->whereBetween('purchases.billing_date', array($from_date, $upto_date))->sum("purchase_invoice.quantity"); ?>


                            <?php $total_sale_return = \App\Sale_return_invoice::query()
                                    ->select(['quantity'])
                                    ->leftJoin('sale_returns', 'sale_returns.id', '=', 'sale_returns_invoice.sale_return_id')
                                    ->where('sale_returns_invoice.product_id',$product->id)
                                    ->whereBetween('sale_returns.sale_return_date', array($from_date, $upto_date))->sum("sale_returns_invoice.quantity"); ?>


                            <?php $total_purchase_return = \App\Purchase_return_invoice::query()
                                    ->select(['quantity'])
                                    ->leftJoin('purchase_returns', 'purchase_returns.id', '=', 'purchase_returns_invoice.purchase_return_id')
                                    ->where('purchase_returns_invoice.product_id',$product->id)
                                    ->whereBetween('purchase_returns.billing_date', array($from_date, $upto_date))->sum("purchase_returns_invoice.quantity"); ?>


                            <?php $row_total = ($total_purchase+$total_sale_return+$product->opening_stock)-($total_sale+$total_purchase_return)?>

                            @if($row_total>0)
                            <?php $last_purchase_cost=\App\Purchase_invoice::where('product_id',$product->id)->orderBy('rate', 'desc')->first() ?>
                            @if($last_purchase_cost)
                            <?php $closing_stock+=$last_purchase_cost->rate*$row_total  ?>
                            @endif
                            @endif


                                @endforeach
        <!-- ----------------------------------- END OF closing STOCK WITH OLD DATE VALUE ---------------------------------->



                            <?php  $qnty_purchase = \App\Purchase::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('gross_total') ?>
                            <?php  $qnty_purchase_return = \App\Purchase_return::whereBetween('billing_date', array($from_date, $upto_date))->sum('grand_total') ?>

                            <?php  $total_sale_grand = \App\Sale::whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('gross_total') ?>
                            <?php  $qnty_sale_return = \App\Sale_return::whereBetween('sale_return_date', array($from_date, $upto_date))->sum('grand_total') ?>

                            <?php  $directExp = \App\Customer::where('ledger_type2',5)->get() ?>
                            <?php $directExpNet=0; ?>
                            @foreach($directExp as $directExp)
                                <?php $directLed = \App\Ledgerdr::where('customer_id',$directExp->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $directExpNet +=$directLed ?>
                            @endforeach


                            <?php  $directIncom = \App\Customer::where('ledger_type2',15)->get() ?>
                            <?php $directIncome=0; ?>
                            @foreach($directIncom as $directIncom)
                                <?php $directIncomLed = \App\Ledgerdr::where('customer_id',$directIncom->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $directIncome +=$directIncomLed ?>
                            @endforeach




                            <?php  $IndirectIncom = \App\Customer::where('ledger_type2',16)->get() ?>
                            <?php $indirectIncome2=0; ?>
                            @foreach($IndirectIncom as $IndirectIncom)
                                <?php $IndirectIncomLed = \App\Ledgerdr::where('customer_id',$IndirectIncom->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $indirectIncome2 +=$IndirectIncomLed ?>
                            @endforeach


                            <?php  $indirectExp = \App\Customer::where('ledger_type2',6)->get() ?>
                            <?php $indirectExpenses=0; ?>
                            @foreach($indirectExp as $indirectExp)
                                <?php $indirectLed = \App\Ledgerdr::where('customer_id',$indirectExp->id)->whereBetween('billing_date', array($from_date, $upto_date))->whereNull('status')->sum('amount'); ?>
                                <?php $indirectExpenses +=$indirectLed ?>
                            @endforeach



                        <?php $dr_gross_total=0; ?>
                       <?php $cr_gross_total=0; ?>
                            <tr>
                                <td>Opening Stock</td>
                                <td>{{sprintf('%0.2f', $opening_stock)}}</td>
                                <?php $dr_gross_total+=$opening_stock; ?>
                                <td>Sales Account</td>
                                <td>{{sprintf('%0.2f', $total_sale_grand-$qnty_sale_return)}}</td>
                                <?php $cr_gross_total+=$total_sale_grand-$qnty_sale_return; ?>
                            </tr>
                            <tr>
                                <td>Purchase Account</td>
                                <td>{{sprintf('%0.2f', $qnty_purchase-$qnty_purchase_return)}}</td>
                                <?php $dr_gross_total+=$qnty_purchase-$qnty_purchase_return; ?>
                                <td>Direct Income</td>
                                <td>{{sprintf('%0.2f', $directIncome)}}</td>
                                <?php $cr_gross_total+=$directIncome; ?>
                            </tr>

                            <tr>
                                <td>Direct Expenses</td>
                                <td>{{sprintf('%0.2f', $directExpNet)}}</td>
                                <?php $dr_gross_total+=$directExpNet; ?>
                                <td>Closing Stock</td>
                                <td>{{sprintf('%0.2f', $closing_stock)}}</td>
                                <?php $cr_gross_total+=$closing_stock; ?>
                            </tr>
                            <?php $gross_profit=0; ?>
                            <?php $gross_loss=0; ?>
                            @if($dr_gross_total>$cr_gross_total)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Gross Loss</td>
                                    <?php $gross_loss +=$dr_gross_total-$cr_gross_total  ?>
                                    <td>{{sprintf('%0.2f', $gross_loss)}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $dr_gross_total)}}</th>
                                    <td></td>
                                    <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $dr_gross_total)}}</th>

                                </tr>

                                <tr>

                                    <td>Gross Loss B/F</td>
                                    <td>{{sprintf('%0.2f', $gross_loss)}}</td>
                                    <td></td>
                                    <td></td>
                                </tr>

                            @else
                                <tr>
                                    <td>Gross Profit</td>
                                    <?php $gross_profit +=$cr_gross_total-$dr_gross_total ?>
                                    <td>{{sprintf('%0.2f', $gross_profit)}}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $cr_gross_total)}}</th>
                                    <td></td>
                                    <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $cr_gross_total)}}</th>

                                </tr>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Gross Profit B/F</td>

                                    <td>{{sprintf('%0.2f', $gross_profit)}}</td>
                                </tr>



                            @endif
                            <tr>
                                <td>Indirect Expenses</td>
                                <td>{{sprintf('%0.2f', $indirectExpenses)}}</td>
                                <?php $gross_loss +=$indirectExpenses ?>
                                <td>Indirect Income</td>
                                <?php $gross_profit +=$indirectIncome2 ?>
                                <td>{{sprintf('%0.2f', $indirectIncome2)}}</td>
                            </tr>

                           @if($gross_loss>$gross_profit)

                               <tr>
                                   <td></td>
                                   <td></td>
                                   <td>Net Loss</td>
                                   <td>{{sprintf('%0.2f', $gross_loss-$gross_profit)}}</td>
                               </tr>

                            <tr>
                                <td>Total</td>
                                <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $gross_loss)}}</th>
                                <td>Total</td>
                                <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $gross_loss)}}</th>

                            </tr>
                           @else

                                <tr>

                                    <td>Net Profit</td>
                                    <td>{{sprintf('%0.2f', $gross_profit-$gross_loss)}}</td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td class="my_th"  style="background-color: #ebf2f6;">Total</td>
                                    <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $gross_profit)}}</th>
                                    <td class="my_th"  style="background-color: #ebf2f6;">Total</td>
                                    <th class="my_th"  style="background-color: #ebf2f6;">{{sprintf('%0.2f', $gross_profit)}}</th>

                                </tr>
                            @endif
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center;">
                        <br>
                        <button class="btn btn-info hidden-print" style="margin-bottom: 20px; " onclick="myFunction()"><span class="glyphicon glyphicon-print"></span> Print</button>
                    </div>
                </div>
                <script>
                    function myFunction() {
                        window.print();
                    }

                </script>

            </div>

            {{--inner content here ------------------------------------}}




@endsection
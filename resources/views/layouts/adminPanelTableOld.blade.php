<!DOCTYPE html>
<html lang="en">
<?php $comp = \App\Company_detail::find(1)?>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{$comp->company_name}} : @yield('title')</title>

    {{--<link href="{{asset('adminDesign/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />--}}
    <link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('admin')}}/bootstrap/css/bootstrap.min.css" />
    <link href="{{asset('adminDesign/css/font-awesome.css')}}" rel="stylesheet">
    <script src="{{asset('adminDesign/js/jquery-2.1.4.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('adminDesign/css/icon-font.min.css')}}" type='text/css' />

    <script type='text/javascript' src='{{ asset('salesAndPurchaseStyle/js/min.js')}}'></script>
    <script src="{{ asset('js/saleJquery.min.js')}}"></script>
    <link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('css/select1.min.css')}}" rel="stylesheet" />

    <script src="{{ asset('js/select2.min.js')}}"></script>
    <script src="{{ asset('js/select1.min.js')}}"></script>

    <script src="{{ asset('js/comboBox.js')}}"></script>
    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('admin')}}/img/splashy/splashy.css" />
    <link rel="stylesheet" href="{{asset('admin')}}/lib/jBreadcrumbs/css/BreadCrumb.css" />
    <link rel="stylesheet" href="{{asset('admin')}}/css/style.css" />
    <!-- theme color-->
    <link rel="stylesheet" href="{{asset('admin')}}/css/blue.css" id="link_theme" />

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('company_logo/')}}/favicon.png" />

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="{{asset('admin')}}/css/ie.css" />
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="{{asset('admin')}}/js/ie/html5.js"></script>
    <script src="{{asset('admin')}}/js/ie/respond.min.js"></script>
    <script src="{{asset('admin')}}/lib/flot/excanvas.min.js"></script>
    <style>

    </style>

    <![endif]-->      </head>

@yield('custom_css')

@yield('manual_style_code')

<body class="full_width">

<div id="maincontainer" class="clearfix">

    <header>

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-inner">
                <div class="container-fluid">

                    <a class="brand pull-left" href="{{url('home')}}">{{$comp->company_name}}</a>

                    <ul class="nav navbar-nav user_menu pull-right">


                        <li class="divider-vertical hidden-sm hidden-xs"></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('admin/img/user_avatar.png')}}" alt="" class="user_avatar">{{ Auth::user()->name }}<b class="caret"></b></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#">My Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="{{url('logout')}}">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>



        {{----}}

    </header>
    <div id="contentwrapper">
        <div class="main_content">
            <div id="jCrumbs" class="breadCrumb module">
                <ul>
                    <li>
                        <a href="{{url('home')}}"><i class="glyphicon glyphicon-home"></i></a>
                    </li>
                    @yield('shortlink')

                </ul>
            </div>

            <!-- page content start here ------------------------------- -->


            @yield('content')




                    <!-- page content ends here ------------------------------- -->


        </div> <!-- content ends here -->
    </div> <!-- main container ends here -->

</div>

<a href="javascript:void(0)" class="sidebar_switch on_switch bs_ttip" data-placement="auto right" data-viewport="body" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">

    <div class="sidebar_inner_scroll">
        <div class="sidebar_inner">
            <form action="http://gebo-admin-3.tzdthemes.com/search_page.html" class="input-group input-group-sm" method="post">
                <input autocomplete="off" name="query" class="search_query form-control input-sm" size="16" placeholder="Search..." type="text">
                <span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button></span>
            </form>
            <div id="side_accordion" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-list-alt"></i> Transaction
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseOne">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a  href="{{url('sale')}}">Sale</a></li>
                                <li><a href="{{url('purchase')}}">Purchase</a></li>
                                <li><a href="{{url('saleReturn')}}">Sale Return</a></li>
                                <li><a href="{{url('purchaseReturn')}}">Purchase Return</a></li>
                                <li><a href="{{url('sale_format_index')}}">Others Sale Bill</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-qrcode"></i> Inventory
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseTwo">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('product')}}">Products</a></li>
                                <li><a href="{{url('product_type')}}">Categories</a></li>
                                <li><a href="{{url('brand')}}">Brands</a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-user"></i> Ledgers
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThree">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('customers')}}">Ledgers</a></li>
                                <li><a href="{{url('ledger_type_route')}}">Ledgers Types</a></li>
                                <li><a href="{{url('ledger_group_route')}}">Ledger Group</a></li>
                            </ul>

                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThreeOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-tasks"></i> Accounts
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThreeOne">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('payment')}}">Payments</a></li>
                                <li><a href="{{url('receipt')}}">Receipts</a></li>
                                <li><a href="{{url('otherCharge')}}">Contro Entry</a></li>
                            </ul>

                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThreeTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-certificate"></i> Allowence
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThreeTwo">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('creditnote')}}">Credit Note</a></li>
                                <li><a href="{{url('debitnote')}}">Debit Note</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThreeThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-file"></i> Reports
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThreeThree">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('day_report')}}">Day Reports</a></li>
                                <li><a href="{{url('report')}}">All Reports</a></li>

                            </ul>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseFour" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-cog"></i> Settings
                        </a>
                    </div>
                    <div class="accordion-body collapse in" id="collapseFour">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('users')}}">Employee</a></li>
                                <li><a href="{{url('#')}}">Profile</a></li>
                                <li><a href="{{url('company')}}">Company Settings</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapse7" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="fa fa-calculator"></i> Calculator
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapse7">
                        <div class="panel-body">
                            <form name="Calc" id="calc">
                                <div class="formSep input-group input-group-sm">
                                    <input class="form-control" name="Input" type="text"/>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" name="clear" value="c" onclick="Calc.Input.value = ''">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </span>
                                </div>
                                <div class="form-group">
                                    <input class="btn form-control btn-default input-sm" name="seven" value="7" onclick="Calc.Input.value += '7'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="eight" value="8" onclick="Calc.Input.value += '8'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="nine" value="9" onclick="Calc.Input.value += '9'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="div" value="/" onclick="Calc.Input.value += ' / '" type="button">
                                </div>
                                <div class="form-group">
                                    <input class="btn form-control btn-default input-sm" name="four" value="4" onclick="Calc.Input.value += '4'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="five" value="5" onclick="Calc.Input.value += '5'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="six" value="6" onclick="Calc.Input.value += '6'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="times" value="x" onclick="Calc.Input.value += ' * '" type="button">
                                </div>
                                <div class="form-group">
                                    <input class="btn form-control btn-default input-sm" name="one" value="1" onclick="Calc.Input.value += '1'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="two" value="2" onclick="Calc.Input.value += '2'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="three" value="3" onclick="Calc.Input.value += '3'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="minus" value="-" onclick="Calc.Input.value += ' - '" type="button">
                                </div>
                                <div class="formSep form-group">
                                    <input class="btn form-control btn-default input-sm" name="dot" value="." onclick="Calc.Input.value += '.'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="zero" value="0" onclick="Calc.Input.value += '0'" type="button">
                                    <input class="btn form-control btn-default input-sm" name="DoIt" value="=" onclick="Calc.Input.value = Math.round( eval(Calc.Input.value) * 1000)/1000" type="button">
                                    <input class="btn form-control btn-default input-sm" name="plus" value="+" onclick="Calc.Input.value += ' + '" type="button">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="push"></div>
        </div>

    </div>

</div>
<script>
    $("option").each(function() {
        $(this).text($(this).text().charAt(0).toUpperCase() + $(this).text().slice(1));
    });
</script>

{{--<script src="{{asset('admin')}}/js/jquery.min.js"></script>--}}
<script src="{{asset('admin')}}/js/jquery-migrate.min.js"></script>
<script src="{{asset('admin')}}/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js"></script>
<!-- touch events for jquery ui-->
{{--<script src="{{asset('admin')}}/js/forms/jquery.ui.touch-punch.min.js"></script>--}}
<!-- easing plugin -->
<script src="{{asset('admin')}}/js/jquery.easing.1.3.min.js"></script>
<!-- smart resize event -->
<script src="{{asset('admin')}}/js/jquery.debouncedresize.min.js"></script>
<!-- js cookie plugin -->
<script src="{{asset('admin')}}/js/jquery_cookie_min.js"></script>
<!-- main bootstrap js -->
<script src="{{asset('admin')}}/bootstrap/js/bootstrap.min.js"></script>
<!-- bootstrap plugins -->
<script src="{{asset('admin')}}/js/bootstrap.plugins.min.js"></script>
<!-- typeahead -->
<script src="{{asset('admin')}}/lib/typeahead/typeahead.min.js"></script>
<!-- enhanced select (chosen) -->
<script src="{{asset('admin')}}/lib/chosen/chosen.jquery.min.js"></script>

<!-- code prettifier -->
<script src="{{asset('admin')}}/lib/google-code-prettify/prettify.min.js"></script>
<!-- sticky messages -->
<script src="{{asset('admin')}}/lib/sticky/sticky.min.js"></script>
<!-- lightbox -->
<script src="{{asset('admin')}}/lib/colorbox/jquery.colorbox.min.js"></script>
<!-- jBreadcrumbs -->
<script src="{{asset('admin')}}/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
<!-- hidden elements width/height -->
<script src="{{asset('admin')}}/js/jquery.actual.min.js"></script>
<!-- custom scrollbar -->
<script src="{{asset('admin')}}/lib/slimScroll/jquery.slimscroll.js"></script>
<!-- fix for ios orientation change -->
<script src="{{asset('admin')}}/js/ios-orientationchange-fix.js"></script>
<!-- to top -->
<script src="{{asset('admin')}}/lib/UItoTop/jquery.ui.totop.min.js"></script>
<!-- mobile nav -->
<script src="{{asset('admin')}}/js/selectNav.js"></script>
<!-- moment.js date library -->
<script src="{{asset('admin')}}/lib/moment/moment.min.js"></script>

<!-- common functions -->
<script src="{{asset('admin')}}/js/pages/gebo_common.js"></script>


<script>
    $(document).ready(function() {
        //* jQuery.browser.mobile (http://detectmobilebrowser.com/)
        //* jQuery.browser.mobile will be true if the browser is a mobile device
        (function(a){jQuery.browser.mobile=/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
        //replace themeforest iframe
        if(jQuery.browser.mobile) {
            if (top !== self) top.location.href = self.location.href;
        }
    });
</script>




</body>

<!-- Mirrored from gebo-admin-3.tzdthemes.com/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jul 2018 09:45:03 GMT -->
</html>


<!DOCTYPE HTML>
<html>
<head>
    <title>@yield('title')</title>

    <title>MUS Technologies</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Pooled Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    {{--<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>--}}
    <link href="{{asset('adminDesign/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('adminDesign/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{asset('adminDesign/css/morris.css')}}" type="text/css"/>
    <link href="{{asset('adminDesign/css/font-awesome.css')}}" rel="stylesheet">
    <script src="{{asset('adminDesign/js/jquery-2.1.4.min.js')}}"></script>
    <link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'/>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('adminDesign/css/icon-font.min.css')}}" type='text/css' />
    <script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('css/bootstrap/js/dataTables.bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/dataTables.bootstrap.min.css')}}">
    @yield('custom_css')
</head>
<style>
input,textarea{
        text-transform:uppercase!important;
    }
    h1,h2,h3,h4,h5,h6,p,td{
        text-transform:uppercase!important;
    }

    /*.left-content{*/
        /*background-color: #ffffff;*/
    /*}*/
   .well{
       background-color: #ffffff;
       /*padding: 2px!important;*/
   }
@media print{
    .sidebar-menu{
        display: none;
    }
    .header-main{
        display: none;
    }
    .left-content{
        width: 100%!important;
    }
    th{
        color: #000000!important;
        background-color: #ffffff!important;
    }
    button{
        display: none;
    }
    a{
        display: none!important;
    }
}
</style>
@yield('manual_style_code')

<body>
<div class="page-container">
    <!--/content-inner-->
    <div class="left-content"> <!-- left corner start here -->
        <div class="mother-grid-inner">
            <!--header start here-->
            <div class="header-main">
                <div class="logo-w3-agile">

                    <h1> <img src="{{ asset('masterTemplet/dist/img/logo.png')}}" width="40" /><a href="www.itcentre.work" style="font-size: 15px;color: #000">IT Centre</a></h1>
                </div>
                <div class="profile_details w3l">
                    <ul>
                        <li class="dropdown profile_details_drop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile_img">
                                    <span class="prfil-img"><img src="{{asset('adminDesign/images/usericon.png')}}" width="40" alt=""> </span>
                                    <div class="user-name" style="margin-left: 6px;margin-top: 14px">
                                        <p style="color: #fff;font-size: 12px;margin-top: 10px;margin-left: 1px;">{{ Auth::user()->name }}</p>
                                    </div>
                                    <i class="fa fa-angle-down" style="background-color: #22BEEF;font-size: 1.0em;padding: 5px;border-radius: 10px"></i>
                                    <i class="fa fa-angle-up"style="background-color: #22BEEF;font-size: 1.0em;padding: 5px;border-radius: 10px"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                            <ul class="dropdown-menu drp-mnu">
                                <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
                                <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
                                <?php if( Auth::check()) {?>
                                <li> <a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> Logout</a> </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"> </div>
            </div>
            <br>

{{---------------------------- content area ------------------------------------------------------}}
<div class="well">
            @yield('content')

</div>
{{---------------------------- content area ------------------------------------------------------}}



            <div class="inner-block">

            </div>

        </div>
    </div>  <!-- left content templet ends here --------------------------------------- -->



            <div class="sidebar-menu">
                <header class="logo1">
                    <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
                </header>
                <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
                <div class="menu">
                    <ul id="menu" >
                        <li><a href="{{url('home')}}"><i class="fa fa-tachometer"></i> <span>Dashboard</span><div class="clearfix"></div></a></li>


                        <li id="menu-academico" ><a href="#"><i class="fa fa-file-text-o"></i>  <span>Transaction</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="{{url('sale')}}"><i class="fa fa-mail-forward (alias)"></i> &nbsp;  Sale</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('purchase')}}"><i class="fa fa-mail-reply (alias)"></i> &nbsp; Purchase</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('saleReturn')}}"><i class="fa fa-arrow-left"></i> &nbsp;  Sale Return</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('purchaseReturn')}}"><i class="fa fa-arrow-right"></i> &nbsp;  Purchase Return&nbsp;</a></li>
                            </ul>
                        </li>
                        <li id="menu-academico" ><a href="#"><i class="fa fa-gift"></i>  <span>Inventory</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="{{url('product')}}"><i class="fa fa-film"></i> &nbsp;&nbsp;  Products</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('product_type')}}"><i class="fa  fa-edit (alias)"></i> &nbsp;&nbsp; Category</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('brand')}}"><i class="fa fa-flag"></i> &nbsp;&nbsp;  Brand</a></li>
                            </ul>
                        </li>

                        <li id="menu-academico" ><a href="{{url('ledgerPanel')}}"><i class="fa fa-group (alias)"></i>  <span>Ledger</span><div class="clearfix"></div></a>

                        </li>

                        <li id="menu-academico" ><a href="#"><i class="fa fa-rupee (alias)"></i>  <span>Accounts</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="{{url('payment')}}"><i class="fa   fa-minus"></i> &nbsp;&nbsp;  Payments</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('receipt')}}"><i class="fa  fa-plus"></i> &nbsp;&nbsp; Receipts</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('otherCharge')}}"><i class="fa fa-random"></i> &nbsp; Contra Entry.&nbsp;</a></li>
                            </ul>
                        </li>

                        <li id="menu-academico" ><a href="#"><i class="fa fa-bar-chart"></i>  <span>Allowence</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="{{url('creditnote')}}"><i class="fa  fa-minus-square"></i> &nbsp;&nbsp;  Credit Notes</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('debitnote')}}"><i class="fa  fa-plus-square"></i> &nbsp;&nbsp; Debit Notes</a></li>
                            </ul>
                        </li>

                        <li><a href="{{url('report')}}"><i class="fa fa-files-o" aria-hidden="true"></i><span>Reports</span><div class="clearfix"></div></a></li>

                        <li id="menu-academico" ><a href="#"><i class="fa fa-cog"></i>  <span>Settings</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="{{url('users')}}"><i class="fa  fa-group (alias)"></i> &nbsp;&nbsp;  Employees</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="{{url('#')}}"><i class="fa  fa-camera"></i> &nbsp;&nbsp; Profile</a></li>
                            </ul>
                        </li>

                    </ul>





                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <script>

            var toggle = true;

            $(".sidebar-icon").click(function() {
                if (toggle)
                {
                    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                    $("#menu span").css({"position":"absolute"});
                }
                else
                {
                    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                    setTimeout(function() {
                        $("#menu span").css({"position":"relative"});
                    }, 400);
                }

                toggle = !toggle;
            });
            $('option').each(function() {
                t = $(this).text();
                $(this).text(t.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }))
            });
        </script>
        <!--js -->
        @yield('custom_js')

        <script src="{{asset('adminDesign/js/jquery.nicescroll.js')}}"></script>
        <script src="{{asset('adminDesign/js/scripts.js')}}"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('adminDesign/js/bootstrap.min.js')}}"></script>
        <!-- /Bootstrap Core JavaScript -->
        <!-- morris JavaScript -->
        <script src="{{asset('adminDesign/js/raphael-min.js')}}"></script>
        <script src="{{asset('adminDesign/js/morris.js')}}"></script>

        <script>
            $(document).ready(function() {
                //BOX BUTTON SHOW AND CLOSE
                jQuery('.small-graph-box').hover(function() {
                    jQuery(this).find('.box-button').fadeIn('fast');
                }, function() {
                    jQuery(this).find('.box-button').fadeOut('fast');
                });
                jQuery('.small-graph-box .box-close').click(function() {
                    jQuery(this).closest('.small-graph-box').fadeOut(200);
                    return false;
                });

                //CHARTS



            });
        </script>
@yield('newscript')
</body>
</html>

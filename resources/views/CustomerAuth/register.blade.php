<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Website CSS style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

 <style>
     /*
/* Created by Filipe Pina
 * Specific styles of signin, register, component
 */
     /*
      * General styles
      */
     #playground-container {
         height: 500px;
         overflow: hidden !important;
         -webkit-overflow-scrolling: touch;
     }
     body, html{
         height: 100%;
         background-repeat: no-repeat;
         background:url(https://i.ytimg.com/vi/4kfXjatgeEU/maxresdefault.jpg);
         font-family: 'Oxygen', sans-serif;
         background-size: cover;
         background-attachment: fixed;
     }

     .main{
         margin:50px 15px;
     }

     h1.title {
         font-size: 50px;
         font-family: 'Passion One', cursive;
         font-weight: 400;
     }

     hr{
         width: 10%;
         color: #fff;
     }

     .form-group{
         margin-bottom: 15px;
     }

     label{
         margin-bottom: 15px;
     }

     input,
     input::-webkit-input-placeholder {
         font-size: 11px;
         padding-top: 3px;
     }

     .main-login{
         background-color: #fff;
         /* shadows and rounded borders */
         -moz-border-radius: 2px;
         -webkit-border-radius: 2px;
         border-radius: 2px;
         -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
         -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
         box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

     }
     .form-control {
         height: auto!important;
         padding: 8px 12px !important;
     }
     .input-group {
         -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
         -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
         box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
     }
     #button {
         border: 1px solid #ccc;
         margin-top: 28px;
         padding: 6px 12px;
         color: #666;
         text-shadow: 0 1px #fff;
         cursor: pointer;
         -moz-border-radius: 3px 3px;
         -webkit-border-radius: 3px 3px;
         border-radius: 3px 3px;
         -moz-box-shadow: 0 1px #fff inset, 0 1px #ddd;
         -webkit-box-shadow: 0 1px #fff inset, 0 1px #ddd;
         box-shadow: 0 1px #fff inset, 0 1px #ddd;
         background: #f5f5f5;
         background: -moz-linear-gradient(top, #f5f5f5 0%, #eeeeee 100%);
         background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f5f5f5), color-stop(100%, #eeeeee));
         background: -webkit-linear-gradient(top, #f5f5f5 0%, #eeeeee 100%);
         background: -o-linear-gradient(top, #f5f5f5 0%, #eeeeee 100%);
         background: -ms-linear-gradient(top, #f5f5f5 0%, #eeeeee 100%);
         background: linear-gradient(top, #f5f5f5 0%, #eeeeee 100%);
         filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f5f5f5', endColorstr='#eeeeee', GradientType=0);
     }
     .main-center{
         margin-top: 30px;
         margin: 0 auto;
         max-width: 800px;
         padding: 10px 40px;
         background:#009edf;
         color: #FFF;
         text-shadow: none;
         -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
         -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
         box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

     }
     span.input-group-addon i {
         color: #009edf;
         font-size: 17px;
     }

     .login-button{
         margin-top: 5px;
     }

     .login-register{
         font-size: 11px;
         text-align: center;
     }

 </style>

    <title>Registration</title>
</head>
<body>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h2 class="title">SNS INFOTECH</h2>
                <hr />
            </div>
        </div>
        <div class="main-login main-center">
            <h3 class="text-center">Registration Form</h3>
            <hr />
            <?php echo Form::open(array('route' => 'storeCustomerFront')); ?>
                <div class="row">

                <div class="col-md-6">
                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Your Name</label>
                     @if ($errors->has('customer_name'))
                    <span style="color:red;font-weight: 600">The Name field is Required|Not more than 40 characters.</span>
                    @endif
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="customer_name" id="name"  placeholder="Enter your Name" required>

                        </div>
                    </div>
                </div>
                </div>
                    <div class="col-md-6">
                <div class="form-group">
                    <label for="address" class="cols-sm-2 control-label">Address</label>
                    @if ($errors->has('address'))
                        <span style="color:red;font-weight: 600">{{ $errors->first('address') }}</span>
                    @endif
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i></span>
                             <textarea required class="form-control" name="address"></textarea>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
               </div>
                </div> <!-- row end here -->
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="email" class="cols-sm-2 control-label">City</label>
                          @if ($errors->has('city'))
                              <span style="color:red;font-weight: 600">{{ $errors->first('city') }}</span>
                          @endif
                          <div class="cols-sm-10">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-building fa" aria-hidden="true"></i></span>
                                  <input type="text" class="form-control" name="city" id="city"  placeholder="Enter your City" required>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="email" class="cols-sm-2 control-label">Zip</label>
                          @if ($errors->has('zip'))
                              <span style="color:red;font-weight: 600">{{ $errors->first('zip') }}</span>
                          @endif
                          <div class="cols-sm-10">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-street-view fa" aria-hidden="true"></i></span>
                                  <input type="text" class="form-control" name="zip" id="zip"  placeholder="Enter your Zip" required>
                              </div>
                          </div>
                      </div>
                  </div>
              </div> <!-- row end here -->
             <div class="row">
                 <div class="col-md-6">
                     <div class="form-group">
                         <label for="email" class="cols-sm-2 control-label">State</label>
                         @if ($errors->has('state'))
                             <span style="color:red;font-weight: 600">{{ $errors->first('state') }}</span>
                         @endif
                         <div class="cols-sm-10">
                             <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-university fa" aria-hidden="true"></i></span>
                                 <input type="text" class="form-control" name="state" id="state"  placeholder="Enter your State" required>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <label for="email" class="cols-sm-2 control-label">Your Mobile</label>
                         @if ($errors->has('mobile'))
                             <span style="color:red;font-weight: 600">{{ $errors->first('mobile') }}</span>
                         @endif
                         <div class="cols-sm-10">
                             <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-mobile fa-4x" aria-hidden="true" style="font-size: 24px"></i></span>
                                 <input type="text" class="form-control" name="mobile" id="mobile"  placeholder="Enter your Mobile No" required>
                             </div>
                         </div>
                     </div>
                 </div>
             </div> <!-- row end here -->

                <div class="row">
                 <div class="col-md-6">
                     <div class="form-group">
                         <label for="email" class="cols-sm-2 control-label">Your Email</label>
                         @if ($errors->has('email'))
                             <span style="color:red;font-weight: 600">{{ $errors->first('email') }}</span>
                         @endif
                         <div class="cols-sm-10">
                             <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                 <input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" required>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <label for="password" class="cols-sm-2 control-label">Password</label>
                         @if ($errors->has('password'))
                             <span style="color:red;font-weight: 600">{{ $errors->first('password') }}</span>
                         @endif
                         <div class="cols-sm-10">
                             <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                 <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" required>
                             </div>
                         </div>
                     </div>
                 </div>
             </div> <!-- row end here -->

                <div class="form-group ">
                    <button type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                </div>

            {{form::close()}}
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
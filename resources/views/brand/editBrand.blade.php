@extends('layouts.adminPanel')
@section('title')
    Brand:Edit
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('brand')}}">Brand Panel</a>
    </li>

    <li>
        <a href="{{url('createBrand')}}">Create Brand</a>
    </li>

    <li>
        <a href="{{url('#')}}">Edit Brand</a>
    </li>
@endsection

@section('content')
<h3 class="heading">Edit Brand</h3>
    {!! Form::open(['method'=>'get', 'url' =>['updateBrand',$editBrand->id]]) !!}

    <div class="row" style="padding: 40px">
        <div class="col-md-6 col-sm-6 col-md-offset-2 col-sm-offset-2" style="margin-top: 10px">
                <input type="text"  class="form-control" name="brand_name" id="brand_name" placeholder="Brand Name"  value="{{$editBrand->brand_name}}" required>
        </div>
        <div class="col-md-4 col-sm-4"style="margin-top: 10px">
            <input type="submit" class="btn btn-success" value="Update">
            <a href="{{url('brand')}}" class="btn btn-info"><span class="glyphicon glyphicon-backward"></span> Back</a>
        </div>
    </div><!-- /.row -->






    {!! Form::close() !!}

    {{--inner content here ------------------------------------}}
    <br>
@endsection
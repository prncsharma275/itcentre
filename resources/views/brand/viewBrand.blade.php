@extends('layouts.adminPanelTable')
@section('title')
    Brand:Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('brand')}}">Brand Panel</a>
    </li>
@endsection
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif
    <h3 class="heading">Brand</h3>
    <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createBrand')}}"><i class="splashy-document_letter_add"></i> New Brand</a>

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif
    <table class="table table-striped table-bordered dTableR" id="dt_a">

        <thead>
        <tr>

            <th style="text-align: center">Brand Name &nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th style="text-align: center">Action&nbsp;&nbsp;&nbsp;&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        @foreach($brand as $brand)
            <tr>
                <td style="text-align: center">{{$brand->brand_name}}</td>

                <td style="text-align: center">
                    <a href="editBrand/{{$brand->id}}"  title="EDIT"><i class="splashy-document_letter_edit"></i></a>
                    <a href="deleteBrand/{{$brand->id}}" onclick="return ConfirmDelete()" title="EDIT"><i class="splashy-document_letter_remove"></i></a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        } );

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    {{--inner content here ------------------------------------}}
    <br>
@endsection
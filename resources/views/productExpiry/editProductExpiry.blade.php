<html>
<head>
    <title>SNS</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
    <script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
    <script src="{{ asset('js/comboBox.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <style>
        input[type="text"]{
            height: 50px;
            width: 100%;
        }
        input[type="date"]{
            height: 50px;
            width: 75%!important;
        }
        .ui-icon .ui-icon-circle-triangle-e{
            color: black;
        }
    </style>
</head>

    <h1 class="page-header text-center">Edit Product ({{$editProduct->belongsToExp_product_date->product_name}})</h1>

<?php    ?>
{!! Form::open(['method'=>'get', 'class'=>'well form-horizontal', 'url' =>['updateProductExpiry',$editProduct->id]]) !!}

<div class="container">

    <div class="row">

        <div class="col-md-6 form-group">
            <div class="ui-widget">
                <label>Product Category</label><br>
                <select name="product_name" id="combobox">
                    <option selected="selected" value="{{$editProduct->product_id}}">{{$editProduct->belongsToExp_product_date->product_name}}</option>
                    <?php  $product_type=\App\Product::All();?>
                    @foreach($product_type as $product_type)
                        <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>

                    @endforeach
                </select>
            </div>

        </div>


        <div class="col-md-6 form-group">
            <label>Batch NO<span style="color: red; font-size: 15px">*</span></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input type="text" class="form-control" name="batchNo" placeholder="Enter Batch No Here" value="{{$editProduct->batch_no}}">
            </div>

        </div>

    </div> <!-- row end here -->

    <div class="row">
        <div class="col-md-6 form-group">
            <label>Mfg.Date<span style="color: red; font-size: 15px"> (dd/mm/yyyy)*</span></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input type="date" class="form-control" name="mfgDate" placeholder="Mfg.Date" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" value="{{date('d-m-Y', strtotime(str_replace('-', '/', $editProduct->mfgDate)))}}">
            </div>

        </div>

        <div class="col-md-6 form-group">
            <label>Exp.Date<span style="color: red; font-size: 15px"> (dd/mm/yyyy)*</span></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input type="date" class="form-control" name="expDate" placeholder="Exp.Date" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" value="{{date('d-m-Y', strtotime(str_replace('-', '/', $editProduct->expDate)))}}">
            </div>
            {{--<input required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" value="" name="dates_pattern0" id="dates_pattern0" list="dates_pattern0_datalist" placeholder="Try it out." type="text">--}}
        </div>

    </div> <!-- row end here -->

    <div class="row">
        <div class="col-md-6 form-group">
            <label>Opening Stock<span style="color: red; font-size: 15px">*</span></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input type="text" class="form-control" name="quantity" placeholder="Opening Stock"  value="{{$editProduct->quantity}}" required>
            </div>

        </div>

        <div class="col-md-6 form-group">
            <label>Selling Price<span style="color: red; font-size: 15px"> (optional)</span></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input type="text" class="form-control" name="sellingPrice" placeholder="Selling Price" value="{{$editProduct->selling_price}}">
            </div>

        </div>

    </div> <!-- row end here -->
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-hdd"></span></button>

            <button type="reset" class="btn btn-info" >Reset <span class="glyphicon glyphicon-refresh"></span></button>
            <a href="{{url('searchProductExpiry?product_name=')}}{{$editProduct->product_id}}" class="btn btn-danger" >Cancel <span class="glyphicon glyphicon-remove"></span></a>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label"></label>
    <div class="col-md-4">

    </div>
</div>

</div>

    </div> <!-- container end here -->




{!! Form::close() !!}

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<script>
    $(function() {
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>
</html>
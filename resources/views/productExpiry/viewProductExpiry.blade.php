<html>
<title>aurthosurgical</title>
<script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('css/bootstrap/js/dataTables.bootstrap.min.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/bootstrap/css/dataTables.bootstrap.min.css')}}">

<body style="margin: 20px">
<a class="btn btn-info" style="margin-left:90px;margin-bottom: 20px; " href="{{url('createProductExpiry')}}">+Add New Product Expiry Details</a>
<a class="btn btn-warning" style="margin-left:90px;margin-bottom: 20px; " href="{{url('productExpiry')}}"> Back To Expiry Page </a>
<a class="btn btn-primary" style="margin-left:90px;margin-bottom: 20px; " href="{{url('home')}}"> Back To Home Dashboard </a>
<table class="table" id="table">
    <thead>
    <tr>

        <th class="text-center">Product Name</th>
        <th class="text-center">Batch No</th>
        <th class="text-center">Mfg.Date</th>
        <th class="text-center">Exp.Date</th>
        <th class="text-center">Quantity</th>
        <th class="text-center">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($exp_product_details as $item)

        <tr>
            <td>{{$item->belongsToExp_product_date->product_name}}</td>
            <td>{{$item->batch_no}}</td>
            <td>{{date('d-m-Y', strtotime(str_replace('-', '/', $item->mfgDate)))}}</td>
            <td>{{date('d-m-Y', strtotime(str_replace('-', '/', $item->expDate)))}}</td>
            <td>{{$item->quantity}}</td>

            <td>
                <a href="editProductExpiry/{{$item->id}}" class="btn btn-info">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </a>
                <a href="deleteProductExpiry/{{$item->id}}/{{$item->product_id}}" class="btn btn-danger" onclick="return ConfirmDelete()">
                    <span class="glyphicon glyphicon-edit"></span> Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );

    //    delete commande
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }


</script>
</body>
</html>
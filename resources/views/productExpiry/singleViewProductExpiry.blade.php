<html>
<head>
    <title>SNS</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <style>
        input[type="text"]{
            height: 50px;
            width: 100%;
        }
        .ui-icon .ui-icon-circle-triangle-e{
            color: black;
        }
    </style>
</head>
<body style="margin: 10px">

<h1 class="text-center">Full View ({{$product->product_name}})</h1>


<form class="well form-horizontal" action="storeProduct" method="get"  id="contact_form">
    <div class="container">

        <div class="row">

            <div class="col-md-6 form-group">
                <label>Product Type<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" name="product_name" value="{{$product->product_type_name}}" readonly>
                </div>

            </div>


            <div class="col-md-6 form-group">
                <label>Product Name<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" name="product_name" value="{{$product->product_name}}" readonly>
                </div>

            </div>

        </div>
        <!-- row end here -->
        <div class="row">
            <div class="col-md-6 form-group">
                <label>Product Code<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" name="product_code" value="{{$product->productCode}}" readonly>
                </div>

            </div>

            <div class="col-md-6 form-group">
                <label>Batch NO<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input type="text" class="form-control" name="batchNo" value="{{$product->batchNo}}" readonly>
                </div>

            </div>
        </div> <!-- end row here -->
        <div class="row">
            <div class="col-md-6 form-group">
                <label>Diameter<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input type="text" class="form-control" name="diameter" value="{{$product->diameter}}" readonly>
                </div>

            </div>
            <div class="col-md-6 form-group">
                <label>Size<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" name="size" value="{{$product->size}}" readonly>
                </div>

            </div>

        </div> <!-- row end here -->


        <div class="row">


            <div class="col-md-6 form-group">
                <label>Mfg.Date<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input type="text" class="form-control datepicker" name="mfgDate" value="{{$product->MfgDate}}" readonly>
                </div>

            </div>

            <div class="col-md-6 form-group">
                <label>Exp.Date<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input type="text" class="form-control datepicker" name="expDate" value="{{$product->expDate}}" readonly>
                </div>

            </div>

        </div> <!-- row end here -->

        <div class="row">


            <div class="col-md-6 form-group">
                <label>Tax %<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input type="text" class="form-control" name="tax" value="{{$product->product_vat_type}}" readonly>
                </div>

            </div>

            <div class="col-md-6 form-group">
                <label>Selling Price<span style="color: red; font-size: 15px"> (optional)</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input type="text" class="form-control" name="sellingPrice" value="{{$product->selling_price}}" readonly>
                </div>

            </div>

        </div> <!-- row end here -->

        <div class="form-group">
            <label class="col-md-4 control-label"></label>
            <div class="col-md-4">

                <a href="{{url('product')}}" class="btn btn-danger" >Back <span class="glyphicon glyphicon-backward"></span></a>
                <a href="{{url('editProduct')}}/{{$product->id}}" class="btn btn-success" >Edit <span class="glyphicon glyphicon-edit"></span></a>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">

        </div>
    </div>

    </div>
</Form>

<!-- form row end here -->
{{---------------------------------------------------------------------------------}}

</body>
</html>

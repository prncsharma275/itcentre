<html>
<head>
    <title>Aurthosurgical:Product Expiry Create</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
    <script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
    <script src="{{ asset('js/comboBox.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <style>
        input[type="text"]{
            height: 50px;
            width: 100%;
        }
        input[type="date"]{
            height: 50px;
            width: 75%!important;
        }
        .ui-icon .ui-icon-circle-triangle-e{
          color: black;
        }
    </style>
</head>
<body style="margin: 10px">

    <h1 class="text-center">Product Expiry</h1>
       <a href="{{url('createProductExpiry')}}" class="btn btn-success btn-lg">+Add New Product Date</a>
       <a href="{{url('home')}}" class="btn btn-danger btn-lg" style="margin-left: 900px">Back To Dashboard</a>
    <hr>
<div class="row">

  <div class="col-md-8 col-md-offset-2">
      <form class="well form-horizontal" action="searchProductExpiry" method="get"  id="contact_form">
 <h2 class="text-center">View Details </h2>

              <div class="row">
                  <div class="col-md-6 col-md-offset-3">

                      <div class="ui-widget">
                          <label>Type Product Name</label>
                          <select name="product_name" id="combobox" required>
                              <option value="">Select one</option>
                              <?php  $product_type=\App\Product::All();?>
                              @foreach($product_type as $product_type)
                                  <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>

                              @endforeach
                          </select>
                      </div>


                    </div>
              </div>
              <!-- row end here -->

         <div class="row" style="margin-top: 10px;">
             <div class="col-md-6 col-md-offset-5">
                 <div class="form-group">

                         <button type="submit" class="btn btn-warning" >Search <span class="glyphicon glyphicon-search"></span></button>

                 </div>
             </div>
         </div>





    </Form>
  </div>

</div>


    <!-- form row end here -->
    {{---------------------------------------------------------------------------------}}


@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

</body>
</html>

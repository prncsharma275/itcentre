<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MUS:INVOICE</title>
    <link rel="stylesheet" href="style.css" media="all" />
    {{--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="{{asset('css/bootstrap/boot4.1.css')}}" rel="stylesheet">
    <style type="text/css" media="print">
        .rupees {}


        @page {
            size: auto;
            /* auto is the initial value */
            margin-left: 15px!important;
            /* this affects the margin in the printer settings */
            margin-right: 5px!important;
            /* this affects the margin in the printer settings */
            margin-top: 10px;
            /* this affects the margin in the printer settings */
            margin-bottom: 5px;
            background-color: gainsboro;
            font-size: 15px;
            /*margin: px;*/

        }



        /*@media print{@page {size: landscape}}*/

        @media print {
            table.report-container {
                page-break-after:always;
            }
            thead.report-header {
                display:table-header-group;
            }
            tfoot.report-footer {
                display:table-footer-group;
            }

            tfoot.report-footer:after {
                counter-increment: page 1;
                /*counter-reset: pages 1;*/
                content: "Page " counter(page) " of " counter(pages);
            }
            .hidden-print {
                display: none !important;
            }
            .report-container
            {
              margin-left: 12px;
              margin-right: 12px;
            }
            #desc
            {
              width: 520px !important;
            }
            #chln
            {
              font-size: 14px!important;
            }
        }

    </style>
</head>

<body style="">
<?php

$customer=DB::table('customers')->find($printBill->customer_id);


 ?>
 <?php $comp = \App\Company_detail::find(1)?>

<table class="report-container">
    <thead class="report-header">
    <tr>
        <th class="report-header-cell">
            <div class="header-info">
                <p class="text-center" id="pageno">CHALLAN RETURN</p>
                <table width="100%" border="1">
                    <tr>
                      <td style="width: 500px">
                          <p class="text-left" style="font-size: 14px;margin-bottom: -1px">&nbsp; </p>
                          <h4 class="text-left" style="margin-bottom: -1px">{{ $comp->company_name }}&nbsp;</h4>
                          <h6 class="text-left">{{ $comp->street_1 }}&nbsp;{{ $comp->street_2 }}<br>
                              {{ $comp->city }},{{ $comp->state }},PIN:{{ $comp->zip }}<br>
                              Email:{{ $comp->email }},<br>Mobile:  {{ $comp->mobile }}
                              <br>

                          </h6>
                      </td>
                        <td style="width: 600px">  <p class="text-left" style="font-size: 14px;margin-bottom: -1px">&nbsp; Return From</p><h4 class="text-left">{{ $customer->ledger_name }}&nbsp;</h4>

                            <h6 class="text-left">{{ $customer->address }}
                                <br>Pin &#45;{{ $customer->zip }},</br>
                                Email-{{ $customer->email }},<br>Cell-{{ $customer->mobile }}
                                <br>&nbsp;</h6></td>

                        <td style="width: 400px">
                            <h5 id="chln" class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Challan Return No. - {{ $printBill->invoice_no }}</h5>
                            <h5  id="chln"class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Challan Return Date -  <?php echo date("d/m/Y", strtotime($printBill->challan_date)); ?> </h5>
                            {{-- <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Entry No. - </h5>
                            <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Entry Date. - </h5> --}}
                        </td>
                    </tr>
                </table>
                <table width="100%"  style="text-align: center;border:1px solid black">
                    <tr>
                      <td style="border:1px solid black">Sl No</td>
                        <td style="border:1px solid black">Product</td>
                        {{-- <td style="width: 65px!important;">HSN</td> --}}
                        <td style="width: 326px!important;border:1px solid black">Description.</td>

                       <td style="width: 100px!important;border:1px solid black">Qnt.</td>
                         {{--
                        <td style="width: 60px!important;">Disc.%</td>
                        <td style="width: 60px!important;">GST %</td>
                        <td style="width: 110px!important;">GST Amt.</td>
                        <td style="width: 110px!important;">Total Amt.</td> --}}
                    </tr>
                    <?php $challan_child=\App\chalan_return_child::where('challan_id',$printBill->invoice_no)->get(); ;

                    $k=0;
                     ?>
                    @foreach ($challan_child as $key )
                      <?php $k++; ?>

                    <tr style="">
                          <td style="border-right:1px solid black"><?php echo $k; ?></td>
                        <td style="border-right:1px solid black;text-align:left">{{$key->product_name}}</td>
                        {{-- <td style="width: 65px!important;">HSN</td> --}}
                           <td id="desc" style="width: 920px;border-right:1px solid black;text-align:left">{{$key->descr}}</td>
                        <td style="width: 70px!important;border-right:1px solid black">{{$key->quantity}}</td>


                         {{--
                        <td style="width: 60px!important;">Disc.%</td>
                        <td style="width: 60px!important;">GST %</td>
                        <td style="width: 110px!important;">GST Amt.</td>
                        <td style="width: 110px!important;">Total Amt.</td> --}}
                    </tr>
                  @endforeach

                </table>
                <table width="100%" border="1" style="text-align: center">
                    <tr>
                        <td style="text-align: left;padding:7px!important;">
                            <br><br><br>Customer's Seal and Signature
                        </td>
                        <td style="text-align: right;padding: 7px!important;">

                            <br><br><br>For,IT Centre Seal and Signature
                        </td>


                    </tr>

                </table>
            </div>
        </th>
    </tr>
    </thead>
    <tfoot class="report-footer">
    <tr>
        <td class="report-footer-cell">
            <div class="footer-info">


                <p class="hidden-print" style="padding-top:5px!important;">&nbsp;<button class="btn btn-success hidden-print" onclick="myFunction()">Print</button>
                    <a href="{{url('purchase_type')}}" class="btn btn-info hidden-print">Create New Invoice</a>
                    <a href="{{url('/challanreturn')}}" class="btn btn-primary hidden-print">Back</a></p>
                <p class="text-center" style="font-size: 18px;margin-bottom: 0;text-transform: uppercase!important;">SUBJECT TO  JURISDICATION</p>
                <p class="text-center" style="font-size: 14px">This is Computer Generated Invoice</p>

            </div>
        </td>
    </tr>
    </tfoot>
    <tbody class="report-content">
    <tr>

    </tr>
    </tbody>
</table>

<script>
    function myFunction() {
        window.print();
    }
</script>
</body>

</html>

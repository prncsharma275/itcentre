@extends('layouts.adminPanel')
@section('title')
    Create Sale
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('challan')}}">Challan Panel</a>
    </li>

    <li>
        <a href="{{url('#')}}">Create Challan</a>
    </li>
@endsection
@section('content')
    <br>
    <h3 class="heading">New Challan Entry</h3>
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>

    @endif

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    {{-- ----------------------------------------inner content here --------------------------------------------------------}}
    <?php echo Form::open(array('route' => 'storeChallan', 'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
    <div class="row"> <!-- row start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Select Customer / <a data-toggle="modal" data-target="#myModal" style="color: green!important;" href="">Create +New</a> </label>
                <select id="customer" name="customer" class="form-control" style="width: 99%!important;"   required>
                    <option value="">Select Customer</option>
                    <?php  $product_type = DB::table('customers')->orderBy('ledger_name', 'asc')->get(); ?>

                    @foreach($product_type as $product_type)
                        <option value="{{$product_type->id}}">{{$product_type->ledger_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Challan  No.<span style="color:#EB3E28;">*</span></label>
                <?php  $currentDate= date("Y/m/d");

                ?>
                <?php  $dateNew = DB::table('challan_session')->whereDate('startDate', '<=', $currentDate)->whereDate('closeDate', '>=', $currentDate)->first(); ?>

                <?php $sale_format = \App\Company_detail::find(1) ?>

                @if($dateNew->bill==0)
                    <?php $number=1;
                    $bilNum = sprintf("%04d", $number);


                    $challanNo=$sale_format->challan."$bilNum"."/".$dateNew->session."";
                    $session_id= $dateNew->id;

                    ?>
                @else
                    <?php $number=$dateNew->bill+1;
                    $bilNum = sprintf("%04d", $number);

                    $challanNo=$sale_format->challan."$bilNum"."/".$dateNew->session."";
                    $session_id= $dateNew->id;
                    ?>

                @endif

                <input type="text" class="form-control" tabindex="0" name="challan_no" value="<?php echo $challanNo ?>" id="inputSuccess2"readonly>
                <input type="hidden" class="form-control" name="session_id" value="<?php echo $session_id ?>" id="inputSuccess2">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Challan Date</label>
                <input type="text" name="challan_date" class="form-control datepicker" value="{{date("d/m/Y")}}">
            </div>
        </div>

    </div> <!-- row ends here -->

    <div class="row" id="retail"> <!-- row2 start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Customer Name<span style="color:#f0ad4e;font-size: 10px"> (In Case of Retail Customer)</span></label>
                {{--<input type="text" class="form-control" id="inputSuccess2" placeholder="Payment Type">--}}
                <input type="text" name="retail_customer_name" class="form-control" id="inputSuccess2"  placeholder="Retail Name">
            </div>
        </div>
        {{-------------------------------------- retail customer starts here ---------------------  --}}

        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">City<span style="color:#f0ad4e;font-size: 10px"> (In Case of Retail Customer)</span></label>
                <input type="text" name="retail_city" class="form-control" id="inputSuccess2"  placeholder="City">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Mobile No <span style="color:#f0ad4e;font-size: 10px"> (In Case of Retail Customer)</span></label>
                <input type="text" name="retail_mobile" class="form-control" placeholder="Mobile">
            </div>
        </div>


    </div> <!-- row2 ends here -------------------------------- -->
    <!-- row2 ends here -------------------------------- -->

    {{-------------------------------------- retail customer ends here ---------------------  --}}

    <br>

    <div class="table-responsive">
        <table id="items" class="table table-striped table-bordered table-condensed" style="font-size: 13px!important;">

            <tr style="background-color: #e9f3f8">
                <th>Select Item /  <a href="" data-toggle="modal" style="color:green;font-weight: 500" data-target="#myModal2">+New Item</a></th>
                <th>Desc.</th>
                <th>Qnty.</th>
                <th>Action</th>
            </tr>


            <tr class="item-row" style="border-bottom: solid 1px black">
                <td style="width: 25%;position: relative" class="item-name">

                    <select  name="rows[0][product]" id="product_name" class="new_product form-control" style="width:99%;"  required>
                        <option value="">Please select Item</option>
                        <?php  $product_type=\App\Product::orderBy('product_name', 'asc')->get();?>
                        @foreach($product_type as $product_type)
                            <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>
                        @endforeach
                    </select>
                </td>
                <td class="main_td"><textarea name="rows[0][desc]" id="desc" class="form-control"  style="height:34px!important"></textarea> </td>
                <td class="main_td"><input name="rows[0][qty]" class="qty form-control" required></td>
                <td>
                    <a class="btn btn-danger btn-sm addrow updateRow0"  href="javascript:;" >ADD</a> </td>
            </tr>





        </table>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 5px!important;">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <button type="submit" class="btn btn-info btn-sm"  name="submit"><i class="splashy-document_a4_add"></i> Save</button>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <button type="reset" class="btn btn-warning btn-sm" name="submit"><i class="splashy-refresh_backwards"></i> Reset</button>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <a href="{{url('challan')}}" class="btn btn-danger btn-sm"><i class="splashy-gem_cancel_1"></i> Cancel</a>
                </div>
            </div>
        </div>
    </div>
    {{form::close()}}


    {{-------------------------------------------------------------------------------------------------------------------------------}}
    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
        <form id="testform">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create New Customer (Basic Details) <span style="font-size: 12px;color:#EB3E28;">* You Can Fill Full Details Later In Party Panel</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-6">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">Customer Name/Company Name<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="customer_name" class="form-control" id="inputSuccess2" placeholder="Customer Name" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-6">
                                <div class="form-group has-success has-feedback">Address<span style="color:#EB3E28;">*</span></label>
                                    <textarea  type="text" name="customer_address" class="form-control" id="inputSuccess2" placeholder="Address"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">Zip</label>
                                    <input  type="text" name="customer_zip" class="form-control" id="inputSuccess2" placeholder="Your Zip">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">City<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="customer_city" class="form-control" id="inputSuccess2" placeholder="City"required>
                                </div>

                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">State<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="customer_state" class="form-control" id="inputSuccess2" placeholder="Your State">
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">Phone</label>
                                    <input  type="text" name="customer_phone" class="form-control" id="inputSuccess2" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">Mobile<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="customer_mobile" class="form-control" id="inputSuccess2" placeholder="Mobile"required>
                                </div>

                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">Email</label>
                                    <input  type="text" name="customer_email" class="form-control" id="inputSuccess2" placeholder="Email">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="form-group has-success has-feedback">Opening Balance</label>
                                    <input  type="text" name="customer_opening_balance" class="form-control" id="inputSuccess2" placeholder="Opening Balance">

                                </div>

                            </div>
                        </div>

                    </div>



                    <div class="modal-footer clearfix">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 col-md-offset-3">
                                <button type="reset" class="btn btn-success" data-dismiss="modal"><i class="fa fa-refresh"></i> Clear</button>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-4 col-sm-4">
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-4 col-sm-4">
                                <button type="submit" id="addnewCustomer"  class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </form>
    </div>
    {{------------------------------- modal for new customer creations---------------------------}}

    {{--------------------------------- modal for new customer creations---------------------------}}
    <div class="modal fade" id="myModal2"  role="dialog" aria-labelledby="myModalLabel">
        <form id="testform1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create New Product</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label>Product Category</label>
                                    <select name="product_type" id="product_type" style="width: 100%" onchange = "myFunction1()">
                                        <option value="">Select one</option>
                                        <option value="create">+Create New</option>
                                        <?php  $product_type=\App\Product_type::orderBy('product_name', 'asc')->get();?>
                                        @foreach($product_type as $product_type)
                                            <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label>Brand Name</label>
                                    <select name="brand" id="brand_name" style="width: 100%" onchange = "myFunction()">
                                        <option value="">Select one</option>
                                        <option value="create">+Create New</option>
                                        <?php  $product_type=\App\Brand::orderBy('brand_name', 'asc')->get();?>
                                        @foreach($product_type as $product_type)
                                            <option value="{{$product_type->id}}">{{$product_type->brand_name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">New Product Category<span style="color:green;font-size: 10px;font-weight: 600"> (Create New If Not in The List)</span></label>
                                    <input  type="text" name="new_product_type" class="form-control" id="new_product_type" placeholder="New Product Category" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">New Brand Name<span style="color:green;font-size: 10px;font-weight: 600"> (Create New If Not in The List)</span></label>
                                    <input  type="text" name="new_brand_name"  class="form-control" id="new_brand_name" placeholder="New Brand Name" disabled>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">Product Name<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="new_product_name" class="form-control" id="new_product_name" placeholder="Product Name" required>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success has-feedback">Product HSN Code<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="hsn_code" class="form-control" id="inputSuccess2" placeholder="HSN Code" required>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">IGST<span style="color:#EB3E28;">*</span><span style="font-size: 12px;color: #5B479F;font-weight: 500"> (Don't use % sign)</span></label>
                                    <input  type="number" name="product_igst" min="0"  onchange="copyTextValue(this);" class="form-control" id="product_igst" placeholder="IGST" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">CGST<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="product_cgst" class="form-control"   id="product_cgst" placeholder="CGST"readonly>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">SGST<span style="color:#EB3E28;">*</span></label>
                                    <input  type="text" name="product_sgst" class="form-control" id="product_sgst" placeholder="SGST"readonly>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">
                                    <label for="inputSuccess2">Selling Price</label>
                                    <input  type="number" name="product_selling_price" min="1" class="form-control" id="inputSuccess2" placeholder="Selling Price">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">MRP</label>
                                    <input  type="number" name="product_mrp" min="1" class="form-control" id="inputSuccess2" placeholder="MRP">
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">Opening Stock</label>
                                    <input  type="number" min="1" name="product_opening_stock" class="form-control" id="inputSuccess2" placeholder="Opening Stock">
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" id="btnReset"  class="btn btn-success" ><i class="fa fa-refresh"></i> Clear Data</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>
                        <button type="submit" id="addProduct"  class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-------------------------------------------------------------------------------------------------------------------------------}}
    <script>


        $(document).ready(function(){
            $('#retail').hide();
        });
        $('#customer').bind('change', function(event) {

            var i= $('#customer').val();
            if(i!=1) //Retail Customer ID
            {
                $('#retail').hide(); // hide the first one

            }
            else{
                $('#retail').show();
                $('#retail').find('input').attr('required','true');
            }

        });


    </script>
    <script>
        //    $('#customer').on('change', function(event) {
        //
        //        var i= $('#customer').val();
        //        if(i==1)
        //        {
        //            $('#retail').show(); // hide the first one
        //        }else{
        //
        //            $('#retail').hide();
        //       }
        //    }


        function myFunction() {
            var option_value = document.getElementById("brand_name").value;
            if (option_value == "create") {
//            document.getElementById('new_brand_name').readonly=false;
                $("#new_brand_name").removeAttr('disabled');
                $("#new_brand_name").attr('required',true);
            }else{
                $("#new_brand_name").attr('disabled',true);
            }
        }

        function myFunction1() {
            var option_value = document.getElementById("product_type").value;
            if (option_value == "create") {
//            document.getElementById('new_brand_name').readonly=false;
                $("#new_product_type").removeAttr('disabled');
                $("#new_product_type").attr('required',true);
            }else{
                $("#new_product_type").attr('disabled',true);
            }

        }

        function copyTextValue() {
            var text1 = document.getElementById("product_igst").value;
            document.getElementById("product_cgst").value = text1/2;
            document.getElementById("product_sgst").value = text1/2;
        }



        $('#btnReset').click(function() {
            $("#brand_name").val(null).trigger("change");
            $("#product_type").val(null).trigger("change");
        });


        var product_type =  [/* states array*/];
        $("#product_type").select2({
            data: product_type
        });

        var brand =  [/* states array*/];
        $("#brand_name").select2({
            data: brand
        });
        document.getElementById('testform').onsubmit= function(e){
            e.preventDefault();
        }

        document.getElementById('testform1').onsubmit= function(e){
            e.preventDefault();
        }

        $('.modal').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $("#brand_name").val(null).trigger("change");
            $("#product_type").val(null).trigger("change");
        });

        $('#addnewCustomer').click(function() {
//
            $.ajax({

                url: '{{ url('/') }}/AjaxNewCustomer',

                type: "post",
                dataType: "json",
                data: {
                    '_token':$('input[name=_token]').val(),
                    'customer_name':$('input[name=customer_name]').val(),
                    'customer_address':$('textarea[name=customer_address]').val(),
                    'customer_zip':$('input[name=customer_zip]').val(),
                    'customer_city':$('input[name=customer_city]').val(),
                    'customer_state':$('input[name=customer_state]').val(),
                    'customer_phone':$('input[name=customer_phone]').val(),
                    'customer_mobile':$('input[name=customer_mobile]').val(),
                    'customer_email':$('input[name=customer_email]').val(),
                    'customer_opening_balance':$('input[name=customer_opening_balance]').val(),
                },

                success:function(data) {
//                $(data, function(key, value) {
                    var Vals    =  data;
                    $('select[name="customer"]').append('<option value="'+ Vals.customer_id+'">'+ Vals.ledger_name +'</option>');
//                });


                    $('#myModal').modal('hide')
                }

            });

        });

        // ------------------------------------------------------------------------------ add new product --------------------




        $('#addProduct').click(function() {

            $.ajax({

                url: '{{ url('/') }}/AjaxNewProduct',

                type: "post",
                dataType: "json",
                data: {
                    '_token':$('input[name=_token]').val(),
                    'product_type':$('select[name=product_type]').val(),
                    'brand':$('select[name=brand]').val(),
                    'new_product_type':$('input[name=new_product_type]').val(),
                    'new_brand_name':$('input[name=new_brand_name]').val(),
                    'product_name':$('input[name=new_product_name]').val(),
                    'hsn_code':$('input[name=hsn_code]').val(),
                    'product_igst':$('input[name=product_igst]').val(),
                    'product_cgst':$('input[name=product_cgst]').val(),
                    'product_sgst':$('input[name=product_sgst]').val(),
                    'product_mrp':$('input[name=product_mrp]').val(),
                    'product_opening_stock':$('input[name=product_opening_stock]').val(),
                    'product_selling_price':$('input[name=product_selling_price]').val()
                },

                success:function(data) {

                    var TotalRow = $('.item-row').length;
                    for (y = 0; y <= TotalRow; y++) {

//

                        $.each(data, function (key, value) {

                            $('select[name="rows['+y+'][product]').append('<option value="' + value + '">' + key + '</option>');

                        });

                    }

//                .item-row
                    $('#myModal2').modal('hide')

                }

            });

//                    }else{
//
//                        $("input[name='rows[" + id + "][cost]']").empty();
//                        $("input[name='rows[" + id + "][costPrice]']").empty();
//                        $("input[name='rows[" + id + "][igst]']").empty();
//                        $("input[name='rows[" + id + "][sgst]']").empty();
//                        $("input[name='rows[" + id + "][cgst]']").empty();
//
//
//                    }

        });
        //    ------------------------------------------------- update row data
        $('.updateRow0').click(function() {

            $.ajax({

                url: '{{ url('/') }}/AjaxUpdateData',

                type: "post",
                dataType: "json",
                data: {
                    '_token': $('input[name=_token]').val()
                },
                success:function(data) {


                    $('select[name="rows[1][product]').empty();

                    '<option value="">Please Select Value</option>'

                    $.each(data, function(key, value) {


                        $('select[name="rows[1][product]').append('<option value="'+ value+'">'+ key +'</option>');

                    });


                }

            });

//                    }else{
//
//                        $("input[name='rows[" + id + "][cost]']").empty();
//                        $("input[name='rows[" + id + "][costPrice]']").empty();
//                        $("input[name='rows[" + id + "][igst]']").empty();
//                        $("input[name='rows[" + id + "][sgst]']").empty();
//                        $("input[name='rows[" + id + "][cgst]']").empty();
//
//
//                    }

        });

    </script>

    <!-- customer select style -->
    <script>
        var country =  [/* states array*/];
        $("#product_name").select1({
            data: country
        });
        var customer =  [/* states array*/];
        $("#customer").select2({
            data: customer
        });
        $('#customer').select2('open').select2('close');
        var payment_type =  [/* states array*/];
        $("#payment_type").select2({
            data: payment_type
        });

        //--------------------------------------- create new customer------------
    </script>



    {{-- ----------------------------------------inner content here --------------------------------------------------------}}

    <script>
        var _round = Math.round;
        Math.round = function(number, decimals /* optional, default 0 */)
        {
            if (arguments.length == 1)
                return _round(number);

            var multiplier = Math.pow(10, decimals);
            return _round(number * multiplier) / multiplier;
        }

        //original function
        // it's also working
        function update_balance() {

        }


        // amount calculation it working without discount without tax value for individual row
        function update_price() {
            var row = $(this).parents('.item-row');
            var rate=row.find('.cost').val();
            var qnty=row.find('.qty').val()
            var price =Number(rate)*Number(qnty);
//                    var price = row.find('.cost').val().replace("$","") * row.find('.qty').val();
            price= Math.round(price, 2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(price) ? row.find('.amt_before_tax').html("N/A") : row.find('.amt_before_tax').val(""+price);

            update_total();
        }

        //                percantage calcultioan
        function discount_percentage(){
            var row = $(this).parents('.item-row');
            var amount_before_tax=row.find('.amt_before_tax').val()
            var discount_percentage=row.find('.disc_percentage').val();
            var total_discount = Number(amount_before_tax)*Number(discount_percentage)/100;
            total_discount = Math.round(total_discount,2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(total_discount) ? row.find('.disc').html("N/A") : row.find('.disc').val(""+total_discount);
        }

        //    taxable value  calculation for individual row
        function taxble_value(){
            var row = $(this).parents('.item-row');
            var discount_amount=row.find('.disc').val();
            var amount_before_tax=row.find('.amt_before_tax').val()
            var price1 = Number(amount_before_tax)-Number(discount_amount);
            price1 = Math.round(price1,2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(price1) ? row.find('.taxbl_amount').html("N/A") : row.find('.taxbl_amount').val(""+price1);

//        update_total();
        }


        //                var row = $(this).parents('.item-row');
        $('#desc').focus(function()
        {
            $(this).animate({ height: '+=100',width:'+=80' }, 'slow');
        }).blur(function()
        {
            $(this).animate({ height: '-=100',width:'-=80' }, 'slow');
        });



        //    each row tax calculation

        function update_tax() {
            var row = $(this).parents('.item-row');



            var $igst=row.find('.igst').val();
            var total_tax_percentage=Number($igst)

//-----------------------------
            var sellingPrice=row.find('.taxbl_amount').val();

            var tax_amount=Number(sellingPrice)*total_tax_percentage/100;
            var total_amount=Number(sellingPrice)+tax_amount;
            tax1 =Math.round(tax_amount,2);
            amount1=Math.round(total_amount,2)
//        isNaN(amount1) ? row.find('.price').html("N/A") : row.find('.price').html("$"+amount1);
            isNaN(tax1) ? row.find('.text_amount').html("N/A") : row.find('.text_amount').val(""+tax1);
            isNaN(amount1) ? row.find('.price ').html("N/A") : row.find('.price ').val(""+amount1);

            update_total();
        }




        // without disc without tax amount calculation for total
        function update_total() {
            var total = 0;
            $('.amt_before_tax').each(function(i){
                price = $(this).val().replace("$","");
                if (!isNaN(price)) total += Number(price);
            });

            total = Math.round(total,2);

            $('#total_amount_input').val(""+total);
            $('#total_amount_text').html(""+total);
//                    $('#total').html(""+total);

            update_balance();
        }

        function total_discount(){
            var total_discount = 0;
            $('.disc').each(function(i){
                price1 = $(this).val().replace("$","");
                if (!isNaN(price1)) total_discount += Number(price1);
            });

            total_discount = Math.round(total_discount,2);

            $('#total_discount_input').val(""+total_discount);
            $('#total_discount_text').html(""+total_discount);

            update_balance();
        }
        //                taxbl_amount

        function total_taxble_amount(){
            var total_taxable = 0;
            $('.taxbl_amount').each(function(i){
                price2 = $(this).val().replace("$","");
                if (!isNaN(price2)) total_taxable += Number(price2);
            });

            total_taxable = Math.round(total_taxable,2);

            $('#total_taxble_input').val(""+total_taxable);
            $('#total_taxble_text').html(""+total_taxable);

            update_balance();
        }

        function total_gst() {
            var total_tax = 0;
            $('.text_amount').each(function(i){
                igst = $(this).val().replace("$","");
                if (!isNaN(igst)) total_tax += Number(igst);
            });

            total_tax = Math.round(total_tax,2);

            $('#total_gst_input').val(""+total_tax);
            $('#total_gst_text').html(""+total_tax);
            update_balance();
        }







        function grandtotal(){
            var grandtotal = 0;
            $('.price').each(function(i){
                price = $(this).val().replace("$","");
                if (!isNaN(price)) grandtotal += Number(price);
            });
            grandtotal = Math.round(grandtotal,2);
            $('#total_sub_input').val(""+grandtotal);
            $('#total_sub_text').html(""+grandtotal)

            var postage_charge=$('#postage_charge').val()
            var grand_total=Number(grandtotal)+Number(postage_charge);
            var grand_total = Math.round(grand_total).toFixed(2);
            $('#grand_total_text').html(""+grand_total);
            $('#grand_total_input').val(""+grand_total);
        }



        function bind(){
            $(".qty").blur(update_price);
            $(".cost").blur(update_price);


            //  discount_percentage
            $(".qty").blur(discount_percentage);
            $(".cost").blur(discount_percentage);
            $(".amt_before_tax").blur(discount_percentage);
            $(".disc_percentage").blur(discount_percentage);
            $(".delete").blur(discount_percentage);

            //        taxable value blur
            $(".qty").blur(taxble_value);
            $(".cost").blur(taxble_value);
            $(".disc").blur(taxble_value);
            $(".amt_before_tax").blur(taxble_value);
            $(".disc_percentage").blur(taxble_value);
            $(".delete").blur(taxble_value);

            //        vat calculation

            $(".qty").blur(update_tax);
            $(".cost").blur(update_tax);
            $(".disc").blur(update_tax);
            $(".taxbl_amount").blur(update_tax);
            $(".amt_before_tax").blur(update_tax);
            $(".disc_percentage").blur(update_tax);
            $(".sgst").blur(update_tax);
            $(".cgst").blur(update_tax);
            $(".igst").blur(update_tax);


            // discount

            $(".qty").blur(total_discount);
            $(".cost").blur(total_discount);
            $(".amt_before_tax").blur(total_discount);
            $(".disc_percentage").blur(total_discount);
            $(".disc").blur(total_discount);
            $(".taxbl_amount").blur(total_discount);
            //                    $("#discount").blur(total_discount);


            // total_taxble_amount

            $(".qty").blur(total_taxble_amount);
            $(".cost").blur(total_taxble_amount);
            $(".amt_before_tax").blur(total_taxble_amount);
            $(".disc_percentage").blur(total_taxble_amount);
            $(".disc").blur(total_taxble_amount);
            $(".taxbl_amount").blur(total_taxble_amount);





            //        gst calculation
            //                    total_gst

            $(".qty").blur(total_gst);
            $(".cost").blur(total_gst);
            $("#subtotal1").blur(total_gst);
            $(".disc_percentage").blur(total_gst);
            $(".disc").blur(total_gst);
            $(".taxbl_amount").blur(total_gst);
            $(".igst").blur(total_gst);
            $(".delete").blur(total_gst);



            //         grand total
            $(".qty").blur(grandtotal);
            $(".cost").blur(grandtotal);
            $("#subtotal1").blur(grandtotal);
            $(".disc_percentage").blur(grandtotal);
            $(".disc").blur(grandtotal);
            $(".taxbl_amount").blur(grandtotal);
            $(".igst").blur(grandtotal);
            $("#postage_charge").blur(grandtotal);
            $(".delete").blur(grandtotal);


        }
        $('#two').click(function(){
            taxble_value();
            total_igst();
            update_tax();
            discount_percentage();
            total_discount();
            update_total();
            subtotalgrand();
            grandtotal();


        });

        $(document).ready(function() {
            var i=1;
            var count=0;
            $('input').click(function(){
                $(this).select();
            });

            $("#paid").blur(update_balance);

            $(document).on('click', '.addrow', function(){
                $(this).text('x Del');
                $(this).attr('class','btn btn-info btn-sm del');
                $(".item-row:last").find('.mybtn').hide();
                i++;

                count++;
                var id=count;
                $(".item-row:last").after('<tr class="item-row" style="border-bottom: solid 1px black">' +
                        '<td style="width: 25%"class="item-name">' +
                        '<select  name="rows[' + id + '][product]" id=\"country' + id + '\" class="new_product form-control"  style="width:99%;"  required>'+
                        <?php  $product_type=\App\Product::orderBy('product_name', 'asc')->get();?>
                        '<option value="">Please select Item</option>'+
                        @foreach($product_type as $product_type)
                            '<option value="{{$product_type->id}}">{{$product_type->product_name}}</option>'+
                        @endforeach
                        '</select>'+
                        '</td>'+
                        '<td class="main_td"><textarea name="rows[' + id + '][desc]" id=\"desc' + id + '\" class=" form-control" style="height:34px!important"></textarea></td>'+
                        '<td class="main_td"><input name="rows[' + id + '][qty]" class="qty form-control"  required></td>' +
                        '<td class="main_td"> <button type="button" class="btn btn-danger btn-sm addrow" id=\"updateRow' + id + '\" >ADD</button>' +
                        '<a class=" delete mybtn  btn btn-system" href="javascript:;"  title="Remove row">X</a>'+
                        '</td></tr>');


                bind();
                $("#country"+id).select1({
                    source: country
                });

                $('#desc'+id).focus(function()
                {
                    $(this).animate({ height: '+=100',width:'+=80' }, 'slow');
                }).blur(function()
                {
                    $(this).animate({ height: '-=100',width:'-=80' }, 'slow');

                });



//-----------------------------------------------------update new product ----------------------------------
                $('#updateRow'+id).click(function() {

                    $.ajax({

                        url: '{{ url('/') }}/AjaxUpdateDataLoop/'+id,

                        type: "get",
                        dataType: "json",

                        success:function(data) {
                            var new_id=id+1;


//

                            $('select[name="rows['+new_id+'][product]').empty();


                            $('select[name="rows['+new_id+'][product]').append('<option value="">Please Select Value</option>');
                            $.each(data, function(key, value) {


                                $('select[name="rows['+new_id+'][product]').append('<option value="'+ value+'">'+ key +'</option>');
                            });


                        }

                    });

//                    }else{
//
//                        $("input[name='rows[" + id + "][cost]']").empty();
//                        $("input[name='rows[" + id + "][costPrice]']").empty();
//                        $("input[name='rows[" + id + "][igst]']").empty();
//                        $("input[name='rows[" + id + "][sgst]']").empty();
//                        $("input[name='rows[" + id + "][cgst]']").empty();
//
//
//                    }

                });
//-----------------------------------------------------update new product ----------------------------------

//  ------------------------ geting cost price and tax ------------------------------------------
                $('select[name="rows['+ id +'][product]"]').on('change', function() {

                    var stateID = $(this).val();
                    var  unique=id;
                    if(stateID) {

                        $.ajax({

                            url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                            type: "GET",

                            dataType: "json",

                            success:function(data) {


                                $("input[name='rows[" + id + "][rate]']").empty();
                                $("input[name='rows[" + id + "][mrp]']").empty();
                                $("input[name='rows[" + id + "][gst]']").empty();
                                $("input[name='rows[" + id + "][unit]']").empty();

                                var Vals = data;
                                $("input[name='rows[" + id + "][rate]']").val(Vals.sellingPrice);
                                $("input[name='rows[" + id + "][mrp]']").val(Vals.mrp);
                                $("input[name='rows[" + id + "][gst]']").val(Vals.gst);

                                alert("Current Quantity is "+ Vals.qnty);

                            }

                        });

                    }else{

                        $("input[name='rows[" + id + "][cost]']").empty();
                        $("input[name='rows[" + id + "][costPrice]']").empty();
                        $("input[name='rows[" + id + "][igst]']").empty();
                        $("input[name='rows[" + id + "][sgst]']").empty();
                        $("input[name='rows[" + id + "][cgst]']").empty();


                    }

                });
//  ------------------------ geting cost price and tax  end here ------------------------------------------

            });

            bind();
            $(document).on('click', '.del', function(){
//                        $(".del").on('click',function(){
                $(this).parent().parent().remove();
                update_total();
                total_discount();
                total_taxble_amount();
                total_gst();
                grandtotal();

//                        });
            });
            $(document).on('click', '.delete', function(){
//                        $(".del").on('click',function(){
                $(this).parent().parent().remove();
                $(".del").eq(-1).text('ADD');
                $('.del').eq(-1).attr('class','btn btn-danger btn-sm addrow');
//                        });
            });

            $('select[name="customer"]').on('change', function() {

                var stateID = $(this).val();

                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/ajax/'+stateID,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {


                            var Vals    =  data;
                            $("input[name='phone']").val(Vals.phone);
                            $("input[name='email']").val(Vals.email);
                            $("input[name='city']").val(Vals.city);
                            $("textarea[name='address']").val(Vals.address);
                            $("input[name='customer_type']").val(Vals.customer_type);



                        }

                    });

                }else{

                    $('select[name="city"]').empty();

                }

            });



//        getting cost price and gst

            $('select[name="rows[0][product]"]').on('change', function() {

                var stateID = $(this).val();
                var unique= 0;
                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {

                            var Vals    =  data;
                            $("input[name='rows[0][rate]']").val(Vals.sellingPrice);
                            $("input[name='rows[0][costPrice]']").val(Vals.cost);
                            $("input[name='rows[0][mrp]']").val(Vals.mrp);
                            $("input[name='rows[0][gst]']").val(Vals.gst);

                            alert("Current Quantity is "+ Vals.qnty);
                        }

                    });

                }else{

                    $('select[name="rows[0][batch_no]"]').empty();

                }

            });
//        ------------------------------------



        });




    </script>



@endsection

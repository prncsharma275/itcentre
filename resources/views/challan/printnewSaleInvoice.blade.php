<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="style.css" media="all" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="" rel="stylesheet">


</head>
  <style>

  @media print {
    .prnt
    {
      display: none;
    }
       body
       {
         min-height: 600px !important;
         margin: 2px;

       }
       @page {
  size: A4;
  margin: 0;
}
       .container{

        padding-right:40px;
}
       #item
       {
         height: 950px;
       }
       #itmdesc
       {
         max-width: 250px;
       }

   #grand
   {
     width: 659px!important;


   }
     #mrp
     {
       width: 66px!important;

     }
   #qntyy
   {
     width: 59px!important;


   }
   #rates
   {
     width: 58px!important;
   }
   #amount
   {
     width: 78px!important;
   }
   #disco
   {
     width: 90px!important;
   }
   #txblamt
   {
     width: 60px!important;
   }
   #sgsti
   {
     width: 175px!important;
   }
   #roow
   {

     height: 800px;


   }



   #prbr3
   {

       width: 1px;
       position: absolute;
       border:0.5px solid red;
        height: 800px;
       margin-left: 455px;
       display: none;

   }
   #prbr4
   {

       width: 1px;
       position: absolute;
       border:0.5px solid red;
        height: 800px;
       margin-left: 511px;
       display: none;


   }
   #prbr5
   {

       width: 1px;
       position: absolute;
       border:0.5px solid black;
        height: 800px;
       margin-left: 580px;

   }
   #prbr6
   {

       width: 1px;
       position: absolute;
       border:0.5px solid black;
        height: 800px;
       margin-left: 717px;

   }

   #prbr8
   {

       width: 1px;
       position: absolute;
       border:0.5px solid black;
        height: 800px;
       margin-left: 896px;

   }


   #prbr9
   {

       width: 1px;
       position: absolute;
       border:0.5px solid black;
        height: 825px;
       margin-left: 24px;

   }

   #prbr11
   {
     width: 1px;
     position: absolute;
     border:0.5px solid black;
      height: 800px;
     margin-left: 657px;
   }
   #prbr12
   {
     width: 1px;
     position: absolute;
     border:0.5px solid black;
      height: 800px;
     margin-left: 777px;
   }
   #prbr13
   {
     width: 1px;
     position: absolute;
     border:0.5px solid black;
      height: 800px;
     margin-left: 837px;
   }

   #midcon
   {
     border: none!important;
   }
   #itm
   {
     border: none!important;
   }
   #logos
   {
     height: 35px!important;


   }
   .grand
   {
     width: 680px;

   }


}
.grand
{
  width:806px!important;

}

  .mrp
  {
    width: 60px;

  }
.qntyy
{
  width: 64px;
}
.rates
{
  width: 60px;
}
.amount
{
  width: 60px;
}
.disco
{
  width: 100px;
}
.txblamt
{
  width: 60px;
}
.sgsti
{
  width: 202px;
}


.gtr
{
  border-right: 1px solid black;
  text-align:center;

}
#trow
{
  height: 50px;
  border: 1px solid red;
}

  </style>
  <style >

#itm
{
  font-size: 13px;
  border-bottom: none;
}
tr
{
  height: 10px;

}
#cd
{
  font-size: 13px;

}
.invc
{
  padding-left: 20px;
}




  </style>
  <?php $comp = \App\Company_detail::find(1);
  $sale_count = DB::table('sale_invoice')->where([
      ['sale_id', '=',$printBill->id ],

  ])->count();
  ?>

  <?php





  $sale_invoice = DB::table('sale_invoice')->where([
      ['sale_id', '=',$printBill->id ],

  ])->get();


    ?>
<body >

<div class="container"   style="margin-left:50px;margin-right:50px; ">

  <div class="header">


      <div class="row">
        <div class="col-sm-12" style=" text-align: center; ">
          <h4 style="font-weight:bolder;margin-top:10px;">TAX INVOICE</h4>
        </div>
      </div>
        <!--Upper Header Section Start !-->

      <div class="row">
        <div class="col-sm-6 " style="border-right:1px solid black;border-left:1px solid black;border-top:1px solid black">





          <div class="row mt-2 " >
            <!--<div class="col-sm-2" style="height:30px;">-->
              <!--<img src="{{asset('company_logo/logo.png')}}" id="logos" height="30" width="80" style="line-height:20px"  alt="">-->
            <!--</div>-->
            {{-- <div class="col-sm-12" style="height:30px;line-height:30px;">
            
            </div> --}}
            <b style="font-size:26px; ">&nbsp;&nbsp;M/S I.T CENTRE</b>
          </div>


            <address class="" style="font-size:10px;margin-bottom:5px;">
          Ganga Market
            1st Floor,OS Complex,Opposite RK Mission Road Pin - 791111, Arunachal Pradesh(INDIA)<br>
                <b>GSTIN:</b> 12EMRPS6621A1Z3,  <b>PAN No:</b> AMPPK8712N,<br>Phone No03561 225775/ Mobile: 9863032009

            </address>


        </div>
        <div class="col-sm-6 text-centre" style="border-right:1px solid black;border-top:1px solid black;">
            <div class="row">
              <table>
                <tr>
                  <td><span style="font-weight:bold">Invoice No:</span><span>{{$printBill->unique_id}}</span></td><td><span style="font-weight:bold">Invoice Date:</span><span ><?php echo date( 'd/m/y', strtotime($printBill->billing_date)) ?></span></td>
                </tr>
                <tr>
                  <td> <span style="font-weight:bold">Order No:</span><span></span></td><td><span style="font-weight:bold">Order Date:</span><span></span></td>
                </tr>
                <tr>
                    <?php
                    if($printBill->challan_no =="NA"){
                        $challan_no="";
                    }else{
                        $challan_no=$printBill->challan_no;
                    }

                        if($printBill->challan_date ==""){
                            $challan_date="";
                        }else{
                            $challan_date=date( 'd/m/y', strtotime($printBill->challan_date));
                        }
                    ?>
                  <td> <span style="font-weight:bold">Challan No:</span><span>{{$challan_no}}</span></td><td><span style="font-weight:bold">Challan Date:</span><span>{{$challan_date}}</span></td>
                </tr>
              </table>
              
              
               
              {{-- <div class="col-sm-12">
                <span style="font-weight:bold">Challan No:</span><span></span>&nbsp;&nbsp;<span style="font-weight:bold">Challan Date:</span><span></span>
              </div> --}}

            </div>
        </div>

      </div>
      <div class="row" style="border:1px solid black">
        <?php
                $cus=\App\Customer::find($printBill->customer_id);
       //print_r($cus);
         ?>
          @if($printBill->customer_id==1 || $printBill->customer_id==2)
            <?php $retcus=DB::table('sales')->find($printBill->id);?>
          <div class="col-sm-6" style="border-right:1px solid black">
            <h6>SALE TO:</h6>
            <address class="" style="font-size:13px;margin-bottom:5px;">
              <b style="font-size:16px; ">@{{ $retcus->retail_name }}</b>
            <br>
              {{ $retcus->address}}&nbsp;<br>Mobile:{{ $retcus->mobile}}&nbsp;<br>Email:{{ $retcus->email}}

            </address>
          </div>
        @else

             <?php
             $customers = DB::table('customers')->where('id',$printBill->customer_id )->get();
             //print_r($cus);
              ?>
             <div class="col-sm-6" style="border-right:1px solid black">
                  <span><b>Sale To :-</b></span><br>

                  <address class="" style="font-size:13px;margin-bottom:5px;">
                    <b style="font-size:16px; ">{{ $cus->ledger_name }}</b>
                  <br>
                    {{ $cus->address}}&nbsp;{{ $cus->city}}&nbsp;{{ $cus->state}}&nbsp{{ $cus->zip}}<br>
                      GSTIN: {{ $cus->vatNo}},PAN No: {{ $cus->pan}}<br>Phone No:{{ $cus->phone}} / Mobile: {{ $cus->mobile}} / Email:{{ $cus->email}}

                  </address>
             </div>
              @endif
              <div class="col-sm-6" style="">
                    {{-- <span><b>Patient Details :-</b></span><br> --}}
                <div class="row">

                  <div class="col-sm-12">
                    <span style="font-weight:bold">E-way Bill: </span><span>{{$printBill->doc}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </div>
                  <div class="col-sm-12">
                    <span style="font-weight:bold">E-way Bill Date : </span><span>{{$printBill->doc}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </div>
                  {{-- <div class="col-sm-12">
                    <span style="font-weight:bold">Doctor's Name: </span><span>{{ $printBill->doc }}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight:bold">OT Date: </span>{{ $printBill->ot }}<span></span>
                  </div> --}}

                </div>
              </div>
      </div>

  </div>



@php
$sale_invoice = DB::table('sale_invoice')->where([
  ['sale_id', '=',$printBill->id ],

])->get();

@endphp
    <!--Upper Header Section End !-->
    <div class="row" id="roow" style="border: 1px solid black;!important;">



      <table class="table1" id="midcon" style="width:100%;border:1px solid black;">

        <!--Heading Section Start !-->
        <tr style="">

          <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: left;;font-weight:bold;font-size:12px;width:1%">SL NO.</th>
          <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:15px;width: 58%;">Perticulars.</th>
          <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:15px;width: 8%;">HSN.</th>
          {{-- <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:12px;width: 5%">MRP.</th> --}}
          <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:15px;width: 5%">Qnty.</th>
          <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:15px;width:4%;">Rate.</th>
          {{-- <th  style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:12px;width:4%;">Amt.</th> --}}
          <th  style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:15px;width: 4%;">Disc. %</th>
          <th  style="border-right:1px solid black;border-bottom:1px solid; display: none; text-align: center;;font-weight:bold;font-size:15px;width: 5%;">Disc Amt.</th>
          <th  style="border-right:1px solid black;border-bottom:1px solid;display: none;  text-align: center;;font-weight:bold;font-size:15px;width: 5%;">Txbl Amt.</th>
          <th  style="border-right:1px solid black;border-bottom:1px solid;   text-align: center;;font-weight:bold;font-size:15px;width: 3%;">GST%</th>
          <th  style="border-right:1px solid black;border-bottom:1px solid; display: none;  text-align: center;;font-weight:bold;font-size:15px;width: 5%;">CGST.</th>
          <th style="border-right:1px solid black;border-bottom:1px solid; display: none;  text-align: center;;font-weight:bold;font-size:15px;width: 5%;">SGST.</th>
          <th style="border-right:1px solid black;border-bottom:1px solid;display: none;  text-align: center;;font-weight:bold;font-size:15px;width: 5%;">IGST.</th>
          <th style="border-right:1px solid black;border-bottom:1px solid;  text-align: center;;font-weight:bold;font-size:15px;width: 7%;">Amt.</th>
        </tr>
        <!--Heading Section End !-->


<!--Items Section Start !-->









<?php
$i=01 ;
  foreach($sale_invoice as $sale){
    $j=$i++;
    $product=\App\Product::find($sale->product_id);
 ?>
        <tr >

          <td id="itm"  style="   text-align: center;border-right:1px solid black;vertical-align: text-top;padding:2px"><?php echo $j; ?></td>
          <td id="itm" style="  text-align: left;border-right:1px solid black;vertical-align: text-top;max-width:50px;">&nbsp;{{$product->product_name}}&nbsp;<b>{{$sale->description}}</b><br><?php if(isset($sale->mfg)){ echo '<b>MFG DATE:</b>&nbsp;'. $sale->mfg; } else { };  ?><br>
            <?php if(isset($sale->expd)){ echo '<b>EXP DATE:</b>&nbsp;'. $sale->expd; } ?>
           </td>
          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;padding:2px;max-width:10px;">{{$product->product_code}}</td>

          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:50px;">{{$sale->quantity}}{{$product->unit_type}}</td>
          <td id="itm"  style="  text-align: right;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"><?php  echo number_format( $sale->rate ,2) ?></td>

          <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px">{{$sale->discount_per}}</td>
          <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:54px;display: none;">{{$sale->discount}}</td>
          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;display: none;">{{$sale->taxable_amount}}</td>
            @if($printBill->tax_type=="CGST&SGST")
          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;display: none;">{{$sale->cgst_tax+$sale->cgst_tax}}</td>
              @else
                  <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;display: none;">{{$sale->igst_tax}}</td>
                    @endif
                      <?php
                      if($printBill->tax_type=="CGST&SGST")
                        {
                            $tx=$printBill->tax_amount;
                            $txx=$tx/2;

                        }
                        else
                        {
                          $txx='';
                        }
                        ?>
          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:50px;min-width:39px;display: none;"></td>
          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:50px;min-width:39px;display: none;"><?php if($txx!=''){ echo number_format( $txx ,2); }  ?></td>
          @if($printBill->tax_type=="CGST&SGST")
          <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:60px;min-width:40px;">{{ $sale->cgst_tax+$sale->cgst_tax  }}  </td>
        @else

              <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:60px;min-width:40px;"><?php echo number_format( $sale->igst_tax ,2) ?></td>
            @endif
          <td id="itm" style="  text-align: right;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:65px;"><?php echo number_format( $sale->amount_before_tax ,2) ?></td>
        </tr>

      <?php
}
       ?>
       <tr >

         <td id="itm"  style="   text-align: center;border-right:1px solid black;vertical-align: text-top;padding:2px"></td>
         <td id="itm" style="  text-align: right;border-right:1px solid black;vertical-align: text-top;max-width:50px;font-weight:bolder"></td>
         <td id="itm" style="  text-align: left;border-right:1px solid black;vertical-align: text-top;padding:2px;max-width:10px;font-weight:bolder"></td>
         <td  id="itm" style="border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td>
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:50px;"></td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;display: none;"><?php echo number_format((float)$sale->discount_per,2) ?></td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:54px;display: none;"></td>

           @if($printBill->tax_type=="CGST&SGST")
         <td style="display:none;" id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px"></td>
             @else
                 <td style="display:none;" id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px"><?php echo number_format($sale->igst_tax,2) ?></td>
                   @endif
                     <?php
                     if($printBill->tax_type=="CGST&SGST")
                       {
                           $tx=$printBill->tax_amount;
                           $txx=$tx/2;

                       }
                       else
                       {
                         $txx='';
                       }
                       ?>
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:50px;min-width:39px;display: none;"></td>
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:50px;min-width:39px;display: none;"></td>
         {{-- <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:60px;min-width:40px;"></td> --}}
         <td id="" style="  text-align: right;border-top:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:65px;font-size:12px;font-weight:bold"><?php echo number_format( $printBill->total_amount_without_anything,2 ) ?></td>
       </tr>
       <tr >

         <td id="itm"  style="   text-align: center;border-right:1px solid black;vertical-align: text-top;padding:2px"></td>
          @if($printBill->tax_type=="CGST&SGST")
               <td id="itm" style="  text-align: right;border-right:1px solid black;vertical-align: text-top;max-width:50px;font-weight:bolder;font-style: oblique;">DISC<br>CGST<BR>SGST</td>
          @else
               <td id="itm" style="  text-align: right;border-right:1px solid black;vertical-align: text-top;max-width:50px;font-weight:bolder">DISC<br>IGST</td>
          @endif

         <td id="itm" style="  text-align: left;border-right:1px solid black;vertical-align: text-top;padding:2px;max-width:10px;font-weight:bolder"></td>
         {{-- <td  id="itm" style="  text-align: ;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td> --}}
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:50px;"></td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td>
         <td id="itm"  style="  text-align: right;border-right:1px solid black;vertical-align: text-top;;padding:2px;display: none;"><?php echo number_format( (float)$sale->discount_per ) ?>.</td>
         <td id="itm"  style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:54px;display: none;"></td>
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:60px;"></td>
           @if($printBill->tax_type=="CGST&SGST")
         <td style="display:none;" id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px"></td>
             @else
                 <td style="display:none;" id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;padding:2px"><?php echo number_format( $sale->igst_tax,2 ) ?></td>
                   @endif
                     <?php
                     if($printBill->tax_type=="CGST&SGST")
                       {
                           $tx=$printBill->tax_amount;
                           $txx=$tx/2;

                       }
                       else
                       {
                         $txx='';
                       }
                       ?>
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:50px;min-width:39px;display: none;"></td>
         <td id="itm" style="  text-align: center;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:50px;min-width:39px;display: none;"></td>

            @if($printBill->tax_type=="CGST&SGST")
              <td id="itm" style="  text-align: right;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:65px;">-<?php echo number_format($printBill->total_discount,2) ?><br>+<?php echo number_format($printBill->total_cgst,2) ?><br>+<?php echo number_format($printBill->total_sgst,2) ?></td>
            </tr>
            @else
              <td id="itm" style="  text-align: right;border-right:1px solid black;vertical-align: text-top;;padding:2px;max-width:64px;min-width:65px;">-{{$printBill->total_discount}}.00<br>+{{$printBill->total_igst}}.00</td>
            </tr>
            @endif




          <span class="prbr1" id="prbr9"></span>


             <span class="prbr3" id="prbr3"></span>
            <span class="prbr4" id="prbr4"></span>
              <span class="prbr5" id="prbr5"></span>
              <span class="prbr6" id="prbr6"></span>

              <span class="prbr8" id="prbr8"></span>

                <span class="prbr10" id="prbr11"></span>
                  <span class="prbr10" id="prbr12"></span>
                  <span class="prbr10" id="prbr13"></span>


  </table>
        </div>
<!--Items Section End !-->

          <!--Amount In word Section End !-->







          <?php



                                  $qnty=0;
                                  ?>
                                  @foreach($sale_invoice as $qnt)
                                          <?php $qnty =$qnty+$qnt->quantity;


                                           ?>
                                   @endforeach

                            <?php

                            $qnty2=0;
                            ?>
                            @foreach($sale_invoice as $qnt2)
                                    <?php $qnty2 =$qnty2+$qnt2->mrp; ?>
                             @endforeach




          <div class="row"  style="height:auto;border:1px solid black;">
            <div id="grand" class="gtr grand" style="" >
              <span style="font-weight:bolder">GRAND TOTAL</span>
            </div>
            {{-- <div id="mrp" class="gtr mrp" >
              <span style="font-size:12px">{{ $qnty2 }}</span>
            </div> --}}
            <div id="qntyy" class="gtr qntyy" >
              <span style="font-size:12px"><b>{{ $qnty }}</b></span>
            </div>
            <span id="gtt" style="font-size:12px; text-align: right;width: 23%;font-family: cursive;"><b>Rs.</b>{{$printBill->grand_total}}</span>

          </div>
          <div class="row" >
            <div class="col-md-12" style="border:1px solid black">
              <?php
                        $number = $printBill->grand_total;
                        $no = round($number);
                        $point = round($number - $no, 2) * 100;
                        $hundred = null;
                        $digits_1 = strlen($no);
                        $i = 0;
                        $str = array();
                        $words = array('0' => '', '1' => 'One', '2' => 'Two',
                                '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
                                '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
                                '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
                                '13' => 'Thirteen', '14' => 'Fourteen',
                                '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
                                '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
                                '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
                                '60' => 'Sixty', '70' => 'Seventy',
                                '80' => 'Eighty', '90' => 'Ninety');
                        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
                        while ($i < $digits_1) {
                            $divider = ($i == 2) ? 10 : 100;
                            $number = floor($no % $divider);
                            $no = floor($no / $divider);
                            $i += ($divider == 10) ? 1 : 2;
                            if ($number) {
                                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                                $str [] = ($number < 21) ? $words[$number] .
                                        " " . $digits[$counter] . $plural . " " . $hundred
                                        :
                                        $words[floor($number / 10) * 10]
                                        . " " . $words[$number % 10] . " "
                                        . $digits[$counter] . $plural . " " . $hundred;
                            } else $str[] = null;
                        }
                        $str = array_reverse($str);
                        $result = implode('', $str);
                        $points = ($point) ?
                                "." . $words[$point / 10] . " " .
                                $words[$point = $point % 10] : '';?>
              <h6><?php echo $result . "Rupees  " . $points . "Only"; ?></h6>
            </div>
          </div>
          <div class="row">
              @if($printBill->tax_type=="CGST&SGST")
            <?php


                     $users = DB::table('sale_invoice')
                     ->select(DB::raw("SUM(taxable_amount) as paidsum"),DB::raw("SUM(tax_amount) as paidsum2"))->addSelect('cgst_tax','sgst_tax','sale_id')
                     ->where('sale_id',$printBill->id )
                     ->groupBy('sgst_tax')
                    ->get();


                    //print_r($users);

            ?>
            <table style="border:1px solid black;width:100%">


            <thead >

                <tr>



                <th style="border-right:1px solid black;text-align:center;">Taxable Value</th>
                  <th colspan="2" style="border-right:1px solid black;text-align:center;">CGST</th>
                <th colspan="2" style="border-right:1px solid black;text-align:center;">SGST</th>
                <th style="border-right:1px solid black;text-align:center;">Total Tax Amount</th>
                  </tr>
                  <tr>
                    <th style="border-right:1px solid black"></th>
                    <th style="border-right:1px solid black;border-top:1px solid black;;text-align:center;font-size:10px;">Rate</th>
                    <th style="border-right:1px solid black;text-align:center;font-size:10px;">Amount</th>

                    <th style="border-right:1px solid black;border-top:1px solid black;;text-align:center;font-size:10px;">Rate</th>
                    <th style="border-right:1px solid black;text-align:center;font-size:10px;">Amount</th>
                    <th style="border-right:1px solid black;text-align:center;font-size:10px;"></th>
                    </tr>
            </thead>
            <tbody>
                @foreach ($users as $summ)
              <tr>


                <td id="tx" style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php echo number_format($summ->paidsum,2) ?></td>
                <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600">{{ $summ->cgst_tax }}%</td>
                <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php $txa1=$summ->paidsum2/2;echo number_format($txa1,2) ?></td>
                <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600">{{ $summ->sgst_tax }}%</td>
                <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php $txa1=$summ->paidsum2/2;echo number_format($txa1,2) ?></td>
                <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php echo number_format($summ->paidsum2,2) ?></td>

              </tr>

                @endforeach

                <tr>
                  <td  style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"> <?php echo number_format($printBill->total_taxable_value,2) ?></td>
                  <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"></td>
                  <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php echo number_format($printBill->total_cgst,2) ?></td>
                  <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"></td>
                    <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php echo number_format($printBill->total_cgst,2) ?></td>
                    <td style="border:1px solid black;text-align:center;font-size:12px;font-weight:600"><?php echo number_format($printBill->total_tax_amount,2) ?></td>
                </tr>
            </tbody>
              </table>
            @else
              <?php


                       $users = DB::table('sale_invoice')
                       ->select(DB::raw("SUM(taxable_amount) as paidsum"),DB::raw("SUM(tax_amount) as paidsum2"))->addSelect('igst_tax')
                       ->where('sale_id',$printBill->id )
                       ->groupBy('igst_tax')
                      ->get();




              ?>
              <table style="border:1px solid black;width:100%">


              <thead >

                  <tr>



                  <th style="border-right:1px solid black;text-align:center;font-size:10px;">Taxable Value</th>
                    <th colspan="2" style="border-right:1px solid black;text-align:center;font-size:10px;">IGST</th>

                  <th style="border-right:1px solid black;text-align:center;font-size:10px;">Total Tax Amount</th>
                    </tr>
                    <tr>



                    <th style="border-right:1px solid black"></th>
                      <th style="border-right:1px solid black;border-top:1px solid black;;text-align:center;font-size:10px;">Rate</th>
                      <th style="border-right:1px solid black;text-align:center;font-size:10px;">Amount</th>
                      <th style="border-right:1px solid black"></th>


                      </tr>
              </thead>
              <tbody>
                  @foreach ($users as $summ)
                <tr>


                  <td style="border:1px solid black;text-align:center;font-size:10px;font-weight: bold"><?php echo number_format($summ->paidsum,2) ?></td>
                  <td style="border:1px solid black;text-align:center;font-size:10px;font-weight: bold">{{ $summ->igst_tax }}%</td>
                  <td style="border:1px solid black;text-align:center;font-size:10px;font-weight: bold"><?php echo number_format($summ->paidsum2,2) ?></td>
                    <td style="border:1px solid black;text-align:center;font-size:10px;font-weight: bold"><?php echo number_format($summ->paidsum2,2) ?></td>



                </tr>

                  @endforeach

                  <tr>
                    <td style="border:1px solid black;text-align:center;font-size:12px;font-weight: bolder"><?php echo number_format($printBill->total_taxable_value,2) ?></td>
                    <td style="border:1px solid black;text-align:center;font-size:12px;font-weight: bolder"></td>
                    <td style="border:1px solid black;text-align:center;font-size:12px;font-weight: bolder"><?php echo number_format($printBill->total_igst,2) ?></td>
                    <td style="border:1px solid black;text-align:center;font-size:12px;font-weight: bolder"><?php echo number_format($printBill->total_igst,2) ?></td>

                  </tr>
              </tbody>
                </table>

            @endif
          </div>
          <div class="row" style="border:1px solid black">
            <div class="col-sm-6" style="border:1px solid black">
              <span><b>Terms And Conditions :-</b></span><br>
              <i style="font-size:14px;font-weight:600" >1.Goods once sold shall not be returned back.</i><br>
              <i style="font-size:14px;font-weight:600">2.Interest @24% per annum will be charged if the payment not  made within 1 week from the date of invoice </i><br>
              {{-- <i style="font-size:14px;font-weight:600">3.Forms C/F/H if not received in time,the application tax will be charge.</i><br> --}}

            </div>
            <div class="col-sm-6" style="border:1px solid black">
                <span ><b>Company Bank Details:-</b></span><br>
              <span style="font-size:13px;font-weight:600"><b>A/C Holder Name:</b>&nbsp;IT CENTRE</span><br>
            <span style="font-size:13px;font-weight:600"><b>A/C NO:</b>&nbsp;79401400000085</span><br>
              <span style="font-size:13px;font-weight:600"><b>BANK NAME :</b>&nbsp;SyndicateBank</span><br>
                  <span style="font-size:13px;font-weight:600"><b>IFSC CODE :</b>&nbsp;SYNB0007940</span><br>
                <span style="font-size:13px;font-weight:600"><b>BRANCH :</b>&nbsp;ITANAGAR BRANCH</span><br>




            </div>
          </div>
          <div class="row" >
            <div class="col-sm-6 col-xs-4 text-center" style="border:1px solid black;">
              <p><b>Customer's Seal And Signature</b></p>
            </div>

            <div class="col-sm-6 col-xs-4 text-center" style="border:1px solid black">
              <p><b>For M/S I.T CENTRE</b></p>
              <span><b>Authorised Signature</b></span>
            </div>
          </div>


      </div>




  </div><br><br>

  <button type="button" name="button" class="btn btn-danger prnt" onclick="myFunction()">Print</button>



</body>

<script>
    function myFunction() {
        window.print();
    }
</script>
</html>

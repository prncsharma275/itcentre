@extends('layouts.adminPanel')
@section('title')
    Sales Panel
@endsection

@section('custom_css')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('css/bootstrap/js/dataTables.bootstrap.min.js')}}"></script>
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap/css/dataTables.bootstrap.min.css')}}">--}}
@endsection

@section('manual_style_code')

    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('challan')}}">Challan Return</a>
    </li>
@endsection
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Challan Return Panel</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModal"><i class="splashy-document_letter_add"></i> New Return</a>

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <table class="table table-striped table-bordered dTableR" id="table">
                <thead>
                  <?php if(isset($tempSale)){ ?>
                <tr>

                    <th style="width: 20%">Challan No</th>
                    <th>Challan Date</th>
                    <th style="width: 25%">Customer Name</th>

                </tr>
                <?php }else { ?>
                  <th style="width: 20%">Challan No</th>
                  <th>Chalan Date  </th>
                  <th style="width: 25%">Against Challan Bill</th>
                  <th style="width: 25%">Customer Name</th>
                  <th>Actions</th>
                  <?php } ?>
                </thead>
                <tbody>

                  <?php if(isset($tempSale)){ ?>

                      <tr>
                        <td>{{ $tempSale->challan_no }}</td>
                          <td><?php echo $newDate = date("d-m-Y", strtotime($tempSale->challan_date)); ?></td>
                          <td><?php $get_cus=DB::table('customers')->find($tempSale->customer_id); ?>{{ $get_cus->ledger_name }}</td>
                      </tr>



                </tbody>
            </table>
            <div class="">
              <a href="{{url('challanDetailsToReturn')}}/{{$tempSale->id}}" type="button" name="button" class="btn btn-success">Challan Return</a>   <button type="button" name="button" class="btn btn-danger">Back</button>&nbsp;<button type="button" name="button" class="btn btn-primary">Backto Dashboard</button>
            </div>

            <?php }else{

              $get_chal=DB::table('chalan_return')->get();
              ?>
              @foreach ($get_chal as $element)
                <tr>
                  <td>{{ $element->invoice_no }}</td>
                    <td><?php $chldl=DB::table('challan')->find($element->challan_id) ?><?php echo $newDate = date("d-m-Y", strtotime($chldl->challan_date)); ?></td>
                    <td><?php $chldl=DB::table('challan')->find($element->challan_id) ?>{{ $chldl->challan_no  }}</td>
                      <td><?php $chldl=DB::table('challan')->find($element->challan_id); $get_cus=DB::table('customers')->find($chldl->customer_id) ?>{{ $get_cus->ledger_name }}</td>
                        <td><a href="{{ url('editreturn') }}/{{ $element->id }}" title="EDIT" class="btn btn-sm btn-default"><i class="splashy-document_letter_edit"></i></a>
                          <a href="{{ url('printreturn') }}/{{ $element->id }}" title="PRINT" target="_blank" class="btn btn-sm btn-default"><i class="splashy-printer"></i></a>
                          <a href="{{ url('deletereturn') }}/{{ $element->id }}" target="_blank" title="Cancel" class="btn btn-sm btn-default" onclick="return ConfirmDelete()"><i class="splashy-document_letter_remove"></i></a></td>
                </tr>
              @endforeach

              <?php
            }
             ?>
    </div>

    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
        <?php echo Form::open(array('route' => 'searchchallanToReturn', 'method'=>'get')); ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Select Bill To Purchase Return</h4>
                </div>
                <div class="modal-body">
                    <select id="saleno" name="purchase_id" style="width:100%;" required>
                        <?php   $product_type = \App\Challan::get(); ?>
                        <?php // $product_type=\App\Sale::All();?>
                        <option value="">Please select Invoice</option>
                        @foreach($product_type as $product_type)
                            <option value="{{$product_type->id}}">{{$product_type->challan_no}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="searchButton" class="btn btn-primary">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <br>

@endsection

@extends('layouts.adminPanelTable')
@section('title')
    Customer Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('customers')}}">Ledger Panel</a>
    </li>
@endsection

@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif

    <h3 class="heading">Ledger Panel</h3>
    <a class="btn btn-default" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModal"><i class="splashy-document_letter_add"></i>  Ledger</a>



    <table  class="table table-striped table-bordered dTableR" id="dt_a">
        <thead>
        <tr>

            <th class="text-center">Name</th>

            <th class="text-center">Ledger Type</th>
            <th class="text-center">Ledger Group</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)

            <tr>
                <td>{{$item->ledger_name}}</td>
                <?php $ledger_type = \App\Ledger_type::find($item->ledger_type2) ?>
                <td>{{$ledger_type->ledger_type_name}}</td>
                <?php $ledger_group = \App\Ledger_group::find($item->ledger_group) ?>
                <td>{{$ledger_group->ledger_group_name}}</td>
                <td>
                    @if($item->id==1)

                        <a href="editCreditor/{{$item->id}}" class="btn btn-default btn-sm disabled" title="EDIT">
                            <i class="splashy-document_letter_edit"></i>
                        </a>
                        <a href="deleteCustomer/{{$item->id}}" class="btn btn-default btn-sm disabled" onclick="return ConfirmDelete()" title="DELETE">
                            <i class="splashy-document_letter_remove"></i>
                        </a>
                     @else
                    @if($item->ledger_type2==1)

                        <a href="editCreditor/{{$item->id}}" class="btn btn-default btn-sm"title="EDIT">
                            <i class="splashy-document_letter_edit"></i>
                        </a>


                    @elseif($item->ledger_type2==2)

                    <a href="editCustomer/{{$item->id}}" class="btn btn-default btn-sm" title="EDIT">
                        <i class="splashy-document_letter_edit"></i>
                    </a>

                    @else
                        <a href="editOtherLedger/{{$item->id}}" class="btn btn-default btn-sm"title="EDIT">
                            <i class="splashy-document_letter_edit"></i>
                        </a>

                    @endif

                      {{--checking if item is = cash the disabled delete option--}}
                       @if($item->id==2)
                            <a href="deleteCustomer/{{$item->id}}" class="btn btn-default btn-sm disabled" onclick="return ConfirmDelete()" title="DELETE">
                                <i class="splashy-document_letter_remove"></i>
                            </a>
                       @else
                            <a href="deleteCustomer/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()"title="DELETE">
                                <i class="splashy-document_letter_remove"></i>
                            </a>
                       @endif
                  @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{--------------------------------------- create modal start here -------------------------------------}}
    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
        <?php echo Form::open(array('route' => 'create_new_ledger_choose', 'method'=>'post')); ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Ledger</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12" >
                            <label for="startDate">Select Ledger Category First<span style="color: red">*</span></label>
                            <select id="saleno" class="form-control" name="ledger_type" style="width:100%;height: 38px" required>
                                <option value="">Select Ledger Category</option>
                                <option value="1">Sundary Creditors</option>
                                <option value="2">Sundary Debtors</option>
                                <option value="3">Others</option>
                            </select>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                    <br>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="searchButton" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    {{--------------------------------------- create modal start here -------------------------------------}}


    <script>

        var customer =  [/* states array*/];
        $("#saleno").select2({
            data: customer

        });

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }
        $(".alert").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });

    </script>
    {{--inner content here ------------------------------------}}

    <br>
@endsection
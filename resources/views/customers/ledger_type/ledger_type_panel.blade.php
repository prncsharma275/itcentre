@extends('layouts.adminPanelTable')
@section('title')
   Ledger Type:Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('#')}}">Ledger Type</a>
    </li>

@endsection
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif

    <h3 class="heading">Ledger Type</h3>
    <a class="btn btn-default disabled" style="margin-bottom: 10px;"  data-toggle="modal" data-target="#myModal"><i class="splashy-document_letter_add"></i> Ledger Type</a>

    <table  class="table table-striped table-bordered dTableR" id="dt_a">
        <thead>
        <tr>

            <th class="text-center">ID&nbsp;&nbsp;</th>

            <th class="text-center">Name</th>
            <th class="text-center">Ledger Group Name</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)

            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->ledger_type_name}}</td>
                <?php $ledger_group = \App\Ledger_group::find($item->ledger_group_id) ?>
                <td>{{$ledger_group->ledger_group_name}}</td>
                <td>
                    @if($item->id==1)
                    <a href="editCustomer/{{$item->id}}" class="btn btn-default btn-sm disabled" title="Edit">
                       <i class="splashy-document_letter_edit"></i>
                     @elseif($item->id==2)
                            <a href="editCustomer/{{$item->id}}" class="btn btn-default btn-sm disabled" title="Edit">
                                <i class="splashy-document_letter_edit"></i>
                       @else
                       <a data-toggle="modal" data-target="#myModal{{$item->id}}" class="btn btn-default btn-sm disabled" title="Edit">

                           <i class="splashy-document_letter_edit"></i>
                    </a>
                    @endif
                </td>
            </tr>

            {{--------------------------------------- create modal start here -------------------------------------}}
            <div class="modal fade" id="myModal{{$item->id}}"  role="dialog" aria-labelledby="myModalLabel">
                <?php echo Form::open(array('url' =>['update_ledger_type',$item->id])); ?>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Ledger</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12" >
                                    <label for="startDate">Name</label>
                                    <input style="width: 100%" type="text" class="form-control" name="ledger_name" value="{{$item->ledger_type_name}}"  required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12" >
                                    <label for="startDate">Ledger Group</label>
                                    <select id="saleno" class="form-control" name="ledger_group" style="width:100%;height: 38px" required>
                                        <option value="{{$ledger_group->id}}">{{$ledger_group->ledger_group_name}}</option>
                                        <?php $new_datas = \App\Ledger_group::all(); ?>
                                        @foreach($new_datas as $data)
                                            <option value="{{$data->id}}">{{$data->ledger_group_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="searchButton" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            {{--------------------------------------- edit modal start here -------------------------------------}}
        @endforeach
        </tbody>
    </table>
    {{--------------------------------------- create modal start here -------------------------------------}}
    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
        <?php echo Form::open(array('route' => 'save_new_ledger_type', 'method'=>'post')); ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Ledger</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12" >
                            <label for="startDate">Name</label>
                            <input style="width: 100%" type="text" class="form-control" name="ledger_name"  required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12" >
                            <label for="startDate">Ledger Group</label>
                            <select id="saleno" class="form-control" name="ledger_group" style="width:100%;height: 38px" required>
                                <?php $new_datas = \App\Ledger_group::all(); ?>
                                @foreach($new_datas as $data)
                                    <option value="{{$data->id}}">{{$data->ledger_group_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="searchButton" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    {{--------------------------------------- create modal start here -------------------------------------}}

    <script>

        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }
        $(".alert").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });

    </script>
    {{--inner content here ------------------------------------}}

    <br>
@endsection
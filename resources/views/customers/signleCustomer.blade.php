@extends('layouts.adminPanelTable')
@section('title')
    Create New Client
@endsection

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
@endsection

@section('content')
    <h4 class="text-center">Full View Customer</h4>
            <?php echo Form::open(array('route' => 'storeCustomer')); ?>
            <div class="container-fluid">

                <div class="row">





                </div> <!-- row end here -->
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>Customer Name/Company Name<span style="color: red; font-size: 15px">*</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control btn-default" value="{{$viewCustomer->ledger_name}}" readonly>

                        </div>

                    </div>
                    <div class="col-md-6 form-group">
                        <label>Address</label>
                        <div class="input-group">
                            <textarea  type="text" class="form-control" name="address" placeholder="Address" readonly>{{$viewCustomer->address}}</textarea>
                        </div>

                    </div>


                </div> <!-- row end here -->
                <div class="row">


                    <div class="col-md-4 form-group">
                        <label>Zip</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="zip" value="{{$viewCustomer->zip}}" readonly>

                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>State</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="state" value="{{$viewCustomer->state}}"readonly>
                        </div>

                    </div>

                    <div class="col-md-4 form-group">
                        <label>City</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="city" value="{{$viewCustomer->city}}" readonly>
                        </div>

                    </div>

                </div>


                <div class="row">



                    <div class="col-md-4 form-group">
                        <label>Phone</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="work_phone" value="{{$viewCustomer->phone}}" readonly>
                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>Mobile</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="mobile" value="{{$viewCustomer->mobile}}"readonly>
                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="email" value="{{$viewCustomer->email}}"readonly>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </div>

                <div class="row">


                    <div class="col-md-4 form-group">
                        <label>GST No<span style="color: red; font-size: 11px"> ( in case of company)</span></label>
                        <div class="input-group">
                            <input   type="text" class="form-control" name="vat" value="{{$viewCustomer->vatNo}}"readonly>
                        </div>

                    </div>

                    <div class="col-md-4 form-group">
                        <label>Other Details<span style="color: red; font-size: 11px"> ( if any)</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="credit_limit" value="{{$viewCustomer->credit_limit}}" readonly>
                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>PAN No<span style="color: red; font-size: 11px"> (if any)</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="pan"  value="{{$viewCustomer->pan}}" readonly>
                        </div>

                    </div>


                </div>
                <div class="row">
                    <div class="col-md-4 form-group">
                        <div class=" has-success has-feedback">
                            <label>Opening Balance</label>
                            <div class="input-group">
                                <input  type="number" min="1" class="form-control" name="opening_balance" value="{{$viewCustomer->opening_balance}}" placeholder="Opening Balance" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Bank A/c No</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="bankAct" value="{{$viewCustomer->bankAct}}" readonly>
                        </div>

                    </div>

                    <div class="col-md-4 form-group">
                        <label>Bank Name</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="BankName" value="{{$viewCustomer->bankName}}" readonly>
                        </div>

                    </div>

                </div> <!-- row end here -->
                <br>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">

                        <a class="btn btn-success" href="{{url('customers')}}"><span class="fa fa-backward"></span> Back</a>
                    </div>
                </div>


            </div>
            {{--</Form>--}}
            {{form::close()}}
                    <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}

<br>
            @endsection

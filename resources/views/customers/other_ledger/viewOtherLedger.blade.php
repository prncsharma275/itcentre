@extends('layouts.adminPanelTable')
@section('title')
    Ledger:Creation
@endsection

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
@endsection

@section('content')
    <h4 class="page-header text-center">View Ledger</h4>

    <div class="container-fluid ">

        <div class="well">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-group">
                    <div class=" has-success has-feedback">
                        <label>Ledger Group</label>
                        <div class="input-group">
                            <select class="form-control" name="ledger_group" required>
                                <?php $group = \App\Ledger_group::find($data->ledger_group) ?>
                                <option value="{{$group->id}}">{{$group->ledger_group_name}}</option>

                            </select>

                        </div>
                    </div>
                </div>

            </div> <!-- row end here -->
            <div class="row">



                <div class="col-sm-6 col-sm-offset-3 form-group">
                    <div class=" has-success has-feedback">
                        <label>Ledger Type</label>
                        <div class="input-group">
                            <select class="form-control" name="ledger_type">

                                <?php $group2 = \App\Ledger_type::where('id',$data->ledger_type2)->first() ?>
                                <option value="{{$group2->id}}">{{$group2->ledger_type_name}}</option>
                            </select>

                        </div>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-6 col-sm-offset-3 form-group">
                    <div class=" has-success has-feedback">
                        <label>Ledger Name<span style="color: red">*</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="ledger_name" value="{{$data->ledger_name}}" placeholder="Enter Ledger Name"readonly>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-6 col-sm-offset-3 form-group">
                    <div class=" has-success has-feedback">
                        <label>Opening Balance <span style="color: green">(if any)</span></label>
                        <div class="input-group">
                            <input  type="number" min="1" class="form-control" value="{{$data->opening_balance}}" name="opening_balance" placeholder="Opening Balance"readonly>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6 col-sm-offset-4">
                    <div class="form-group">

                        <a class="btn btn-success" href="{{url('customers')}}">Back</a>

                    </div>
                </div>

            </div>


        </div>
    </div>
    {{--</Form>--}}


    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    <br>
@endsection

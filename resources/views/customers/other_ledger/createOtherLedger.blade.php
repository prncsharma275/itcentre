@extends('layouts.adminPanelTable')
@section('title')
    Ledger:Creation
@endsection

@section('custom_css')
    @endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('customers')}}">Ledger Panel</a>
    </li>

    <li>
        <a href="{{url('#')}}">Create Ledger</a>
    </li>
@endsection

@section('content')
    <h3 class="heading">Add New Ledger</h3>
    <?php echo Form::open(array('route' => 'saveOtherLedger')); ?>
    <div class="container-fluid ">

<div class="well">

    <div class="row">

        <div class="col-sm-6 col-sm-offset-3 form-group">
            <div class=" has-success has-feedback">
                <label>Ledger Name<span style="color: red">*</span></label>
                <input  type="text" class="form-control" name="ledger_name" placeholder="Enter Ledger Name"required>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>

    <div class="row">


        <div class="col-sm-6 col-sm-offset-3 form-group">
            <div class=" has-success has-feedback">
                <label>Ledger Category<span style="color: red">*</span></label>
                <select class="form-control" name="ledger_type" required>
                    <option value="">Select Ledger Type</option>
                    <?php $gatas = \App\Ledger_type::all() ?>
                    @foreach($gatas as $gata)
                        <option value="{{$gata->id}}">{{$gata->ledger_type_name}}</option>
                    @endforeach
                </select>

            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-sm-6 col-sm-offset-3 form-group">
            <div class=" has-success has-feedback">
                <label>Opening Balance <span style="color: green">(if any)</span></label>
                    <input  type="number" min="0" class="form-control" name="opening_balance" placeholder="Opening Balance">

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6 col-sm-offset-3 form-group">
            <div class=" has-success has-feedback">
                <label>Balance Type <span style="color: green">(if any)</span></label>
                <select class="form-control" name="balance_type">
                    <option value="">Select One Value</option>
                    <option value="dr">Dr.(Inward)</option>
                    <option value="cr">Cr.(Outward)</option>
                </select>

            </div>
        </div>
    </div>

       <div class="row">
           <div class="col-sm-6 col-sm-offset-4">
               <div class="form-group">
                       <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-hdd"></span></button>
                       <a class="btn btn-success" href="{{url('customers')}}">Back</a>

               </div>
           </div>

       </div>


</div>
    </div>
    {{--</Form>--}}
    {{form::close()}}

    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    <br>
@endsection

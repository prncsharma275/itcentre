@extends('layouts.adminPanel')
@section('title')
    Sales Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        .select2.select2-container.select-container--default{
            height: 38px!important;
        }

        .well{
            background-color: #ffffff;
        }
        select{
            padding: 10px!important;
        }
    </style>
@endsection

@section('content')
    <h3 class="text-center">Ledger Panel</h3>
    <hr>
    <div class="row" style="margin: 70px;">
        <div class="col-sm-4">
            <a class="btn btn-info" style="padding: 30px" href="{{url('customers')}}"><i class="fa fa-users"></i> Ledgers</a>
        </div>

        <div class="col-sm-4">
            <a class="btn btn-danger" style="padding: 30px" href="{{url('ledger_type_route')}}"><i class="fa fa-user"></i> Ledger Types</a>
        </div>
        <div class="col-sm-4">
            <a class="btn btn-success" style="padding: 30px" href="{{url('ledger_group_route')}}"><i class="fa fa-list"></i> Ledger Group</a>
        </div>
    </div> <!-- row end here -->
    <br>
    <script>
        var product_type =  [/* states array*/];
        $("#product_type").select2({
            data: product_type
        });
    </script>
@endsection
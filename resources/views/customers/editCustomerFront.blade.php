<html>
<head>
    <title>SNS</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/comboBox.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
</head>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Edit Profile</h1>


    {{--<form class="well form-horizontal" action="storeCustomer" method="POST"  id="contact_form">--}}
    <?php echo Form::open(array('url' =>['updateCustomerFront',$customer->id])); ?>
    <div class="container">

        <div class="row">

        </div> <!-- row end here -->
        <div class="row">
            <div class="col-md-6 form-group">
                <label>Customer Name<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" name="customer_name" value="{{$customer->ledger_name}}" placeholder="Enter Customer Name Here" readonly>
                    <input  type="hidden" class="form-control" value="{{$customer->unique_id}}" name="unique_id">
                </div>

            </div>
            <div class="col-md-6 form-group">

                <label>Address</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <textarea  type="text" class="form-control"  name="address" placeholder="Address" style="width: 440px!important;resize: none " required>{{$customer->address}}</textarea>
                </div>
            </div>


        </div> <!-- row end here -->
        <div class="row">

            <div class="col-md-4 form-group">
                <label>Zip<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" value="{{$customer->zip}}" name="zip" placeholder="Zip" required>

                </div>

            </div>
            <div class="col-md-4 form-group">
                <label>State<span style="color: red; font-size: 11px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" value="{{$customer->state}}"  name="state" placeholder="State" required>
                </div>

            </div>
            <div class="col-md-4 form-group">
                <label>City<span style="color: red; font-size: 11px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" value="{{$customer->city}}"  name="city" placeholder="City" required>
                </div>

            </div>


        </div>


        <div class="row">
            <div class="col-md-4 form-group">
                <label>Mobile<span style="color: red; font-size: 11px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="text" class="form-control" name="mobile" value="{{$customer->mobile}}" placeholder="enter your mobile number" required>
                </div>

            </div>

            <div class="col-md-4 form-group">
                <label>Email<span style="color: red; font-size: 11px">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input  type="email" class="form-control" name="email" value="{{$customer->email}}" placeholder="Enter valid Email" required>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>


            <div class="col-md-4 form-group">
                <label>Account Password<span style="color: red; font-size: 11px;">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <input style="width:240px!important;" type="password" class="form-control" name="password" value="{{$customer->password}}" placeholder="Enter Password" required>
                </div>

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label"></label>
            <div class="col-md-4">
                <button type="submit" class="btn btn-warning" >Update <span class="glyphicon glyphicon-hdd"></span></button>
                <a class="btn btn-success" href="{{url('CustomerHome')}}">Back</a>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label"></label>
            <div class="col-md-4">

            </div>
        </div>

    </div>
    {{--</Form>--}}
    {{form::close()}}
            <!-- form row end here -->
    {{---------------------------------------------------------------------------------}}
</div>

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
<script>
    $(":file").filestyle({buttonBefore: true});
</script>
</html>
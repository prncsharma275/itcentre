@extends('layouts.adminPanelTable')
@section('title')
    Contra
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('otherCharge')}}">Contra Panel</a>
    </li>
@endsection

@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <h3 class="heading">Contra Entry</h3>
    <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createOtherCharge')}}"><i class="splashy-document_letter_add"></i>  New Contra</a>
    <table class="table table-striped table-bordered dTableR" id="dt_a">
        <thead>
        <tr>

            <th class="text-center">Contra No.</th>
            <th class="text-center">Date&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">Cr.</th>
            <th class="text-center">Dr.</th>
            <th class="text-center">Amount &nbsp;&nbsp;&nbsp;</th>

            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($product as $item)

            <tr>
                <td>{{$item->journal_no}}</td>
                <td><?php echo date( 'd/m/y', strtotime($item->journal_date)) ?></td>
                <td style="text-transform: uppercase!important;">{{$item->belongsToCustomer->ledger_name}}</td>
                <td style="text-transform: uppercase!important;">{{$item->belongsToCustomer2->ledger_name}}</td>
                <td>{{$item->total_amount}}</td>

                <td>

                    <a href="editOtherCharge/{{$item->id}}" class="btn btn-default btn-sm" title="EDIT">
                        <i class="splashy-document_letter_edit"></i>
                    </a>
                    <a href="printJournal/{{$item->id}}" class="btn btn-default btn-sm disabled" title="PRINT">
                        <i class="splashy-printer"></i>
                    </a>
                    <a href="deleteOtherCharge/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()" title="DELETE">
                        <i class="splashy-document_letter_remove"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
        $(document).ready( function () {
            $('#dt_a').dataTable( {
                "aoColumns": [
                    null,
                    { "sType": "date-uk" },
                    null,
                    null,
                    null,

                ]
            });

        } );

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    <br>
    {{--inner content here ------------------------------------}}
@endsection
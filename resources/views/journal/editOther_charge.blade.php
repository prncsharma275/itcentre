@extends('layouts.adminPanel')
@section('title')
    Contra Entry
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }


    </style>
    <script>

        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('otherCharge')}}">Contra Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Edit Contra</a>
    </li>
@endsection

@section('content')

    <h3 class="heading">Update Contra Entry</h3>

    <?php echo Form::open(array('url' =>['updateOtherCharge',$journal->id],'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
    <div class="row"> <!-- row start here -->

        <div class="col-md-6">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Contra Voucher No<span style="color:#EB3E28;">*</span></label>
                <input type="text" class="form-control" tabindex="0" name="journal_no" value="{{$journal->journal_no}}" id="inputSuccess2"readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Contra Date<span style="color:#EB3E28;">*</span></label>
                <input type="text" name="journal_date" class="form-control datepicker" value="<?php echo date( 'd/m/Y', strtotime($journal->journal_date)) ?>">
            </div>
        </div>

    </div> <!-- row ends here -->
    <br>

    <table id="items" class="table table-striped table-bordered table-condensed">

        <tr style="background-color: #e9f3f8">
            <th >Particulars</th>
            <th >DR / CR</th>
            <th>Amount</th>
        </tr>
        <tr class="item-row" style="border-bottom: solid 1px black">

            <td style="min-width: 50%"><select id="country" name="ledger_cr"  style="width:99%;" required>
                    <option value="{{$journal->ledger_cr}}">{{$journal->belongsToCustomer2->ledger_name}}</option>
                    <?php $bank_names= \App\Customer::where('ledger_type2',4)->orWhere('ledger_type2',3)->get(); ?>
                    @foreach($bank_names as $bank)
                        <option value="{{$bank->id}}">{{$bank->ledger_name}}</option>
                    @endforeach
                </select></td>
            <td style="min-width: 10%"><input name="cr" class="amount form-control " value="CR" style="width: 99%; height: 38px"readonly></td>
            <td><input name="amount_cr" id="amount_cr" class="amount form-control" value="{{$journal->amount_cr}}" style="width: 99%; height: 38px"required></td>


        </tr>

        <tr class="item-row" style="border-bottom: solid 1px black">

            <td style="min-width: 50%">

                <select id="customer" name="ledger_dr"  style="width:99%;" required>
                    <option value="{{$journal->ledger_dr}}">{{$journal->belongsToCustomer->ledger_name}}</option>

                    <?php $bank_names= \App\Customer::where('ledger_type2',4)->orWhere('ledger_type2',3)->get(); ?>
                    @foreach($bank_names as $bank)
                        <option value="{{$bank->id}}">{{$bank->ledger_name}}</option>
                    @endforeach
                </select></td>
            <td  style="min-width: 10%"><input name="dr" class="amount form-control" value="DR" style="width: 99%; height: 38px"readonly></td>
            <td><input name="amount_dr" id="amount_dr" class="amount form-control " value="{{$journal->amount_dr}}" style="width: 99%; height: 38px"readonly>
                <input type="hidden" name="previous_dr" value="{{$journal->ledger_dr}}">
                <input type="hidden" name="previous_cr" value="{{$journal->ledger_cr}}">
            </td>


        </tr>

<tr>
    <td colspan="3">
        <textarea name="narration" class="form-control" style="width: 100%!important;resize: none;border-color: green" required placeholder="Description">{{$journal->narration}}</textarea>
    </td>
</tr>
    </table>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <button type="submit" class="btn btn-success btn-sm" name="submit"><i class="splashy-document_a4_add"></i> Update</button>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <button type="reset" class="btn btn-warning btn-sm" name="submit"><i class="splashy-refresh_backwards"></i> Reset</button>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-4">
                    <a href="{{url('otherCharge')}}" class="btn btn-danger btn-sm"><i class="splashy-gem_cancel_1"></i> Cancel</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered table-striped">
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
                <tr>
                    <td width="50%">Total Amount</td>
                    <td><span class="fa fa-rupee"></span>

                        <input  name="total_amount" value="{{$journal->total_amount}}" id="total_amount" style="background-color: #fff!important;border:none!important;width: 90%;text-align: left" readonly>
                </tr>
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
            </table>


        </div>
    </div>

    {{form::close()}}

    <script>
        $("option").each(function() {
            $(this).text($(this).text().charAt(0).toUpperCase() + $(this).text().slice(1));
        });

        var customer =  [/* states array*/];
        $("#customer").select2({
            data: customer
        });
        $('#customer').select2('open').select2('close');
        var payment_mode =  [/* states array*/];
        $("#payment_mode").select2({
            data: payment_mode
        });

        var customer =  [/* states array*/];
        $("#country").select2({
            data: customer
        });

        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
        //                -------------payment type details--------

        function copy()
        {
            var box1=$('#amount_cr').val();
            $('#amount_dr').val(box1);
            $('#total_amount').val(box1);
        }
        $("#amount_cr").blur(copy);

    </script>
    <br>
@endsection
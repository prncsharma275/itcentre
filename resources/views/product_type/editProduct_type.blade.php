@extends('layouts.adminPanel')
@section('title')
    Product Category:Edit
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('product_type')}}">Product Category Panel</a>
    </li>
    <li>
        <a href="{{url('createProduct_type')}}">Create Product Category </a>
    </li>

    <li>
        <a href="{{url('#')}}">Edit Product Category </a>
    </li>
@endsection

@section('content')
<h3 class="heading">Edit Product Category</h3>
    {!! Form::open(['method'=>'get', 'url' =>['updateProdcut_type',$editProduct_type->id]]) !!}

    <div class="row">
<br>
        <br>
        <div class="col-md-6 col-sm-6 col-md-offset-2 col-sm-offset-2" style="margin-top: 10px">
            <input type="text"  value="{{$editProduct_type->product_name}}" class="form-control" name="product_name" id="product_name" placeholder="Product Type Name"   aria-describedby="basic-addon1" required>
        </div>
        <div class="col-md-4 col-sm-4" style="margin-top: 10px">
            <input type="submit" class="btn btn-success" value="Update">
            <a href="{{url('product_type')}}" class="btn btn-info"><span class="glyphicon glyphicon-backward"></span> Back</a>
        </div>
    </div><!-- /.row -->

    </form>
    {{--inner content here ------------------------------------}}
    <br>
@endsection
@extends('layouts.adminPanelTable')
@section('title')
    Category
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('product_type')}}">Product Category Panel</a>
    </li>
@endsection
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif
    <h3 class="heading">Product Category</h3>
    <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createProduct_type')}}"><i class="splashy-document_letter_add"></i>  Product Category</a>

    <table class="table table-striped table-bordered dTableR" id="dt_a">
        <thead>
        <tr>
            <th style="text-align: center">Category Name</th>
            <th style="text-align: center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($viewProduct_type as $viewProduct_type)

            <tr>

                <td style="text-align: center">{{$viewProduct_type->product_name}}</td>

                <td style="text-align: center">

                    <a href="editProduct_type/{{$viewProduct_type->id}}" title="EDIT">
                        <i class="splashy-document_letter_edit"></i>
                    </a>
                    <a href="deleteProduct_type/{{$viewProduct_type->id}}" onclick="return ConfirmDelete()" title="DELETE">
                        <i class="splashy-document_letter_remove"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();

            $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert").slideUp(500);
            });

        } );

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    {{--inner content here ------------------------------------}}

@endsection
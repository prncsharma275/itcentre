<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>IT Centre</title>
    <link rel="stylesheet" href="style.css" media="all" />
    {{--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="{{asset('css/bootstrap/boot4.1.css')}}" rel="stylesheet">
    <style type="text/css" media="print">
        .rupees {}


        @page {
            size: auto;
            /* auto is the initial value */
            margin-left: 15px!important;
            /* this affects the margin in the printer settings */
            margin-right: 5px!important;
            /* this affects the margin in the printer settings */
            margin-top: 10px;
            /* this affects the margin in the printer settings */
            margin-bottom: 5px;
            background-color: gainsboro;
            font-size: 15px;
            /*margin: px;*/

        }



        /*@media print{@page {size: landscape}}*/

        @media print {

            tfoot.report-footer:after {
                counter-increment: page 1;
                /*counter-reset: pages 1;*/
                content: "Page " counter(page) " of " counter(pages);
            }
            .hidden-print {
                display: none !important;
            }
        }

    </style>
</head>
<?php $comp = \App\Company_detail::find(1)?>
<body style="">
<p class="text-center" id="pageno">Payment Copy</p>
<table width="100%" border="1">
    <tr>
        <td style="width: 600px"><h4 class="text-left">&nbsp;{{$comp->company_name}}</h4>
            <h6 class="text-left">&nbsp;{{$comp->street_1}}
                <br>&nbsp;{{$comp->street_2}} {{$comp->city}}, Pin &#45; {{$comp->zip}}, {{$comp->state}}</br>
                &nbsp;Email- {{$comp->email}}, Cell-{{$comp->mobile}}
                <br>&nbsp;{{$comp->gst_no}}</h6></td>
        <td style="width: 500px">
            <p class="text-left" style="font-size: 14px;margin-bottom: -1px">&nbsp;Issue From</p>
            {{--//retail customer condtion--}}
            @if($printBill->customer_id==1)
                <h4 class="text-left" style="margin-bottom: -1px">&nbsp;{{$printBill->retail_name}}({{$printBill->belongsToCustomer->ledger_name}})</h4>
                <h6 class="text-left">&nbsp;{{$printBill->city}}
                    <br>&nbsp;{{$printBill->mobile}}
                </h6>
            @else
                <h4 class="text-left" style="margin-bottom: -1px">&nbsp;{{$printBill->belongsToCustomer->ledger_name}}</h4>
                <h6 class="text-left">&nbsp;{{$printBill->belongsToCustomer->address}}
                    <br>&nbsp;{{$printBill->belongsToCustomer->city}}&nbsp; {{$printBill->belongsToCustomer->state}}
                    <br>&nbsp;{{$printBill->belongsToCustomer->email}} &nbsp; {{$printBill->belongsToCustomer->mobile}}
                    @if($printBill->belongsToCustomer->vatNo !="")
                        <br>&nbsp;GSTIN-{{$printBill->belongsToCustomer->vatNo}}
                    @endif
                </h6>
            @endif

        </td>
        <td style="width: 450px">
            <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Payment No. - {{$printBill->unique_id}}</h5>
            <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Payment Date - <?php echo date( 'd/m/y', strtotime($printBill->billing_date)) ?></h5>
            @if($printBill->payment_type=='cash')
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Payment Type:<span style="color: #000;text-transform: capitalize!important;"> <?php echo $printBill->payment_type ?></span></h5>

            @elseif($printBill->payment_type=='cheque')
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Payment Type:<span style="color: #000;text-transform: capitalize!important;"> <?php echo $printBill->payment_type ?></span></h5>
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Cheque No:<span style="color: #000"> <?php echo $printBill->cheque_no ?></span></h5>
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Cheque Issue Date:<span style="color: #000"> <?php echo $printBill->cheque_issue_date ?></span></h5>
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Cheque Deposit Date:<span style="color: #000"> <?php echo $printBill->cheque_diposit_date ?></span></h5>

            @else
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Payment Type:<span style="color: #000;text-transform: capitalize!important;"> <?php echo $printBill->payment_type ?></span></h5>
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Fund Type:<span style="color: #000"> <?php echo $printBill->fund_type ?></span></h5>
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Transaction ID :<span style="color: #000"> <?php echo $printBill->fund_transaction_no ?></span></h5>
                <h5 class="text-left" style="margin-top: -2px;font-size: 17px">&nbsp;Transaction Date :<span style="color: #000"> <?php echo $printBill->fund_transfer_date ?></span></h5>

            @endif
        </td>
    </tr>
</table>

<table width="100%" border="1" style="text-align: center">
    <tr>
        <td style="width: 50%">Description</td>
        <td style="width: 50%">Amount</td>
    </tr>
    <tr>
        <td style="width: 50%">{{$printBill->desc}}</td>
        <td style="width: 50%">{{$printBill->amount}}</td>

    </tr>
    <tr style="border-right: solid 1px #000!important;">
        <?php
        $number = $printBill->amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
                '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
                '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
                '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
                '13' => 'Thirteen', '14' => 'Fourteen',
                '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
                '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
                '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
                '60' => 'Sixty', '70' => 'Seventy',
                '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                        " " . $digits[$counter] . $plural . " " . $hundred
                        :
                        $words[floor($number / 10) * 10]
                        . " " . $words[$number % 10] . " "
                        . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
                "." . $words[$point / 10] . " " .
                $words[$point = $point % 10] : '';?>

        <td colspan="2" style="text-align: left;padding: 7px!important;"> <?php echo $result . "Rupees  " . $points . "Only"; ?></td>
    </tr>

    <tr>
        <td style="text-align: left;padding:7px!important;"><br>
            <br><br><br>Party's Seal and Signature
        </td>
        <td style="text-align: right;padding: 7px!important;">
            <br>
            <br><br><br>For,IT Centre Seal and Signature
        </td>


    </tr>

    <tr>
        <td colspan="2">
            <div class="footer-info">
                <p class="hidden-print" style="padding-top:5px!important;">&nbsp;<button class="btn btn-success hidden-print" onclick="myFunction()">Print</button>
                    <a href="{{url('createPayment')}}" class="btn btn-info hidden-print">Create New Payment</a>
                    <a href="{{url('/payment')}}" class="btn btn-primary hidden-print">Back</a></p>
                <p class="text-center" style="font-size: 18px;margin-bottom: 0;text-transform: uppercase!important;">SUBJECT TO {{$comp->city}} JURISDICATION</p>
                <p class="text-center" style="font-size: 14px">This is Computer Generated Invoice</p>

            </div>
        </td>
    </tr>
</table>


<script>
    function myFunction() {
        window.print();
    }
</script>
</body>

</html>

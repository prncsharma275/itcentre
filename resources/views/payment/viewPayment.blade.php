@extends('layouts.adminPanelTable')
@section('title')
    Payment
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('payment')}}">Payment Panel</a>
    </li>
@endsection

@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
            <h3 class="heading">Payment</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createPayment')}}"><i class="splashy-document_letter_add"></i> Payment</a>
            <table class="table table-striped table-bordered dTableR" id="dt_a">
                <thead>
                <tr>

                    <th class="text-center">Payment No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Supplier Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product as $item)

                    <tr>
                        <td>{{$item->unique_id}}</td>
                        <td><?php echo date( 'd/m/y', strtotime($item->billing_date)) ?></td>
                        <td>{{$item->belongsToCustomer->ledger_name}}</td>
                        <td>{{$item->amount}}</td>


                        <td>

                            <a href="editPayment/{{$item->id}}" class="btn btn-default btn-sm" title="EDIT">
                                <i class="splashy-document_letter_edit"></i>
                            </a>
                            <a href="{{route('payment_print',$item->id)}}" target="_blank" class="btn btn-default btn-sm" title="PRINT">
                                <i class="splashy-printer"></i>
                            </a>
                            <a href="deletePayment/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()"title="DELETE">
                                <i class="splashy-document_letter_remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    @if(Session::has('status'))
        <?php $id =   Session::get('status')  ; ?>
        <div id="myModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Print Invoice</h4>
                    </div>
                    <div class="modal-body">
                        <h2>Do You Want To Print Previous Bill?</h2>
                    </div>
                    <div class="modal-footer">
                        <a id="report" class="btn btn-success"  target="_blank" href="{{route('payment_print',$id)}}">Print</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(function() {
                $('#myModal').modal('show');
//                        $('#report').click();
            });
        </script>
    @endif
            <script>
                jQuery.extend( jQuery.fn.dataTableExt.oSort, {
                    "date-uk-pre": function ( a ) {
                        var ukDatea = a.split('/');
                        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    },

                    "date-uk-asc": function ( a, b ) {
                        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                    },

                    "date-uk-desc": function ( a, b ) {
                        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                    }
                } );
                $(document).ready( function () {
                    $('#table').dataTable( {
                        "aoColumns": [
                            null,
                            { "sType": "date-uk" },
                            null,
                            null,
                            null,

                        ]
                    });

                } );

                //    delete commande
                function ConfirmDelete()
                {
                    var x = confirm("Are you sure you want to Cancel This Bill?");
                    if (x)
                        return true;
                    else
                        return false;
                }


            </script>
            {{--inner content here ------------------------------------}}
<br>
@endsection
@extends('layouts.adminPanel')
@section('title')
  Payment
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('payment')}}">Payment Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Create Payment</a>
    </li>
@endsection
@section('content')
            <br>
            <h3 class="heading">Create Payment</h3>
                    <?php echo Form::open(array('route' => 'storePayment', 'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
                    <div class="row"> <!-- row start here -->
                        <div class="col-md-4">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Select Ledger<span style="color:#EB3E28;">*</span></label>
                                <select id="customer" name="customer" class="form-control" style="width: 99%!important;"   required>
                                    <?php  $product_type = DB::table('customers')->get(); ?>
                                    <option value="">Please select Ledger</option>
                                    @foreach($product_type as $product_type)
                                        <option value="{{$product_type->id}}">{{$product_type->ledger_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Payment Voucher No<span style="color:#EB3E28;">*</span></label>
                                <?php  $currentDate= date("Y/m/d");

                                ?>
                                <?php  $dateNew = DB::table('payment_bill_session')->whereDate('startDate', '<=', $currentDate)->whereDate('closeDate', '>=', $currentDate)->first(); ?>

                                <?php $sale_format = \App\Company_detail::find(1) ?>

                                    @if($dateNew->bill==0)
                                        <?php $number=1;
                                        $bilNum = sprintf("%04d", $number);


                                        $invoiceNo=$sale_format->billing_payment."$dateNew->session"."/".$bilNum."";
                                        $session_id= $dateNew->id;

                                        ?>
                                    @else
                                        <?php $number=$dateNew->bill+1;
                                        $bilNum = sprintf("%04d", $number);

                                        $invoiceNo=$sale_format->billing_payment."$dateNew->session"."/".$bilNum."";
                                        $session_id= $dateNew->id;
                                        ?>

                                    @endif


                                <input type="text" class="form-control" tabindex="0" name="sale_return_no" value="<?php echo $invoiceNo ?>" id="inputSuccess2"readonly>
                                <input type="hidden" class="form-control" name="session_id" value="<?php echo $session_id ?>" id="inputSuccess2">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Payment Date<span style="color:#EB3E28;">*</span></label>
                                <input type="text" name="sale_return_date" class="form-control datepicker" value="{{date("d/m/Y")}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Payment Mode<span style="color:#EB3E28;">*</span></label>
                                <select id="payment_mode" name="payment_type" class="payment_selected_type form-control" style="width: 99%!important;"   required>
                                    <option value="">Please select Mode</option>
                                    <option value="cash">Cash</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="fund">Fund Transfer</option>
                                </select>
                            </div>
                        </div>
                    </div> <!-- row ends here -->
                    <br>
                    <div class="row" id="payment_cheque">
                        <div class="col-md-3">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Cheque No.<span style="color:#EB3E28;">*</span></label>
                                <input type="text" name="cheque_no" id="cheque_no" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Issue Date<span style="color:#EB3E28;">*</span></label>
                                <input type="text" name="cheque_issue_date" id="cheque_issue_date" class="form-control datepicker" value="{{date("d/m/Y")}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Deposit Date<span style="color:#EB3E28;">*</span></label>
                                <input type="text" name="cheque_diposit_date" id="cheque_diposit_date" class="form-control datepicker" value="{{date("d/m/Y")}}">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Bank Name<span style="color:#EB3E28;">*</span></label>
                                <select name="cheque_bank_id" id="cheque_bank_id" class="form-control">
                                    <option value="">select Bank</option>
                                    <?php $bank_names= \App\Customer::where('ledger_type2',4)->get(); ?>
                                    @foreach($bank_names as $bank)
                                        <option value="{{$bank->id}}">{{$bank->ledger_name}}</option>
                                     @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="payment_fund">
                        <div class="col-md-3">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Fund Type<span style="color:#EB3E28;">*</span> <span style="color:green;font-size: 13px">(NEFT/ RTGS/ IMPS)</span></label>
                                <input type="text" name="fund_type" id="fund_type" class="form-control"required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Transaction No.<span style="color:#EB3E28;">*</span></label>
                                <input type="text" name="fund_transaction_no" id="fund_transaction_no" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Transferred Date<span style="color:#EB3E28;">*</span></label>
                                <input type="text" name="fund_transfer_date" id="fund_transfer_date" class="form-control datepicker" value="{{date("d/m/Y")}}"required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group has-success has-feedback">
                                <label for="inputSuccess2">Bank Name<span style="color:#EB3E28;">*</span></label>
                                <select name="fund_bank_id" id="fund_bank_id" class="form-control" required>
                                    <option value="">select Bank</option>
                                    <?php $bank_names= \App\Customer::where('ledger_type2',4)->get(); ?>
                                    @foreach($bank_names as $bank)
                                        <option value="{{$bank->id}}">{{$bank->ledger_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <table id="items" class="table table-striped table-bordered table-condensed">

                        <tr style="background-color: #e9f3f8">
                            <th >Description</th>
                            <th>Amt.</th>
                        </tr>

                        <tr class="item-row" style="border-bottom: solid 1px black">

                            <td style="min-width: 80%"> <input name="desc"  class="form-control"  style="width: 99%; height: 38px"required></td>
                            <td><input name="amount" type="number" step="0.01" class="amount form-control " style="width: 99%; height: 38px"required></td>


                        </tr>


                    </table>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <button type="submit" class="btn btn-success btn-sm" name="submit"><i class="splashy-document_a4_add"></i> Save</button>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <button type="reset" class="btn btn-warning btn-sm" name="submit"><i class="splashy-refresh_backwards"></i> Reset</button>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <a href="{{url('payment')}}" class="btn btn-danger btn-sm"><i class="splashy-gem_cancel_1"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered table-striped">
                                <tr style="background-color: #e9f3f8">
                                    <td width="50%" style="color: #fff!important;"></td>
                                    <td style="color: #fff!important;"></td>
                                </tr>
                                <tr>
                                    <td width="50%">Total Outstanding (Till Date)</td>
                                    <td><span class="fa fa-rupee"></span>

                                        <input  name="total_outstanding" id="total_outstanding" style="background-color: #fff!important;border:none!important;width: 90%;text-align: left" readonly></td>
                                </tr>

                                <tr>
                                    <td width="50%">Paid Amount</td>
                                    <td><span class="fa fa-rupee"></span>
                                        <span id="paid_amount"></span>
                                        <input type="hidden" name="paid_amount_input" id="paid_amount_input"></td>
                                </tr>

                                <tr>
                                    <td width="50%">Balance</td>
                                    <td><span class="fa fa-rupee"></span>
                                        <span id="new_outstanding"></span>
                                        <input type="hidden" name="new_outstanding" id="new_outstanding_input">
                                    </td>
                                </tr>
                                <tr style="background-color: #e9f3f8">
                                    <td width="50%" style="color: #fff!important;"></td>
                                    <td style="color: #fff!important;"></td>
                                </tr>
                            </table>


                        </div>


                    </div>

                    {{form::close()}}
            <script>
                $('option').each(function() {
                    t = $(this).text();
                    $(this).text(t.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }))
                });
                var customer =  [/* states array*/];
                $("#customer").select2({
                    data: customer
                });
                $('#customer').select2('open').select2('close');
                var payment_mode =  [/* states array*/];
                $("#payment_mode").select2({
                    data: payment_mode
                });
                $( function() {
                    $( ".datepicker" ).datepicker({
                        dateFormat: 'dd/mm/yy'
                    });
                } );
                //                -------------payment type details--------
                $(document).ready(function(){
                    $('#payment_cheque').hide();
                    $('#payment_fund').hide();
                });



                $('.payment_selected_type').bind('change', function(event) {

                    var i= $('.payment_selected_type').val();
                    if(i=="cash")
                    {
                        $('#payment_cheque').hide();
                        $('#payment_fund').hide();
                        $('#payment_cheque').find('input').removeAttr('required');
                        $('#payment_cheque').find('select').removeAttr('required');
                        $('#payment_fund').find('input').removeAttr('required');
                        $('#payment_fund').find('select').removeAttr('required');
                    }

                    if(i=="cheque") // equal to a selection option
                    {
                        $('#payment_cheque').show();
                        $('#payment_fund').hide(); // hide the first one
                        $('#payment_cheque').find('input').attr('required','true');
                        $('#payment_cheque').find('select').attr('required','true');
                        $('#payment_fund').find('input').removeAttr('required');
                        $('#payment_fund').find('select').removeAttr('required');
                    }
                    if(i=="fund")
                    {
                        $('#payment_cheque').hide(); // hide the first one
                        $('#payment_fund').show(); // show the other one
                        $('#payment_fund').find('input').attr('required','true');
                        $('#payment_fund').find('select').attr('required','true');
                        $('#payment_cheque').find('input').removeAttr('required');
                        $('#payment_cheque').find('select').removeAttr('required');
                    }
                });

                //                --------------------- getting Balance of supplier ------------------
                $('select[name="customer"]').on('change', function() {

                    var stateID = $(this).val();

                    if(stateID) {

                        $.ajax({

                            url: '{{ url('/') }}/ajaxPayment/'+stateID,

                            type: "GET",

                            dataType: "json",

                            success:function(data) {


                                var Vals    =  data;
                                $("input[name='total_outstanding']").val(Vals.balance);

                            }

                        });

                    }else{

                        $('select[name="city"]').empty();

                    }

                });

                function update_price(){
                    price = $('.amount').val();

                    $('#paid_amount_input').val(""+price);
                    $('#paid_amount').html(""+price)
                }

                function update_balance(){
                    paid_amount = $('.amount').val();
                    outstanding = $('#total_outstanding').val();
                    new_balance=Number(outstanding)-Number(paid_amount)
                    $('#new_outstanding_input').val(""+new_balance);
                    $('#new_outstanding').html(""+new_balance)
                }

                $(".amount").blur(update_price);
                $(".amount").blur(update_balance);


            </script>
    <br>
         @endsection
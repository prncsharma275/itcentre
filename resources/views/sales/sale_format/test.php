<!DOCTYPE html>
<html>

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    <title>Editable Invoice</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('salesAndPurchaseStyle/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('salesAndPurchaseStyle/css/print.css')}}" media="print" />
    <script type='text/javascript' src='{{ asset('salesAndPurchaseStyle/js/min.js')}}'></script>
    <script src="{{ asset('js/saleJquery.min.js')}}"></script>
    <link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js')}}"></script>
    <style>
        .billing_table input,select{
            font-size: 12px!important;
            padding: 0px;
            text-align: center;
        }
        .select2-container--default{
            font-size: 12px;
            width:330px!important;
        }
        .billing_table th{
            font-size: 12px;
        }
        textarea:focus{
            background-color: #ffffff;
        }
        textarea:hover{
            background: none;
        }
        .main_table{
            width: 100%;
        }
        .main_td{
            width: 100%;
        }

    </style>

</head>

<body style="margin-left: 20px;margin-right: 10px;">
<div class="container">
    <h2 class="text-center">SALE INVOICE</h2>
    <hr>
</div>
<div class="container-fluid billing_details" style="padding: 0">
    <?php echo Form::open(array('route' => 'storeSale')); ?>
    <div class="row">
        <div class="col-md-3">
            <label>Customer Name</label>
            <select id="customer" name="customer" style="width:300px;" required>

                <?php  $product_type=\App\Customer::All();?>
                <option value="">Please select Customer</option>
                @foreach($product_type as $product_type)
                <option value="{{$product_type->id}}">{{$product_type->customer_name}}</option>
                @endforeach
            </select><br>
            <label for="address">Address</label>
            <textarea name="address" class="form-control" style="resize: none;  padding: 5px; background-color: #eeeeee; border: none;color: blueviolet;font-weight: 300" readonly></textarea>
            <label for="city">City</label>
            <input name="city" id="city" class="form-control" type="text"  style="background-color: #eeeeee; border: none; padding: 5px; color: blueviolet;font-weight: 600" readonly >


        </div> <!-- col-md-6 end here -->
        <div class="col-md-3 col-md-offset-1">
            <label for="phone">Mobile</label>
            <input name="phone" class="form-control" type="text"  style="background-color: #eeeeee; border: none; padding: 5px; color: blueviolet;font-weight: 600" readonly><br>
            <label>Email</label><br>
            <input name="email" class="form-control" type="text"  style="background-color: #eeeeee; border: none; padding: 5px; color: blueviolet;font-weight: 600" readonly>
            <label for="tax_type"style="padding-top: 10px">GST</label>
            <select name="tax_type" id="tax_type" class="form-control" required>
                <option value="{{$text_type}}">{{$text_type}}</option>

            </select>

        </div>
        <div class="col-md-4 col-md-offset-1">

            <?php  $currentDate= date("Y/m/d");

            ?>
            <?php  $date = DB::table('sale_bill_session')->whereDate('startDate', '<=', $currentDate)->whereDate('closeDate', '>=', $currentDate)->get(); ?>


            @foreach($date as $dateNew)
            @if($dateNew->bill==0)
            <?php $number=1;
            $bilNum = sprintf("%04d", $number);


            $invoiceNo="SKO/"."$dateNew->session"."/".$bilNum."";
            $session_id= $dateNew->id;

            ?>
            @else
            <?php $number=$dateNew->bill+1;
            $bilNum = sprintf("%04d", $number);

            $invoiceNo="SKO/"."$dateNew->session"."/".$bilNum."";
            $session_id= $dateNew->id;
            ?>

            @endif
            @endforeach

            <table  class="table table-bordered main_table">
                <tr>
                    <td class="meta-head" style="background-color: #EEEEEE">Invoice #</td>
                    <td style="background-color: #1f648b;color: #ffffff"><span><?php echo $invoiceNo ?></span>
                        <input type="hidden"  name="sale_id" value="<?php echo $invoiceNo ?>">
                        <input type="hidden" name="session_id" value="<?php echo $session_id ?>">
                    </td>
                </tr>
                <tr>

                    <td class="meta-head" style="background-color: #EEEEEE">Date</td>
                    <td style="background-color: #1f648b;color: #ffffff"><div class="date"><?php echo date('d-m-Y') ?></div></td>

                </tr>
                <tr>

                    <td class="meta-head" style="background-color: #EEEEEE">Challan NO</td>
                    <td style="background-color: #1f648b;"><input style="width: 100%;height: 25px"  class="form-control" name="challan_no"> </td>

                </tr>
                <tr>

                    <td class="meta-head" style="background-color: #EEEEEE">Challan Date (dd/mm/yyyy)</td>
                    <td style="background-color: #1f648b;"><input style="width: 100%;height: 25px" class="form-control" name="challan_date"> </td>

                </tr>
                <tr>

                    <td class="meta-head" style="background-color: #EEEEEE">Order NO</td>
                    <td style="background-color: #1f648b;"><input style="width: 100%;height: 25px" class="form-control" name="order_no"> </td>

                </tr>
                <tr>

                    <td class="meta-head" style="background-color: #EEEEEE">Order Date (dd/mm/yyyy)</td>
                    <td style="background-color: #1f648b;"><input style="width: 100%;height: 25px" class="form-control" name="order_date"> </td>

                </tr>
            </table>
        </div>
    </div> <!-- row ends here -->



</div> <!-- billing container end here -->


<table id="items"  class="table table-bordered billing_table">

    <tr>
        <th style="background-color: #ccc">Item</th>
        <th style="background-color: #ccc">Batch No</th>
        <th style="background-color: #ccc">Mfg.Date</th>
        <th style="background-color: #ccc">Exp.Date</th>
        <th style="background-color: #ccc">Qnty.</th>
        <th style="background-color: #ccc">Unit.</th>
        <th style="background-color: #ccc">Rate</th>
        <th style="background-color: #ccc">Amt.</th>
        <th style="background-color: #ccc">SGST %</th>
        <th style="background-color: #ccc">CGST %</th>
        <th style="background-color: #ccc">IGST %</th>
        <th style="background-color: #ccc">Txt.Amt</th>
        <th style="background-color: #ccc">Total</th>
    </tr>

    <tr class="item-row" style="border-bottom: solid 1px black">
        <td style="width: 31%" class="item-name"><div class="delete-wpr">
                <div>
                    <select id="country" name="rows[0][product]"  style="width:99%;" required>
                        <option value="">Please select Item</option>
                        <?php  $product_type=\App\Product::All();?>
                        @foreach($product_type as $product_type)
                        <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>
                        @endforeach
                    </select>
                </div>
                <a class="delete" href="javascript:;" title="Remove row">X</a></div></td>
        <td class="main_td"><input name="rows[0][batch_no]" class="form-control" style="width: 116%;margin-left:-7px;height: 38px"></td>
        <td class="main_td"><input name="rows[0][mfg_date]" class="mfg_date form-control" style="width:127%; margin-left:-7px;height:38px" ></td>
        <td class="main_td"><input name="rows[0][exp_date]" class="exp_date form-control" style="width:127%; margin-left:-7px;height:38px" ></td>
        <td class="main_td"><input name="rows[0][qty]" class="qty form-control" style="width: 58px" required></td>
        <td class="main_td"><select name="rows[0][unit]" class="qty form-control" style="width: 73px" required>
                <option>pcs</option>
                <option>box</option>
            </select></td>
        <td><input name="rows[0][cost]" class="cost form-control" style="width: 67px;text-align: center" required></td>
        <td class="main_td"><input name="rows[0][amt_before_tax]" class="amt_before_tax form-control" style="width: 80px;text-align: center" required></td>
        <td class="main_td"><input name="rows[0][costPrice]" class="costPrice" type="hidden">
            <input name="rows[0][sgst]" class="sgst form-control" style="width: 40px;text-align: center" readonly></td>
        <td class="main_td"><input name="rows[0][cgst]" class="cgst form-control" style="width: 40px;text-align: center" readonly></td>
        <td class="main_td"><input name="rows[0][igst]" class="igst form-control" style="width: 40px;text-align: center" readonly></td>
        <td class="main_td"><input name="rows[0][text_amount]" class="text_amount form-control" style="width:75px;text-align: center" readonly></td>
        <td class="main_td"><input name="rows[0][price]" class="price form-control" style="width: 100px" readonly></td>

    </tr>



<!--    <tr id="hiderow">-->
<!--        <td colspan="13"><a id="addrow" href="javascript:;" class="btn btn-default" title="Add a row">Add New Product</a></td>-->
<!--    </tr>-->

    <tr id="hiderow">
        <td colspan="12"><a id="addrow" href="javascript:;" title="Add a row">Add New Product</a></td>
    </tr>
</table>
</div>
<br>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <input type="submit" id ='two' onclick="change()" class="btn btn-success" name="submit" value="Save">
            <input type="reset" class="btn btn-danger" value="Clear">
            <a href="{{url('sale')}}" class="btn btn-info">Back</a>
        </div> <!-- col end here -->
        <div class="col-md-6">
            <table class="table table-bordered">
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line">Total Amount(without Tax)</td>
                    <td class="total-value"><div id="subtotal"></div>
                        <input id="subtotal1" type="hidden"  name="subtotal_without_vat">
                    </td>
                </tr>
                @if($text_type=="IGST")
                <tr>

                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line">Total IGST </td>
                    <td class="total-value">
                        <div id="total" style="display: none"></div>
                        <div id="total_igst"></div>
                        <input id="total_igst_text"  type="hidden" name="grand_igst">
                    </td>
                </tr>

                @endif
                @if($text_type=="CGST&SGST")
                <tr>

                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line">Total CGST</td>
                    <td class="total-value">
                        <div id="total" style="display: none"></div>
                        <div id="total_cgst"></div>
                        <input id="total_cgst_text"  type="hidden" name="grand_cgst">
                    </td>
                </tr>
                <tr>

                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line">Total SGST </td>
                    <td class="total-value">
                        <div id="total" style="display: none"></div>
                        <div id="total_sgst"></div>
                        <input id="total_sgst_text"  type="hidden" name="grand_sgst">
                    </td>
                </tr>

                @endif
                <tr>

                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line">Total Tax </td>
                    <td class="total-value">
                        <div id="total" style="display: none"></div>
                        <div id="grand_tax"></div>
                        <input id="grand_tax_text"  type="hidden" name="grand_tax">
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="blank"></td>
                    <td colspan="2" class="total-line" style="background-color: #1f648b;color: white">Sub Total</td>
                    <td class="total-value" style="background-color: #1f648b;color: white">

                        <div id="subtotalGrand"></div>
                        <input id="subtotalGrand1"  type="hidden" name="subtotalGrand">
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line">Discount @<input style="width: 70px;float: right" name="discount" id="discount"placeholder="% here"></td>


                    <td class="total-value">
                        <textarea style="display: none" id="paid">$0.00</textarea>
                        <div id="total_spcl_discount"></div>
                        <input id="total_spcl_discount1"  name="total_spcl_discount">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line balance">Frieght & Courier Charge </td>
                    <td class="total-value balance">
                        <input class="courierCharge" name="fright">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line balance">Total</td>
                    <td class="total-value balance">
                        <div class="total1"></div>
                        <input id="total2" type="hidden" name="total_after_vat">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line balance">Grand Total</td>
                    <td class="total-value balance">
                        <div class="grand_total" style="background-color: #269abc;color:white"></div>
                        <input id="grand_total1" type="hidden" name="grand_total">
                    </td>
                </tr>
            </table>
        </div>

    </div> <!-- row ends here -->
</div>
{{form::close()}}

<script>
    function print_today() {
        // ***********************************************

        var now = new Date();
        var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();
        function fourdigits(number) {
            return (number < 1000) ? number + 1900 : number;
        }
        var today =  months[now.getMonth()] + " " + date + ", " + (fourdigits(now.getYear()));
        return today;
    }

    // from http://www.mediacollege.com/internet/javascript/number/round.html
    function roundNumber(number,decimals) {
        var newString;// The new rounded number
        decimals = Number(decimals);
        if (decimals < 1) {
            newString = (Math.round(number)).toString();
        } else {
            var numString = number.toString();
            if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
                numString += ".";// give it one at the end
            }
            var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
            var d1 = Number(numString.substring(cutoff,cutoff+1));// The value of the last decimal place that we'll end up with
            var d2 = Number(numString.substring(cutoff+1,cutoff+2));// The next decimal, after the last one we want
            if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
                if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
                    while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
                        if (d1 != ".") {
                            cutoff -= 1;
                            d1 = Number(numString.substring(cutoff,cutoff+1));
                        } else {
                            cutoff -= 1;
                        }
                    }
                }
                d1 += 1;
            }
            if (d1 == 10) {
                numString = numString.substring(0, numString.lastIndexOf("."));
                var roundedNum = Number(numString) + 1;
                newString = roundedNum.toString() + '.';
            } else {
                newString = numString.substring(0,cutoff) + d1.toString();
            }
        }
        if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
            newString += ".";
        }
        var decs = (newString.substring(newString.lastIndexOf(".")+1)).length;
        for(var i=0;i<decimals-decs;i++) newString += "0";
        //var newNumber = Number(newString);// make it a number if you like
        return newString; // Output the result to the form field (change for your purposes)
    }


    //original function
    // it's also working
    function update_balance() {

    }


    // amount calculation it working
    function update_price() {
        var row = $(this).parents('.item-row');
        var price = row.find('.cost').val().replace("$","") * row.find('.qty').val();
        price = roundNumber(price,2);
        //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
        isNaN(price) ? row.find('.amt_before_tax').html("N/A") : row.find('.amt_before_tax').val(""+price);

        update_total();
    }

    //    each row tax calculation

    function update_tax() {
        var row = $(this).parents('.item-row');

        var val1 = document.getElementById('tax_type').value;
        if(val1=="CGST&SGST"){
            var $cgst=row.find('.cgst').val();
            var $sgst=row.find('.sgst').val();
            var total_tax_percentage=Number($cgst)+Number($sgst);
        }else{
            var $igst=row.find('.igst').val();
            var total_tax_percentage=Number($igst)
        }
//-----------------------------
        var sellingPrice=row.find('.cost').val();
        var qnty=row.find('.qty').val();
        var tax_amount=(Number(qnty)*Number(sellingPrice))*total_tax_percentage/100;
        var total_amount=(Number(qnty)* Number(sellingPrice))+tax_amount;
        tax1 =roundNumber(tax_amount,2);
        amount1=roundNumber(total_amount,2)
//        isNaN(amount1) ? row.find('.price').html("N/A") : row.find('.price').html("$"+amount1);
        isNaN(tax1) ? row.find('.text_amount').html("N/A") : row.find('.text_amount').val(""+tax1);
        isNaN(amount1) ? row.find('.price ').html("N/A") : row.find('.price ').val(""+amount1);

        update_total();
    }

    //each row total amount calculation
    //    function update_price() {
    //        var row = $(this).parents('.item-row');
    //        var price = row.find('.cost').val().replace("$","") * row.find('.qty').val();
    //        price = roundNumber(price,2);
    //        //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
    //        isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').val(""+price);
    //
    //        update_total();
    //    }


    //original function it's working
    function update_total() {
        var total = 0;
        $('.price').each(function(i){
            price = $(this).val().replace("$","");
            if (!isNaN(price)) total += Number(price);
        });

        total = roundNumber(total,2);

        $('#subtotal1').val(""+total);
        $('#subtotal').html(""+total);
        $('#total').html(""+total);

        update_balance();
    }


    function total_igst() {
        var total2 = 0;
        $('.text_amount').each(function(i){
            igst = $(this).val().replace("$","");
            if (!isNaN(igst)) total2 += Number(igst);
        });

        total2 = roundNumber(total2,2);
        var half=total2/2;
        var full=total2;
        var val1 = document.getElementById('tax_type').value;
        if(val1=="CGST&SGST"){
            $('#total_cgst_text').val(""+half);
            $('#total_cgst').html(""+half);
            $('#total_sgst_text').val(""+half);
            $('#total_sgst').html(""+half);
            update_balance();
        }else{
            $('#total_igst_text').val(""+full);
            $('#total_igst').html(""+full);
            update_balance();
        }
        $('#grand_tax_text').val(""+full);
        $('#grand_tax').html(""+full);

//        update_balance();
    }



    function subtotalgrand(){

        var subtotal=$('#subtotal1').val();
        var tax=$('#grand_tax_text').val();
        var total =Number(subtotal)+Number(tax);
        var subtotal_filter = roundNumber(total,2);
        $('#subtotalGrand1').val(""+subtotal_filter);
        $('#subtotalGrand').html(""+subtotal_filter)
    }

    //total special disount
    function totalspclDiscount(){
        var totaldiscount = $('#subtotalGrand').val()*$('#discount').val()/100;

        totaldiscount = roundNumber(totaldiscount,2);
        $('#total_spcl_discount1').val(""+totaldiscount);


    }


    function grandtotal(){
        var subtotal=$('#subtotalGrand1').val();
        var friegh=$('.courierCharge').val();
        var discount=$('#total_spcl_discount1').val();
        var grandtotaladd=(subtotal-Number(discount))+Number(friegh);
        grandtotaladd = roundNumber(grandtotaladd,2);
        var new_num = Math.round(grandtotaladd).toFixed(2);

        $('.total1').html("$"+grandtotaladd);
        $('#total2').val(""+grandtotaladd);
        $('.grand_total').html("$"+new_num);
        $('#grand_total1').val(""+new_num);
    }








    function bind() {
        $(".qty").blur(update_price);
        $(".cost").blur(update_price);

//        vat calculation

        $(".qty").blur(update_tax);
        $(".cost").blur(update_tax);
        $(".amt_before_tax").blur(update_tax);



//        gst calculation


        $(".qty").blur(total_igst);
        $(".cost").blur(total_igst);
        $("#subtotal1").blur(total_igst);
        $("#vat_percentage").blur(total_igst);
        $("#cst_percentage").blur(total_igst);
        $("#discount").blur(total_igst);
        $("#total_spcl_discount1").blur(total_igst);
        $(".courierCharge").blur(total_igst);
        $(".delete").blur(total_igst);


//        subtotal grand

        $(".qty").blur(subtotalgrand);
        $(".cost").blur(subtotalgrand);
        $("#subtotal1").blur(subtotalgrand);
        $("#vat_percentage").blur(subtotalgrand);
        $("#cst_percentage").blur(subtotalgrand);

//        discount

        $(".qty").blur(totalspclDiscount);
        $(".cost").blur(totalspclDiscount);
        $("#subtotal1").blur(totalspclDiscount);
        $("#vat_percentage").blur(totalspclDiscount);
        $("#cst_percentage").blur(totalspclDiscount);
        $("#discount").blur(totalspclDiscount);




//         grand total
        $(".qty").blur(grandtotal);
        $(".cost").blur(grandtotal);
        $("#subtotal1").blur(grandtotal);
        $("#total_spcl_discount1").blur(grandtotal);
        $(".courierCharge").blur(grandtotal);
        $(".delete").blur(grandtotal);


    }
    $('#two').click(function(){
        total_igst();
        totalspclDiscount();
        update_total();
        grandtotal();


    });

    $(document).ready(function() {
        var i=1;
        var count=1;
        var country =  [/* states array*/];
        $("#country").select2({
            data: country
        });

        var tax_type =  [/* states array*/];
        $("#tax_type").select2({
            data: tax_type
        });
        var customer =  [/* states array*/];
        $("#customer").select2({
            data: customer
        });
        $('input').click(function(){
            $(this).select();
        });

        $("#paid").blur(update_balance);

        $("#addrow").click(function(){
            i++;

            count++;
            var id=count;
            $(".item-row:last").after('<tr class="item-row" style="border-bottom: solid 1px black"><td style="width: 31%"  class="item-name"><div class="delete-wpr">' +
                '<div>'+
                '<select id=\"country' + (++i) + '\" name="rows[' + id + '][product]" style="width:99%;" required>'+
                <?php  $product_type=\App\Product::All();?>
                '<option value="">Please select Item</option>'+
                    @foreach($product_type as $product_type)
            '<option value="{{$product_type->id}}">{{$product_type->product_name}}</option>'+
                    @endforeach
            '</select>'+
            '</div>'+
            '<a class="delete" href="javascript:;" title="Remove row">X</a></div></td>' +
            '<td class="main_td"><input name="rows[' + id + '][batch_no]" class="form-control" style="width: 116%;margin-left:-7px;height: 38px"></td>'+
            '<td class="main_td"><input name="rows[' + id + '][mfg_date]" class="mfg_date form-control" style="width:127%; margin-left:-7px;height:38px" required></td>'+
            '<td class="main_td"><input name="rows[' + id + '][exp_date]" class="exp_date form-control" style="width:127%; margin-left:-7px;height:38px" required></td>'+
            '<td class="main_td"><input class="qty form-control" style="width: 58px" name="rows[' + id + '][qty]" required></td>' +
            '<td class="main_td"><select name="rows[' + id + '][unit]" class="qty form-control" style="width: 73px" required>'+
            '<option>pcs</option>'+
            '<option>box</option>'+
            '</select></td>'+
            '<td class="main_td"><input name="rows[' + id + '][costPrice]" class="costPrice" type="hidden"><input class="cost form-control" style="width: 67px;text-align: center" name="rows[' + id + '][cost]" required></td>' +
            '<td class="main_td"><input name="rows[' + id + '][amt_before_tax]" class="amt_before_tax form-control" style="width: 80px;text-align: center" required></td>'+
            '<td class="main_td"><input name="rows[' + id + '][sgst]" class="sgst form-control" style="width: 40px;text-align: center" readonly></td>'+
            '<td class="main_td"><input name="rows[' + id + '][cgst]" class="cgst form-control" style="width: 40px;text-align: center" readonly></td>'+
            '<td class="main_td"><input name="rows[' + id + '][igst]" class="igst form-control" style="width: 40px;text-align: center" readonly></td>'+
            '<td class="main_td"><input name="rows[' + id + '][text_amount]" class="text_amount form-control" style="width:75px;text-align: center" readonly></td>' +
            '<td class="main_td"><input class="price form-control"  name="rows[' + id + '][price]"readonly></td></tr>');
            if($(".delete").length > 0) $(".delete").show();
            bind();
            $("#country"+i).select2({
                source: country
            });



//  ------------------------ geting cost price and tax ------------------------------------------
            $('select[name="rows['+ id +'][product]"]').on('change', function() {

                var stateID = $(this).val();
                var  unique=id;
                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {
                            $("input[name='rows[" + id + "][cost]']").empty();
                            $("input[name='rows[" + id + "][costPrice]']").empty();
                            $("input[name='rows[" + id + "][igst]']").empty();
                            $("input[name='rows[" + id + "][sgst]']").empty();
                            $("input[name='rows[" + id + "][cgst]']").empty();
                            $("input[name='rows[" + id + "][unit]']").empty();

                            var Vals = data;

                            var cgst = (Vals.gst) / 2;
                            var sgst = (Vals.gst) / 2;
                            $("input[name='rows[" + id + "][cost]']").val(Vals.sellingPrice);
                            $("input[name='rows[" + id + "][costPrice]']").val(Vals.cost);

                            var val1 = document.getElementById('tax_type').value;
                            if (val1 == 'IGST') {
                                $("input[name='rows[" + id + "][igst]']").val(Vals.gst);
                            } else {
                                $("input[name='rows[" + id + "][sgst]']").val(sgst);
                                $("input[name='rows[" + id + "][cgst]']").val(cgst);
                            }
//                        $("input[name='rows[0][costPrice]']").val(Vals.cost);
//                        $("input[name='rows[0][igst]']").val(Vals.gst);
//                        $("input[name='rows[0][cost]']").val(Vals.sellingPrice);


                        }

                    });

                }else{

                    $("input[name='rows[" + id + "][cost]']").empty();
                    $("input[name='rows[" + id + "][costPrice]']").empty();
                    $("input[name='rows[" + id + "][igst]']").empty();
                    $("input[name='rows[" + id + "][sgst]']").empty();
                    $("input[name='rows[" + id + "][cgst]']").empty();


                }

            });
//  ------------------------ geting cost price and tax  end here ------------------------------------------

        });

        bind();
        $(document).on('click', '.delete', function(){
            $(".delete").on('click',function(){
                $(this).parents('.item-row').remove();
                update_total();
                if ($(".delete").length < 2) $(".delete").hide();
            });
        });

        $("#cancel-logo").click(function(){
            $("#logo").removeClass('edit');
        });
        $("#delete-logo").click(function(){
            $("#logo").remove();
        });
        $("#change-logo").click(function(){
            $("#logo").addClass('edit');
            $("#imageloc").val($("#image").attr('src'));
            $("#image").select();
        });
        $("#save-logo").click(function(){
            $("#image").attr('src',$("#imageloc").val());
            $("#logo").removeClass('edit');
        });

        $("#date").val(print_today());

        $('select[name="customer"]').on('change', function() {

            var stateID = $(this).val();

            if(stateID) {

                $.ajax({

                    url: '{{ url('/') }}/ajax/'+stateID,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {


                        var Vals    =  data;
                        $("input[name='phone']").val(Vals.phone);
                        $("input[name='email']").val(Vals.email);
                        $("input[name='city']").val(Vals.city);
                        $("textarea[name='address']").val(Vals.address);



                    }

                });

            }else{

                $('select[name="city"]').empty();

            }

        });



//        getting cost price and gst

        $('select[name="rows[0][product]"]').on('change', function() {

            var stateID = $(this).val();
            var unique= 0;
            if(stateID) {

                $.ajax({

                    url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {

                        var Vals    =  data;

                        var cgst=(Vals.gst)/2;
                        var sgst=(Vals.gst)/2;
                        $("input[name='rows[0][cost]']").val(Vals.sellingPrice);
                        $("input[name='rows[0][costPrice]']").val(Vals.cost);


                        var val1 = document.getElementById('tax_type').value;
                        if(val1=='IGST') {
                            $("input[name='rows[0][igst]']").val(Vals.gst);
                        } else {
                            $("input[name='rows[0][sgst]']").val(sgst);
                            $("input[name='rows[0][cgst]']").val(cgst);
                        }
//                        $("input[name='rows[0][costPrice]']").val(Vals.cost);
//                        $("input[name='rows[0][igst]']").val(Vals.gst);
//                        $("input[name='rows[0][cost]']").val(Vals.sellingPrice);


                    }

                });

            }else{

                $('select[name="rows[0][batch_no]"]').empty();

            }

        });
//        ------------------------------------



    });
</script>
</body>

</html>
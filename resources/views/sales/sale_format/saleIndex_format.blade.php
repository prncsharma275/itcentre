@extends('layouts.adminPanelTableOld')
@section('title')
    Sales Panel
@endsection

@section('custom_css')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('css/bootstrap/js/dataTables.bootstrap.min.js')}}"></script>
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap/css/dataTables.bootstrap.min.css')}}">--}}
@endsection

@section('manual_style_code')

    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('sale_format_index')}}">Other Sale Panel</a>
    </li>
@endsection
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Other Sale Panel</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('sale_format_type')}}"><i class="splashy-document_letter_add"></i> New Other Sale</a>

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <table class="table table-striped table-bordered dTableR" id="table">
                <thead>
                <tr>

                    <th style="width: 20%">Bill No</th>
                    <th>Bill Date</th>
                    <th style="width: 25%">Customer Name</th>
                    <th>Bill Amount</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>


    <h4 class="text-center"></h4>
    @if(Session::has('status'))
        <?php $id =   Session::get('status')  ;
        $customer =   Session::get('customer')  ;
        ?>
        <div id="myModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Print Invoice</h4>
                    </div>
                    <div class="modal-body">
                        <h2>Do You Want To Print Previous Bill?</h2>
                    </div>
                    <div class="modal-footer">
                        @if($customer!='1')
                            <a id="report" class="btn btn-success"  target="_blank" href="{{route('sale_invoice_format_print',$id)}}">Print</a>
                        @else
                            <a id="report" class="btn btn-success"  target="_blank" href="{{route('sale_invoice_retail_format_print',$id)}}">Print</a>
                        @endif
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(function() {
                $('#myModal').modal('show');
//                        $('#report').click();
            });
        </script>
    @endif
    <script>
        //                $(document).ready(function() {
        //                    $('#table').DataTable();
        //                } );
        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );

        $(document).ready(function () {
            $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{ url('allposts1') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [

                    { "data": "unique_id" },
                    { "data": "billing_date" },
                    { "data": "customer_id" },
                    { "data": "grand_total" },
                    { "data": "action" }
                ]


            });
        });

        //$(document).ready( function () {
        //    $('#table').dataTable( {
        //        "aoColumns": [
        //            null,
        //            { "sType": "date-uk" },
        //            null,
        //            null,
        //            null,
        //
        //        ]
        //    });
        //
        //} );
        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    <br>
@endsection

@extends('layouts.adminPanel')
@section('title')
    View Sale
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }

        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            color: #fff;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .modal-dialog{
            width: 810px!important;
        }
        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }

     

    </style>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('sale')}}">Sale Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Sale Alter</a>
    </li>
@endsection
@section('content')

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    {{-- ----------------------------------------inner content here --------------------------------------------------------}}

    <h3 class="heading">SALE INVOICE (View Mode)</h3>
    <?php echo Form::open(array('url' =>['updateSale',$editPurchase->id], 'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>

    <div class="row"> <!-- row start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Select Customer</label>
                <select id="customer" name="customer" class="form-control" style="width: 99%!important;"   readonly>
                    <option value="{{$editPurchase->customer_id}}">{{$editPurchase->belongsTocustomer->ledger_name}}</option>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Invoice No.<span style="color:#EB3E28;">*</span></label>
                <input type="text" class="form-control" tabindex="0" name="invoice_no"  value="{{$editPurchase->unique_id}}" id="inputSuccess2" readonly>
                <input type="hidden" name="old_customer_id" value="{{$editPurchase->customer_id}}">
            </div>
        </div>
        <?php
        $orderdate_entry = explode('-', $editPurchase->billing_date);
        $year_entry = $orderdate_entry[0];
        $month_entry   = $orderdate_entry[1];
        $day_entry  = $orderdate_entry[2];

        ?>
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Invoice Date<span style="color:#EB3E28;">*</span></label>
                <input type="text" name="invoice_date" class="form-control" value="{{$day_entry}}/{{$month_entry}}/{{$year_entry}}">
            </div>
        </div>

    </div> <!-- row ends here -->
    <div class="row" id="retail"> <!-- row2 start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Customer Name<span style="color:#f0ad4e;font-size: 10px"> (In Case of Retail Customer)</span></label>
                {{--<input type="text" class="form-control" id="inputSuccess2" placeholder="Payment Type">--}}
                <input type="text" name="retail_customer_name" value="{{$editPurchase->retail_name}}" class="form-control" id="inputSuccess2"  placeholder="Retail Name">
            </div>
        </div>
        {{-------------------------------------- retail customer starts here ---------------------  --}}

        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">City<span style="color:#f0ad4e;font-size: 10px"> (In Case of Retail Customer)</span></label>
                <input type="text" name="retail_city" class="form-control" value="{{$editPurchase->city}}" id="inputSuccess2"  placeholder="City">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Mobile No <span style="color:#f0ad4e;font-size: 10px"> (In Case of Retail Customer)</span></label>
                <input type="text" name="retail_mobile" class="form-control" value="{{$editPurchase->mobile}}"  placeholder="Mobile">
            </div>
        </div>


    </div> <!-- row2 ends here -------------------------------- -->


    <div class="row"> <!-- row2 start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Order / Challan  No.</label>
                <input type="text" name="order_no" value="{{$editPurchase->order_no}}" class="form-control" id="inputSuccess2" placeholder="Order No.">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Order / Challan Date</label>
                <input type="text" name="order_date" class="form-control " value="{{$editPurchase->order_date}}"  placeholder="Order Date">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">GST Type</label>
                <input type="text" name="tax_type" class="form-control" id="inputSuccess2" value="{{$editPurchase->tax_type}}" placeholder="GST Type">
            </div>
        </div>

    </div> <!-- row2 ends here -------------------------------- -->
    <div class="table-responsive">

        <table id="items" class="table table-striped table-bordered table-condensed" style="font-size: 13px!important;">

            <tr style="background-color: #e9f3f8;font-size: 12px!important;">
                <th>Select Item</th>
                <th>Desc.</th>
                <th>Qnty.</th>
                <th>Rate</th>
                <th>Amt.</th>
                <th>Disc %</th>
                <th>Disc.</th>
                <th>Txbl Amt.</th>
                <th>GST %</th>
                <th>Txt.Amt</th>
                <th>Total</th>

            </tr>
            <?php $sale_invoice = DB::table('sale_invoice')->where([
                    ['sale_id', '=',$editPurchase->id ],

            ])->get();


            ?>
            {{--count total number of row where sale id = this--}}
            <?php $total_sale_count = \App\Sale_invoice::where(['sale_id' => $editPurchase->id])->count(); ?>

            <?php   $count=0; ?>
            @foreach($sale_invoice as $sale)

                <tr class="item-row">
                    <td style="width: 25%"  class="item-name">
                        <?php  ?>
                        <select id="monti{{$sale->id}}" name="rows[{{$count}}][product]"  style="width:100%;" readonly>
                            <?php $product_name = DB::table('products')->select('id', 'product_name')->where([
                            ['id', '=',$sale->product_id ],  ])->first();  ?>
                            <option value="{{$product_name->id}}" selected="selected">{{$product_name->product_name}}</option>

                            <?php  $product_type=\App\Product::All();?>
                            @foreach($product_type as $product_type)
                                <option value="{{$product_type->id}}">{{$product_type->product_name}}</option>
                            @endforeach
                        </select>
                    </td>

                    <td><textarea name="rows[{{$count}}][desc]" id="desc{{$count}}"  class="form-control" style="height:34px!important">{{$sale->description}}</textarea>
                        <input type="hidden" name="rows[{{$count}}][sale_invoice]" value="{{ $sale->id }}">
                        <input type="hidden" name="rows[{{$count}}][product_old]" value="{{$sale->product_id}}">
                    </td>
                    <td class="main_td"><input name="rows[{{$count}}][qty]" class="qty form-control" value="{{$sale->quantity}}"  readonly>
                        <input type="hidden" name="rows[{{$count}}][old_qnty]"  value="{{$sale->quantity}}">
                    </td>
                    <td><input name="rows[{{$count}}][rate]" class="cost form-control"  value="{{$sale->rate}}"  readonly></td>
                    <td class="main_td"><input name="rows[{{$count}}][amount]"  value="{{$sale->amount_before_tax}}" class="amt_before_tax form-control"  readonly></td>
                    <td style="width: 5%" class="main_td"><input name="rows[{{$count}}][disc_percentage]"  value="{{$sale->discount_per}}" class="disc_percentage form-control"  ></td>
                    <td style="width: 7%" class="main_td"><input name="rows[{{$count}}][disc]"  value="{{$sale->discount}}" class="disc form-control"></td>
                    <td style="width: 8%"  class="main_td"><input name="rows[{{$count}}][taxbl_amount]"  value="{{$sale->taxable_amount}}" class="taxbl_amount form-control" readonly>
                        <input name="rows[{{$count}}][costPrice]" class="costPrice" type="hidden">
                    </td>
                    @if($editPurchase->tax_type=="CGST&SGST")
                        <?php
                        $total_tax_percentage=$sale->sgst_tax+$sale->cgst_tax; ?>
                    @else
                        <?php    $total_tax_percentage=$sale->igst_tax;  ?>
                    @endif
                    <td style="width: 5%" class="main_td"><input name="rows[{{$count}}][gst]"  value="{{$total_tax_percentage}}" class="igst  form-control"  readonly></td>
                    <td style="width: 7%" class="main_td"><input name="rows[{{$count}}][text_amount]"  value="{{$sale->tax_amount}}" class="text_amount form-control"  readonly>
                        <input type="hidden" name="rows[{{$count}}][amount_date_taxble]"  class="amount_date_taxble">
                    <td style="width: 8%" class="main_td"><input name="rows[{{$count}}][price]"  value="{{$sale->total_amount}}" class="price form-control"  readonly></td>
                </tr>
                <script>
                    var name='<?php echo $sale->id; ?>';
                    var country =  [/* states array*/];
                    $("#monti"+name).select1({
                        data: country
                    });

                    var mycount='<?php echo $count ?>';
                    $('#desc'+mycount).focus(function()
                    {
                        $(this).animate({ height: '+=100',width:'+=80' }, 'slow');
                    }).blur(function()
                    {
                        $(this).animate({ height: '-=100',width:'-=80' }, 'slow');
                    });

                    //
                    $('select[name="rows[{{$count}}][product]"]').on('change', function() {

                        var stateID = $(this).val();
                        var  unique=1;
                        if(stateID) {

                            $.ajax({

                                url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                                type: "GET",

                                dataType: "json",

                                success:function(data) {
                                    $("input[name='rows[{{$count}}][cost]']").empty();
                                    $("input[name='rows[{{$count}}][costPrice]']").empty();
                                    $("input[name='rows[{{$count}}][igst]']").empty();
                                    $("input[name='rows[{{$count}}][sgst]']").empty();
                                    $("input[name='rows[{{$count}}][cgst]']").empty();


                                    var Vals = data;

                                    var cgst = (Vals.gst) / 2;
                                    var sgst = (Vals.gst) / 2;
                                    $("input[name='rows[{{$count}}][cost]']").val(Vals.mrp);
                                    $("input[name='rows[{{$count}}][costPrice]']").val(Vals.cost);


                                    var val1 = document.getElementById('tax_type').value;
                                    if (val1 == 'IGST') {
                                        $("input[name='rows[{{$count}}][igst]']").val(Vals.gst);
                                    } else {
                                        $("input[name='rows[{{$count}}][sgst]']").val(sgst);
                                        $("input[name='rows[{{$count}}][cgst]']").val(cgst);
                                    }
//                        $("input[name='rows[0][costPrice]']").val(Vals.cost);
//                        $("input[name='rows[0][igst]']").val(Vals.gst);
//                        $("input[name='rows[0][cost]']").val(Vals.sellingPrice);


                                }

                            });

                        }else{

                            $("input[name='rows[{{$count}}][cost]']").empty();
                            $("input[name='rows[{{$count}}][costPrice]']").empty();
                            $("input[name='rows[{{$count}}][igst]']").empty();
                            $("input[name='rows[{{$count}}][sgst]']").empty();
                            $("input[name='rows[{{$count}}][cgst]']").empty();


                        }

                    });

                    //       get mfgdate and exp date of given batch number



                </script>
                <?php   $count=$count+1; ?>

            @endforeach

        </table>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <a href="{{url('sale')}}" class="btn btn-primary btn-sm"><i class="splashy-gem_cancel_1"></i> Cancel</a>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-6 col-sm-6">
            <table class="table table-bordered table-striped">
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
                <tr>
                    <td width="50%">Total Amount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_amount_text">{{$editPurchase->total_amount_without_anything}}</span>
                        <input type="hidden" value="{{$editPurchase->total_amount_without_anything}}" name="total_amount_input" id="total_amount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Discount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_discount_text">{{$editPurchase->total_discount}}</span>
                        <input type="hidden" value="{{$editPurchase->total_discount}}" name="total_discount_input" id="total_discount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Taxble Amt.</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_taxble_text">{{$editPurchase->total_taxable_value}}</span>
                        <input type="hidden" value="{{$editPurchase->total_taxable_value}}" name="total_taxble_input" id="total_taxble_input"></td>
                </tr>



                <tr>
                    <td width="50%">Total GST</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_gst_text">{{$editPurchase->total_tax_amount}}</span>
                        <input type="hidden" value="{{$editPurchase->total_tax_amount}}" name="total_gst_input" id="total_gst_input"></td>
                </tr>

                <tr>
                    <td width="50%">Total Gross Amt.</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="total_sub_text">{{$editPurchase->gross_total}}</span>
                        <input type="hidden" name="total_sub_input" value="{{$editPurchase->gross_total}}" id="total_sub_input">
                        <input type="hidden"   name="postage_charge" id="postage_charge">
                    </td>
                </tr>

                <tr>
                    <td width="50%">Grand Total</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="grand_total_text">{{$editPurchase->grand_total}}</span>
                        <input type="hidden" value="{{$editPurchase->grand_total}}"  name="grand_total_input" id="grand_total_input"></td>
                </tr>
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
            </table>


        </div>
    </div>


    </div>
    {{form::close()}}
    <script>


        $(document).ready(function(){
            const getRetail = document.getElementById('customer');
            if(getRetail.value==1){
                $('#retail').show();
                $('#retail').find('input').attr('readonly','true');
            }else{
                $('#retail').hide();
            }
        });
        $('#customer').bind('change', function(event) {

            var i= $('#customer').val();
            if(i!=1) //Retail Customer ID
            {
                $('#retail').hide(); // hide the first one

            }
            else{
                $('#retail').show();
                $('#retail').find('input').attr('readonly','true');
            }

        });

    </script>

    <script>
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
        function deleteArticle(id) {
            if (confirm('This Data is saved ! Are you sure you want to delete this?')) {
//        if (confirm("Press a button!") == true) {
                $.ajax({
                    url: '{{ url('/') }}/sale_invoiceAjax/' + id,
                    data: {"_token": "{{ csrf_token() }}"},
                    type: 'DELETE',
                    success: function (result) {
                        console.log(result);
                        location.reload();
                    }
                });


            }
        }

        function print_today() {
            // ***********************************************

            var now = new Date();
            var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
            var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();
            function fourdigits(number) {
                return (number < 1000) ? number + 1900 : number;
            }
            var today =  months[now.getMonth()] + " " + date + ", " + (fourdigits(now.getYear()));
            return today;
        }

        var _round = Math.round;
        Math.round = function(number, decimals /* optional, default 0 */)
        {
            if (arguments.length == 1)
                return _round(number);

            var multiplier = Math.pow(10, decimals);
            return _round(number * multiplier) / multiplier;
        }

        //original function
        // it's also working
        function update_balance() {

        }


        // amount calculation it working without discount without tax value for individual row
        function update_price() {
            var row = $(this).parents('.item-row');
            var rate=row.find('.cost').val();
            var qnty=row.find('.qty').val()
            var price =Number(rate)*Number(qnty);
//                    var price = row.find('.cost').val().replace("$","") * row.find('.qty').val();

            price= Math.round(price, 2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(price) ? row.find('.amt_before_tax').html("N/A") : row.find('.amt_before_tax').val(""+price);

            update_total();
        }

        // discount_percantage calcultioan
        function discount_percentage(){
            var row = $(this).parents('.item-row');
            var amount_before_tax=row.find('.amt_before_tax').val()
            var discount_percentage=row.find('.disc_percentage').val();
            var total_discount = Number(amount_before_tax)*Number(discount_percentage)/100;
            total_discount = Math.round(total_discount,2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(total_discount) ? row.find('.disc').html("N/A") : row.find('.disc').val(""+total_discount);
        }


        //    taxable value  calculation for individual row
        function taxble_value(){
            var row = $(this).parents('.item-row');
            var discount_amount=row.find('.disc').val();
            var amount_before_tax=row.find('.amt_before_tax').val()
            var price1 = Number(amount_before_tax)-Number(discount_amount);
            price1 = Math.round(price1,2);
            //isNaN(price) ? row.find('.price').html("N/A") : row.find('.price').html("$"+price);
            isNaN(price1) ? row.find('.taxbl_amount').html("N/A") : row.find('.taxbl_amount').val(""+price1);

//        update_total();
        }





        //    each row tax calculation

        function update_tax() {
            var row = $(this).parents('.item-row');



            var $igst=row.find('.igst').val();
            var total_tax_percentage=Number($igst)

//-----------------------------
            var sellingPrice=row.find('.taxbl_amount').val();

            var tax_amount=Number(sellingPrice)*total_tax_percentage/100;
            var total_amount=Number(sellingPrice)+tax_amount;
            tax1 =Math.round(tax_amount,2);
            amount1=Math.round(total_amount,2)
//        isNaN(amount1) ? row.find('.price').html("N/A") : row.find('.price').html("$"+amount1);
            isNaN(tax1) ? row.find('.text_amount').html("N/A") : row.find('.text_amount').val(""+tax1);
            isNaN(amount1) ? row.find('.price ').html("N/A") : row.find('.price ').val(""+amount1);

            update_total();
        }




        // without disc without tax amount calculation for total
        function update_total() {
            var total = 0;
            $('.amt_before_tax').each(function(i){
                price = $(this).val().replace("$","");
                if (!isNaN(price)) total += Number(price);
            });

            total = Math.round(total,2);

            $('#total_amount_input').val(""+total);
            $('#total_amount_text').html(""+total);
//                    $('#total').html(""+total);

            update_balance();
        }




        function total_discount(){
            var total_discount = 0;
            $('.disc').each(function(i){
                price1 = $(this).val().replace("$","");
                if (!isNaN(price1)) total_discount += Number(price1);
            });

            total_discount = Math.round(total_discount,2);

            $('#total_discount_input').val(""+total_discount);
            $('#total_discount_text').html(""+total_discount);

            update_balance();
        }
        //                taxbl_amount

        function total_taxble_amount(){
            var total_taxable = 0;
            $('.taxbl_amount').each(function(i){
                price2 = $(this).val().replace("$","");
                if (!isNaN(price2)) total_taxable += Number(price2);
            });

            total_taxable = Math.round(total_taxable,2);

            $('#total_taxble_input').val(""+total_taxable);
            $('#total_taxble_text').html(""+total_taxable);

            update_balance();
        }

        function total_gst() {
            var total_tax = 0;
            $('.text_amount').each(function(i){
                igst = $(this).val().replace("$","");
                if (!isNaN(igst)) total_tax += Number(igst);
            });

            total_tax = Math.round(total_tax,2);

            $('#total_gst_input').val(""+total_tax);
            $('#total_gst_text').html(""+total_tax);
            update_balance();
        }







        function grandtotal(){
            var grandtotal = 0;
            $('.price').each(function(i){
                price = $(this).val().replace("$","");
                if (!isNaN(price)) grandtotal += Number(price);
            });
            grandtotal = Math.round(grandtotal,2);
            $('#total_sub_input').val(""+grandtotal);
            $('#total_sub_text').html(""+grandtotal)

            var postage_charge=$('#postage_charge').val()
            var grand_total=Number(grandtotal)+Number(postage_charge);
            var grand_total = Math.round(grand_total).toFixed(2);
            $('#grand_total_text').html(""+grand_total);
            $('#grand_total_input').val(""+grand_total);
        }





        function bind() {
            $(".qty").blur(update_price);
            $(".cost").blur(update_price);


            //  discount_percentage
            $(".qty").blur(discount_percentage);
            $(".cost").blur(discount_percentage);
            $(".amt_before_tax").blur(discount_percentage);
            $(".disc_percentage").blur(discount_percentage);
            $(".delete").blur(discount_percentage);

            //        taxable value blur
            $(".qty").blur(taxble_value);
            $(".cost").blur(taxble_value);
            $(".disc").blur(taxble_value);
            $(".amt_before_tax").blur(taxble_value);
            $(".disc_percentage").blur(taxble_value);
            $(".delete").blur(taxble_value);

            //        vat calculation

            $(".qty").blur(update_tax);
            $(".cost").blur(update_tax);
            $(".disc").blur(update_tax);
            $(".taxbl_amount").blur(update_tax);
            $(".amt_before_tax").blur(update_tax);
            $(".disc_percentage").blur(update_tax);
            $(".sgst").blur(update_tax);
            $(".cgst").blur(update_tax);
            $(".igst").blur(update_tax);


            // discount

            $(".qty").blur(total_discount);
            $(".cost").blur(total_discount);
            $(".amt_before_tax").blur(total_discount);
            $(".disc_percentage").blur(total_discount);
            $(".disc").blur(total_discount);
            $(".taxbl_amount").blur(total_discount);
//                    $("#discount").blur(total_discount);


// total_taxble_amount

            $(".qty").blur(total_taxble_amount);
            $(".cost").blur(total_taxble_amount);
            $(".amt_before_tax").blur(total_taxble_amount);
            $(".disc_percentage").blur(total_taxble_amount);
            $(".disc").blur(total_taxble_amount);
            $(".taxbl_amount").blur(total_taxble_amount);





//        gst calculation
//                    total_gst

            $(".qty").blur(total_gst);
            $(".cost").blur(total_gst);
            $("#subtotal1").blur(total_gst);
            $(".disc_percentage").blur(total_gst);
            $(".disc").blur(total_gst);
            $(".taxbl_amount").blur(total_gst);
            $(".igst").blur(total_gst);
            $(".delete").blur(total_gst);



//         grand total


            $(".qty").blur(grandtotal);
            $(".cost").blur(grandtotal);
            $("#subtotal1").blur(grandtotal);
            $(".disc_percentage").blur(grandtotal);
            $(".disc").blur(grandtotal);
            $(".taxbl_amount").blur(grandtotal);
            $(".igst").blur(grandtotal);
            $("#postage_charge").blur(grandtotal);
            $(".delete").blur(grandtotal);


        }
        $('#two').click(function(){
            taxble_value();
            total_igst();
            update_tax();
            discount_percentage();
            total_discount();
            update_total();
            subtotalgrand();
            grandtotal();


        });

        $(document).ready(function() {
            var i=1;
            var count= <?php echo $total_sale_count; ?>+1;
            var country =  [/* states array*/];
            $("#country").select2({
                data: country
            });

            var customer =  [/* states array*/];
            $("#customer").select2({
                data: customer
            });

            var payment_type =  [/* states array*/];
            $("#payment_type").select2({
                data: payment_type
            });

            $('input').click(function(){
                $(this).select();
            });

            $("#paid").blur(update_balance);

            $("#addrow").click(function(){
                i++;

                count++;
                var id=count;
                $(".item-row:last").after('<tr class="item-row" style="border-bottom: solid 1px black"><td style="width: 31%"  class="item-name">' +
                        '<select id=\"country' + (++i) + '\" name="rows[' + id + '][product]" style="width:99%;" readonly>'+
                        <?php  $product_type=\App\Product::All();?>
                        '<option value="">Please select Item</option>'+
                        @foreach($product_type as $product_type)
                '<option value="{{$product_type->id}}">{{$product_type->product_name}}</option>'+
                        @endforeach
                '</select>'+
                        '</td>' +
                        '<td class="main_td"><textarea name="rows[' + id + '][desc]" id=\"desc' + id + '\" style="height:34px!important" class="form-control"></textarea></td>'+
                        '<td class="main_td"><input class="qty form-control"   name="rows[' + id + '][qty]" readonly></td>' +
                        '<td class="main_td"><input type="hidden"  name="rows[' + id + '][sale_invoice]" value="0"><input class="cost form-control"  name="rows[' + id + '][rate]" readonly></td>' +
                        '<td class="main_td"><input name="rows[' + id + '][amount]" class="amt_before_tax form-control"  readonly></td>'+
                        '<td class="main_td"><input name="rows[' + id + '][disc_percentage]" class="disc_percentage form-control"  ></td>' +
                        '<td class="main_td"><input name="rows[' + id + '][disc]" class="disc form-control"  ></td>'+
                        '<td class="main_td"><input name="rows[' + id + '][taxbl_amount]" class="taxbl_amount form-control"  readonly></td>'+
                        '<td class="main_td"><input name="rows[' + id + '][gst]" class="igst form-control" readonly></td>'+
                        '<td class="main_td"><input name="rows[' + id + '][text_amount]" class="text_amount form-control"  readonly></td>' +
                        '<td class="main_td"><input class="price form-control"  name="rows[' + id + '][price]"readonly></td>'+
                        '<td class="main_td"><a class="delete btn btn-success" href="javascript:;" title="Remove row">X</a></td></tr>');
                if($(".delete").length > 0) $(".delete").show();
                bind();
                $("#country"+i).select1({
                    source: country
                });
                $('#desc'+id).focus(function()
                {
                    $(this).animate({ height: '+=100',width:'+=80' }, 'slow');
                }).blur(function()
                {
                    $(this).animate({ height: '-=100',width:'-=80' }, 'slow');

                });

//  ------------------------ geting cost price and tax ------------------------------------------
                $('select[name="rows['+ id +'][product]"]').on('change', function() {

                    var stateID = $(this).val();
                    var  unique=id;
                    if(stateID) {

                        $.ajax({

                            url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                            type: "GET",

                            dataType: "json",

                            success:function(data) {
                                $("input[name='rows[" + id + "][cost]']").empty();
                                $("input[name='rows[" + id + "][costPrice]']").empty();
                                $("input[name='rows[" + id + "][igst]']").empty();
                                $("input[name='rows[" + id + "][unit]']").empty();

                                var Vals = data;



                                $("input[name='rows[" + id + "][cost]']").val(Vals.sellingPrice);
                                $("input[name='rows[" + id + "][costPrice]']").val(Vals.cost);
                                $("input[name='rows[" + id + "][mrp]']").val(Vals.mrp);
                                $("input[name='rows[" + id + "][igst]']").val(Vals.gst);

//                        $("input[name='rows[0][costPrice]']").val(Vals.cost);
//                        $("input[name='rows[0][igst]']").val(Vals.gst);
//                        $("input[name='rows[0][cost]']").val(Vals.sellingPrice);


                            }

                        });

                    }else{

                        $("input[name='rows[" + id + "][cost]']").empty();
                        $("input[name='rows[" + id + "][costPrice]']").empty();
                        $("input[name='rows[" + id + "][igst]']").empty();

                    }

                });
//  ------------------------ geting cost price and tax  end here ------------------------------------------

            });

            bind();
            $(document).on('click', '.delete', function(){

                $(this).parents('.item-row').remove();
                update_total();
                total_discount();
                total_taxble_amount();
                total_gst();
                grandtotal();
                discount_percentage();
                if ($(".delete").length < 1) $(".delete").hide();

            });

            $("#cancel-logo").click(function(){
                $("#logo").removeClass('edit');
            });
            $("#delete-logo").click(function(){
                $("#logo").remove();
            });
            $("#change-logo").click(function(){
                $("#logo").addClass('edit');
                $("#imageloc").val($("#image").attr('src'));
                $("#image").select();
            });
            $("#save-logo").click(function(){
                $("#image").attr('src',$("#imageloc").val());
                $("#logo").removeClass('edit');
            });

            $("#date").val(print_today());

            $('select[name="customer"]').on('change', function() {

                var stateID = $(this).val();

                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/ajaxPurchase/'+stateID,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {


                            var Vals    =  data;
                            $("input[name='phone']").val(Vals.phone);
                            $("input[name='email']").val(Vals.email);
                            $("input[name='city']").val(Vals.city);
                            $("textarea[name='address']").val(Vals.address);
                            $("input[name='customer_type']").val(Vals.customer_type);



                        }

                    });

                }else{

                    $('select[name="city"]').empty();

                }

            });



//        getting cost price and gst

            $('select[name="rows[0][product]"]').on('change', function() {

                var stateID = $(this).val();
                var unique= 0;
                if(stateID) {

                    $.ajax({

                        url: '{{ url('/') }}/AjaxCostGst/'+stateID+'/'+unique,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {

                            var Vals    =  data;

                            var cgst=(Vals.gst)/2;
                            var sgst=(Vals.gst)/2;
                            $("input[name='rows[0][cost]']").val(Vals.sellingPrice);
                            $("input[name='rows[0][costPrice]']").val(Vals.cost);
                            $("input[name='rows[0][mrp]']").val(Vals.mrp);

                            $("input[name='rows[0][igst]']").val(Vals.gst);

//                        $("input[name='rows[0][costPrice]']").val(Vals.cost);
//                        $("input[name='rows[0][igst]']").val(Vals.gst);
//                        $("input[name='rows[0][cost]']").val(Vals.sellingPrice);


                        }

                    });

                }else{

                    $('select[name="rows[0][batch_no]"]').empty();

                }

            });
//        ------------------------------------



        });
    </script>
    </form>

    {{-------------------------------------------------------------------------------------------------------------------------------}}

    <br><br>
@endsection
@extends('layouts.adminPanel')
@section('title')
    Creditor
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 40px;
            font-family: Verdana;
            font-size: 12px;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .panel-default > .panel-heading{
            background-color: #14c1d7!important;
            color: #ffffff;
        }
        .panel-info > .panel-footer{
            background-color: #14c1d7!important;
            color: #ffffff!important;
        }
        .panel-body{
            background-color: #fff!important;
        }



        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }
        /*.select1-container{*/
        /*height: 34px!important;*/

        /*}*/
        .table .select1-container--default .select1-selection--single {
            margin-top: -24px!important;
            margin-left: -10px!important;
            margin-right: -7px!important;
            height: 38px!important;
        }
        .select1-selection__arrow{
            top: 6px!important;
        }
        .select1-container--default .select1-selection--single .select1-selection__rendered{
            line-height: 18px!important;
        }

        .select1.select1-container.select1-container--default{
            height: 38px!important;
        }
        .ui-datepicker-week-end a {
            color: #EB3E28!important;
        }
        .ui-datepicker-header{
            background-color: #0E8F9F!important;
            color: #ffffff;!important;
        }
        th{
            /*background-color: #eb3e28!important;*/
            background-color:#1F648B!important;
            font-size: 12px!important;
            color: #fff!important;
        }

        select{
            padding: 9px!important;
        }
        .modal-dialog{
            width: 810px!important;
        }
        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }

    </style>
@endsection

@section('content')

            <h4 class="page-header text-center">Full View({{$viewCreditor->ledger_name}})</h4>


            {{--<form class="well form-horizontal" action="storeCustomer" method="POST"  id="contact_form">--}}
            <?php echo Form::open(array('url' =>['updateCreditor',$viewCreditor->id])); ?>
            <div class="container-fluid">

                <div class="row">





                </div> <!-- row end here -->
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>Supplier Name/Company Name<span style="color: red; font-size: 15px">*</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="creditor_name" value="{{$viewCreditor->ledger_name}}" readonly>

                        </div>

                    </div>
                    <div class="col-md-6 form-group">
                        <label>Address</label>
                        <div class="input-group">
                            <textarea  type="text" class="form-control" name="address" readonly>{{$viewCreditor->address}}</textarea>
                        </div>

                    </div>


                </div> <!-- row end here -->
                <div class="row">


                    <div class="col-md-4 form-group">
                        <label>Zip</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="zip" value="{{$viewCreditor->zip}}" readonly>

                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>State</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="state" value="{{$viewCreditor->state}}" readonly>
                        </div>

                    </div>

                    <div class="col-md-4 form-group">
                        <label>City</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="city" value="{{$viewCreditor->city}}" readonly>
                        </div>

                    </div>

                </div>


                <div class="row">



                    <div class="col-md-4 form-group">
                        <label>Phone</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="work_phone" value="{{$viewCreditor->phone}}" readonly>
                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>Mobile</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="mobile" value="{{$viewCreditor->mobile}}" readonly>
                        </div>

                    </div>
                    <div class="col-md-4 form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="email" value="{{$viewCreditor->email}}" readonly>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </div>

                <div class="row">


                    <div class="col-md-6 form-group">
                        <label>Vat No<span style="color: red; font-size: 11px"> ( in case of company)</span></label>
                        <div class="input-group">
                            <input   type="text" class="form-control" name="vat" value="{{$viewCreditor->vatNo}}" readonly>
                        </div>

                    </div>


                    <div class="col-md-6 form-group">
                        <label>PAN No<span style="color: red; font-size: 11px"> (if any)</span></label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="pan" value="{{$viewCreditor->pan}}" readonly>
                        </div>

                    </div>


                </div>
                <div class="row">

                    <div class="col-md-4 form-group">
                        <div class=" has-success has-feedback">
                            <label>Opening Balance</label>
                            <div class="input-group">
                                <input  type="number" min="1" class="form-control"  value="{{$viewCreditor->opening_balance}}" name="opening_balance" placeholder="Opening Balance" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Bank A/c No</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="bankAct" value="{{$viewCreditor->bankAct}}" readonly>
                        </div>

                    </div>

                    <div class="col-md-4 form-group">
                        <label>Bank Name</label>
                        <div class="input-group">
                            <input  type="text" class="form-control" name="BankName" value="{{$viewCreditor->bankName}}" readonly>
                        </div>

                    </div>

                </div> <!-- row end here -->
                <br>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">

                        <a class="btn btn-success" href="{{url('customers')}}"><span class="glyphicon glyphicon-backward"></span> Back</a>
                    </div>
                </div>


            </div>
            {{--</Form>--}}
            {{form::close()}}
                    <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}


            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
<br>
            @endsection

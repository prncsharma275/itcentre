@extends('layouts.adminPanelTable')
@section('title')
    Sales Panel
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
        .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
            background-color:  #e1f8ff;
        }

    </style>
@endsection

@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if(Session::has('flash_message1'))
        <div class="alert alert-danger">
            {{ Session::get('flash_message1') }}
        </div>
    @endif

    <h4 class="text-center">Supplier</h4>
            <a class="btn btn-warning" style="margin-bottom: 10px;background-color: #EB3E28 " href="{{url('createCreditors')}}">+ New Supplier</a>
            <a class="btn btn-warning" style="margin-bottom: 10px" href="{{url('ledgerPanel')}}">Back</a>
    <table class="table-striped table-bordered table-responsive" id="table" style="width: 100%; color: #000!important;">
                <thead>
                <tr>

                    <th class="text-center">Name/Company Name</th>

                    <th class="text-center">Email &nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">City &nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Mobile &nbsp;&nbsp;&nbsp;&nbsp;</th>

                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)

                    <tr class="item{{$item->id}}">
                        <td>{{$item->ledger_name}}</td>

                        <td>{{$item->email}}</td>
                        <td>{{$item->city}}</td>
                        <td>{{$item->mobile}}</td>

                        <td>
                            <a href="singleCreditor/{{$item->id}}" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-eye-open"></span> View
                            </a>
                            <a href="editCreditor/{{$item->id}}" class="btn btn-info btn-sm">
                                <span class="glyphicon glyphicon-edit"></span> Edit
                            </a>
                            <a href="deleteCreditor/{{$item->id}}" class="btn btn-danger btn-sm" onclick="return ConfirmDelete()">
                                <span class="glyphicon glyphicon-remove"></span> Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <script>
                $(document).ready(function() {
                    $('#table').DataTable();
                } );

                //    delete commande
                function ConfirmDelete()
                {
                    var x = confirm("Are you sure you want to Cancel This Bill?");
                    if (x)
                        return true;
                    else
                        return false;
                }
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });

            </script>
            {{--inner content here ------------------------------------}}

    <br>
@endsection
@extends('layouts.adminPanel')
@section('title')
    Creditor
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('customers')}}">Ledger Panel</a>
    </li>

    <li>
        <a href="{{url('#')}}">Edit Ledger</a>
    </li>
@endsection

@section('content')
            <h3 class="heading">Edit Ledger</h3>


            {{--<form class="well form-horizontal" action="storeCustomer" method="POST"  id="contact_form">--}}
            <?php echo Form::open(array('url' =>['updateCreditor',$editCreditor->id],'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
            <div class="container-fluid">

                <div class="row">





                </div> <!-- row end here -->
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class=" has-success has-feedback">
                        <label>Supplier Name/Company Name<span style="color: red; font-size: 15px">*</span></label>
                            <input  type="text" class="form-control" name="creditor_name" value="{{$editCreditor->ledger_name}}" placeholder="Enter Customer Name Here"required>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class=" has-success has-feedback">
                        <label>Address</label>
                            <input  type="text" class="form-control" value="{{$editCreditor->address}}" name="address" placeholder="Address">
                        </div>

                    </div>


                </div> <!-- row end here -->
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>Zip</label>
                            <input  type="text" class="form-control" name="zip" value="{{$editCreditor->zip}}" placeholder="Zip">
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>State</label>
                            <select class="form-control" name="state" required>
                                <option value="{{$editCreditor->state}}">{{$editCreditor->state}}</option>
                                <?php $states = \App\State::all() ?>
                                @foreach($states as $state)
                                    <option value="{{$state->state_name}} - {{sprintf('%02d',($state->id))}}">{{$state->state_name}} - {{sprintf('%02d',($state->id))}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>City</label>
                            <input  type="text" class="form-control" name="city" value="{{$editCreditor->city}}" placeholder="City"required>
                        </div>

                    </div>

                </div>


                <div class="row">



                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>Phone</label>
                            <input  type="text" class="form-control" name="work_phone" value="{{$editCreditor->phone}}" placeholder="enter phone number">
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>Mobile</label>
                            <input  type="text" class="form-control" name="mobile" value="{{$editCreditor->mobile}}" placeholder="enter your mobile number" required>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>Email</label>
                            <input  type="text" class="form-control" name="email" value="{{$editCreditor->email}}" placeholder="Enter valid Email">
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </div>

                <div class="row">


                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>GST No<span style="color: red; font-size: 11px"> ( in case of company)</span></label>
                            <input   type="text" class="form-control" name="vat" value="{{$editCreditor->vatNo}}" placeholder="Enter GST No">
                        </div>

                    </div>


                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                        <label>PAN No<span style="color: red; font-size: 11px"> (if any)</span></label>
                            <input  type="text" class="form-control" name="pan" value="{{$editCreditor->pan}}" placeholder="Enter Pan No">
                        </div>

                    </div>


                    <div class="col-md-4 col-sm-4">
                        <div class=" has-success has-feedback">
                            <label>Other Details<span style="color: red; font-size: 11px"> (if any)</span></label>
                            <input  type="text" class="form-control" name="other" value="{{$editCreditor->pan}}" placeholder="">
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class=" has-success has-feedback">
                            <label>Opening Balance</label>
                                <input  type="number" min="1" class="form-control"  value="{{$editCreditor->opening_balance}}" name="opening_balance" placeholder="Opening Balance">
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3">
                        <div class=" has-success has-feedback">
                            <label>Balance Type</label>
                            <select class="form-control" name="balance_type">
                                @if($editCreditor->balance_type=='dr')
                                <option value="dr">Dr.(Inward)</option>
                                <option value="cr">Cr.(Outward)</option>
                                 @elseif($editCreditor->balance_type=='cr')
                                    <option value="cr">Cr.(Outward)</option>
                                    <option value="dr">Dr.(Inward)</option>
                                   @else
                                    <option value="">Select One Value</option>
                                    <option value="dr">Dr.(Inward)</option>
                                    <option value="cr">Cr.(Outward)</option>
                                @endif
                            </select>

                        </div>

                    </div>

                    <div class="col-md-3 col-sm-3">
                        <div class=" has-success has-feedback">
                        <label>Bank A/c No</label>
                            <input  type="text" class="form-control" name="bankAct" value="{{$editCreditor->bankAct}}" placeholder="Enter Account Number">
                        </div>

                    </div>

                    <div class="col-md-3 col-sm-3">
                        <div class=" has-success has-feedback">
                        <label>Bank Name</label>
                            <input  type="text" class="form-control" name="BankName" value="{{$editCreditor->bankName}}" placeholder="enter Bank Name">
                        </div>

                    </div>

                </div> <!-- row end here -->
                <br>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-6 col-sm-6">
                        <button type="submit" class="btn btn-warning" >Update</button>
                        <a class="btn btn-success" href="{{url('customers')}}">Back</a>
                    </div>
                </div>


            </div>
            {{--</Form>--}}
            {{form::close()}}
                    <!-- form row end here -->
            {{---------------------------------------------------------------------------------}}


            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif


            <br>
            @endsection

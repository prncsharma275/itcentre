@extends('layouts.adminPanel')
@section('title')
    Debit Note
@endsection

@section('custom_css')
@endsection

@section('manual_style_code')
    <style>
        input,textarea,select{
            text-transform: capitalize!important;
        }
        input{
            font-size: 13px!important;
        }
        .delete {
            display: block;
            text-decoration: none;
            position: relative;
            font-weight: bold;
            padding: 0px 3px;
            border: 1px solid;
            top: 0px!important;
            left: 31px;
            font-family: Verdana;
            font-size: 10px;
            color: #fff;
            background-color: green;
            width: 17px;
        }
        table tr, td, th{
            color: #000!important;
            /*padding: 5px!important;*/
        }

        .form-control:focus {
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        .select2-selection.select2-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }

        .select1-selection.select1-selection--single:focus{
            border-color: #FF0000!important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;
        }
        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }



        a:focus{
            color: black!important;
            /*text-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)!important;*/
        }
        .select1-container .select1-selection--single {
            height: 31px!important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #3c763d;
            border-radius: 4px;
            height: 34px;
            font-size: 13px;

        }



    </style>
    <script>
        // ----------------------------- datepicker --------------------------------------
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
    </script>
@endsection
@section('shortlink')
    <li>
        <a href="{{url('debitnote')}}">Debit Note Panel</a>
    </li>
    <li>
        <a href="{{url('#')}}">Edit Debit Note</a>
    </li>
@endsection
@section('content')

    <h3 class="heading">Debit Note (View Mode)</h3>
    <?php echo Form::open(array('url' =>['updateDebitnote',$editProduct->id],'onsubmit'=> "return confirm('Do you really want to submit the form?');")); ?>
    <div class="row"> <!-- row start here -->
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Select Supplier<span style="color:#EB3E28;">*</span></label>
                <select id="customer" name="customer" style="width: 99%!important;" readonly>

                    <?php  $product_type = DB::table('customers')->where('ledger_type', '=', 'creditor')->get(); ?>
                    <option value="{{$editProduct->supplier_id}}">{{$editProduct->belongsToCustomer->ledger_name}}</option>

                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Voucher No<span style="color:#EB3E28;">*</span></label>
                <input type="text" class="form-control" tabindex="0" name="sale_return_no" value="<?php echo $editProduct->unique_no ?>" id="inputSuccess2"readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-success has-feedback">
                <label for="inputSuccess2">Date<span style="color:#EB3E28;">*</span></label>
                <input type="text" name="sale_return_date" class="form-control datepicker" value="<?php echo date( 'd/m/Y', strtotime($editProduct->billing_date)) ?>">
            </div>
        </div>
    </div>
    <table id="items" class="table table-striped table-bordered table-condensed">

        <tr style="background-color: #e9f3f8">
            <th >Description</th>
            <th>Amt.</th>
        </tr>

        <tr class="item-row" style="border-bottom: solid 1px black">

            <td style="min-width: 80%"> <input name="desc" value="{{$editProduct->desc}}" class="form-control"  style="width: 99%; height: 38px"readonly></td>
            <td><input name="amount"  value="{{$editProduct->amount}}" class="amount form-control " style="width: 99%; height: 38px"readonly></td>


        </tr>


    </table>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="row">


                <div class="col-md-3 col-sm-4 col-xs-4">
                    <a href="{{url('debitnote')}}" class="btn btn-danger btn-sm"><i class="splashy-gem_cancel_1"></i> Cancel</a>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <table class="table table-bordered table-striped">
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
                <tr>
                    <td width="50%">Total Outstanding (Till Date)</td>
                    <td><span class="fa fa-rupee"></span>

                        <input  name="total_outstanding" value="{{$editProduct->total_outstanding}}" id="total_outstanding" style="background-color: #fff!important;border:none!important;width: 90%;text-align: left" readonly></td>
                </tr>

                <tr>
                    <td width="50%">Paid Amount</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="paid_amount">{{$editProduct->amount}}</span>
                        <input type="hidden" value="{{$editProduct->amount}}" name="paid_amount_input" id="paid_amount_input"></td>
                </tr>

                <tr>
                    <td width="50%">Balance</td>
                    <td><span class="fa fa-rupee"></span>
                        <span id="new_outstanding">{{$editProduct->new_outstanding}}</span>
                        <input type="hidden" name="new_outstanding" value="{{$editProduct->new_outstanding}}" id="new_outstanding_input">
                    </td>
                </tr>
                <tr style="background-color: #e9f3f8">
                    <td width="50%" style="color: #fff!important;"></td>
                    <td style="color: #fff!important;"></td>
                </tr>
            </table>
        </div>
    </div>

    {{form::close()}}
    <script>

        var customer =  [/* states array*/];
        $("#customer").select2({
            data: customer
        });
        $('#customer').select2('open').select2('close');
        var payment_mode =  [/* states array*/];
        $("#payment_mode").select2({
            data: payment_mode
        });
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: 'dd/mm/yy'
            });
        } );
        //                -------------payment type details--------

        //                --------------------- getting Balance of supplier ------------------
        $('select[name="customer"]').on('change', function() {

            var stateID = $(this).val();

            if(stateID) {

                $.ajax({

                    url: '{{ url('/') }}/ajaxPayment/'+stateID,

                    type: "GET",

                    dataType: "json",

                    success:function(data) {


                        var Vals    =  data;
                        $("input[name='total_outstanding']").val(Vals.balance);

                    }

                });

            }else{

                $('select[name="city"]').empty();

            }

        });

        function update_price(){
            price = $('.amount').val();

            $('#paid_amount_input').val(""+price);
            $('#paid_amount').html(""+price)
        }

        function update_balance(){
            paid_amount = $('.amount').val();
            outstanding = $('#total_outstanding').val();
            new_balance=Number(outstanding)-Number(paid_amount)
            $('#new_outstanding_input').val(""+new_balance);
            $('#new_outstanding').html(""+new_balance)
        }

        $(".amount").blur(update_price);
        $(".amount").blur(update_balance);


    </script>
    <br>
@endsection
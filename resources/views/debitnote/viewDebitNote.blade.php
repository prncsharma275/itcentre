@extends('layouts.adminPanelTable')
@section('title')
   Debit Note
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }

    </style>

@endsection

@section('shortlink')
    <li>
        <a href="{{url('debitnote')}}">Debit Note Panel</a>
    </li>
@endsection

@section('content')

            <h3 class="heading">Debit Note Panel</h3>
            <a class="btn btn-default" style="margin-bottom: 10px;" href="{{url('createDebitnote')}}"><i class="splashy-document_letter_add"></i> Debit Note</a>
            <table class="table table-striped table-bordered dTableR" id="dt_a">
                <thead>
                <tr>

                    <th class="text-center">Debit Note No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Supplier Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th class="text-center">Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product as $item)

                    <tr>
                        <td>{{$item->unique_no}}</td>
                        <td><?php echo date( 'd/m/y', strtotime($item->date)) ?></td>
                        <td>{{$item->belongsToCustomer->ledger_name}}</td>
                        <td>{{$item->amount}}</td>


                        <td>

                            <a href="editDebitnote/{{$item->id}}" class="btn btn-default btn-sm" title="EDIT">
                                <i class="splashy-document_letter_edit"></i>
                            </a>
                            <a href="printDebitnote/{{$item->id}}" class="btn btn-default btn-sm"title="PRINT">
                                <i class="splashy-printer"></i>
                            </a>
                            <a href="deleteDebitnote/{{$item->id}}" class="btn btn-default btn-sm" onclick="return ConfirmDelete()"title="DELETE">
                                <i class="splashy-document_letter_remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <script>
                jQuery.extend( jQuery.fn.dataTableExt.oSort, {
                    "date-uk-pre": function ( a ) {
                        var ukDatea = a.split('/');
                        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    },

                    "date-uk-asc": function ( a, b ) {
                        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                    },

                    "date-uk-desc": function ( a, b ) {
                        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                    }
                } );
                $(document).ready( function () {
                    $('#table').dataTable( {
                        "aoColumns": [
                            null,
                            { "sType": "date-uk" },
                            null,
                            null,
                            null,

                        ]
                    });

                } );


                //    delete commande
                function ConfirmDelete()
                {
                    var x = confirm("Are you sure you want to Cancel This Bill?");
                    if (x)
                        return true;
                    else
                        return false;
                }


            </script>
           <br>
    @endsection
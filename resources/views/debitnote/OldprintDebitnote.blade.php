<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mus technologies</title>
    <link rel="stylesheet" href="{{ asset('salesAndPurchaseStyle/css/printInvoice.css')}}" media="all" />

    <style type="text/css" media="print">

        .rupees{

        }
        @page {
            size: auto;   /* auto is the initial value */
            margin-left: 25px;  /* this affects the margin in the printer settings */
            margin-right: 25px;  /* this affects the margin in the printer settings */
            margin-top:0;  /* this affects the margin in the printer settings */
            font-size: 18px;
            margin-bottom: 0;
        }
        @media print{
            /*.below_notice{*/
            /*width: 600px!important;*/
            /*margin-right: 10px!important;*/
            /*}*/
            .bottom_box{
                position: relative;
                bottom: 0px;
            }
            button{

                display: none!important;
            }
            a{
                display: none!important;
            }
            .command{
                display: none!important;
            }
            .command1,.command2{
                display: none;!important;
            }
            footer{
                margin-top: 10px;
            }
        }

    </style>
</head>

{{-----------------------------------------------------------}}

<body style="height: 23.3cm!important;">
<header class="clearfix">
    <div id="logo">
        <img src="{{asset('salesAndPurchaseStyle/images/logo.png')}}">
    </div>





    <div id="company">


        <h2 class="name" style="margin-top: -2px">M/S I.T. CENTRE</h2>
        <div class="address"style="margin-top:-21px;">1st Floor, OS Complex, Opposite RK Mission Road</div>
        <div class="email">Ganga Market, Itanagar, Pin &#45; 791111,Arunachal Pradesh</div>
        {{--<div class="email"><a href="mailto:{{$printBill->belongsToCustomer->email}}">{{$printBill->belongsToCustomer->email}}</a></div>--}}
        <div class="email">Email- itcentre.ita@gmail.com, Cell-8963032009 </div>
        <div class="email">GSTIN-12EMRPS6621A1Z3</div>

    </div>

</header>
<div class="invoice"><h3>DEBIT NOTE</h3></div>
<main>
    <div id="details" class="clearfix">
        <div id="client">


            <div class="to">ISSUE BY:</div>
            <h2 class="name" style="margin-top: -2px">{{$printBill->belongsToCustomer->ledger_name}}</h2>
            <div class="address"style="margin-top:-21px">{{$printBill->belongsToCustomer->address}} {{$printBill->belongsToCustomer->city}}, {{$printBill->belongsToCustomer->state}}, {{$printBill->belongsToCustomer->zip}}</div>
            <div class="email">{{$printBill->belongsToCustomer->mobile}}</div>
            <div class="email"><a href="mailto:{{$printBill->belongsToCustomer->email}}">{{$printBill->belongsToCustomer->email}}</a></div>
            <div class="email">VAT/CST- {{$printBill->belongsToCustomer->vatNo}}</div>
            <div class="email">D/L NO.{{$printBill->belongsToCustomer->dlno}}</div>
        </div>

        <div id="invoice">
            <h1 style="color: #000000;font-size: 1.1em;"><span style="font-size: 1.1em;color: #777777">Debit Note No - </span>{{$printBill->unique_no}}</h1>
            <div class="date">Date of Issue:<span style="color: #000"> <?php echo date( 'd/m/y', strtotime($printBill->billing_date)) ?></span></div>

        </div>
    </div>
    <div class="billcontainer" style="height: 155px">
        <div class="head" style="border-bottom: solid 1px #000;">
            <ul>
                <li style="padding:2px;width:49px;">SL.No</li>
                <li style="padding:5px;width:1080px; text-align: left">Description</li>
                <li style="padding:2px;width: 111px;">Amt.</li>
            </ul>
        </div>

        <?php $sale_invoice = DB::table('sale_invoice')->where([
                ['sale_id', '=',$printBill->id ],

        ])->get();  ?>




        <div class="head" style="text-align:left;font-size:20px;">
            <ul>
                <li style="padding:4px;text-align:center;width:47px;">1</li>


                <li style="padding:2px;width: 1080px; text-align: left;">{{$printBill->desc}}</li>
                <li style="padding:2px;width: 133px; text-align: center;">{{$printBill->amount}}</li>
            </ul>
        </div>




        <div class="floatline slno" style="height: 186px"></div>

        <div class="floatline rate" style="height: 186px"></div>
    </div>
    <div class="head" style="padding:2px; border: solid 1px">
        <ul>
            <li style="padding:2px;width:46px;"></li>
            <li style="padding:2px;width: 1080px;">TOTAL</li>
            <li style="padding:2px;width: 121px;text-align: right">{{$printBill->amount}}.00</li>
            <!--           <li style="padding:2px;width: 120px;text-align: right">1000{{$printBill->subtotal_without_vat}}</li>-->
        </ul>
    </div>

    <div class="totalsum">
        <div class="sumdetails" style="padding-bottom: 3px;float: none;width: 100%">
            <p style="margin-left: 5px;">Terms & Conditions <span style="float: right;margin-right: 2px"> E. & O. E</span></p>
            <ol>
                <li>Goods once sold will not be taken back.</li>
                <li>Our responsibility ceases when goods are handed over to the carrier.</li>
                <li>Intrest @24% charged if the payment is not made within 15 days.</li>
                <li>Forms C/F/H if not received in time, the applicable tax will be charge.</li>
                <li>All disputes are Subject to Itanagar jurisdiction.</li>
            </ol>
        </div>

    </div>
</main>
{{--<div class="clearfix:after"></div>--}}
{{--Money Number to Word--}}
<?php
$number = $printBill->amount;
$no = round($number);
$point = round($number - $no, 2) * 100;
$hundred = null;
$digits_1 = strlen($no);
$i = 0;
$str = array();
$words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
$digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
while ($i < $digits_1) {
    $divider = ($i == 2) ? 10 : 100;
    $number = floor($no % $divider);
    $no = floor($no / $divider);
    $i += ($divider == 10) ? 1 : 2;
    if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
    } else $str[] = null;
}
$str = array_reverse($str);
$result = implode('', $str);
$points = ($point) ?
        "." . $words[$point / 10] . " " .
        $words[$point = $point % 10] : '';?>

<div class="money" style="font-size: 21px;padding-left: 30px;padding-top:10px">
    <?php
    echo $result . "Rupees  " . $points . "Only";
    ?>
</div>
<div class="signature">
    <div class="write"style="float:right;margin-right: 5px">
        <h2 style="padding-right: 10px;padding-bottom: 5px">For M/S I.T. CENTRE</h2>
        <h3 style="padding-left: 42px;padding-top: 31px">Authorised Signatory</h3>
    </div>

</div>

<section id="command">
    <button class="command" onclick="myFunction()">Print</button>
    <a href="{{url('createDebitnote')}}" class="command command2">Create New Invoice</a>
    <a href="{{url('/debitnote')}}" class="command command1">Back</a>
    <a href="{{url('/home')}}" class="command command4">Home</a>
</section>

<script>
    function myFunction() {
        window.print();
    }
    //    history.pushState(null, null, 'no-back-button');
    //    window.addEventListener('popstate', function(event) {
    //        history.pushState(null, null, 'no-back-button');
    //    });
</script>




</body>
</html>

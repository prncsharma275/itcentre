<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Brand::create(array('brand_name'=>'Dell'));
       Brand::create(array('brand_name'=>'Lenovo'));
       Brand::create(array('brand_name'=>'Hp'));
       Brand::create(array('brand_name'=>'Sony'));
       Brand::create(array('brand_name'=>'Apple'));
       Brand::create(array('brand_name'=>'Acer'));
       Brand::create(array('brand_name'=>'Micromax'));
       Brand::create(array('brand_name'=>'Samsung'));
       Brand::create(array('brand_name'=>'Xollo'));
       Brand::create(array('brand_name'=>'HCL'));
    }
}

<?php

use Illuminate\Database\Seeder;
use App\CustomerLogin;
use Illuminate\Support\Facades\Hash;
class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $customer= new CustomerLogin();
        $customer->customer_name = "Mintu Ansari";
        $customer->unique_id = "12354";
        $customer->address = "matelli";
        $customer->zip = "735223";
        $customer->city = "matelli";
        $customer->state = "west Bengal";
        $customer->email = "mintu@gmail.com";
        $customer->mobile = "9641519241";
        $customer->password = Hash::make("123456");
        $customer->save();
    }
}

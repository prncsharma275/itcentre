<?php

use Illuminate\Database\Seeder;
use App\Product_type;

class Product_typeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product_type::create(array('product_name'=>'Desktop'));
        Product_type::create(array('product_name'=>'Laptop'));
        Product_type::create(array('product_name'=>'Tablet'));
        Product_type::create(array('product_name'=>'Mobile'));
        Product_type::create(array('product_name'=>'Printer'));
        Product_type::create(array('product_name'=>'Camera'));
        Product_type::create(array('product_name'=>'Projector'));
        Product_type::create(array('product_name'=>'Keyboard'));
        Product_type::create(array('product_name'=>'Mouse'));

    }
}

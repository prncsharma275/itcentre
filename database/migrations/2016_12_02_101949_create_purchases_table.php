<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchase_id',12);
            $table->integer('creditor_id')->unsigned()->default(0);
            $table->integer('product_id')->unsigned()->default(0);
            $table->string('serial_number',60);
            $table->integer('quantity',false, true)->length(10);
            $table->float('rate',false, true)->length(10);
            $table->float('discount_percentage',false, true)->length(10);
            $table->float('discount_amount',false, true)->length(10);
            $table->float('amount',false, true)->length(10);
            $table->float('vat_rate',false, true)->length(10);
            $table->float('vat_amount',false, true)->length(10);
            $table->float('discount_percentage_after',false, true)->length(10);
            $table->float('discount_amount_after',false, true)->length(10);
            $table->float('total_amount',false, true)->length(10);
            $table->foreign('creditor_id')->references('id')->on('creditors')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unique_id',false, true)->length(10);
            $table->string('creditor_name',50);
            $table->string('address');
            $table->string('zip',7);
            $table->string('state');
            $table->string('city');
            $table->string('home_phone',12);
            $table->string('work_phone',12);
            $table->string('mobile',12);
            $table->string('email',40);
            $table->string('vatNo',15);
            $table->string('sms_mobile',12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditors');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->unsigned()->default(0);
            $table->integer('product_type_id')->unsigned()->default(0);
            $table->string('product_name',100);
            $table->float('product_vat_type');
            $table->integer('quantity',false, true)->length(10) ;
            $table->binary('product_description');
            $table->string('productPic');
            $table->integer('cost_price',false, true)->length(10);
            $table->integer('selling_price',false, true)->length(10);
            $table->foreign('brand_id')->references('id')->on('brands')->onUpdate('cascade');
            $table->foreign('product_type_id')->references('id')->on('product_types')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

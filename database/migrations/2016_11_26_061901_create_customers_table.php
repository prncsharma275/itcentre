<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id',10);
            $table->string('customer_name',50);
            $table->string('billing_name',50);
            $table->string('address');
            $table->string('zip',7);
            $table->string('state');
            $table->string('city');
            $table->string('home_phone',12);
            $table->string('work_phone',12);
            $table->string('mobile',15)->unique();
            $table->string('email')->unique();
            $table->string('credit_limit',3);
            $table->string('vatNo',15);
            $table->string('sms_mobile',12);
            $table->string('proprietor',50);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

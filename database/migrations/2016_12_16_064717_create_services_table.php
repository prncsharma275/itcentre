<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serviceNumber',20);
            $table->integer('customer_id')->unsigned()->default(0);
            $table->string('customer_mobile',15);
            $table->string('customer_address',15);
            $table->string('prouduct_name',50);
            $table->date('purchase_date');
            $table->string('serialNumber',50);
            $table->date('service_date');
            $table->date('warrenty_date');
            $table->string('engineer_name');
            $table->string('service_charge');
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other_charge extends Model
{
    protected $table = 'journals';

    protected $fillable=array('unique_id','billing_date','customer_id','type','amount','payment_type','cheque_no','bank_name','cheque_issue_date','cheque_deposit_date');


    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','ledger_dr');
    }
    public function belongsToCustomer2(){
        return $this->belongsTo('App\Customer','ledger_cr');
    }

}

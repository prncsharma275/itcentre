<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Purchase extends Model
{
    protected $table = 'purchases';

    protected $fillable=array('purchase_id','creditor_id','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');

    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','supplier_id');
    }
}

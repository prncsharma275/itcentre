<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creditor extends Model
{
    protected $table = 'creditors';

    protected $fillable=array('unique_id','creditor_name','address','zip','state','city','home_phone','work_phone',
        'mobile','email','vatNo','sms_mobile');

    public  function hasManyPurchase(){
        return $this->hasMany('App\Purchase','id');
    }
}

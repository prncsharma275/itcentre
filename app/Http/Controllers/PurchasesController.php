<?php

namespace App\Http\Controllers;

use App\Creditor;
use App\Customer;
use App\Ledgerdr;
use App\Purchase;
use App\Purchase_bill_session;
use Hamcrest\Text\IsEmptyString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Illuminate\Database\QueryException;
use Session;
use App\Purchase_invoice;
use App\Product;
use App\Cancel_Purchase;
use App\Cancel_Purchase_invoice;
use App\Exp_table;
use DateTime;
use App\Ledgercr;
use App\Input_tax;
class PurchasesController extends Controller
{

    public function PurchaseIndex()
    {
//        $purchaseIndex=Purchase::orderBy('id', 'desc')->where('status','=',null)->get();

//        $purchaseIndex= Purchase::orderBy('id', 'desc')->get();
        return View::make('purchase/purchaseIndex');
    }

    public function allpurchase(Request $request)
    {

        $columns = array(

            0 =>'party_invoice',
            1=> 'party_invoice_date_correct',
            2=> 'invoice_no',
            3=> 'supplier_id',
            4=> 'grand_total',
            5=> 'action',
        );

        $totalData = Purchase::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Purchase::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Purchase::where('party_invoice','LIKE',"%{$search}%")
                ->orWhere('party_invoice_date_correct', 'LIKE',"%{$search}%")
                ->orWhere('invoice_no', 'LIKE',"%{$search}%")
                ->orWhere('grand_total', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Purchase::where('party_invoice','LIKE',"%{$search}%")
                ->orWhere('party_invoice_date_correct', 'LIKE',"%{$search}%")
                ->orWhere('invoice_no', 'LIKE',"%{$search}%")
                ->orWhere('grand_total', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $customer =Customer::find($post->supplier_id);
                $edit =  route('editPurchase',$post->id);
                $print =  route('printPurchase',$post->id);
                $delete =  route('deletePurchase',$post->id);

                if($post->status=='cancel'){
                    $nestedData['party_invoice'] = '<del style="color: red">'.$post->party_invoice.'</del>';
                    $nestedData['party_invoice_date_correct'] = date('d-m-Y',strtotime($post->party_invoice_date_correct)) ;
                    $nestedData['invoice_no'] = '<del style="color: red">'.$post->invoice_no.'</del>' ;
                    $nestedData['supplier_id'] = $customer->ledger_name;
                    $nestedData['grand_total'] = $post->grand_total;
                    $nestedData['action'] = "&emsp;<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default disabled' ><i class='splashy-document_letter_edit'></i></a>
                                          &emsp;<a href='{$print}' title='PRINT' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          &emsp;<a href='{$delete}' title='DELETE' class='btn btn-sm btn-default disabled' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;

                }else{

                    $nestedData['party_invoice'] = $post->party_invoice;
                    $nestedData['party_invoice_date_correct'] = date('d-m-Y',strtotime($post->party_invoice_date_correct)) ;
                    $nestedData['invoice_no'] = $post->invoice_no ;
                    $nestedData['supplier_id'] = $customer->ledger_name;
                    $nestedData['grand_total'] = $post->grand_total;
                    $nestedData['action'] = "&emsp;<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default' ><i class='splashy-document_letter_edit'></i></a>
                                          &emsp;<a href='{$print}' title='PRINT' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          &emsp;<a href='{$delete}' title='DELETE' class='btn btn-sm btn-default' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;

                }


            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }




    public function purchase_type()
    {
        return View::make('purchase/purchaseType');
    }

    public function store_purchase_tax_type(Request $request){
        return redirect()->route('createPurchase', [$request->tax_type]);
    }

    public function createPurchase($id)
    {
        return View::make('purchase/createPurchase')->with('text_type',$id);
    }



    public function storePurchase(Request $request)
    {

        $this->validate($request, [
            'invoice_no' => 'required|unique:purchases',
        ],
            [
                'invoice_no.unique'      => 'Sorry, This SL No. Is Already Used ',
            ]);

//        $data = Input::all();
//        print_r($data);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month   = $billing_date[1];
        $billing_year  = $billing_date[2];


        $party_invoice_date = explode('/', $request->input('party_invoice_date'));
        $party_invoice_day = $party_invoice_date[0];
        $party_invoice_month   = $party_invoice_date[1];
        $party_invoice_year  = $party_invoice_date[2];


        $tax_type = $request->input('tax_type');
        $total_tax=$request->input('total_gst_input');
        $gross_total=$request->input('total_sub_input');
        $grand_total=$request->input('grand_total_input');


        $check_duplicate = Purchase::where('invoice_no',$request->input('invoice_no'))->first();
        if(! $check_duplicate) {

            try{
                DB::beginTransaction();

                $sale = new Purchase();
                $sale->invoice_no = $request->input('invoice_no');
                $sale->party_invoice = $request->input('party_invoice_no');
                $sale->party_invoice_date = $request->input('party_invoice_date');
                $sale->party_invoice_date_correct = date($party_invoice_year . '-' . $party_invoice_month . '-' . $party_invoice_day);
                $sale->order_no = $request->input('order_no');
                $sale->order_date = $request->input('order_date');
                $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $sale->supplier_id = $request->input('customer');
                $sale->amount_without_anything = $request->input('total_amount_input');
                if ($tax_type == "CGST&SGST") {
                    $sale->total_cgst = $total_tax / 2;
                    $sale->total_sgst = $total_tax / 2;
                } else {
                    $sale->total_igst = $total_tax;
                }
                $sale->tax_type = $tax_type;
                $sale->total_tax_amount = $total_tax;
                $sale->total_taxable_amount = $request->input('total_taxble_input');
                $sale->total_discount = $request->input('total_discount_input');
                $sale->gross_total = $gross_total;
                $sale->postage_charge = $request->input('postage_charge');
                $sale->grand_total = $grand_total;
                $sale->grand_total_word =$this->amount_in_words($grand_total);
                $sale->save();

//        **************** store in ledger ********************

                $sale_session = Purchase_bill_session::find($session_id);
                $sale_session->bill = $sale_session->bill + 1;
                $sale_session->save();


                $ledgerdr = new Ledgerdr();
                $ledgerdr->unique_id = $request->input('party_invoice_no');
                $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $ledgerdr->customer_id = $request->input('customer');
                $ledgerdr->type = "purchase";
                $ledgerdr->amount = $grand_total;
                $myledger = Customer::find($request->customer);
                if($myledger->ledger_group==1){
                    $ledgerdr->extra ="-";
                }else{
                    $ledgerdr->extra ="+";
                }
                $ledgerdr->purchase_id = $sale->id;
                $ledgerdr->save();
                //        **************** store in ledger ********************


//entry into purchase invoice table

                foreach ($input as $row) {

                    $flight = new Purchase_invoice();
                    $flight->purchase_id = $sale->id;
                    $flight->product_id = $row['product'];
                    $flight->entry_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->desc = $row['desc'];
                    $flight->quantity = $row['qty'];
                    $flight->rate = $row['rate'];
                    $flight->amount_before_tax = $row['amount'];
                    $flight->discount_per = $row['disc_percentage'];
                    $flight->discount_amt = $row['disc'];
                    $flight->taxable_amount = $row['taxbl_amount'];
                    if ($tax_type == "CGST&SGST") {
                        $flight->sgst_tax = $row['gst'] / 2;
                        $flight->cgst_tax = $row['gst'] / 2;
                    } else {
                        $flight->igst_tax = $row['gst'];
                    }
                    $flight->tax_amount = $row['text_amount'];
                    $flight->total_amount = $row['price'];
                    $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->save();

                }
//        bill no and session save


                DB::commit();

                Session::flash('flash_message', 'New Purchase has successfully added!');
                return redirect()->route('purchase');

            } catch(\Exception $e){
                DB::rollback();
                Session::flash('flash_message', 'Network Problem Please try again');
                return redirect()->back();
//echo $e->getMessage();
            }

        }else{
            return redirect()->route('purchase');
        }
    }



    public function singleViewPurchase($id)
    {
        $editPurchase=Purchase::find($id);
        return View::make('purchase/singleViewPurchase')->with('editPurchase',$editPurchase);
    }


    public function editPurchase($id)
    {
        $editPurchase=Purchase::find($id);
        return View::make('purchase/editPurchase')->with('editPurchase',$editPurchase);
    }


    public function printPurchaseInvoice($id){
        $printBill=Purchase::find($id);
        return View::make('purchase/printPurchaseInvoice')->with('printBill',$printBill);
    }


    public function updatePurchase(Request $request, $id)
    {
//        $data2 = Input::all();
//        print_r($data2);
//        die();
        $input = $request->input('rows');

        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month   = $billing_date[1];
        $billing_year  = $billing_date[2];

        $party_invoice_date = explode('/', $request->input('party_invoice_date'));
        $party_invoice_day = $party_invoice_date[0];
        $party_invoice_month   = $party_invoice_date[1];
        $party_invoice_year  = $party_invoice_date[2];

//   ----------------------------tax fields----------------------------------



        $tax_type = $request->input('tax_type');
        $total_tax=$request->input('total_gst_input');
        $gross_total=$request->input('total_sub_input');
        $grand_total=$request->input('grand_total_input');


        try{
            DB::beginTransaction();


            $sale= Purchase::find($id);
            $sale->party_invoice =$request->input('party_invoice_no');
            $sale->party_invoice_date = $request->input('party_invoice_date');
            $sale->party_invoice_date_correct = date($party_invoice_year . '-' . $party_invoice_month . '-' . $party_invoice_day);
            $sale->order_no = $request->input('order_no');
            $sale->order_date = $request->input('order_date');
            $sale->billing_date =date($billing_year.'-'.$billing_month.'-'.$billing_day);
            $sale->supplier_id=$request->input('customer');
            $sale->amount_without_anything=$request->input('total_amount_input');
            if($tax_type=="CGST&SGST"){
                $sale->total_cgst=$total_tax/2;
                $sale->total_sgst=$total_tax/2;
            }else{
                $sale->total_igst=$total_tax;
            }
            $sale->tax_type=$tax_type;
            $sale->total_tax_amount=$total_tax;
            $sale->total_taxable_amount=$request->input('total_taxble_input');
            $sale->total_discount=$request->input('total_discount_input');
            $sale->gross_total=$gross_total;
            $sale->postage_charge=$request->input('postage_charge');
            $sale->grand_total=$grand_total;
            $sale->grand_total_word =$this->amount_in_words($grand_total);
            $sale->save();

            // **************** store in ledger ********************
            $ledgerdr= Ledgerdr::where('purchase_id','=',$id)->first();
            $ledgerdr->unique_id= $request->input('party_invoice_no');
            $ledgerdr->customer_id = $request->input('customer');
            $ledgerdr->type = "purchase";
            $ledgerdr->amount =$grand_total ;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==1){
                $ledgerdr->extra ="-";
            }else{
                $ledgerdr->extra ="+";
            }
            $ledgerdr->save();
//  **************** store in ledger ********************

//-----------------------------  purchase invoice table ----------------

            foreach ($input as $row) {
                if($row['sale_invoice']>0){
                    $flight = Purchase_invoice::find($row['sale_invoice']);
                }else{
                    $flight = new Purchase_invoice();
                }

                $flight->purchase_id = $sale->id;
                $flight->product_id = $row['product'];
                $flight->entry_date=date($billing_year.'-'.$billing_month.'-'.$billing_day);
                $flight->desc =$row['desc'];
                $flight->quantity = $row['qty'];
                $flight->rate = $row['rate'];
                $flight->amount_before_tax = $row['amount'];
                $flight->discount_per = $row['disc_percentage'];
                $flight->discount_amt = $row['disc'];
                $flight->taxable_amount = $row['taxbl_amount'];
                if($tax_type=="CGST&SGST"){
                    $flight->sgst_tax = $row['gst']/2;
                    $flight->cgst_tax = $row['gst']/2;
                }else{
                    $flight->igst_tax = $row['gst'];
                }
                $flight->tax_amount = $row['text_amount'];
                $flight->total_amount = $row['price'];
                $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $flight->save();
            }

            DB::commit();

            Session::flash('flash_message', 'New Purchase has successfully added!');
            return redirect()->route('purchase');




        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }




    }


    public function deletePurchase($id)
    {

        try{
            DB::beginTransaction();
            $saleDelete= Purchase::find($id);
            $saleDelete->status="cancel";
            $saleDelete->save();

            $ledger_creditors= Ledgerdr::where('purchase_id', '=', $id)->first();
            $ledger_creditors->status="cancel";
            $ledger_creditors->save();

            $purchase_invoice= Purchase_invoice::where('purchase_id', '=', $id)->get();
            foreach($purchase_invoice as $invoice){
                $invoice->status="cancel";
                $invoice->save();
            }

            DB::commit();

            Session::flash('flash_message', 'Selected Purchase Invoice successfully Canceled!');


            return redirect()->route('purchase');




        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }



    public function myformAjax($id)

    {

        $cities = DB::table("creditors")

            ->where("id",$id)

            ->get();
        foreach($cities as $city){



            return json_encode(array("phone"=>$city->mobile,"email"=>$city->email,'city'=>$city->city,'address'=>$city->address));
        }
    }

    public function purchaseInvoiceAjax($id){

        $item= new Purchase_invoice();
        $item::find($id)->delete();


    }

    public function AjaxNewSupplier(Request $request)
    {
        $this->validate($request, [
            'creditor_name' => 'required',


        ]);
//
//        $data= Input::all();
//        print_r($data);
//        die();

        $customer = new Customer();

        $customer->ledger_name = $request->creditor_name;
        $customer->address = $request->customer_address;
        $customer->zip = $request->customer_zip;
        $customer->state = $request->customer_state;
        $customer->city = $request->customer_city;
        $customer->phone = $request->customer_phone;
        $customer->mobile = $request->customer_mobile;
        $customer->email = $request->customer_email;
        $customer->ledger_type = 'creditor';
        $customer->ledger_type2 = 1;
        $customer->ledger_group =2;
        $customer->opening_balance =$request->customer_opening_balance;
        $customer->save();
        $city = Customer::find($customer->id);

        return json_encode(array("customer_id" => $city->id, "ledger_name" => $city->ledger_name));

    }

    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }


    public function changePurchasePartyBillDate(){

        $posts = Purchase::get();

        foreach($posts as $post){
            $date = str_replace('/', '-',$post->party_invoice_date);
            echo "Party Bill Date = ".$date ." ==> ". date('Y-m-d',strtotime($date))." <br>";

            $post->party_invoice_date_correct = date('Y-m-d',strtotime($date));
            $post->save();
        }
    }

}

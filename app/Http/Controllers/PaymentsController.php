<?php

namespace App\Http\Controllers;


use App\Bank;
use App\Creditor;
use App\Ledgerdr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Payment;
use App\Customer;
use Illuminate\Database\QueryException;
use Monolog\Handler\NullHandlerTest;
use Session;
use DateTime;
use App\Payment_bill_session;
use App\Ledgercr;
use App\Cash;
class PaymentsController extends Controller
{

    public function paymentIndex()
    {
        $product= Payment::orderBy('id', 'desc')->get();
        return View::make('payment/viewPayment')->with('product',$product);
    }


 public function singleViewPayment($id)
 {

     $editProduct=Payment::find($id);

     $balance = DB::table("ledgerdr")->where("customer_id",$editProduct->supplier_id)->get();

     $amount = '';
     foreach ($balance as $rity) {

         if ($rity->extra == "+") {
             $amount = $amount + $rity->amount;
         } elseif($rity->extra == "-") {
             $amount = $amount - $rity->amount;
         }

     }
     return View::make('payment/singleViewPayment')->with('editProduct',$editProduct)->with('amount',$amount);



 }

    public function createPayment()
    {
      return View::make('payment/createPayment');
    }

    public function ajaxPayment($id)
    {


        $cusotmer = Customer::find($id);

//        $balance = DB::table("ledgerdr")->where("customer_id",$id)->get();

  $addAmount = \App\Ledgerdr::where('customer_id',$id)->where('extra','=','+')->sum('amount');
  $subsAmount = \App\Ledgerdr::where('customer_id',$id)->where('extra','=','-')->sum('amount');

     $total_amount = $addAmount-$subsAmount;
        $finalamount = $total_amount+$cusotmer->opening_balance;

            return json_encode(array('balance'=>$finalamount));

    }



    public function storePayment(Request $request)
    {
//        $data= Input::all();
//        print_r($data);
//        die();

        $this->validate($request, [
            'desc' => 'required',
            'amount' => 'required',

        ]);

        $payment_date = explode('/', $request->input('sale_return_date'));
        $payment_day = $payment_date[0];
        $payment_month = $payment_date[1];
        $payment_year = $payment_date[2];

        try{
            DB::beginTransaction();
            $product = new Payment();

            $product->unique_id = $request->sale_return_no;
            $product->supplier_id = $request->customer;
            $product->billing_date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $product->payment_type =$request->payment_type;
            $product->cheque_no =$request->cheque_no;
            $product->cheque_bank_id =$request->cheque_bank_id;
            $product->cheque_issue_date =$request->cheque_issue_date;
            $product->cheque_diposit_date =$request->cheque_diposit_date;
            $product->fund_type =$request->fund_type;
            $product->fund_transaction_no =$request->fund_transaction_no;
            $product->fund_transfer_date =$request->fund_transfer_date;
            $product->fund_bank_id =$request->fund_bank_id;
            $product->total_outstanding =$request->total_outstanding;
            $product->paid_amount =$request->paid_amount_input;
            $product->new_outstanding =$request->new_outstanding;
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            $product->amount_in_words =$this->amount_in_words($request->amount);
            $product->save();

            $session_id=$request->input('session_id');
            $sale_session=Payment_bill_session::find($session_id);

            $sale_session->bill=$sale_session->bill+1;

            $sale_session->save();
            //        **************** store in ledger ********************

            $ledgercr=new Ledgerdr();
            $ledgercr->unique_id=$request->sale_return_no;
            $ledgercr->billing_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $ledgercr->customer_id = $request->customer;
            $ledgercr->type = "Payment";
            $ledgercr->amount =$request->amount;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==1){
                $ledgercr->extra ="+";
            }else{
                $ledgercr->extra ="-";
            }
            $ledgercr->payment_id =$product->id;
            $ledgercr->save();
            //        **************** store in ledger ********************

            //        **************** store in cash ledger ********************
            if($request->payment_type=="cash"){
                $ledgerdr=new Cash();
                $ledgerdr->unique_id=$request->sale_return_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->customer;
                $ledgerdr->type = "Payment";
                $ledgerdr->amount =$request->amount;
                $ledgerdr->extra ="-";
                $ledgerdr->payment_id =$product->id;
                $ledgerdr->save();
            }elseif($request->payment_type=="cheque"){

                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->sale_return_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->customer;
                $ledgerdr->type = "Payment";
                $ledgerdr->amount =$request->amount;
                $ledgerdr->through =$request->payment_type;
                $ledgerdr->cheque_no =$request->cheque_no;
                $ledgerdr->cheque_issue_date =$request->cheque_issue_date;
                $ledgerdr->chuque_deposit_date =$request->cheque_diposit_date;
                $ledgerdr->bank_id =$request->cheque_bank_id;
                $ledgerdr->extra ="-";
                $ledgerdr->payment_id =$product->id;
                $ledgerdr->save();

            }

            else{
                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->sale_return_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->customer;
                $ledgerdr->type = "Payment";
                $ledgerdr->amount =$request->amount;
                $ledgerdr->through =$request->payment_type;
                $ledgerdr->fund_type =$request->fund_type;
                $ledgerdr->transation_no =$request->fund_transaction_no;
                $ledgerdr->transaction_date =$request->fund_transfer_date;
                $ledgerdr->bank_id =$request->fund_bank_id;
                $ledgerdr->extra ="-";
                $ledgerdr->payment_id =$product->id;
                $ledgerdr->save();
            }
            //        **************** store in cash ledger ********************


            DB::commit();
            Session::flash('flash_message', 'New Payment has successfully added!');

//        return redirect()->route('product');
            return redirect('payment')->with('status', $product->id);


        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

    public function editPayment($id)
    {
        $editProduct=Payment::find($id);

        $balance = DB::table("ledgerdr")->where("customer_id",$editProduct->supplier_id)->get();

        $amount = '';
        foreach ($balance as $rity) {

            if ($rity->extra == "+") {
                $amount = $amount + $rity->amount;
            } elseif($rity->extra == "-") {
                $amount = $amount - $rity->amount;
            }

        }
        return View::make('payment/editPayment')->with('editProduct',$editProduct)->with('amount',$amount);
    }


    public function updatePayment(Request $request, $id)
    {
//
//        $data= Input::all();
//        print_r($data);
//        die();

        $this->validate($request, [
            'desc' => 'required',
            'amount' => 'required',

        ]);

        $payment_date = explode('/', $request->input('sale_return_date'));
        $payment_day = $payment_date[0];
        $payment_month = $payment_date[1];
        $payment_year = $payment_date[2];


        try{
            DB::beginTransaction();


            $product=Payment::find($id);

            $product->unique_id = $request->sale_return_no;
            $product->supplier_id = $request->customer;
            $product->billing_date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $product->payment_type =$request->payment_type;
            $product->cheque_no =$request->cheque_no;
            $product->cheque_bank_id =$request->cheque_bank_id;
            $product->cheque_issue_date =$request->cheque_issue_date;
            $product->cheque_diposit_date =$request->cheque_diposit_date;
            $product->fund_type =$request->fund_type;
            $product->fund_transaction_no =$request->fund_transaction_no;
            $product->fund_transfer_date =$request->fund_transfer_date;
            $product->fund_bank_id =$request->fund_bank_id;
            $product->total_outstanding =$request->total_outstanding;
            $product->paid_amount =$request->paid_amount_input;
            $product->new_outstanding =$request->new_outstanding;
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            $product->amount_in_words =$this->amount_in_words($request->amount);
            $product->save();


//         **************** store in ledger ********************
            $ledgercr= Ledgerdr::where('payment_id','=',$id)->first();
            $ledgercr->billing_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $ledgercr->customer_id = $request->customer;
            $ledgercr->type = "Payment";
            $ledgercr->amount =$request->amount;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==1){
                $ledgercr->extra ="+";
            }else{
                $ledgercr->extra ="-";
            }
            $ledgercr->save();
//  **************** store in ledger ********************

            //
            //
            //         **************** store in Cash ********************
            if($request->previouse_payment_type=="cash"){
                $previouse_payment= Cash::where('payment_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }else{
                $previouse_payment= Bank::where('payment_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }

            if($request->payment_type=="cash"){
                $ledgerdr=new Cash();
                $ledgerdr->unique_id=$request->sale_return_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->customer;
                $ledgerdr->type = "Payment";
                $ledgerdr->amount =$request->amount;
                $ledgerdr->extra ="-";
                $ledgerdr->payment_id =$product->id;
                $ledgerdr->save();
            }elseif($request->payment_type=="cheque"){

                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->sale_return_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->customer;
                $ledgerdr->type = "Payment";
                $ledgerdr->amount =$request->amount;
                $ledgerdr->through =$request->payment_type;
                $ledgerdr->cheque_no =$request->cheque_no;
                $ledgerdr->cheque_issue_date =$request->cheque_issue_date;
                $ledgerdr->chuque_deposit_date =$request->cheque_diposit_date;
                $ledgerdr->bank_id =$request->cheque_bank_id;
                $ledgerdr->extra ="-";
                $ledgerdr->payment_id =$product->id;
                $ledgerdr->save();

            }

            else{
                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->sale_return_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->customer;
                $ledgerdr->type = "Payment";
                $ledgerdr->amount =$request->amount;
                $ledgerdr->through =$request->payment_type;
                $ledgerdr->fund_type =$request->fund_type;
                $ledgerdr->transation_no =$request->fund_transaction_no;
                $ledgerdr->transaction_date =$request->fund_transfer_date;
                $ledgerdr->bank_id =$request->fund_bank_id;
                $ledgerdr->extra ="-";
                $ledgerdr->payment_id =$product->id;
                $ledgerdr->save();
            }





//  **************** store in ledger ********************



            DB::commit();

            Session::flash('flash_message', 'Payment has successfully updated!');

//        return redirect()->route('product');
            return redirect('payment')->with('status', $product->id);

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }


    public function deletePayment($id)
    {
        try{
            DB::beginTransaction();
            $item= new Payment();
            $product=Payment::find($id);
//*********** delete from ledger **********
            $ledgerdr=Ledgerdr::where('payment_id','=',$id)->first();
            $ledgerdr::find($ledgerdr->id)->delete();
//*********** delete from ledger **********

            if($product->payment_type=='cash'){
                $ledgerdr1=Cash::where('payment_id','=',$id)->first();
                $ledgerdr1::find($ledgerdr1->id)->delete();

            }else {
                $ledgerdr1=Bank::where('payment_id','=',$id)->first();
                $ledgerdr1::find($ledgerdr1->id)->delete();
            }

//*********** delete from ledger **********

            $item::find($id)->delete();



            DB::commit();

            Session::flash('flash_message', 'Selected Receipt successfully Deleted!');


            return redirect()->route('payment');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

 public function printPayment($id){
     $editProduct=Payment::find($id);
     return View::make('payment/printPayment')->with('printBill',$editProduct);
 }


    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }
}

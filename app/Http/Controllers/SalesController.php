<?php

namespace App\Http\Controllers;

use App\Purchase_invoice;
use App\Purchase_return_invoice;
use App\Sale;
use App\Sale_bill_session;
use App\Sale_return_invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Session;
use Illuminate\Database\QueryException;
use App\Sale_invoice;
use App\Product;
use App\Product_type;
use App\Brand;
use App\Cancel_Sale;
use App\Cancel_Sale_invoice;
use DateTime;
use App\Exp_table;
use App\Ledgerdr;
use App\Output_tax;
use App\Cash;
use App\Customer;




class SalesController extends Controller
{

    public function saleIndex()
    {

        return View::make('sales/saleIndex');

    }



    public function allposts(Request $request)
    {
        $columns = array(

            0 =>'unique_id',
            1=> 'billing_date',
           2=> 'customer_id',
           3=> 'gst_type',
           4=> 'net_amount',
           5=> 'gstamount',
           6=> 'grand_total',
           7=> 'action',
        );
        $totalData = Sale::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Sale::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Sale::where('unique_id','LIKE',"%{$search}%")
                ->orWhere('billing_date', 'LIKE',"%{$search}%")
                ->orWhere('grand_total', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Sale::where('unique_id','LIKE',"%{$search}%")
                ->orWhere('billing_date', 'LIKE',"%{$search}%")
                ->orWhere('grand_total', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
              $customer =Customer::find($post->customer_id);
                $edit =  route('editSale',$post->id);
               if($post->customer_id!='1'){
                    $print =  route('printnewsaleInvoice',$post->id);
                }else{
                    $print =  route('printnewsaleInvoice',$post->id);
                }
                $delete =  route('deleteSale',$post->id);

                if($post->status=='cancel'){
                    $nestedData['unique_id'] = '<del style="color: red">'.$post->unique_id.'</del>';
                    $nestedData['billing_date'] = date('d-m-Y',strtotime($post->billing_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    $nestedData['gst_type'] = $post->tax_type;
                    $nestedData['net_amount'] = $post->total_taxable_value;
                    $nestedData['gstamount'] = $post->total_tax_amount;
                    $nestedData['grand_total'] = $post->grand_total;
                    $nestedData['action'] = "&emsp;<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default disabled' ><i class='splashy-document_letter_edit'></i></a>
                                          &emsp;<a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          &emsp;<a href='{$delete}' title='Cancel' class='btn btn-sm btn-default disabled' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }else{
                    $nestedData['unique_id'] = $post->unique_id;
                    $nestedData['billing_date'] = date('d-m-Y',strtotime($post->billing_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    $nestedData['gst_type'] = $post->tax_type;
                    $nestedData['net_amount'] = $post->total_taxable_value;
                    $nestedData['gstamount'] = $post->total_tax_amount;
                    $nestedData['grand_total'] = $post->grand_total;
                    $nestedData['action'] = "&emsp;<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default' ><i class='splashy-document_letter_edit'></i></a>
                                          &emsp;<a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          &emsp;<a href='{$delete}' title='Cancel' class='btn btn-sm btn-default' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }


            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }


    public function sale_type()
    {
        return View::make('sales/saleType');
    }

    public function store_tax_type(Request $request)
    {
//        return View::make('sales/createSale')->with('tax_type',$request->tax_type);
        return redirect()->route('createSale', [$request->tax_type]);
    }
     public function printnewsaleInvoice($id)
    {
        $get_sale=Sale::find($id);

        return View::make('challan/printnewSaleInvoice')->with('printBill',$get_sale);

    }

    public function createSale($id)
    {
        return View::make('sales/createSale')->with('text_type', $id);
    }

    public function storeSale(Request $request)
    {

        $this->validate($request, [
            'unique_id' => 'required|unique:sales',
        ],
        [
            'unique_id.unique'      => 'Sorry, This Invoice No. Is Already Used ',
        ]);

//        $data = Input::all();
//        print_r($data);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


//   ----------------------------tax fields----------------------------------


        $tax_type = $request->input('tax_type');
        $total_tax = $request->input('total_gst_input');
        $gross_total = $request->input('total_sub_input');
        $grand_total = $request->input('grand_total_input');

       $check_duplicate = Sale::where('unique_id',$request->input('unique_id'))->first();
        if(! $check_duplicate) {

            try{
                DB::beginTransaction();
                $sale = new Sale();
                $sale->unique_id = $request->input('unique_id');
                $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $sale->order_no = $request->input('order_no');
                $sale->order_date = $request->input('order_date');
                $sale->customer_id = $request->input('customer');
                $sale->payment_type = $request->input('payment_type');
                $sale->retail_name = $request->input('retail_customer_name');
                $sale->city = $request->input('retail_city');
                $sale->mobile = $request->input('retail_mobile');
                $sale->email = $request->input('retail_email');
                $sale->payment_desc = $request->input('payment_description');
                $sale->total_amount_without_anything = $request->input('total_amount_input');
                $sale->total_discount = $request->input('total_discount_input');
                $sale->total_taxable_value = $request->input('total_taxble_input');
                if ($tax_type == "CGST&SGST") {
                    $sale->total_cgst = $total_tax / 2;
                    $sale->total_sgst = $total_tax / 2;
                } else {
                    $sale->total_igst = $total_tax;
                }
                $sale->tax_type = $tax_type;
                $sale->total_tax_amount = $total_tax;
                $sale->gross_total = $gross_total;
                $sale->postage_charge = $request->input('postage_charge');
                $sale->grand_total = $grand_total;
                $sale->grand_total_word =  $this->amount_in_words($grand_total);
                $sale->save();

                //        bill no and session save
                $sale_session = Sale_bill_session::find($session_id);
                $sale_session->bill = $sale_session->bill + 1;
                $sale_session->save();


//        **************** store in ledger ********************
                if ($request->input('customer') == 1) {

                    $ledgerdr = new Ledgerdr();
                    $ledgerdr->unique_id = $request->input('unique_id');
                    $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $ledgerdr->customer_id = $request->input('customer');
                    $ledgerdr->type = "CashSale";
                    $ledgerdr->amount = $grand_total;
                    $ledgerdr->extra = "+";
                    $ledgerdr->sale_id = $sale->id;
                    $ledgerdr->save();
                    //        **************** store in ledger ********************

                    $cash_ledger = new Cash();
                    $cash_ledger->unique_id = $request->input('unique_id');
                    $cash_ledger->date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $cash_ledger->customer_id = $request->input('customer');
                    $cash_ledger->type = "CashSale";
                    $cash_ledger->amount = $grand_total;
                    $cash_ledger->extra = "+";
                    $cash_ledger->sale_id = $sale->id;
                    $cash_ledger->save();

                } else {
                    $ledgerdr = new Ledgerdr();
                    $ledgerdr->unique_id = $request->input('unique_id');
                    $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $ledgerdr->customer_id = $request->input('customer');
                    $ledgerdr->type = "CreditSale";
                    $ledgerdr->amount = $grand_total;
                    $myledger = Customer::find($request->customer);
                    if($myledger->ledger_group==2){
                        $ledgerdr->extra ="-";
                    }else{
                        $ledgerdr->extra ="+";
                    }
                    $ledgerdr->sale_id = $sale->id;
                    $ledgerdr->save();
                }


//       $billNo= Sale::where('id', '=', $sale->id)->first();


                foreach ($input as $row) {

                    $flight = new Sale_invoice();
                    $flight->sale_id = $sale->id;
                    $flight->product_id = $row['product'];


                    $flight->description = $row['desc'];
                    $flight->quantity = $row['qty'];
                    $flight->rate = $row['rate'];
                    $flight->amount_before_tax = $row['amount'];
                    $flight->discount_per = $row['disc_percentage'];
                    $flight->discount = $row['disc'];
                    $flight->taxable_amount = $row['taxbl_amount'];
                    if ($tax_type == "CGST&SGST") {
                        $flight->sgst_tax = $row['gst'] / 2;
                        $flight->cgst_tax = $row['gst'] / 2;
                    } else {
                        $flight->igst_tax = $row['gst'];
                    }
                    $flight->tax_amount = $row['text_amount'];
                    $flight->total_amount = $row['price'];
                    $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->save();


                }


                DB::commit();
                Session::flash('flash_message', 'New sale successfully added!');
                return redirect('sale')->with('status', $sale->id)->with('customer',$request->input('customer'));



            } catch(\Exception $e){
                DB::rollback();
                Session::flash('flash_message', 'Network Problem Please try again');
                return redirect()->back();
//echo $e->getMessage();
            }

        }else{
            return redirect()->route('sale');
        }
    }

    public function singleViewSale($id)
    {
        $editSale = Sale::find($id);
        return View::make('sales/singleViewSale')->with('editPurchase', $editSale);
    }

    public function editSale($id)
    {
        $editSale = Sale::find($id);
        return View::make('sales/editSale')->with('editPurchase', $editSale);
    }

    public function printSaleInvoice($id)
    {
        $printBill = Sale::find($id);
        return View::make('sales/printSaleInvoice')->with('printBill', $printBill);
    }

    public function updateSale(Request $request, $id)
    {
//                $data2 = Input::all();
//        print_r($data2);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


//   ----------------------------tax fields----------------------------------


        $tax_type = $request->input('tax_type');
        $total_tax = $request->input('total_gst_input');
        $gross_total = $request->input('total_sub_input');
        $grand_total = $request->input('grand_total_input');

        try{
            DB::beginTransaction();
            $sale = Sale::find($id);
            $sale->unique_id = $request->input('invoice_no');
            $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
            $sale->order_no = $request->input('order_no');
            $sale->order_date = $request->input('order_date');
            $sale->customer_id = $request->input('customer');
            $sale->payment_type = $request->input('payment_type');
            $sale->retail_name = $request->input('retail_customer_name');
            $sale->city = $request->input('retail_city');
            $sale->mobile = $request->input('retail_mobile');
            $sale->payment_desc = $request->input('payment_description');
            $sale->total_amount_without_anything = $request->input('total_amount_input');
            $sale->total_discount = $request->input('total_discount_input');
            $sale->total_taxable_value = $request->input('total_taxble_input');
            if ($tax_type == "CGST&SGST") {
                $sale->total_cgst = $total_tax / 2;
                $sale->total_sgst = $total_tax / 2;
            } else {
                $sale->total_igst = $total_tax;
            }
            $sale->tax_type = $tax_type;
            $sale->total_tax_amount = $total_tax;
            $sale->gross_total = $gross_total;
            $sale->postage_charge=$request->input('postage_charge');
            $sale->grand_total = $grand_total;
            $sale->grand_total_word =$this->amount_in_words($grand_total);
            $sale->save();


            if ($request->input('old_customer_id') == 1) {

                $cash_ledger1 = Cash::where('sale_id', '=', $id)->first();
                if($cash_ledger1){
                    $item = new Cash();
                    $item::find($cash_ledger1->id)->delete();
                }


            }

            if ($request->input('customer') == 1) {

                // **************** store in ledger ********************
                $ledgerdr = Ledgerdr::where('sale_id', '=', $id)->first();
//        $ledgerdr->unique_id= $sale_id;
                $ledgerdr->customer_id = $request->input('customer');
                $ledgerdr->type = "CashSale";
                $ledgerdr->amount = $grand_total;
                $ledgerdr->extra = "+";

                $ledgerdr->save();
//  **************** store in ledger ********************

                $cash_ledger = new Cash();
                $cash_ledger->unique_id = $request->input('invoice_no');
                $cash_ledger->date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $cash_ledger->customer_id = $request->input('customer');
                $cash_ledger->type = "CashSale";
                $cash_ledger->amount = $grand_total;
                $cash_ledger->extra = "+";
                $cash_ledger->sale_id =$id;
                $cash_ledger->save();

            } else {
                // **************** store in ledger ********************
                $ledgerdr = Ledgerdr::where('sale_id', '=', $id)->first();
//        $ledgerdr->unique_id= $sale_id;
                $ledgerdr->customer_id = $request->input('customer');
                $ledgerdr->type = "CreditSale";
                $ledgerdr->amount = $grand_total;
                $myledger = Customer::find($request->customer);
                if($myledger->ledger_group==2){
                    $ledgerdr->extra ="-";
                }else{
                    $ledgerdr->extra ="+";
                }
                $ledgerdr->save();
//  **************** store in ledger ********************
            }
//-----------------------------  purchase invoice table ----------------

            foreach ($input as $row) {
                if ($row['sale_invoice'] > 0) {
                    $flight = Sale_invoice::find($row['sale_invoice']);
                } else {
                    $flight = new Sale_invoice();
                }

                $flight->sale_id = $sale->id;
                $flight->product_id = $row['product'];


                $flight->description = $row['desc'];
                $flight->quantity = $row['qty'];
                $flight->rate = $row['rate'];
                $flight->amount_before_tax = $row['amount'];
                $flight->discount_per = $row['disc_percentage'];
                $flight->discount = $row['disc'];
                $flight->taxable_amount = $row['taxbl_amount'];
                if ($tax_type == "CGST&SGST") {
                    $flight->sgst_tax = $row['gst'] / 2;
                    $flight->cgst_tax = $row['gst'] / 2;
                } else {
                    $flight->igst_tax = $row['gst'];
                }
                $flight->tax_amount = $row['text_amount'];
                $flight->total_amount = $row['price'];
                $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $flight->save();
            }
            DB::commit();


            return redirect('sale')->with('status', $sale->id);



        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }




    }


    public function deleteSale($id)
    {

        try{
            DB::beginTransaction();
            $saleDelete = Sale::find($id);
            $saleDelete->status = "cancel";
            $saleDelete->save();

            $ledger_creditors = Ledgerdr::where('sale_id', '=', $id)->first();
            $ledger_creditors->status = "cancel";
            $ledger_creditors->save();

            if ($saleDelete->customer_id ==1) {
                $ledger_cash = Cash::where('unique_id', '=', $saleDelete->unique_id)->first();
                $ledger_cash->status = "cancel";
                $ledger_cash->save();
            }


            $sale_invoice = Sale_invoice::where('sale_id', '=', $id)->get();
            foreach ($sale_invoice as $invoice) {
                $invoice->status = "cancel";
                $invoice->save();
            }


            DB::commit();
            return redirect()->route('sale');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
//            return redirect()->back();
echo $e->getMessage();
        }





    }





    public function myformAjax($id)

    {

        $cities = DB::table("customers")
            ->where("id", $id)
            ->get();
        foreach ($cities as $city) {
            return json_encode(array("phone" => $city->mobile, "customer_type" => $city->customer_type, "email" => $city->email, 'city' => $city->city, 'address' => $city->address));
        }
    }


//subcategory of batch number


    public function AjaxCostGst($id, $unique)

    {

        $result = $id;
        $result_explode = explode('|', $result);
        $id1 = $result_explode[0];

        $unique_id = $unique;


        $city = DB::table("products")
            ->where("id", $id1)
            ->first();
       $qnty_sale = Sale_invoice::where('product_id',$id1)->whereNull('status')->sum('quantity');
$qnty_sale_return = Sale_return_invoice::where('product_id',$id1)->sum('quantity') ;
$qnty_purchase = Purchase_invoice::where('product_id',$id1)->whereNull('status')->whereNull('status')->sum('quantity') ;
$qnty_purchase_return = Purchase_return_invoice::where('product_id',$id1)->sum('quantity');
$qnty_challan = \App\Challan_child::where('product_id',$id1)->sum('quantity');

$opening_stock = $city->opening_stock ;
$total_qun = ($opening_stock+$qnty_purchase+$qnty_sale_return)-($qnty_sale+$qnty_purchase_return+$qnty_challan);


            return json_encode(array("cost" => $city->cost_price, "mrp" => $city->mrp, "sellingPrice" => $city->selling_price, "unique_id" => $unique_id, "gst" => $city->igst,'qnty'=>$total_qun));



    }


    public function saleInvoiceAjax($id)
    {

        $item = new Sale_invoice();
        $item::find($id)->delete();


    }

//    new customer create
    public function AjaxNewCustomer(Request $request)
    {
        $this->validate($request, [
            'customer_name' => 'required',


        ]);
//
//        $data= Input::all();
//        print_r($data);
//        die();

        $customer = new Customer();


        $customer->ledger_name = $request->customer_name;
        $customer->address = $request->customer_address;
        $customer->gst_no = $request->gst_no;
        $customer->state_code = $request->state_code;
        $customer->zip = $request->customer_zip;
        $customer->state = $request->customer_state;
        $customer->city = $request->customer_city;
        $customer->phone = $request->customer_phone;
        $customer->mobile = $request->customer_mobile;
        $customer->email = $request->customer_email;
        $customer->opening_balance = $request->customer_opening_balance;
        $customer->ledger_type2 = 2;
        $customer->ledger_group =1;
        $customer->ledger_type = 'debtor';

        $customer->save();
        $city = Customer::find($customer->id);

        return json_encode(array("customer_id" => $city->id, "ledger_name" => $city->ledger_name));

    }

//AjaxNewProduct
    public function AjaxNewProduct(Request $request)
    {
//        $data=Input::all();
//        print_r($data);
//        die();
        $this->validate($request, [
            'product_name' => 'required',
        ]);

        $product = new Product;
        if($request->product_type=="create"){
         $newProductType= new Product_type;
            $newProductType->product_name=$request->new_product_type;
            $newProductType->save();
            $product->product_type_id =$newProductType->id;
            $product->product_type_name = $newProductType->product_name;
        }else{
            $product->product_type_id =$request->product_type;
            $product_type_name = Product_type::find($request->product_type);
            $product->product_type_name = $product_type_name->product_name;
        }
        if($request->brand=="create"){
            $newBrand= new Brand;
            $newBrand->brand_name=$request->new_brand_name;
            $newBrand->save();
            $product->brand_name_id =$newBrand->id;

        }else{
            $product->brand_name_id =$request->brand;

        }

//        $product->brand_name_id = $request->product_brand_id;

        $product->product_name = $request->product_name;
        $product->product_code = $request->hsn_code;
        $product->igst = $request->product_igst;
        $product->cgst = $request->product_cgst;
        $product->sgst = $request->product_sgst;
        $product->selling_price = $request->product_selling_price;
        $product->opening_stock = $request->product_opening_stock;
        $product->qnty = $product->qnty + $request->product_opening_stock;
        $product->unit_type = "pcs";
        $product->mrp = $request->product_mrp;
        $product->save();


        $cities = DB::table("products")->where('id',$product->id)
            ->pluck("id","product_name");

        return json_encode($cities);
    }

    function AjaxUpdateData(){
        $cities = DB::table("products")->orderBy('product_name', 'asc')
            ->pluck("id","product_name");

        return json_encode($cities);
    }

    function AjaxUpdateDataLoop($id){
        $cities = DB::table("products")->orderBy('product_name', 'asc')->pluck("id","product_name");


        return json_encode($cities);
}

    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }

}



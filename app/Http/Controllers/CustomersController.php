<?php

namespace App\Http\Controllers;

use App\Ledger_group;
use App\Ledger_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

use App\Customer;
use Monolog\Handler\NullHandlerTest;
use Session;


class CustomersController extends Controller
{

    public function ledgerPanel(){
        $data = DB::table('customers')->get();
        return View::make('customers/ledger_panel')->withData( $data );;
    }

//----------------------------------------Ledger functions -----------------------------
//---------------------------------------------------------------------------------------
    public function index()
   {
$data = DB::table('customers')->get();
    return  View::make( 'customers/index')->withData( $data );
   }

    public function create_new_ledger_choose(Request $request){
       if($request->ledger_type==2){
           return redirect()->route('createCustomer')->with('ledger_type_id',$request);
//
       }elseif($request->ledger_type==1){
           return redirect()->route('createCreditors')->with('ledger_type_id',$request);
       }else{

           return redirect()->route('createOtherLedger')->with('ledger_type_id',$request);
       }
    }



    public function createCustomer()
    {
        return View::make('customers/createCustomer');
    }

//-----------------------------------------------------  New Sundry Debtors entry -----------------------------

        public function storeCustomer(Request $request)
    {

//        $data= Input::all();
//        print_r($data);
//        die();
        $this->validate($request, [
            'customer_name' => 'required',


        ]);


        $customer = new Customer();


        $customer->ledger_name = $request->customer_name;
        $customer->address = $request->address;
        $customer->zip = $request->zip;
        $customer->state = $request->state;
        $customer->city = $request->city;
        $customer->phone = $request->work_phone;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email;
        $customer->vatNo = $request->vat;
        $customer->bankName = $request->BankName;
        $customer->bankAct = $request->bankAct;
        $customer->pan = $request->pan;
        $customer->ledger_type = 'debtor';
        $customer->ledger_type2 = 2;
        $customer->ledger_group =1;
        $customer->opening_balance =$request->opening_balance;
         $customer->balance_type =$request->balance_type;
        $customer->save();

        Session::flash('flash_message', 'New Customer has successfully added!');

        return redirect()->route('customers');
    }

public function singleCustomer($id)
{
    $viewCustomer=Customer::find($id);
    return View::make('customers/signleCustomer')->with('viewCustomer',$viewCustomer);
}




    public function editCustomer($id)
    {
        $editCustomer=Customer::find($id);
        return View::make('customers/editCustomer')->with('editCustomer',$editCustomer);
    }


    public function updateCustomer(Request $request, $id)
    {
        $this->validate($request, [
            'customer_name' => 'required',

        ]);

//        $item= new Customer();
//
       $customer = Customer::find($id);
        $customer->ledger_name = $request->customer_name;
        $customer->address = $request->address;
        $customer->zip = $request->zip;
        $customer->state = $request->state;
        $customer->city = $request->city;
        $customer->phone = $request->work_phone;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email;
        $customer->vatNo = $request->vat;
        $customer->bankName = $request->BankName;
        $customer->bankAct = $request->bankAct;
        $customer->pan = $request->pan;
        $customer->ledger_type = 'debtor';
        $customer->ledger_type2 = 2;
        $customer->ledger_group =1;
        $customer->opening_balance =$request->opening_balance;
        $customer->balance_type =$request->balance_type;
        $customer->save();

        Session::flash('flash_message', 'Customer successfully Updated!');


        return redirect()->route('customers');

    }


    public function deleteCustomer($id)
    {


        try{
            $item= new Customer;
            $item::find($id)->delete();


            \Session::flash('flash_message', 'Selected Item successfully Deleted!');
        }
        catch (QueryException $e){
            if($e->getCode() == "23000"){
                \Session::flash('flash_message1', "You can't delete this item ! Plz delete all  related Product/invoice before delete this Item!");
            }
        }



        return redirect()->route('customers');
    }

//----------------------------------------Sundary Debtors ends here ------------------------- -----------------------------
//-------------------------------------------------------------------------------------------------------------------------

public function ledger_type_route(){
    $data = Ledger_type::all();
    return  View::make( 'customers/ledger_type/ledger_type_panel')->withData( $data );
}

public function save_new_ledger_type(Request $request){

//    $data = Input::all();
//    print_r($data);
//    die();



    $flight = new Ledger_type();

    $flight->ledger_type_name = $request->ledger_name;
    $flight->ledger_group_id = $request->ledger_group;
    $flight->save();

    Session::flash('flash_message', 'New Ledger successfully added!');

    return redirect()->route('ledger_type_route');
}


    public function update_ledger_type(Request $request, $id)
    {

        $item = Ledger_type::find($id);

        $item->ledger_type_name = $request->ledger_name;
        $item->ledger_group_id = $request->ledger_group;
        $item->save();

        Session::flash('flash_message', 'Ledger successfully Updated!');


        return redirect()->route('ledger_type_route');

    }

//----------------------------------------Ledger Group function -----------------------------
//---------------------------------------------------------------------------------------

    public function ledger_group_route(){
        $data = Ledger_group::all();
        return  View::make( 'customers/ledger_group/ledger_group_panel')->withData( $data );
    }

    public function save_new_ledger_group(Request $request){

//    $data = Input::all();
//    print_r($data);
//    die();



        $flight = new Ledger_group();

        $flight->ledger_group_name = $request->ledger_name;
        $flight->save();

        Session::flash('flash_message', 'New Ledger successfully added!');

        return redirect()->route('ledger_group_route');
    }


    public function update_ledger_group(Request $request, $id)
    {

        $item = Ledger_group::find($id);

        $item->ledger_group_name = $request->ledger_name;
        $item->save();

        Session::flash('flash_message', 'Ledger successfully Updated!');


        return redirect()->route('ledger_group_route');

    }

//------------------------------------------------------- other ledger functions are there ----------------------------

public  function createOtherLedger(){
    return View::make('customers/other_ledger/createOtherLedger');
}

    public function saveOtherLedger(Request $request){

//        $value = Input::all();
//        print_r($value);
//        die();
        $data = New Customer();
        $data->ledger_name=$request->ledger_name;
        $data->opening_balance=$request->opening_balance;
        $data->balance_type =$request->balance_type;
        $data->ledger_type2=$request->ledger_type;
        $ledger_group = Ledger_type::find($request->ledger_type);
        $data->ledger_group=$ledger_group->ledger_group_id;

        $data->save();

        return redirect()->route('customers');
    }


    public  function editOtherLedger($id){
        $data = Customer::find($id);
//        print_r($data);
//        die();
        return View::make('customers/other_ledger/editOtherLedger')->with('data',$data);
    }

public function updateOtherLedger(Request $request,$id){
    $data = Customer::find($id);
    $data->ledger_name=$request->ledger_name;
    $data->opening_balance=$request->opening_balance;
    $data->balance_type =$request->balance_type;
    $data->ledger_type2=$request->ledger_type;
    $ledger_group = Ledger_type::find($request->ledger_type);
    $data->ledger_group=$ledger_group->ledger_group_id;

    $data->save();

    return redirect()->route('customers');
}

    public function viewOtherLedger($id){
        $data = Customer::find($id);
//        print_r($data);
//        die();
        return View::make('customers/other_ledger/viewOtherLedger')->with('data',$data);
    }

//    public function customerLogin()
//    {
//        return View::make('customers/customerLogin');
//    }
//
//    public function customerAuth(Request $request)
//    {
//
//    }



}

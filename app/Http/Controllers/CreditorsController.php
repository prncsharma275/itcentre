<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Creditor;
use App\Customer;
use Monolog\Handler\NullHandlerTest;
use Session;
use App\Ledgercr;

class CreditorsController extends Controller
{
    public function index()
    {

   $data = DB::table('customers')->where('ledger_type', '=', 'creditor')->get();
        return  View::make( 'creditors/index')->withData( $data );

    }

    public function singleCreditor($id)
    {
        $viewCreditor=Customer::find($id);
        return View::make('creditors/singleCreditors')->with('viewCreditor',$viewCreditor);
    }

    public function createCreditors()
    {
        return View::make('creditors/createCreditors');

    }


    public function storeCreditor(Request $request)
    {

        $this->validate($request, [
            'creditor_name' => 'required',

        ]);

        $creditor = new Customer();

        $creditor->ledger_name = $request->creditor_name;
        $creditor->address = $request->address;
        $creditor->zip = $request->zip;
        $creditor->state = $request->state;
        $creditor->city = $request->city;
        $creditor->phone = $request->work_phone;
        $creditor->mobile = $request->mobile;
        $creditor->email = $request->email;
        $creditor->vatNo = $request->vat;
        $creditor->bankName = $request->BankName;
        $creditor->bankAct = $request->bankAct;
        $creditor->pan = $request->pan;
        $creditor->pan = $request->pan;
        $creditor->ledger_type = 'creditor';
        $creditor->ledger_type2 = 1;
        $creditor->ledger_group =2;
        $creditor->opening_balance =$request->opening_balance;
        $creditor->balance_type =$request->balance_type;
        $creditor->save();

        Session::flash('flash_message', 'New Creditor has successfully added!');

        return redirect()->route('customers');

    }





    public function editCreditor($id)
    {
        $editCreditor=Customer::find($id);
        return View::make('creditors/editCreditors')->with('editCreditor',$editCreditor);

    }


    public function updateCreditor(Request $request, $id)
    {

        $this->validate($request, [
            'creditor_name'=>'required',

        ]);

        $creditor = Customer::find($id);

        $creditor->ledger_name = $request->creditor_name;
        $creditor->address = $request->address;
        $creditor->zip = $request->zip;
        $creditor->state = $request->state;
        $creditor->city = $request->city;
        $creditor->phone = $request->work_phone;
        $creditor->mobile = $request->mobile;
        $creditor->email = $request->email;
        $creditor->vatNo = $request->vat;
        $creditor->bankName = $request->BankName;
        $creditor->bankAct = $request->bankAct;
        $creditor->pan = $request->pan;
        $creditor->ledger_type = 'creditor';
        $creditor->ledger_type2 = 1;
        $creditor->ledger_group =2;
        $creditor->opening_balance =$request->opening_balance;
        $creditor->balance_type =$request->balance_type;
        $creditor->save();

        Session::flash('flash_message', 'Creditor successfully Updated!');


        return redirect()->route('customers');

    }


    public function deleteCreditor($id)
    {



        try{
            $item= new Customer();
            $item::find($id)->delete();

            \Session::flash('flash_message', 'Selected Item successfully Deleted!');
        }
        catch (QueryException $e){
            if($e->getCode() == "23000"){
                \Session::flash('flash_message1', "You can't delete this item ! Plz delete all  related Product/invoice before delete this Item!");
            }
        }

        return redirect()->route('customers');
    }
}

<?php

namespace App\Http\Controllers;

use App\Company_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


//    company settings

    public function company()
    {
       $company = Company_detail::find(1);
        return View::make('company/company_setting')->with('company',$company);
    }


    public function storeCompany(Request $request,$id)
    {

//    $data = Input::all();
//    print_r($data);
//        die();

        $company = Company_detail::find($id);
        $company->company_name = $request->company_name;
        $company->street_1 = $request->street1;
        $company->street_2 = $request->street2;
        $company->city = $request->city;
        $company->state = $request->state;
        $company->zip = $request->zip;
        $company->email = $request->email;
        $company->phone = $request->phone;
        $company->mobile = $request->mobile;
        $company->gst_no = $request->gst;
        $company->website = $request->website;
        $company->logo = $request->logo;
        $company->billing_sale = $request->sale_bill;
        $company->billing_purchase = $request->purchase_bill;
        $company->billing_sale_return = $request->sale_return_bill;
        $company->billing_purchase_return = $request->purchase_return_bill;
        $company->billing_receipt = $request->receipt_bill;
        $company->billing_payment = $request->payment_bill;
        $company->billing_debit_note = $request->debit_note_bill;
        $company->billing_credit_note = $request->credit_note_bill;
        $company->billing_contra = $request->contra_note_bill;
        $company->save();

        Session::flash('flash_message', 'Company Detail Saved!');

//        return redirect()->route('product');
        return redirect()->route('home');

    }
}

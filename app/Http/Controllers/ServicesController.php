<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use App\Service;
use App\Service_detail;
use Illuminate\Support\Facades\DB;

use Session;

class ServicesController extends Controller
{

    public function services()
    {
        return View::make('services/service_index');
    }


    public function createService()
    {
      return View::make('services/createService');
    }

    public function storeService(Request $request)
    {

// print_r($_POST);
//        die();
        $this->validate($request, [
            'customerName' => 'required',
            'product_name' => 'required',
            'serialNumber' => 'required',
            'remarks' => 'required',
            'serviceNumber' => 'required|unique:services',


        ]);
        if (isset($_POST['save_exit']) or isset($_POST['save_next'])) {
            $flight = new Service;
            $flight->complainDate=date("Y/m/d");
            $flight->serviceNumber = $request->serviceNumber;
            $flight->customer_id = $request->customerName;
            $flight->customer_mobile = $request->phone;
            $flight->customer_address = $request->address;
            $flight->email = $request->email;
            $flight->customer_city = $request->city;
            $flight->prouduct_name = $request->product_name;
            $flight->prouduct_description = $request->productRemarks;
            $flight->purchase_date = $date = date('Y-m-d', strtotime(str_replace('-', '/', $request->purchaseDate)));
            $flight->serialNumber = $request->serialNumber;
            $flight->remarks = $request->remarks;
            $flight->save();

            if (isset($_POST['save_exit'])) {
                Session::flash('flash_message', 'New Brand successfully added!');

                return redirect()->route('services');
            } else {
                return redirect('nextPage')->with('serviceid',$request->serviceNumber);
            }
        }else{
            echo"you are not authorised please got to home page";
        }
    }

    public function servicePending($id)
    {
        $editService=Service::find($id);
        $servicNumber=$editService->serviceNumber;

        return redirect('nextPage')->with('serviceid',$servicNumber);
    }


    public function updateService(Request $request, $id)
    {

        $this->validate($request, [

            'ServiceDate' => 'required',
            'engineer' => 'required',
            'natureOfServie' => 'required',
            'serviceStatus' => 'required',

        ]);

        $data = Input::all();
        $gift = Service::find($id);
        $gift->service_date = $date4 = date('Y-m-d', strtotime(str_replace('-', '/', $data['ServiceDate'])));
        $gift->warrenty_date = $data['warrenty'];
        $gift->engineer_name = $data['engineer'];
        $gift->service_charge = $data['ServiceCharge'];
        $gift->status = $data['serviceStatus'];
        $gift->serviceNature = $data['natureOfServie'];
        $gift->reasonCancel = $data['reasonCancel'];
        $gift->replacePartName = $data['partName1'];
        $gift->replaceQnty = $data['replacementQnty1'];
        $gift->replacePrice = $data['replacementPrice1'];
        $gift->replaceServiceCharge = $data['replacementServiceCharge'];
        $gift->replaceAmount = $data['replacementAmount1'];
        $gift->save();

         Session::flash('flash_message', ' successfully Updated!');


        return redirect()->route('services');

    }
public function serviceView($id)
{
    $service=Service::find($id);
    return View::make('services/singleViewService')->with('service',$service);
}


    public function editService($id){

        $service=Service::find($id);
        return View::make('services/editService')->with('service',$service);

    }
        public function editPendingService($id)
        {
            $service=Service::find($id);
            return View::make('services/editPendingService')->with('service',$service);
        }
    public function updateService2(Request $request,$id)
   {


        $this->validate($request, [

            'ServiceDate' => 'required',
            'engineer' => 'required',
            'natureOfServie' => 'required',
            'serviceStatus' => 'required',

        ]);

        $data = Input::all();
         $gift = Service::find($id);
        $gift->customer_mobile = $data['phone'];
        $gift->prouduct_name = $data['ProductName'];
       $gift->prouduct_description = $data['productRemarks'];
        $gift->purchase_date =$date = date('Y-m-d', strtotime(str_replace('-', '/', $data['purchaseDate'])));
        $gift->serialNumber = $data['SerialNumber'];
        $gift->remarks = $data['remarks'];
        $gift->customer_city = $data['city'];
        $gift->service_date =$date1 = date('Y-m-d', strtotime(str_replace('-', '/', $data['ServiceDate'])));
        $gift->warrenty_date = $data['warrenty'];
        $gift->engineer_name = $data['engineer'];
         $gift->status = $data['serviceStatus'];
       if($data['natureOfServie'] =="REPLACEMENT"){
           $gift->serviceNature = $data['natureOfServie'];
           $gift->service_charge = "";
           $gift->replacePartName = $data['partName1'];
           $gift->replaceQnty = $data['replacementQnty1'];
           $gift->replacePrice = $data['replacementPrice1'];
           $gift->replaceServiceCharge = $data['replacementServiceCharge'];
           $gift->replaceAmount = $data['replacementAmount1'];
           $gift->totalAmount = $data['totalAmount'];
       }elseif($data['natureOfServie'] =="AMC"){
           $gift->serviceNature = $data['natureOfServie'];
           $gift->service_charge = "";
           $gift->replacePartName = "";
           $gift->replaceQnty = "";
           $gift->replacePrice = "";
           $gift->replaceServiceCharge = "";
           $gift->replaceAmount = "";
           $gift->totalAmount = "";
       }
       else{
           $gift->serviceNature = $data['natureOfServie'];
           $gift->service_charge = $data['ServiceCharge'];
           $gift->replacePartName = "";
           $gift->replaceQnty = "";
           $gift->replacePrice = "";
           $gift->replaceServiceCharge = "";
           $gift->replaceAmount = "";
           $gift->totalAmount = "";
       }
        $gift->reasonCancel = $data['reasonCancel'];



        $gift->save();
       $detail = new Service_detail();
       $detail->serviceNumber=$data['serviceNumber'];
       $detail->service_date=$date1 = date('Y-m-d', strtotime(str_replace('-', '/', $data['ServiceDate'])));
       $detail->engineer_name=$data['engineer'];
       $detail->save();


        Session::flash('flash_message', ' successfully Updated!');


        return redirect()->route('services');

    }

    public function updateServicePending(Request $request,$id)
    {
//        print_r($_POST);
//        die();

        $this->validate($request, [

            'phone' => 'required',
            'ProductName' => 'required',
            'SerialNumber' => 'required',
            'remarks' => 'required',

        ]);

        $data = Input::all();
        $gift = Service::find($id);
        $gift->customer_mobile = $data['phone'];
        $gift->prouduct_name = $data['ProductName'];
        $gift->prouduct_description = $data['productRemarks'];
        $gift->purchase_date = $date2 = date('Y-m-d', strtotime(str_replace('-', '/', $data['purchaseDate'])));
        $gift->serialNumber = $data['SerialNumber'];
        $gift->remarks = $data['remarks'];
        $gift->customer_city = $data['city'];

        $gift->save();

        Session::flash('flash_message', ' successfully Updated!');


        return redirect()->route('services');

    }
    public function deleteService($id)
    {
        $item= new Service();
        $item::find($id)->delete();

        Session::flash('flash_message', 'Selected Service Deleted successfully !');


        return redirect()->route('services');
    }
    public function servicePrint($id)
    {
        $service=Service::find($id);
        return View::make('services/servicePrint')->with('service',$service);
    }


    public function engineerServicePending($engineer)
    {
        $service_nonvisited = DB::table('services')->where([
            ['status', '=','nonVisited' ],

        ])->where([['engineer_name', '=',$engineer ],])->get();
        return View::make('services/engineeNonVisit')->with('service_nonvisited',$service_nonvisited);
    }

    public function engineerEditService($id)
    {
        $service=Service::find($id);
        return View::make('services/engineerEditService')->with('service',$service);
    }

    //in engineer page
    public function updateEngineerFeedback(Request $request,$id)
    {
        $this->validate($request, [

            'serviceStatus' => 'required',
            'engineerFeedback' => 'required',

        ]);
        $data = Input::all();
        $gift = Service::find($id);
        $gift->status = $data['serviceStatus'];
        $gift->engineerFeedback = $data['engineerFeedback'];
        $gift->save();
        Session::flash('flash_message', 'FeedBack updated successfully !');


        return redirect()->action('HomeController@index');
//        return redirect()->route('engineerServicePending/{{}}');

    }
    //backend page in engineer sections

    public function EngineerServiceView($id){

        $service=Service::find($id);
        return View::make('services/AfterEngineerFeedbackView')->with('service',$service);
    }
    //backend page manage engineer feedback
    public function EngineerServiceManage($id)
    {
        $service=Service::find($id);
        return View::make('services/EngineerServiceManage')->with('service',$service);
    }

    public function EngineerFeedbackUpdateService(Request $request,$id)
    {


        $this->validate($request, [

            'ServiceDate' => 'required',
            'engineer' => 'required',
            'natureOfServie' => 'required',
            'serviceStatus' => 'required',

        ]);

        $data = Input::all();
        $gift = Service::find($id);
        $gift->customer_mobile = $data['phone'];
        $gift->prouduct_name = $data['ProductName'];
        $gift->prouduct_description = $data['productRemarks'];
        $gift->purchase_date =$date = date('Y-m-d', strtotime(str_replace('-', '/', $data['purchaseDate'])));
        $gift->serialNumber = $data['SerialNumber'];
        $gift->remarks = $data['remarks'];
        $gift->customer_city = $data['city'];
        $gift->service_date =$date1 = date('Y-m-d', strtotime(str_replace('-', '/', $data['ServiceDate'])));
        $gift->warrenty_date = $data['warrenty'];
        $gift->engineer_name = $data['engineer'];
        $gift->status = $data['serviceStatus'];
        if($data['natureOfServie'] =="REPLACEMENT"){
            $gift->serviceNature = $data['natureOfServie'];
            $gift->service_charge = "";
            $gift->replacePartName = $data['partName1'];
            $gift->replaceQnty = $data['replacementQnty1'];
            $gift->replacePrice = $data['replacementPrice1'];
            $gift->replaceServiceCharge = $data['replacementServiceCharge'];
            $gift->replaceAmount = $data['replacementAmount1'];
            $gift->totalAmount = $data['totalAmount'];
        }elseif($data['natureOfServie'] =="AMC"){
            $gift->serviceNature = $data['natureOfServie'];
            $gift->service_charge = "";
            $gift->replacePartName = "";
            $gift->replaceQnty = "";
            $gift->replacePrice = "";
            $gift->replaceServiceCharge = "";
            $gift->replaceAmount = "";
            $gift->totalAmount = "";
        }
        else{
            $gift->serviceNature = $data['natureOfServie'];
            $gift->service_charge = $data['ServiceCharge'];
            $gift->replacePartName = "";
            $gift->replaceQnty = "";
            $gift->replacePrice = "";
            $gift->replaceServiceCharge = "";
            $gift->replaceAmount = "";
            $gift->totalAmount = "";
        }
        $gift->reasonCancel = $data['reasonCancel'];



        $gift->save();
        $detail = new Service_detail();

        $detail->serviceNumber=$data['serviceNumber'];
        $detail->service_date=$date1 = date('Y-m-d', strtotime(str_replace('-', '/', $data['ServiceDate'])));
        $detail->engineer_name=$data['engineer'];
        $detail->engineerFeedback=$data['engineerFeedback'];
        $detail->save();


        Session::flash('flash_message', ' successfully Updated!');


        return redirect()->route('services');

    }
//ajax data sending in create service

    public function myformAjax($id)

    {

        $cities = DB::table("customers")

            ->where("id",$id)

            ->get();
foreach($cities as $city){



        return json_encode(array("phone"=>$city->mobile,"email"=>$city->email,'city'=>$city->city,'address'=>$city->address));

////        print_r($city);
//        die();
}
    }

//    customer service code start here

public function CustomerServiceIndex(){
    return View::make('customers/customerServiceIndex');
}
    public function CustomerService()
    {
        return View::make('customers/customerService');
    }



public function storeCustomerService(Request $request)
{

    $this->validate($request, [

        'product_name' => 'required',
        'serialNumber' => 'required',
        'remarks' => 'required',
        'serviceNumber' => 'required|unique:services',


    ]);


        $flight = new Service;
        $flight->complainDate=date("Y/m/d");
        $flight->serviceNumber = $request->serviceNumber;
        $flight->customer_id = $request->customerName;
        $flight->customer_mobile = $request->customerMobile;
        $flight->customer_address = $request->customerAddress;
        $flight->email = $request->customerEmail;
        $flight->customer_city = $request->customerCity;
        $flight->prouduct_name = $request->product_name;
        $flight->prouduct_description = $request->productRemarks;
        $flight->purchase_date = $date = date('Y-m-d', strtotime(str_replace('-', '/', $request->purchaseDate)));
        $flight->serialNumber = $request->serialNumber;
        $flight->remarks = $request->remarks;
        $flight->save();


            Session::flash('flash_message', 'Thank You ! We will Get you very soon');

    return redirect()->action('CustomerHomeController@index');
    }


 public function CustomerServiceView($id)

{
//    removing last 4 digit of id
   $mid= substr($id, 0, -4);

    //removing fist 18 digit of id
   $mainId= substr($mid,18);

    $service=Service::find($mainId);
    return View::make('customers/CustomerServiceView')->with('service',$service);
}

    public function CustomerServicePrint($id)
    {
        $mid= substr($id, 0, -4);

        //removing fist 18 digit of id
        $mainId= substr($mid,18);

        $service=Service::find($mainId);
        return View::make('customers/CustomerServicePrint')->with('service',$service);
    }
}

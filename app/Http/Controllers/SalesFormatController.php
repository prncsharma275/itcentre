<?php

namespace App\Http\Controllers;

use App\Purchase_invoice;
use App\Purchase_return_invoice;
use App\Sale;
use App\Sale_bill_session;
use App\Sale_bill_session_format;
use App\Sale_format;
use App\Sale_invoice_format;
use App\Sale_return_invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Session;
use Illuminate\Database\QueryException;
use App\Sale_invoice;
use App\Product;
use App\Product_type;
use App\Brand;
use App\Cancel_Sale;
use App\Cancel_Sale_invoice;
use DateTime;
use App\Exp_table;
use App\Ledgerdr;
use App\Output_tax;
use App\Cash;
use App\Customer;



class SalesFormatController extends Controller
{

    public function saleIndex_format()
    {

        return View::make('sales/sale_format/saleIndex_format   ');

    }



    public function allposts1(Request $request)
    {

        $columns = array(

            0 =>'unique_id',
            1=> 'billing_date',
            2=> 'customer_id',
            3=> 'grand_total',
            4=> 'action',
        );

        $totalData = Sale_format::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Sale_format::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Sale_format::where('unique_id','LIKE',"%{$search}%")
                ->orWhere('billing_date', 'LIKE',"%{$search}%")
                ->orWhere('grand_total', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Sale_format::where('unique_id','LIKE',"%{$search}%")
                ->orWhere('billing_date', 'LIKE',"%{$search}%")
                ->orWhere('grand_total', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $customer =Customer::find($post->customer_id);
                $edit =  route('editSale_format',$post->id);
                if($post->customer_id!='1'){
                    $print =  route('sale_invoice_format_print',$post->id);
                }else{
                    $print =  route('sale_invoice_format_retail_print',$post->id);
                }

                $delete =  route('deleteSale_format',$post->id);

                if($post->status=='cancel'){
                    $nestedData['unique_id'] = '<del style="color: red">'.$post->unique_id.'</del>';
                    $nestedData['billing_date'] = date('d-m-Y',strtotime($post->billing_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    $nestedData['grand_total'] = $post->grand_total;
                    $nestedData['action'] = "&emsp;<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default disabled' ><i class='splashy-document_letter_edit'></i></a>
                                          &emsp;<a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          &emsp;<a href='{$delete}' title='Cancel' class='btn btn-sm btn-default disabled' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }else{
                    $nestedData['unique_id'] = $post->unique_id;
                    $nestedData['billing_date'] = date('d-m-Y',strtotime($post->billing_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    $nestedData['grand_total'] = $post->grand_total;
                    $nestedData['action'] = "&emsp;<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default' ><i class='splashy-document_letter_edit'></i></a>
                                          &emsp;<a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          &emsp;<a href='{$delete}' title='Cancel' class='btn btn-sm btn-default' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }


            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }


    public function sale_type_format()
    {
        return View::make('sales/sale_format/saleType_format');
    }

    public function store_tax_type_format(Request $request)
    {
//        return View::make('sales/createSale')->with('tax_type',$request->tax_type);
        return redirect()->route('createSale_format', [$request->tax_type]);
    }

    public function createSale_format($id)
    {
        return View::make('sales/sale_format/createSale_format')->with('text_type', $id);
    }

    public function storeSale_format(Request $request)
    {

        $this->validate($request, [
            'unique_id' => 'required|unique:sales_format',
        ],
        [
            'unique_id.unique'      => 'Sorry, This Invoice No. Is Already Used ',
        ]);

//        $data = Input::all();
//        print_r($data);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


//   ----------------------------tax fields----------------------------------


        $tax_type = $request->input('tax_type');
        $total_tax = $request->input('total_gst_input');
        $gross_total = $request->input('total_sub_input');
        $grand_total = $request->input('grand_total_input');

       $check_duplicate = Sale_format::where('unique_id',$request->input('unique_id'))->first();
        if(! $check_duplicate) {

            try{
                DB::beginTransaction();
                $sale = new Sale_format();
                $sale->unique_id = $request->input('unique_id');
                $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $sale->order_no = $request->input('order_no');
                $sale->order_date = $request->input('order_date');
                $sale->customer_id = $request->input('customer');
                $sale->payment_type = $request->input('payment_type');
                $sale->retail_name = $request->input('retail_customer_name');
                $sale->city = $request->input('retail_city');
                $sale->mobile = $request->input('retail_mobile');
                $sale->email = $request->input('retail_email');
                $sale->payment_desc = $request->input('payment_description');
                $sale->total_amount_without_anything = $request->input('total_amount_input');
                $sale->total_discount = $request->input('total_discount_input');
                $sale->total_taxable_value = $request->input('total_taxble_input');
                if ($tax_type == "CGST&SGST") {
                    $sale->total_cgst = $total_tax / 2;
                    $sale->total_sgst = $total_tax / 2;
                } else {
                    $sale->total_igst = $total_tax;
                }
                $sale->tax_type = $tax_type;
                $sale->total_tax_amount = $total_tax;
                $sale->gross_total = $gross_total;
                $sale->postage_charge = $request->input('postage_charge');
                $sale->grand_total = $grand_total;
                $sale->grand_total_word =  $this->amount_in_words($grand_total);
                $sale->save();

                //        bill no and session save
                $sale_session = Sale_bill_session_format::find($session_id);
                $sale_session->bill = $sale_session->bill + 1;
                $sale_session->save();


//        **************** store in ledger ********************

//       $billNo= Sale::where('id', '=', $sale->id)->first();


                foreach ($input as $row) {

                    $flight = new Sale_invoice_format();
                    $flight->sale_id = $sale->id;
                    $flight->product_id = $row['product'];


                    $flight->description = $row['desc'];
                    $flight->quantity = $row['qty'];
                    $flight->rate = $row['rate'];
                    $flight->amount_before_tax = $row['amount'];
                    $flight->discount_per = $row['disc_percentage'];
                    $flight->discount = $row['disc'];
                    $flight->taxable_amount = $row['taxbl_amount'];
                    if ($tax_type == "CGST&SGST") {
                        $flight->sgst_tax = $row['gst'] / 2;
                        $flight->cgst_tax = $row['gst'] / 2;
                    } else {
                        $flight->igst_tax = $row['gst'];
                    }
                    $flight->tax_amount = $row['text_amount'];
                    $flight->total_amount = $row['price'];
                    $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->save();


                }


                DB::commit();
                Session::flash('flash_message', 'New sale successfully added!');
                return redirect('sale_format_index')->with('status', $sale->id)->with('customer',$request->input('customer'));



            } catch(\Exception $e){
                DB::rollback();
                Session::flash('flash_message', $e->getMessage());
                return redirect()->back();
//echo $e->getMessage();
            }

        }else{
            return redirect()->route('sale_format_index');
        }
    }

    public function editSale_format($id)
    {
        $editSale = Sale_format::find($id);
        return View::make('sales/sale_format/editSale_format')->with('editPurchase', $editSale);
    }

    public function updateSale_format(Request $request, $id)
    {
//                $data2 = Input::all();
//        print_r($data2);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


//   ----------------------------tax fields----------------------------------


        $tax_type = $request->input('tax_type');
        $total_tax = $request->input('total_gst_input');
        $gross_total = $request->input('total_sub_input');
        $grand_total = $request->input('grand_total_input');

        try{
            DB::beginTransaction();
            $sale = Sale_format::find($id);
            $sale->unique_id = $request->input('invoice_no');
            $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
            $sale->order_no = $request->input('order_no');
            $sale->order_date = $request->input('order_date');
            $sale->customer_id = $request->input('customer');
            $sale->payment_type = $request->input('payment_type');
            $sale->retail_name = $request->input('retail_customer_name');
            $sale->city = $request->input('retail_city');
            $sale->mobile = $request->input('retail_mobile');
            $sale->payment_desc = $request->input('payment_description');
            $sale->total_amount_without_anything = $request->input('total_amount_input');
            $sale->total_discount = $request->input('total_discount_input');
            $sale->total_taxable_value = $request->input('total_taxble_input');
            if ($tax_type == "CGST&SGST") {
                $sale->total_cgst = $total_tax / 2;
                $sale->total_sgst = $total_tax / 2;
            } else {
                $sale->total_igst = $total_tax;
            }
            $sale->tax_type = $tax_type;
            $sale->total_tax_amount = $total_tax;
            $sale->gross_total = $gross_total;
            $sale->postage_charge=$request->input('postage_charge');
            $sale->grand_total = $grand_total;
            $sale->grand_total_word =$this->amount_in_words($grand_total);
            $sale->save();


//-----------------------------  purchase invoice table ----------------

            foreach ($input as $row) {
                if ($row['sale_invoice'] > 0) {
                    $flight = Sale_invoice_format::find($row['sale_invoice']);
                } else {
                    $flight = new Sale_invoice_format();
                }

                $flight->sale_id = $sale->id;
                $flight->product_id = $row['product'];


                $flight->description = $row['desc'];
                $flight->quantity = $row['qty'];
                $flight->rate = $row['rate'];
                $flight->amount_before_tax = $row['amount'];
                $flight->discount_per = $row['disc_percentage'];
                $flight->discount = $row['disc'];
                $flight->taxable_amount = $row['taxbl_amount'];
                if ($tax_type == "CGST&SGST") {
                    $flight->sgst_tax = $row['gst'] / 2;
                    $flight->cgst_tax = $row['gst'] / 2;
                } else {
                    $flight->igst_tax = $row['gst'];
                }
                $flight->tax_amount = $row['text_amount'];
                $flight->total_amount = $row['price'];
                $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $flight->save();
            }
            DB::commit();


            return redirect('sale_format_index')->with('status', $sale->id);



        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }




    }


    public function deleteSale_format($id)
    {

        try{
            DB::beginTransaction();
            $saleDelete = Sale_format::find($id);
            $saleDelete->status = "cancel";
            $saleDelete->save();


            $sale_invoice = Sale_invoice_format::where('sale_id', '=', $id)->get();
            foreach ($sale_invoice as $invoice) {
                $invoice->status = "cancel";
                $invoice->save();
            }


            DB::commit();
            return redirect()->route('sale_format_index');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
//            return redirect()->back();
echo $e->getMessage();
        }
    }

    public function sale_invoiceAjax_format($id)
    {

        $item = new Sale_invoice_format();
        $item::find($id)->delete();


    }


    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }
}




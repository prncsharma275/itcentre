<?php

namespace App\Http\Controllers;


use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Debitnote;
use Illuminate\Database\QueryException;
use Monolog\Handler\NullHandlerTest;
use Session;
use DateTime;
use App\Debitnote_bill_session;
use App\Ledgerdr;

class DebitnoteController extends Controller
{

    public function debitnoteIndex()
    {
        $product= Debitnote::orderBy('id', 'desc')->get();
        return View::make('debitnote/viewDebitNote')->with('product',$product);
    }


 public function singleViewDebitnote($id)
 {

     $product=Debitnote::find($id);
     return View::make('debitnote/singleViewDebitnote')->with('editProduct',$product);

 }

    public function createDebitnote()
    {
      return View::make('debitnote/createDebitnote');
    }

    public function ajaxDebitnote($id){
        $cities = DB::table("creditors")

            ->where("id",$id)

            ->get();
        foreach($cities as $city){
            return json_encode(array("phone"=>$city->mobile,"email"=>$city->email,'city'=>$city->city,'address'=>$city->address));
        }
    }

    public function storeDebitnote(Request $request)
    {
//        $data= Input::all();
//        print_r($data);
//        die();

        $this->validate($request, [
            'desc' => 'required',
            'amount' => 'required',

        ]);
        $payment_date = explode('/', $request->input('sale_return_date'));
        $payment_day = $payment_date[0];
        $payment_month = $payment_date[1];
        $payment_year = $payment_date[2];


        try{
            DB::beginTransaction();
            $product = new Debitnote();

            $product->unique_no = $request->sale_return_no;;
            $product->supplier_id = $request->customer;
            $product->billing_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            $product->amount_in_words =$this->amount_in_words($request->amount);
            $product->total_outstanding = $request->total_outstanding;
            $product->paid_amount_input = $request->paid_amount_input;
            $product->new_outstanding = $request->new_outstanding;
            $product->save();
            //        **************** store in ledger ********************

            $ledgercr=new Ledgerdr();
            $ledgercr->unique_id=$request->sale_return_no;
            $ledgercr->billing_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $ledgercr->customer_id = $request->customer;
            $ledgercr->type = "DebitNote";
            $ledgercr->amount =$request->amount;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==1){
                $ledgercr->extra ="+";
            }else{
                $ledgercr->extra ="-";
            }
            $ledgercr->debit_not_id =$product->id;
            $ledgercr->save();
            //        **************** store in ledger ********************
            $product->save();

            $session_id=$request->input('session_id');
            $sale_session=Debitnote_bill_session::find($session_id);

            $sale_session->bill=$sale_session->bill+1;

            $sale_session->save();



            DB::commit();

            Session::flash('flash_message', 'New Debit Note has successfully added!');

            return redirect()->route('printDebitnote', [$product->id]);

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

    public function editDebitnote($id)
    {
        $editProduct=Debitnote::find($id);
        return View::make('debitnote/editDebitnote')->with('editProduct',$editProduct);
    }


    public function updateDebitnote(Request $request, $id)
    {

//        $data= Input::all();
//        print_r($data);
//        die();

        $this->validate($request, [
            'desc' => 'required',
            'amount' => 'required',

        ]);

        $payment_date = explode('/', $request->input('sale_return_date'));
        $payment_day = $payment_date[0];
        $payment_month = $payment_date[1];
        $payment_year = $payment_date[2];


        try{
            DB::beginTransaction();
            $product=Debitnote::find($id);
            $product->unique_no = $request->sale_return_no;;
            $product->supplier_id = $request->customer;
            $product->billing_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            $product->amount_in_words =$this->amount_in_words($request->amount);
            $product->total_outstanding = $request->total_outstanding;
            $product->paid_amount_input = $request->paid_amount_input;
            $product->new_outstanding = $request->new_outstanding;
            $product->save();

            //        **************** store in ledger ********************

            //         **************** store in ledger ********************

            $ledgercr= Ledgerdr::where('debit_not_id','=',$id)->first();
            $ledgercr->billing_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $ledgercr->customer_id = $request->customer;
            $ledgercr->type = "DebitNote";
            $ledgercr->amount =$request->amount;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==1){
                $ledgercr->extra ="+";
            }else{
                $ledgercr->extra ="-";
            }
            $ledgercr->save();
//  **************** store in ledger ********************
            //        **************** store in ledger ********************



            DB::commit();

            Session::flash('flash_message', 'New Debit Note has successfully updated!');

//        return redirect()->route('product');
            return redirect()->route('printDebitnote', [$id]);

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }


    public function deleteDebitnote($id)
    {

        try{
            DB::beginTransaction();
            $item= new Debitnote();
            $product=Debitnote::find($id);
            //*********** delete from ledger **********
            $ledgerdr=Ledgerdr::where('debit_not_id','=',$id)->first();
            $ledgerdr::find($ledgerdr->id)->delete();
//*********** delete from ledger **********
            $item::find($id)->delete();



            DB::commit();

            Session::flash('flash_message', 'Selected Debit Note successfully Deleted!');


            return redirect()->route('debitnote');
            
        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

    public function printDebitnote($id){
        $editProduct=Debitnote::find($id);
        return View::make('debitnote/printDebitnote')->with('printBill',$editProduct);
    }

    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }
}

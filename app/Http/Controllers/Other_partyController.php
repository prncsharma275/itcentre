<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Other_party;
use Session;

class Other_partyController extends Controller
{

    public function other_party()
    {
        $other_party = DB::table('customers')->where('ledger_type', '=', 'expenses')->orWhere('ledger_type','=','income')
            ->orWhere('ledger_type','=','assets')->orWhere('ledger_type','=','liabilities')->get();
//        $other_ledger= Customer::orderBy('id', 'desc')->get();
        return View::make('customers/other_ledger/other_party_index')->with('other_ledger',$other_party);
    }




    public function saveNewParty(Request $request)
    {
//        $data=Input::all();
//            print_r($data);
//        die();

        $this->validate($request, [
            'ledger_name' => 'required|unique:customers',


        ]);

        $flight = new Customer();

        $flight->ledger_name = $request->ledger_name;
        $flight->ledger_type = $request->ledger_type;
        $flight->save();

        Session::flash('flash_message', 'New Ledger successfully added!');

        return redirect()->route('other_ledger');
    }


    public function updateNewParty(Request $request, $id)
    {

        $this->validate($request, [

            'ledger_name' => 'required',


        ]);
        $item = Customer::find($id);

        $item->ledger_name = $request->ledger_name;
        $item->ledger_type = $request->ledger_type;
        $item->save();

        Session::flash('flash_message', 'Ledger successfully Updated!');


        return redirect()->route('other_ledger');

    }


    public function deleteOther_party($id)
    {


        try{
            $item= new Other_party();
            $item::find($id)->delete();


            \Session::flash('flash_message', 'Selected Item successfully Deleted!');
        }
        catch (QueryException $e){
            if($e->getCode() == "23000"){
                \Session::flash('flash_message1', "You can't delete this item ! Plz delete all  related Product/invoice before delete this Item!");
            }
        }

        return redirect()->route('other_ledger');
    }
}

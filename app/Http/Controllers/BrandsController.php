<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use App\Brand;
use Session;

class BrandsController extends Controller
{

    public function viewBrand()
    {
        $brand= Brand::orderBy('id', 'desc')->get();
        return View::make('brand/viewBrand')->with('brand',$brand);
    }


    public function createBrand()
    {
      return View::make('brand/createBrand');
    }

    public function storeBrand(Request $request)
    {
        $this->validate($request, [
            'brand_name' => 'required|unique:brands|max:15',


        ]);

        $flight = new Brand;

        $flight->brand_name = $request->brand_name;
        $flight->save();

        Session::flash('flash_message', 'New Brand successfully added!');

        return redirect()->route('brand');
    }

    public function editBrand($id)
    {
        $editBrand=Brand::find($id);
        return View::make('brand/editBrand')->with('editBrand',$editBrand);
    }


    public function updateBrand(Request $request, $id)
    {


        $flight =Brand::find($id);

        $flight->brand_name = $request->brand_name;
        $flight->save();

        Session::flash('flash_message', 'Brand successfully Updated!');


        return redirect()->route('brand');

    }


    public function deleteBrand($id)
    {
        try{
            $item= new Brand;
            $item::find($id)->delete();


            \Session::flash('flash_message', 'Selected Product Brand successfully Deleted!');
        }
        catch (QueryException $e){
            if($e->getCode() == "23000"){
                \Session::flash('flash_message1', "You can't delete this Brand ! Plz delete all  related Product/invoice before delete this Product Brand!");
            }
        }

        Session::flash('flash_message', 'Selected Brand successfully Deleted!');


        return redirect()->route('brand');
    }
}

<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Exp_table;
use App\Product_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use App\Product;

use Monolog\Handler\NullHandlerTest;
use Session;
use DateTime;

class ProductsController extends Controller
{

    public function viewProduct()
    {
        $product= Product::orderBy('id', 'desc')->get();
        return View::make('product/viewProduct')->with('product',$product);
    }
 public function singleViewProduct($id)
 {

     $product=Product::find($id);
     return View::make('product/singleViewProduct')->with('editProduct',$product);

 }

    public function createProduct()
    {
      return View::make('product/createProduct');
    }

    public function storeProduct(Request $request)
    {
//        $data=Input::all();
//        print_r($data);
//        die();
        $this->validate($request, [

            'product_name' => 'required|unique:products',
            'igst' => 'required|numeric',
            'cgst' => 'required|numeric',
            'sgst' => 'required|numeric',
        ],
            [
                'product_name.unique'      => 'Sorry, '.$request->product_name. ' Is Already Exist ',
            ]
        );

        $product = new Product;





        if($request->product_type_id=="create"){
            $newProductType= new Product_type;
            $newProductType->product_name=$request->new_product_type;
            $newProductType->save();
            $product->product_type_id =$newProductType->id;
            $product->product_type_name = $newProductType->product_name;
        }else{
            $product->product_type_id =$request->product_type_id;
            $product_type_name = Product_type::find($request->product_type_id);
            $product->product_type_name = $product_type_name->product_name;
        }
        if($request->product_brand_id=="create"){
            $newBrand= new Brand;
            $newBrand->brand_name=$request->new_brand_name;
            $newBrand->save();
            $product->brand_name_id =$newBrand->id;

        }else{
            $product->brand_name_id =$request->product_brand_id;

        }
        $product->product_name = $request->product_name;
        $product->product_desc = $request->product_desc;
        $product->product_code = $request->product_hsn_code;
        $product->igst = $request->igst;
        $product->cgst = $request->cgst;
        $product->sgst = $request->sgst;
         $product->selling_price = $request->selling_price;
         $product->opening_stock = $request->opening_stock;
         $product->qnty = $product->qnty+$request->opening_stock;
         $product->unit_type = $request->unit_type;
         $product->mrp = $request->mrp;
        $product->save();

        Session::flash('flash_message', 'New Product has successfully added!');

//        return redirect()->route('product');
        return redirect()->action(
            'ProductsController@createProduct');
    }

    public function editProduct($id)
    {
        $editProduct=Product::find($id);
        return View::make('product/editProduct')->with('editProduct',$editProduct);
    }


    public function updateProduct(Request $request, $id)
    {

//        $data=Input::all();
//        print_r($data);
//        die();


        $this->validate($request, [

            'product_type_id' => 'required',
            'product_name' => 'required',
            'igst' => 'required|numeric',
            'cgst' => 'required|numeric',
            'sgst' => 'required|numeric',



        ]);
        $product=Product::find($id);

        $product->product_type_id = $request->product_type_id;
        $product_type_name=Product_type::find($request->product_type_id);
        $product->product_type_name = $product_type_name->product_name;
        $product->brand_name_id = $request->product_brand_id;
        $product->product_name = $request->product_name;
        $product->product_desc = $request->product_desc;
        $product->product_code = $request->product_hsn_code;
        $product->igst = $request->igst;
        $product->cgst = $request->cgst;
        $product->sgst = $request->sgst;
        $product->selling_price = $request->selling_price;
        $product->opening_stock = $request->opening_stock;
        $product->qnty = ($product->qnty+$request->opening_stock)-$request->opening_stock_old;
        $product->unit_type = $request->unit_type;
        $product->mrp = $request->mrp;
        $product->save();

        Session::flash('flash_message', 'Product successfully Updated!');


        return redirect()->action(
            'ProductsController@viewProduct');

    }


    public function deleteProduct($id)
    {


        try{
            $item= new Product();
            $item::find($id)->delete();


            \Session::flash('flash_message', 'Selected Item successfully Deleted!');
        }
        catch (QueryException $e){
            if($e->getCode() == "23000"){
                \Session::flash('flash_message1', "You can't delete this item ! Plz delete all  related Product/invoice before delete this Item!");
            }
        }


        return redirect()->action(
            'ProductsController@viewProduct');
    }

//    Product Expiry code here ----------------------------------------- ------------


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Frontend_product;
use Session;
use App\Product;
use Illuminate\Support\Facades\DB;



class Frontend_productsController extends Controller
{


    public function productImage()
    {
//        $brand= Brand::orderBy('id', 'desc')->get();
        return View::make('product/productImage');
    }



public function viewProductImage(Request $request)
{
    $this->validate($request, [
            'product_id' => 'required',
    ]);
    $file=$request->product_id;
    $product_image = DB::table('frontend_products')->where('product_id', $file)->get();
//    $product_image=Frontend_product::find($file);
    return View::make('product/viewProductImage')->with('product_image',$product_image);
}




    public function createProductImage()
    {
      return View::make('productImage/createProductImage');
    }

    public function upload(Request $request)
    {

        $this->validate($request, [
            'image_file' => 'required',
            'image_file.*' => 'image|mimes:jpeg,jpg,png|max:2000|dimensions:max_width=2000,max_height=1024',
            'product_id'=>'required',
        ]);
        $file=Input::file('image_file');

        if (Input::hasFile('image_file')) {
          foreach($file as $file)
          {
              $filename= $request->product_id.$file->getClientOriginalName();
//              $file->move('uploads','3'. $file->getClientOriginalName());
              $file->move('uploads', $filename);
              $flight = new Frontend_product;
              $flight->productImage = $filename;
              $flight->product_id = $request->product_id;

              $flight->save();
          }

        }

        return back()
            ->with('success', 'You have successfully upload images.');

    }




    public function editProductImage($id)
    {
        $editImage=Frontend_product::find($id);
        return View::make('productImage/editProductImage')->with('editImage',$editImage);
    }


    public function updateProductImage(Request $request)
    {

        $this->validate($request, [
            'image_file' => 'required',
            'image_file.*' => 'image|mimes:jpeg,jpg,png|max:2000|dimensions:max_width=1024,max_height=1024',

        ]);
        $oldfile =  Frontend_product::find($request->id);
//        print_r($oldfile);
//        die();
        $file = Input::file('image_file');
        if (Input::hasFile('image_file')) {
            foreach ($file as $file) {
                $filename = $request->product_id . $file->getClientOriginalName();
//              $file->move('uploads','3'. $file->getClientOriginalName());
                $file->move('uploads', $filename);
                $flight = Frontend_product::find($request->id);
                $flight->productImage = $filename;
                $flight->product_id = $request->product_id;
//                 $path= base_path('uploads/'.$oldfile->productImage);
//                print_r($path);
//                die();
                $file_path = base_path("uploads/{$oldfile->productImage}");

                if(File::exists($file_path)) File::delete($file_path);


                $flight->save();
            }


            return redirect('productImage')->with('message', 'You just updated an image!');
        }
    }




    public function deleteProductImage($id)
    {
        $item= new Frontend_product();

        $file_path = base_path("uploads/{$item->productImage}");

        if(File::exists($file_path)) File::delete($file_path);
        $item::find($id)->delete();

        Session::flash('flash_message', 'Selected Brand successfully Deleted!');

        return back();

    }
}

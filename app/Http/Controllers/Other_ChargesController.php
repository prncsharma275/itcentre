<?php

namespace App\Http\Controllers;


use App\Creditor;
use App\Other_charge;
use App\Other_charge_bill_session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Receipt;
use App\Customer;
use Illuminate\Database\QueryException;
use Monolog\Handler\NullHandlerTest;
use Session;
use DateTime;
use App\Receipt_bill_session;
use App\Ledgerdr;
use App\Cash;
use App\Bank;
class Other_ChargesController extends Controller
{

    public function otherChargeIndex()
    {
        $product= Other_charge::orderBy('id', 'desc')->get();
        return View::make('journal/journalView')->with('product',$product);
    }


 public function singleViewOtherCharge($id)
 {

     $editJournal=Other_charge::find($id);
     return View::make('journal/singleViewOther_charge')->with('journal',$editJournal);

 }

    public function createOtherCharge()
    {
      return View::make('journal/createOther_charge');
    }

    public function storeJournal(Request $request)
    {
//        $data= Input::all();
//        print_r($data);
//        die();

        $this->validate($request, [
            'ledger_dr' => 'required',
            'ledger_cr' => 'required',
            'amount_dr' => 'required',
            'amount_cr' => 'required',

        ]);

        $payment_date = explode('/', $request->input('journal_date'));
        $payment_day = $payment_date[0];
        $payment_month = $payment_date[1];
        $payment_year = $payment_date[2];


        try{
            DB::beginTransaction();
            $product = new Other_charge();

            $product->journal_no = $request->journal_no;
            $product->journal_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $product->ledger_dr =$request->ledger_dr;
            $product->ledger_cr =$request->ledger_cr;
            $product->amount_dr =$request->amount_dr;
            $product->amount_cr =$request->amount_cr;
            $product->narration =$request->narration;
            $product->total_amount =$request->total_amount;
            $product->save();

            $session_id=$request->input('session_id');
            $sale_session=Other_charge_bill_session::find($session_id);

            $sale_session->bill=$sale_session->bill+1;

            $sale_session->save();
            //        **************** store in ledger ********************

            if($request->ledger_dr==2) {
                $ledgerdr = new Cash();
                $ledgerdr->unique_id = $request->journal_no;
                $ledgerdr->date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->ledger_cr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount = $request->amount_dr;
                $ledgerdr->extra = "+";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->save();

            }else{
                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->journal_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id =  $request->ledger_cr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount =$request->amount_cr;
                $ledgerdr->extra ="+";
                $ledgerdr->bank_id =$request->ledger_dr;
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->save();
            }

//        Cr entry details here

            if($request->ledger_cr==2){
                $ledgerdr=new Cash();
                $ledgerdr->unique_id=$request->journal_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->ledger_dr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount =$request->amount_cr;
                $ledgerdr->extra ="-";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->save();

            }else{
                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->journal_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id =  $request->ledger_dr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount =$request->amount_cr;
                $ledgerdr->extra ="-";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->bank_id =$request->ledger_cr;
                $ledgerdr->save();
            }

            //        **************** store in ledger ********************


            DB::commit();
            return redirect()->route('otherCharge');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

    public function editOtherCharge($id)
    {
       $editJournal=Other_charge::find($id);

        return View::make('journal/editOther_charge')->with('journal',$editJournal);
    }


    public function updateOtherCharge(Request $request, $id)
    {

//        $data= Input::all();
//        print_r($data);
//        die();

        $this->validate($request, [
            'ledger_dr' => 'required',
            'ledger_cr' => 'required',
            'amount_dr' => 'required',
            'amount_cr' => 'required',

        ]);

        try{
            DB::beginTransaction();

            $product=Other_charge::find($id);

            $payment_date = explode('/', $request->input('journal_date'));
            $payment_day = $payment_date[0];
            $payment_month = $payment_date[1];
            $payment_year = $payment_date[2];

            $product->journal_date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
            $product->ledger_dr =$request->ledger_dr;
            $product->ledger_cr =$request->ledger_cr;
            $product->amount_dr =$request->amount_dr;
            $product->amount_cr =$request->amount_cr;
            $product->narration =$request->narration;
            $product->total_amount =$request->total_amount;
            $product->save();
//---------------------------------------------------------------------
            //delete old dr data from ledger

            $prevoise_data_dr=Customer::find($request->previous_dr);
            if($request->previous_dr==2) {
                $previouse_payment = Cash::where('contra_id', '=', $id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();

            }else{
                $previouse_payment= Bank::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }

//-------------------------------------------------------
//     delete old cr data from ledger



            if($request->previous_cr==2){
                $previouse_payment= Cash::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();

            }else{
                $previouse_payment= Bank::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }

//------------------------------------------------------------

//enrty new fresh updated data into ledgers


            //       dr code entry here
            if($request->ledger_dr==2) {
                $ledgerdr = new Cash();
                $ledgerdr->unique_id = $request->journal_no;
                $ledgerdr->date = date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->ledger_cr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount = $request->amount_dr;
                $ledgerdr->extra = "+";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->save();

            }else{
                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->journal_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id =  $request->ledger_cr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount =$request->amount_cr;
                $ledgerdr->extra ="+";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->bank_id =$request->ledger_dr;
                $ledgerdr->save();
            }

//        Cr entry details here

            if($request->ledger_cr==2){
                $ledgerdr=new Cash();
                $ledgerdr->unique_id=$request->journal_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id = $request->ledger_dr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount =$request->amount_cr;
                $ledgerdr->extra ="-";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->save();

            }else{
                $ledgerdr=new Bank();
                $ledgerdr->unique_id=$request->journal_no;
                $ledgerdr->date =date($payment_year . '-' . $payment_month . '-' . $payment_day);
                $ledgerdr->customer_id =  $request->ledger_dr;
                $ledgerdr->type = "Contra";
                $ledgerdr->amount =$request->amount_cr;
                $ledgerdr->extra ="-";
                $ledgerdr->contra_id = $product->id;
                $ledgerdr->bank_id =$request->ledger_cr;
                $ledgerdr->save();
            }


            DB::commit();

            Session::flash('flash_message', 'Other Charge has successfully updated!');

//        return redirect()->route('product');
            return redirect()->route('otherCharge');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

    public function printJournal($id){
        $editProduct=Other_charge::find($id);
        return View::make('journal/printJournal')->with('printBill',$editProduct);
    }


    public function deleteOtherCharge($id)
    {

        try{
            DB::beginTransaction();
            $item= new Other_charge();
            $product=Other_charge::find($id);

            if($product->ledger_dr==2){
                $previouse_payment= Cash::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }else{
                $previouse_payment= Bank::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }

//-------------------------------------------------------
//     delete old cr data from ledger

            if($product->ledger_cr==2){
                $previouse_payment= Cash::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }else{
                $previouse_payment= Bank::where('contra_id','=',$id)->first();
                $previouse_payment::find($previouse_payment->id)->delete();
            }
//--------------------------------------------------------------------
            $item::find($id)->delete();



            DB::commit();
            Session::flash('flash_message', 'Selected Journal successfully Deleted!');


            return redirect()->route('otherCharge');


        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }



    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }


}

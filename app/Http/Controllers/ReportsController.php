<?php

namespace App\Http\Controllers;

use App\Challan;
use App\Challan_child;
use App\Customer;
use App\Receipt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use App\Brand;
use App\Sale;
use App\Sale_invoice;
use App\Sale_return;
use App\Sale_return_invoice;
use App\Purchase;
use App\Purchase_invoice;
use App\Purchase_return;
use App\Purchase_return_invoice;
use App\Creditnote;
use App\Debitnote;
use App\Product;
use App\Cash;
use App\Ledgerdr;
use App\Ledgercr;
use Session;

class ReportsController extends Controller
{

    public function reportIndex()
    {
        return View::make('report/viewReportIndex');
    }
public function customer_ledger(Request $request)
{
    $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
    $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));
    $customer = Customer::find($request->customer_id);
    if($customer->ledger_group==1){
        return View::make('report/customer_ledgerAssets')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer_id',$request->customer_id);
    }else{
        return View::make('report/customer_ledgerLiabilities')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer_id',$request->customer_id);
    }

}

    public function supplier_report(Request $request)
{
    $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
    $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));
    return View::make('report/supplier_ledger')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer_id',$request->customer_id);
}


    public function gst_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/gst_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function sale_day_book(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/sale_day')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function purchase_day_book(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/purchase_day')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }



    public function payment_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/payment_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function receipt_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/receipt_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function sale_return_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/sale_return_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function purchase_return_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/purchase_return_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function cash_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/cash_ledger')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function bank_report(Request $request)
    {
//        $data = Input::all();
//        print_r($data);
//        die();

        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        if($request->customer_id=='all'){
            return View::make('report/bank_ledger')->with('from_date',$from_date)->with('upto_date',$upto_date);

        }else{
            return View::make('report/single_bank_ledger')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer_id',$request->customer_id);
        }


    }


    public function creditnote_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/creditnote_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function debitnote_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/debitnote_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function stock_report(Request $request){
//        $data = Input::all();
//        print_r($data);
//        die();
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        return View::make('report/stock_report')->with('from_date',$from_date)->with('product_id',$request->product_id);
    }

    public function current_stock_report(Request $request){
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        return View::make('report/current_stock_report')->with('from_date',$from_date);
    }


    public function contra_report(Request $request)
    {
        $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
        $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));

        return View::make('report/contra_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function day_report()
    {
        return View::make('report/day_report/index');
    }


    public function showDayReport(Request $request)
    {
//        $data = Input::all();
//        print_r($data);
//        die();
//        1 stands for daily
//        2 stands for monthly
//        3 stands for custom

        if($request->report_type==3){

            $from_date= date($request->input('from_year').'-'.$request->input('from_month').'-'.$request->input('from_day'));
            $upto_date= date($request->input('upto_year').'-'.$request->input('upto_month').'-'.$request->input('upto_day'));
            $msg = 'Custom Day Report';
            return View::make('report/day_report/custom_report')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('msg',$msg);

        }elseif($request->report_type==2){


            return View::make('report/day_report/monthly_report');


    }else{
            $from_date= date("Y-m-d");
            $upto_date= date("Y-m-d");
            $msg = 'Current Day Report';
            return View::make('report/day_report/custom_report')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('msg',$msg);
    }


    }


    public function custom_sale_report($from_date,$upto_date){
        return View::make('report/sale_day')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function custom_purchase_report($from_date,$upto_date){
        return View::make('report/purchase_day')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function custom_purchaseReturn_report($from_date,$upto_date){
        return View::make('report/purchase_return_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function custom_saleReturn_report($from_date,$upto_date){
        return View::make('report/sale_return_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


    public function custom_receipt_report($from_date,$upto_date){
        return View::make('report/receipt_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function custom_payment_report($from_date,$upto_date){
        return View::make('report/payment_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }
    public function custom_creditNote_report($from_date,$upto_date){
        return View::make('report/creditnote_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }
    public function custom_debitNote_report($from_date,$upto_date){
        return View::make('report/debitnote_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }
    public function custom_contra_report($from_date,$upto_date){
        return View::make('report/contra_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }




    public function monthly_sale_report($month){
     return View::make('report/day_report/monthly_report/sale_day')->with('i',$month);
    }

public function monthly_purchase_report($month){
    return View::make('report/day_report/monthly_report/purchase_day')->with('i',$month);

    }
public function monthly_saleReturn_report($month){

    return View::make('report/day_report/monthly_report/sale_return_report')->with('i',$month);
    }
public function monthly_purchaseReturn_report($month){
    return View::make('report/day_report/monthly_report/purchase_return_report')->with('i',$month);
    }
public function monthly_receipt_report($month){
    return View::make('report/day_report/monthly_report/receipt_report')->with('i',$month);
    }
public function monthly_payment_report($month){
    return View::make('report/day_report/monthly_report/payment_report')->with('i',$month);
    }
public function monthly_creditNote_report($month){
    return View::make('report/day_report/monthly_report/creditnote_report')->with('i',$month);
    }
public function monthly_debitNote_report($month){
    return View::make('report/day_report/monthly_report/debitnote_report')->with('i',$month);
    }
public function monthly_contra_report($month){
    return View::make('report/day_report/monthly_report/contra_report')->with('i',$month);
    }

//------------------------------- trial Balance ------------------------------

    public function trial_balnace($from_date,$upto_date){
        return View::make('report/trial/trial_balance')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }


//   Indirect Expenses
    public function indirect_exp_report($from_date,$upto_date){
        return View::make('report/trial/indirectExp/indirect_exp_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function detail_indirect_exp($from_date,$upto_date,$id){
        return View::make('report/trial/indirectExp/detail_indirect_exp')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer',$id);
    }

//    ------------- direct Exp
    public function direct_exp_report($from_date,$upto_date){
        return View::make('report/trial/directExp/direct_exp_report')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function detail_direct_exp($from_date,$upto_date,$id){
        return View::make('report/trial/directExp/detail_direct_exp')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer',$id);
    }


//    ------------- Current Assets Report
    public function current_assets_report($from_date,$upto_date){
        return View::make('report/trial/CurrentAssets/current_assets')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

    public function detail_current_assets($from_date,$upto_date,$id){
        return View::make('report/trial/CurrentAssets/detail_current_assets')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer',$id);
    }



//    ------------- Current Liablities Report
    public function current_liablities($from_date,$upto_date){
        return View::make('report/trial/CurrentLiablities/current_liablities')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }

//    public function detail_direct_exp($from_date,$upto_date,$id){
//        return View::make('report/trial/directExp/detail_direct_exp')->with('from_date',$from_date)->with('upto_date',$upto_date)->with('customer',$id);
//    }

//  Profit and loss account

    public function pl_account($from_date,$upto_date){
        return View::make('report/pl_account/pl_account')->with('from_date',$from_date)->with('upto_date',$upto_date);
    }



    public function challan_date_issue(){

    $challan_data=Challan::all();
        foreach($challan_data as $challan_data){
            $challan_child_data=Challan_child::where('challan_id','=',$challan_data->id)->get();
            foreach($challan_child_data as $challan_child_data){
                $challan_child_data->track_date=$challan_data->challan_date;
                $challan_child_data->save();
            }
        }
    }



}

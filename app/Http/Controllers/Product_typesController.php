<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use App\Product_type;
use Session;

class Product_typesController extends Controller
{

    public function viewProduct_type()
    {
        $viewProduct_type= Product_type::orderBy('id', 'desc')->get();
        return View::make('product_type/viewProduct_type')->with('viewProduct_type',$viewProduct_type);
    }


    public function createProduct_type()
    {
      return View::make('product_type/createProduct_type');
    }



    public function storeProduct_type(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required|unique:product_types|max:40',


        ]);

       $product= new Product_type;
        $product->product_name = $request->product_name;
     $product->save();


        Session::flash('flash_message', 'New Brand successfully added!');

        return redirect()->route('product_type');
    }

    public function editProduct_type($id)
    {
        $editProduct_type=Product_type::find($id);
        return View::make('product_type/editProduct_type')->with('editProduct_type',$editProduct_type);
    }


    public function updateProdcut_type(Request $request, $id)
    {

         $item= new Product_type;
         $item = Product_type::find($id);
        $item->product_name=$request->product_name;
        $item->save();
        Session::flash('flash_message', 'Product Category successfully Updated!');

        return redirect()->route('product_type');

    }


    public function deleteProduct_type($id)
    {
        try{
            $item= new Product_type;
            $item::find($id)->delete();


            \Session::flash('flash_message', 'Selected Item successfully Deleted!');
        }
        catch (QueryException $e){
            if($e->getCode() == "23000"){
                \Session::flash('flash_message1', "You can't delete this item ! Plz delete all  related Product/invoice before delete this Item!");
            }
        }

        return redirect()->route('product_type');
    }
}

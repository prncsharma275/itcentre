<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\CustomerLogin;
use Illuminate\Support\Facades\Input;
use Monolog\Handler\NullHandlerTest;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;


class CustomerHomeController extends Controller
{
    public function __construct(){
//        $this->middleware('customer');
    }

    public function index()
    {
        if (Auth::guard('customer')->check()){
            return View('Customer-home');
        }else{
         return  redirect('/');
        }



    }

    public function customerRegister()
    {
        if (Auth::guard('customer')->check()){
            return redirect('CustomerHome');
        }
        return View('CustomerAuth/register');
    }


    public function storeCustomerFront(Request $request)
    {
        $this->validate($request, [
            'customer_name' => 'required|Max:40',
            'address' => 'required|Max:200',
            'zip' => 'required|Max:8',
            'state' => 'required',
            'city' => 'required',
            'mobile' => 'required|Max:12|unique:customers',
            'email'  => 'Required|Between:3,64|unique:customers',
            'password'     => 'Required|Between:6,12',
        ]);

        $customer= new CustomerLogin();
        $customer->customer_name = $request->customer_name;
        $customer->unique_id = "12354";
        $customer->address = $request->address;
        $customer->zip = $request->zip;
        $customer->city = $request->city;
        $customer->state = $request->state;
        $customer->email = $request->email;
        $customer->mobile = $request->mobile;
        $customer->password = Hash::make($request->password);
        $customer->save();

        Session::flash('flash_message', 'New Customer has successfully added!');

        return redirect('customerLogin');
    }


    public function editCustomerFront()
    {
      $id= Auth::guard('customer')->user()->id;
        $customer=CustomerLogin::find($id);
        return View::make('customers/editCustomerFront')->with('customer',$customer);
    }

    public function updateCustomerFront(Request $request, $id)
{
    $this->validate($request, [
        'customer_name' => 'required|Max:40',
        'address' => 'required|Max:200',
        'zip' => 'required|Max:8',
        'state' => 'required',
        'city' => 'required',
        'mobile' => 'required|Max:12',
        'email'  => 'Required|Between:3,64|email',
        'password'     => 'Required|Min:6',
    ]);
    $data = Input::all();
//    print_r($data);
//    die();
    $customer = CustomerLogin::find($id);
    $customer->address = $request->address;
    $customer->zip =  $request->zip;
    $customer->city =  $request->city;
    $customer->mobile =  $request->mobile;
    $customer->state =  $request->state;
    $customer->email =  $request->email;

    $customer->password = Hash::make($request->password);
    $customer->save();

    Session::flash('flash_message', ' successfully Updated!');


        return redirect()->action('CustomerHomeController@index');

}

}

<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Sale_return_output_tax;
use App\Sale;
use App\Sale_invoice;
use App\Sale_bill_session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Session;
use Illuminate\Database\QueryException;
use App\Sale_return;
use App\Sale_return_invoice;
use App\Sale_return_temp;
use App\Sale_return_invoice_temp;
use App\Sale_return_bill_session;
use App\Product;
use App\Exp_table;
use DateTime;
use App\Ledgerdr;



class Sale_returnsController extends Controller
{
    public function saleReturnIndex()
    {

        $saleIndex = Sale_return::orderBy('id', 'desc')->get();
        return View::make('salesReturn/saleReturnIndex')->with('saleIndex', $saleIndex);
    }


//    single view sale return
    public function singleViewSaleReturn($id){
        $viewSaleReturn=Sale_return::find($id);
        return View::make('salesReturn/singleViewReturn')->with('editSale',$viewSaleReturn);
    }

    public function printSaleReturn($id){

        $printBill=Sale_return::find($id);
        return View::make('salesReturn/printSalerReturn')->with('printBill',$printBill);
    }

    public function searchSaleToReturn(Request $request)
    {

            $sale_id = $request->input('sale_id');
        $viewSale = Sale::find($sale_id);

// -------------- DELETE PREVIOUSE SALE RETURN TEMP TABLE DATA -------------

        $flight1 = Sale_return_temp::where('sale_id', $sale_id)->first();
        if ($flight1 != null) {

            $flight1->delete();
        }
//------------- SENDING SALES TABLE DATA TO TEMP SALE RETURN TABLE -------------
        $sale = New Sale_return_temp();
        $sale->unique_id = $viewSale->unique_id;
        $sale->sale_id = $viewSale->id;
        $sale->billing_date = $viewSale->billing_date;
        $sale->customer_id = $viewSale->customer_id;
        $sale->total_amount_without_anything = $viewSale->total_amount_without_anything;
        $sale->total_discount = $viewSale->total_discount;
        $sale->total_taxable_value = $viewSale->total_taxable_value;
        $sale->tax_type = $viewSale->tax_type;
        $sale->total_cgst = $viewSale->total_cgst;
        $sale->total_igst = $viewSale->total_igst;
        $sale->total_sgst = $viewSale->total_sgst;
        $sale->total_tax_amount = $viewSale->total_tax_amount;
        $sale->gross_total = $viewSale->gross_total;
        $sale->grand_total = $viewSale->grand_total;
        $sale->customer_type = $viewSale->customer_type;
        $sale->address = $viewSale->address;
        $sale->city = $viewSale->city;
        $sale->mobile = $viewSale->mobile;
        $sale->email = $viewSale->email;
        $sale->retail_name = $viewSale->retail_name;
        $sale->payment_type = $viewSale->payment_type;
        $sale->payment_desc = $viewSale->payment_desc;
        $sale->save();

// ------- SENDING SALE INVOICE DATA TO SALE RETURN TEMP INVOICE TABLE ----------

        $sale_invoice = Sale_invoice::where('sale_id', '=', $viewSale->id)->get();

        $temp_id = $sale->id;
        foreach ($sale_invoice as $invoice) {
            $flight = New Sale_return_invoice_temp();
            $flight->temp_id = $temp_id;
            $flight->sale_id = $invoice->sale_id;
            $flight->sale_invoice_no = $invoice->id;
            $flight->product_id = $invoice->product_id;
            $flight->description = $invoice->description;
            $flight->mrp = $invoice->mrp;
            $flight->quantity = $invoice->quantity;
            $flight->rate = $invoice->rate;
            $flight->amount_before_tax = $invoice->amount_before_tax;
            $flight->discount = $invoice->discount;
            $flight->taxable_amount = $invoice->taxable_amount;
            if ($viewSale->tax_type == "CGST&SGST") {
                $flight->sgst_tax = $invoice->sgst_tax;
                $flight->cgst_tax = $invoice->cgst_tax;
            } else {
                $flight->igst_tax = $invoice->igst_tax;
            }
            $flight->tax_amount = $invoice->tax_amount;
            $flight->total_amount = $invoice->total_amount;
            $flight->save();


        }

            $temp1Sale = Sale_return_temp::where('sale_id', '=', $viewSale->id)->first();
//
            return View::make('salesReturn/showSearch')->with('temp1Sale', $temp1Sale);

    }

    public function saleDetailsToReturn($id){
        $tempSale=Sale_return_temp::find($id);
        return View::make('salesReturn/createSaleReturn')->with('tempSale', $tempSale);

    }
//    delete invoice by ajax
    public function saleReturn_invoiceAjax($id){

        $item= new Sale_return_invoice_temp();
        $item::find($id)->delete();

    }
//    store in sale retrun
    public function storeSaleReturn(Request $request){
//        $data2 = Input::all();
//        print_r($data2);
//        die();
        $input = $request->input('rows');
        $billing_date = explode('/', $request->input('sale_return_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


        try{
            DB::beginTransaction();


            $sale = New Sale_return();
            $sale->sale_return_no = $request->input('sale_return_no');
            $sale->sale_return_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
            $sale->against_sale_no = $request->input('against_sale_no');
            $sale->sale_id = $request->input('sale_id');
            $sale->tax_type = $request->input('tax_type');
            $sale->customer_id =$request->input('customer');
            $sale->total_amount_without_anything = $request->input('total_amount_input');
            $sale->total_discount = $request->input('total_discount_input');
            $sale->total_taxble_value = $request->input('total_taxble_input');
            if ($request->input('tax_type') == "CGST&SGST") {
                $sale->total_cgst =$request->input('total_gst_input') / 2;
                $sale->total_sgst = $request->input('total_gst_input') / 2;
            } else {
                $sale->total_igst = $request->input('total_gst_input');
            }
            $sale->total_tax = $request->input('total_gst_input');
            $sale->total_sub = $request->input('total_sub_input');
            $sale->grand_total = $request->input('grand_total_input');
            $sale->grand_total_word =$this->amount_in_words($request->input('grand_total_input'));
            $sale->save();

//        **************** store in ledger ********************
            $ledgerdr=new Ledgerdr();
            $ledgerdr->unique_id= $request->input('sale_return_no');
            $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
            $ledgerdr->customer_id = $request->input('customer');
            $ledgerdr->type = "sale Return";
            $ledgerdr->amount =$request->input('grand_total_input') ;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==2) {
                $ledgerdr->extra = "+";
            } else{
                $ledgerdr->extra = "-";
            }
            $ledgerdr->sale_return_id =$sale->id;
            $ledgerdr->save();
//        **************** store in ledger ********************

            foreach ($input as $row) {
                $flight = New Sale_return_invoice();
                $flight->sale_return_id = $sale->id;
                $flight->sale_id = $request->input('sale_id');
                $flight->sale_invoice_no =$row['sale_invoice_id'];
                $flight->product_id = $row['product'];
                $flight->description =$row['desc'];
                $flight->quantity = $row['qty'];
                $flight->rate = $row['rate'];
                $flight->amount_before_tax = $row['amount'];
                $flight->discount = $row['disc'];
                $flight->taxable_amount = $row['taxbl_amount'];
                if ($request->input('tax_type') == "CGST&SGST") {
                    $flight->sgst_tax = $row['gst'] / 2;
                    $flight->cgst_tax = $row['gst'] / 2;
                } else {
                    $flight->igst_tax = $row['gst'];
                }
                $flight->tax_amount = $row['text_amount'];
                $flight->total_amount = $row['price'];
                $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $flight->save();
            }

//        bill no increment every time
            $session_id=$request->input('session_id');
            $sale_session=sale_return_bill_session::find($session_id);
            $sale_session->bill=$sale_session->bill+1;
            $sale_session->save();



            DB::commit();
            return redirect()->route('printSaleReturn', [$sale->id]);

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }

//    delete full sale return
   public function deleteSaleReturn($id){


       try{
           DB::beginTransaction();

           $saleDelete= Sale_return::find($id);

//  ********************** delete from ledger ***********************
           $ledgerdr= Ledgerdr::where('sale_return_id','=',$id)->first();
           $ledgerdr::find($ledgerdr->id)->delete();
//  ********************** delete from ledger ***********************

           $saleDelete::find($id)->delete();

           Session::flash('flash_message', 'Selected Invoice  successfully Canceled !');



           DB::commit();
           return redirect()->route('saleReturn');

       } catch(\Exception $e){
           DB::rollback();
           Session::flash('flash_message', 'Network Problem Please try again');
           return redirect()->back();
//echo $e->getMessage();
       }


   }



    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }
}

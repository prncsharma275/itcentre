<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Ledgerdr;
use App\Purchase;
use App\Purchase_invoice;
use App\Purchase_bill_session;
use App\Purchase_return_input_tax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Illuminate\Database\QueryException;
use Session;
use App\Purchase_return;
use App\Purchase_return_invoice;
use App\Purchase_return_temp;
use App\Purchase_return_invoice_temp;
use App\Purchase_return_bill_session;
use App\Product;
use App\Exp_table;
use App\Ledgercr;

use DateTime;


class Purchase_returnController extends Controller
{

    public function purchaseReturnIndex()
    {

        $saleIndex = Purchase_return::orderBy('id', 'desc')->get();
        return View::make('purchaseReturn/purchaseReturnIndex')->with('saleIndex', $saleIndex);
    }


//    single view sale return
    public function singleViewPurchaseReturn($id){
        $viewSaleReturn=Purchase_return::find($id);
        return View::make('purchaseReturn/singleViewReturn')->with('editSale',$viewSaleReturn);
    }

    public function printPurchaseReturn($id){

        $printBill=Purchase_return::find($id);
        return View::make('purchaseReturn/printpPurchaseReturn')->with('printBill',$printBill);
    }

    public function searchPurchaseToReturn(Request $request)
    {

//        if($request->input('searchButton')) {
        $purchase_id = $request->input('purchase_id');
        $viewSale = Purchase::find($purchase_id);
//print_r($viewSale);
//        die();


//        delete pervious data from temp table

        $flight1 = Purchase_return_temp::where('purchase_id', $purchase_id)->first();
        if ($flight1 != null) {

            $flight1->delete();
        }
//        ************* ----------- **********
        $sale = New Purchase_return_temp();
        $sale->invoice_no = $viewSale->invoice_no;
        $sale->order_no = $viewSale->order_no;
        $sale->order_date = $viewSale->order_date;
        $sale->billing_date = $viewSale->billing_date;
        $sale->supplier_id = $viewSale->supplier_id;
        $sale->party_invoice = $viewSale->party_invoice;
        $sale->party_invoice_date = $viewSale->party_invoice_date;
        $sale->purchase_id = $viewSale->id;
        $sale->amount_without_anything = $viewSale->amount_without_anything;
        $sale->total_taxble_value = $viewSale->total_taxable_amount;
        $sale->total_cgst = $viewSale->total_cgst;
        $sale->total_sgst = $viewSale->total_sgst;
        $sale->total_igst = $viewSale->total_igst;
        $sale->tax_type = $viewSale->tax_type;
        $sale->total_tax_amount = $viewSale->total_tax_amount;
        $sale->total_discount = $viewSale->total_discount;
        $sale->gross_total = $viewSale->gross_total;
        $sale->grand_total = $viewSale->grand_total;
        $sale->save();
        $sale_invoice = Purchase_invoice::where('purchase_id', '=', $viewSale->id)->get();
//        $temp_id=$temptable['temp_id'];

        foreach ($sale_invoice as $invoice) {
            $flight = New Purchase_return_invoice_temp();
            $flight->temp_id = $sale->id;
            $flight->purchase_id = $invoice->purchase_id;
            $flight->product_id = $invoice->product_id;
            $flight->purchase_invoice_id = $invoice->id;
            $flight->desc = $invoice->desc;
            $flight->quantity = $invoice->quantity;
            $flight->rate = $invoice->rate;
            $flight->amount_before_tax = $invoice->amount_before_tax;
            $flight->discount_amt = $invoice->discount_amt;
            $flight->taxable_amount = $invoice->taxable_amount;
            if ($viewSale->tax_type == "CGST&SGST") {
                $flight->sgst_tax = $invoice->sgst_tax;
                $flight->cgst_tax = $invoice->cgst_tax;
            } else {
                $flight->igst_tax = $invoice->igst_tax;
            }
            $flight->tax_amount = $invoice->tax_amount;
            $flight->total_amount = $invoice->total_amount;
            $flight->save();


        }

        $temp1Sale = Purchase_return_temp::where('purchase_id', '=', $viewSale->id)->first();
//
        return View::make('purchaseReturn/showSearch')->with('temp1Sale', $temp1Sale);

    }

    public function purchaseDetailsToReturn($id){
        $tempSale=Purchase_return_temp::find($id);
        return View::make('purchaseReturn/createPurchaseReturn')->with('tempSale', $tempSale);

    }
//    delete invoice by ajax
    public function purchaseReturn_invoiceAjax($id){

        $item= new Purchase_return_invoice_temp();
        $item::find($id)->delete();

    }
//    store in sale retrun
    public function storePurchaseReturn(Request $request){
//        $data2 = Input::all();
//        print_r($data2);
//        die();
        $input = $request->input('rows');
        $billing_date = explode('/', $request->input('sale_return_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];



        try{
            DB::beginTransaction();


            $sale = New Purchase_return();
            $sale->invoice_no = $request->input('purchase_return_no');
            $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
            $sale->against_purchase_no = $request->input('against_sale_no');
            $sale->purchase_id = $request->input('purchase_id');
            $sale->supplier_id =$request->input('customer');
            $sale->amount_without_anything = $request->input('total_amount_input');
            $sale->total_discount = $request->input('total_discount_input');
            $sale->total_taxble_value = $request->input('total_taxble_input');
            if ($request->input('tax_type') == "CGST&SGST") {
                $sale->total_cgst =$request->input('total_gst_input') / 2;
                $sale->total_sgst = $request->input('total_gst_input') / 2;
            } else {
                $sale->total_igst = $request->input('total_gst_input');
            }
            $sale->tax_type = $request->input('tax_type');
            $sale->total_tax_amount = $request->input('total_gst_input');
            $sale->gross_total = $request->input('total_sub_input');
            $sale->grand_total = $request->input('grand_total_input');
            $sale->grand_total_word =$this->amount_in_words($request->input('grand_total_input'));
            $sale->save();


//        **************** store in ledger ********************
            $ledgerdr=new Ledgerdr();
            $ledgerdr->unique_id= $request->input('purchase_return_no');
            $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
            $ledgerdr->customer_id = $request->input('customer');
            $ledgerdr->type = "purchase Return";
            $ledgerdr->amount =$request->input('grand_total_input') ;
            $myledger = Customer::find($request->customer);
            if($myledger->ledger_group==1) {
                $ledgerdr->extra = "+";
            } else{
                $ledgerdr->extra = "-";
                }

            $ledgerdr->purchase_return_id =$sale->id;
            $ledgerdr->save();
//        **************** store in ledger ********************

            foreach ($input as $row) {
                $flight = New Purchase_return_invoice();
                $flight->purchase_id = $request->input('purchase_id');
                $flight->purchase_invoice_id = $row['purchase_invoice_id'];
                $flight->purchase_return_id = $sale->id;
                $flight->product_id = $row['product'];
                $flight->desc = $row['desc'];
                $flight->quantity = $row['qty'];
                $flight->rate = $row['rate'];
                $flight->amount_before_tax = $row['amount'];
                $flight->discount_amt = $row['disc'];
                $flight->taxable_amount = $row['taxbl_amount'];
                if ($request->input('tax_type') == "CGST&SGST") {
                    $flight->sgst_tax = $row['gst'] / 2;
                    $flight->cgst_tax = $row['gst'] / 2;
                } else {
                    $flight->igst_tax = $row['gst'];
                }
                $flight->tax_amount = $row['text_amount'];
                $flight->total_amount = $row['price'];
                $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $flight->save();
            }

//        bill no increment every time

            $sale_session=Purchase_return_bill_session::find($request->input('session_id'));
            $sale_session->bill=$sale_session->bill+1;
            $sale_session->save();




            DB::commit();

            return redirect()->route('printPurchaseReturn', [$sale->id]);

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }




    }

//    delete full sale return
    public function deletePurchaseReturn($id){


        try{
            DB::beginTransaction();


            $saleDelete= Purchase_return::find($id);

            $sale_invoice= Purchase_return_invoice::where('purchase_return_id', '=', $saleDelete->id)->get();

            //  ********************** delete from ledger ***********************
            $ledgerdr= Ledgerdr::where('purchase_return_id','=',$id)->first();
            $ledgerdr::find($ledgerdr->id)->delete();
//  ********************** delete from ledger ***********************
            $saleDelete::find($id)->delete();

            Session::flash('flash_message', 'Selected Invoice  successfully Canceled !');




            DB::commit();

            return redirect()->route('purchaseReturn');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }



    }


    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }
}

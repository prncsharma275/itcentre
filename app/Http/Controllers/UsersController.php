<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::all ();
        return  View::make( 'admin/index')->withData( $data );
    }

    public function singleUser($id)
    {
        $viewUser=User::find($id);
        return View::make('admin/singleUser')->with('viewUser',$viewUser);
    }

    public function register()
    {
        return  View::make( 'admin/createUser');
    }


    public function storeUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email'  => 'Required|email|Between:3,64|unique:users',
            'role'  => 'Required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = new User();

        $user->name = $request->name;
        $user->employee_code = $request->employee_code;
        $user->doj = $request->doj;
        $user->adhar_no = $request->adhar_no;
        $user->add1 = $request->add1;
        $user->add2 = $request->add2;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->phone = $request->phone;
        $user->mobile_1 = $request->mobile_1;
        $user->mobile_2 = $request->mobile_2;
        $user->pan_no = $request->pan_no;
        $user->dob = $request->dob;
        $user->account_no = $request->account_no;
        $user->bank_name = $request->bank_name;
        $user->branch_name = $request->branch_name;
        $user->ifsc_code = $request->ifsc_code;
        $user->password = Hash::make ($request->password);


        $user->save();

        Session::flash('flash_message', 'New user has successfully added!');

        return redirect()->route('users');
    }



    public function editUser($id)
    {
        $viewUser=User::find($id);
        return View::make('admin/editUser')->with('viewUser',$viewUser);
    }


    public function updateUser(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email'  => 'Required|email|Between:3,64',
            'role'  => 'Required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);
        $user= User::find($id);
        $user->name = $request->name;
        $user->employee_code = $request->employee_code;
        $user->doj = $request->doj;
        $user->adhar_no = $request->adhar_no;
        $user->add1 = $request->add1;
        $user->add2 = $request->add2;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->phone = $request->phone;
        $user->mobile_1 = $request->mobile_1;
        $user->mobile_2 = $request->mobile_2;
        $user->pan_no = $request->pan_no;
        $user->dob = $request->dob;
        $user->account_no = $request->account_no;
        $user->bank_name = $request->bank_name;
        $user->branch_name = $request->branch_name;
        $user->ifsc_code = $request->ifsc_code;
        if($request->passwordold!=$request->password){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        Session::flash('flash_message', 'Customer successfully Updated!');


        return redirect()->route('users');
    }


    public function deleteUser($id)
    {
        $item= new User();
        $item::find($id)->delete();

        Session::flash('flash_message', 'Selected Customer successfully Deleted!');


        return redirect()->route('users');
    }
}

<?php

namespace App\Http\Controllers;

use App\Challan;
use App\Challan_child;
use App\Challan_session;
use App\Customer;
use App\Ledgerdr;
use App\Product;
use App\Sale;
use App\Sale_bill_session;
use App\Sale_return_invoice;
use App\chalan_return;
use App\chalan_return_child;
use App\chalan_return_session;
use App\Sale_invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class ChallanController extends Controller
{
    public function challan()
    {

        return View::make('challan/challanindex');

    }
    public function new_challan_entry()
    {

        return View::make('challan/new_challan_entry');

    }
    public function printChallan($id)
    {
          $get_challan=Challan::find($id);

        return View::make('challan/printchallan')->with('challan',$get_challan);

    }
    public function allposts_challan(Request $request)
    {

        $columns = array(

            0 =>'challan_no',
            1=> 'challan_date',
            2=> 'customer_id',
            3=> 'return_no',
            4=> 'bill_no ',
            5=> 'action',
        );

        $totalData = Challan::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Challan::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Challan::where('challan_no','LIKE',"%{$search}%")
                ->orWhere('challan_date', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Challan::where('challan_no','LIKE',"%{$search}%")
                ->orWhere('challan_date', 'LIKE',"%{$search}%")
                ->orWhereHas('belongsToCustomer', function($q) use ($search){
                    $q->where('ledger_name', 'LIKE', "%{$search}%");
                })
                ->count();
        }


        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
              $return_data=chalan_return::where('against_challan_no','=',$post->challan_no)->first();
              $bill_no=Sale::where('challan_no','=',$post->challan_no)->first();
                $customer =Customer::find($post->customer_id);
                $edit =  route('editChallan',$post->id);
                if($post->customer_id!='1'){
                    $print =  route('printChallan',$post->id);
                }else{
                    $print =  route('printChallan',$post->id);
                }
                $sale_bill =  route('saleChallan',$post->id);
                $delete =  route('deleteChallan',$post->id);

                if($post->status=='cancel'){
                    $nestedData['challan_no'] = '<del style="color: red">'.$post->challan_no.'</del>';
                    $nestedData['challan_date'] = date('d-m-Y',strtotime($post->challan_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    if(!empty($return_data)){
                        $nestedData['return_no'] = $return_data->invoice_no;
                    }else{
                        $nestedData['return_no'] = "-";
                    }
                    if(!empty($bill_no)){
                        $nestedData['bill_no'] = $bill_no->unique_id;
                    }else{
                        $nestedData['bill_no'] = "-";
                    }

                    $nestedData['action'] = "<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default disabled' ><i class='splashy-document_letter_edit'></i></a>
                                          <a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          <a href='{$sale_bill}' title='SALE' target='_blank' class='btn btn-sm btn-default disabled' ><i class='splashy-tag_add'></i></a>
                                          <a href='{$delete}' title='Cancel' class='btn btn-sm btn-default disabled' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }elseif(!empty($bill_no)){
                    $print_sale_challan=Sale::where('challan_no','=',$post->challan_no)->first();
                    $printChallanSale=route('printnewsaleInvoice',$print_sale_challan->id);
                    $nestedData['challan_no'] = $post->challan_no;
                    $nestedData['challan_date'] = date('d-m-Y',strtotime($post->challan_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    if(!empty($return_data)){
                        $nestedData['return_no'] = $return_data->invoice_no;
                    }else{
                        $nestedData['return_no'] = "-";
                    }
                    if(!empty($bill_no)){
                        $nestedData['bill_no'] = $bill_no->unique_id;
                    }else{
                        $nestedData['bill_no'] = "-";
                    }
                    $nestedData['action'] = "<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default' ><i class='splashy-document_letter_edit'></i></a>
                                         <a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          <a href='{$printChallanSale}' title='PRINT SALE BILL' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-print_add'></i></a>
                                          <a href='{$delete}' title='Cancel' class='btn btn-sm btn-default' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }else{

                    $nestedData['challan_no'] = $post->challan_no;
                    $nestedData['challan_date'] = date('d-m-Y',strtotime($post->challan_date)) ;
                    $nestedData['customer_id'] = $customer->ledger_name;
                    if(!empty($return_data)){
                        $nestedData['return_no'] = $return_data->invoice_no;
                    }else{
                        $nestedData['return_no'] = "-";
                    }
                    if(!empty($bill_no)){
                        $nestedData['bill_no'] = $bill_no->unique_id;
                    }else{
                        $nestedData['bill_no'] = "-";
                    }
                    $nestedData['action'] = "<a href='{$edit}' title='EDIT' class='btn btn-sm btn-default' ><i class='splashy-document_letter_edit'></i></a>
                                         <a href='{$print}' title='PRINT' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-printer'></i></a>
                                          <a href='{$sale_bill}' title='SALE' target='_blank' class='btn btn-sm btn-default' ><i class='splashy-tag_add'></i></a>
                                          <a href='{$delete}' title='Cancel' class='btn btn-sm btn-default' onclick='return ConfirmDelete()' ><i class='splashy-document_letter_remove'></i></a>
                                          ";
                    $data[] = $nestedData;
                }


            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }
    public function challan_return()
    {
      $get=db::table('chalan_return')->get();
    return View::make('challan/returnchallan')->with('editChallan', $get);
    }

    public function storeChallan(Request $request)
    {

        $this->validate($request, [
            'challan_no' => 'required|unique:challan',
        ],
            [
                'challan_no.unique'      => 'Sorry, This challan_no No. Is Already Used ',
            ]);

//        $data = Input::all();
//        print_r($data);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('challan_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


//   ----------------------------tax fields----------------------------------




        $check_duplicate = Challan::where('challan_no',$request->input('challan_no'))->first();
        if(! $check_duplicate) {

            try{
                DB::beginTransaction();
                $challan = new Challan();
                $challan->challan_no = $request->input('challan_no');
                $challan->challan_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $challan->customer_id = $request->input('customer');
                $challan->save();

                //        bill no and session save
                $challan_session = Challan_session::find($session_id);
                $challan_session->bill = $challan_session->bill + 1;
                $challan_session->save();




//       $billNo= Sale::where('id', '=', $sale->id)->first();


                foreach ($input as $row) {

                    $flight = new Challan_child();
                    $flight->challan_id = $challan->id;
                    $flight->product_id = $row['product'];
                    $product_name=Product::find($row['product']);
                    $flight->product_name=$product_name->product_name;
                     $flight->descr = $row['desc'];
                    $flight->quantity = $row['qty'];
                    $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->save();


                }


                DB::commit();
                Session::flash('flash_message', 'New Challan successfully added!');
                return redirect('challan')->with('status', $challan->id)->with('customer',$request->input('customer'));



            } catch(\Exception $e){
                DB::rollback();
                Session::flash('flash_message', 'Network Problem Please try again');
                return redirect()->back();
//echo $e->getMessage();
            }

        }else{
            return redirect()->route('sale');
        }
    }

    public function editChallan($id)
    {
        $editChallan = Challan::find($id);
        return View::make('challan/editChallan')->with('editChallan', $editChallan);
    }
    public function challan_child_deleteAjax($id)
    {

        $item = new Challan_child();
        $item::find($id)->delete();


    }
    public function updateChallan(Request $request, $id)
    {
//                $data2 = Input::all();
//        print_r($data2);
//        die();

        $input = $request->input('rows');


        $billing_date = explode('/', $request->input('challan_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];


//   ----------------------------tax fields----------------------------------



            try{
                DB::beginTransaction();
                $challan = Challan::find($id);
                $challan->challan_no = $request->input('challan_no');
                $challan->challan_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $challan->customer_id = $request->input('customer');
                $challan->save();



//       $billNo= Sale::where('id', '=', $sale->id)->first();


                foreach ($input as $row) {
                    if ($row['sale_invoice'] > 0) {
                        $flight = Challan_child::find($row['sale_invoice']);
                    } else {
                        $flight = new Challan_child();
                    }
                    $flight->challan_id = $challan->id;
                    $flight->product_id = $row['product'];
                    $product_name=Product::find($row['product']);
                    $flight->product_name=$product_name->product_name;
                    $flight->descr = $row['desc'];
                    $flight->quantity = $row['qty'];
                    $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->save();


                }
            DB::commit();


            return redirect('challan')->with('status', $challan->id);



        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
            return redirect()->back();
//echo $e->getMessage();
        }
    }

    public function deleteChallan($id)
    {

        try{
            DB::beginTransaction();
            $saleDelete = Challan::find($id);
            $saleDelete->status = "cancel";
            $saleDelete->save();

            $sale_invoice = Challan_child::where('challan_id', '=', $id)->get();
            foreach ($sale_invoice as $invoice) {
                $invoice->status = "cancel";
                $invoice->save();
            }

            DB::commit();
            return redirect()->route('challan');

        } catch(\Exception $e){
            DB::rollback();
            Session::flash('flash_message', 'Network Problem Please try again');
//            return redirect()->back();
            echo $e->getMessage();
        }





    }

    public function searchchallanToReturn(Request $request)
{
  $id=$request->input('purchase_id');
  $get_challan=Challan::whereid($id)->first();
  //print_r($get_challan);
  return View::make('challan/returnchallan')->with('tempSale', $get_challan);

}
public function challanDetailsToReturn($id)
{

$get_challan=DB::table('challan')->find($id);

return View::make('challan/challanDetailsToReturn')->with('tempSale', $get_challan);
}


public function challanreturn(Request $request)
{
  $input = $request->input('rows');


  // $billing_date = explode('/', $request->input('challan_date'));
  // $billing_day = $billing_date[0];
  // $billing_month = $billing_date[1];
  // $billing_year = $billing_date[2];



          $challan =new chalan_return;
          $challan->challan_id = $request->input('challan_id');
          $challan->challan_date = $request->input('challan_date');
          $challan->against_challan_no = $request->input('challan_no_old');
            $challan->invoice_no = $request->input('challan_no');
          $challan->customer_id = $request->input('customer');
          $challan->save();


            $session_id = $request->input('session_id');

          $challan_session = chalan_return_session::find($session_id);
          $challan_session->bill = $challan_session->bill + 1;
          $challan_session->save();
//       $billNo= Sale::where('id', '=', $sale->id)->first();

foreach ($input as $row) {

    $flight =new chalan_return_child;
    $flight->challan_id = $request->input('challan_no');
    $flight->product_id = $row['product'];
    $product_name=Product::find($row['product']);
    $flight->product_name=$product_name->product_name;
     $flight->descr = $row['desc'];
    $flight->quantity = $row['qty'];
    $flight->track_date = $request->input('challan_date');
    $flight->save();

return View::make('challan/returnchallan');
}
}



public function editreturn($id)
{
    $editChallan = chalan_return::find($id);
    return View::make('challan/editreturnchalan')->with('editChallan', $editChallan);
}
    public function saleChallan($id)
{
    return View::make('challan/salechallan')->with('id', $id);
}



public function printSaleInvoice($id)
{
    $printBill = Sale::find($id);
    return View::make('challan/printchallan')->with('printBill', $printBill);
}
public function printreturn($id)
{
    $printBill = chalan_return::find($id);
    return View::make('challan/printchallanreturn')->with('printBill', $printBill);
}

    public function storeChallanSale(Request $request)
    {

        $this->validate($request, [
            'unique_id' => 'required|unique:sales',
        ],
            [
                'unique_id.unique'      => 'Sorry, This Invoice No. Is Already Used ',
            ]);

//        $data = Input::all();
//        print_r($data);
//        die();
        $input = $request->input('rows');


        $session_id = $request->input('session_id');
        $billing_date = explode('/', $request->input('invoice_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];

        $challan_date = explode('/', $request->input('challan_date'));
        $challan_day = $challan_date[0];
        $challan_month = $challan_date[1];
        $challan_year = $challan_date[2];


//   ----------------------------tax fields----------------------------------


        $tax_type = $request->input('tax_type');
        $total_tax = $request->input('total_gst_input');
        $gross_total = $request->input('total_sub_input');
        $grand_total = $request->input('grand_total_input');

        $check_duplicate = Sale::where('unique_id',$request->input('unique_id'))->first();
        if(! $check_duplicate) {

            try{
                DB::beginTransaction();
                $sale = new Sale();
                $sale->unique_id = $request->input('unique_id');
                $sale->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                $sale->challan_no = $request->input('challan_no');
                $sale->challan_date = date($challan_year . '-' . $challan_month . '-' . $challan_day);
                $sale->customer_id = $request->input('customer');
                $sale->payment_type = $request->input('payment_type');
                $sale->retail_name = $request->input('retail_customer_name');
                $sale->city = $request->input('retail_city');
                $sale->mobile = $request->input('retail_mobile');
                $sale->email = $request->input('retail_email');
                $sale->payment_desc = $request->input('payment_description');
                $sale->total_amount_without_anything = $request->input('total_amount_input');
                $sale->total_discount = $request->input('total_discount_input');
                $sale->total_taxable_value = $request->input('total_taxble_input');
                if ($tax_type == "CGST&SGST") {
                    $sale->total_cgst = $total_tax / 2;
                    $sale->total_sgst = $total_tax / 2;
                } else {
                    $sale->total_igst = $total_tax;
                }
                $sale->tax_type = $tax_type;
                $sale->total_tax_amount = $total_tax;
                $sale->gross_total = $gross_total;
                $sale->postage_charge = $request->input('postage_charge');
                $sale->grand_total = $grand_total;
                $sale->grand_total_word =  $this->amount_in_words($grand_total);
                $sale->save();

                //        bill no and session save
                $sale_session = Sale_bill_session::find($session_id);
                $sale_session->bill = $sale_session->bill + 1;
                $sale_session->save();


//        **************** store in ledger ********************
                if ($request->input('customer') == 1) {

                    $ledgerdr = new Ledgerdr();
                    $ledgerdr->unique_id = $request->input('unique_id');
                    $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $ledgerdr->customer_id = $request->input('customer');
                    $ledgerdr->type = "CashSale";
                    $ledgerdr->amount = $grand_total;
                    $ledgerdr->extra = "+";
                    $ledgerdr->sale_id = $sale->id;
                    $ledgerdr->save();
                    //        **************** store in ledger ********************

                    $cash_ledger = new Cash();
                    $cash_ledger->unique_id = $request->input('unique_id');
                    $cash_ledger->date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $cash_ledger->customer_id = $request->input('customer');
                    $cash_ledger->type = "CashSale";
                    $cash_ledger->amount = $grand_total;
                    $cash_ledger->extra = "+";
                    $cash_ledger->sale_id = $sale->id;
                    $cash_ledger->save();

                } else {
                    $ledgerdr = new Ledgerdr();
                    $ledgerdr->unique_id = $request->input('unique_id');
                    $ledgerdr->billing_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $ledgerdr->customer_id = $request->input('customer');
                    $ledgerdr->type = "CreditSale";
                    $ledgerdr->amount = $grand_total;
                    $myledger = Customer::find($request->customer);
                    if($myledger->ledger_group==2){
                        $ledgerdr->extra ="-";
                    }else{
                        $ledgerdr->extra ="+";
                    }
                    $ledgerdr->sale_id = $sale->id;
                    $ledgerdr->save();
                }


//       $billNo= Sale::where('id', '=', $sale->id)->first();


                foreach ($input as $row) {

                    $flight = new Sale_invoice();
                    $flight->sale_id = $sale->id;
                    $flight->product_id = $row['product'];
                    $flight->description = $row['desc'];
                    $flight->quantity = $row['qty'];
                    $flight->rate = $row['rate'];
                    $flight->amount_before_tax = $row['amount'];
                    $flight->discount_per = $row['disc_percentage'];
                    $flight->discount = $row['disc'];
                    $flight->taxable_amount = $row['taxbl_amount'];
                    if ($tax_type == "CGST&SGST") {
                        $flight->sgst_tax = $row['gst'] / 2;
                        $flight->cgst_tax = $row['gst'] / 2;
                    } else {
                        $flight->igst_tax = $row['gst'];
                    }
                    $flight->tax_amount = $row['text_amount'];
                    $flight->total_amount = $row['price'];
                    $flight->ischallansale = "yes";
                    $flight->track_date = date($billing_year . '-' . $billing_month . '-' . $billing_day);
                    $flight->save();


                }


                DB::commit();
                Session::flash('flash_message', 'New sale successfully added!');
                return redirect('sale')->with('status', $sale->id)->with('customer',$request->input('customer'));



            } catch(\Exception $e){
                DB::rollback();
                Session::flash('flash_message', 'Network Problem Please try again');
                return redirect()->back();
//echo $e->getMessage();
            }
//
        }else{
            return redirect()->route('sale');
        }
    }
    static function amount_in_words($amount){
        $number = $amount;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
            "." . $words[$point / 10] . " " .
            $words[$point = $point % 10] : '';
        return $result . "Rupees  " . $points . "Only";
    }
  }

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Purchase_return_invoice_temp extends Model
{
    protected $table = 'purchase_returns_invoice_temp';

    protected $fillable=array('purchase_id','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','sub_total','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_return_temp extends Model
{
    protected $table = 'purchase_returns_temp';

    protected $fillable=array('purchase_id','supplier_id','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');

    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','supplier_id');
    }
}

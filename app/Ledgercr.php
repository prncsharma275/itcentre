<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledgercr extends Model
{
    protected $table = 'ledgercr';

    protected $fillable=array('unique_id','billing_date','supplier_id','type','amount','balance');
}

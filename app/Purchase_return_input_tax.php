<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_return_input_tax extends Model
{
    protected $table = 'purchase_return_input_tax';

    protected $fillable=array('unique_id','billing_date','customer_id','type','amount','balance');
}

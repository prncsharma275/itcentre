<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale_return_output_tax extends Model
{
    protected $table = 'sale_return_output_tax';

    protected $fillable=array('unique_id','billing_date','customer_id','type','amount','balance');
}

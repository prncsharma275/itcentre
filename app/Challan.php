<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challan extends Model
{
    protected $table = 'challan';


    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','customer_id');
    }
}

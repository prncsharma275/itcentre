<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable=array('brand_name');


    public  function hasManyProduct(){
        return $this->hasMany('App\Product','id');
    }



}

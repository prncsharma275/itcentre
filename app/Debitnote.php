<?php

namespace App;

use Illuminate\Database\Eloquent\Model;




class Debitnote extends Model
{
    protected $table = 'debitnotes';

    protected $fillable=array('supplier_id','date','desc','narration','amount');


    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','supplier_id');
    }


}

<?php

namespace App;
use App\Notifications\CustomerResetPasswordNotification;



class CustomerLogin extends User
{
   protected $table="customers";

   /**
    * Send the password reset notification.
    *
    * @param  string  $token
    * @return void
    */
   public function sendPasswordResetNotification($token)
   {
      $this->notify(new CustomerResetPasswordNotification($token));
   }
}

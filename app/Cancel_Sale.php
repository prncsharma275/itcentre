<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancel_Sale extends Model
{
    protected $table = 'cancel_sales';

    protected $fillable=array('sale_id','customer_id','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');
}

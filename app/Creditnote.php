<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Brand;
use App\Product_type;



class Creditnote extends Model
{
    protected $table = 'creditnotes';

    protected $fillable=array('customer_id','date','desc','narration','amount');


    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','customer_id');
    }

}

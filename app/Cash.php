<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Brand;
use App\Product_type;



class Cash extends Model
{
    protected $table = 'cash';

    protected $fillable=array('date','whom','type','amount');


    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','customer_id');
    }

}

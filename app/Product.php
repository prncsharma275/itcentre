<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Brand;
use App\Product_type;



class Product extends Model
{
    protected $table = 'products';

    protected $fillable=array('brand_id','product_type_id','product_name','product_vat_type','quantity','product_description','productPic',
        'cost_price','selling_price');

    public function belongsToBrand(){
        return $this->belongsTo('App\Brand','brand_name_id');
    }

    public function belongsToProduct_type(){
        return $this->belongsTo('App\Product_type','product_type_id');
    }


    public  function hasManyProduct_Exp(){
        return $this->hasMany('App\Exp_table','id');
    }


}

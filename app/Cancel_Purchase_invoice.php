<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancel_Purchase_invoice extends Model
{
    protected $table = 'cancel_purchase_invoice';

    protected $fillable=array('purchase_id','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','sub_total','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');
}

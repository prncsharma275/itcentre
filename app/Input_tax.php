<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Input_tax extends Model
{
    protected $table = 'input_tax';

    protected $fillable=array('unique_id','billing_date','customer_id','type','amount','balance');
}

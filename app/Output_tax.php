<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Output_tax extends Model
{
    protected $table = 'output_tax';

    protected $fillable=array('unique_id','billing_date','customer_id','type','amount','balance');
}

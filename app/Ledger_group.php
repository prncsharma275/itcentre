<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledger_group extends Model
{
    protected $table = 'ledger_groups';

    protected $fillable=array('id');

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledgerdr extends Model
{
    protected $table = 'ledgerdr';

    protected $fillable=array('unique_id','billing_date','supplier_id','type','amount','balance');
}

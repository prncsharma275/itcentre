<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable=array('unique_id','billing_date','supplier_id','type','amount','payment_type','cheque_no','bank_name','cheque_issue_date','cheque_deposit_date');

    public function belongsToCustomer(){
        return $this->belongsTo('App\Customer','supplier_id');
    }
}

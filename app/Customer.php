<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Brand;
use App\Service;
use App\Purchase;

class Customer extends Authenticatable
{
    protected $table = 'customers';
    use Notifiable;


    protected $fillable=array('id','ledger_name','address','zip','state','city','home_phone','work_phone',
        'mobile','email','credit_limit','vatNo','sms_mobile','proprietor','password');


    protected $hidden = [
        'password', 'remember_token',
    ];

    public  function hasManySale(){
        return $this->hasMany('App\Sale','id');
    }



    public  function hasManyPurchase(){
        return $this->hasMany('App\Purchase','id');
    }
    public  function hasManyChallan(){
        return $this->hasMany('App\Challan','id');
    }


    public  function hasManyService(){
        return $this->hasMany('App\Service','id');
    }
}

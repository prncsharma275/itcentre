<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale_invoice_format extends Model
{
    protected $table = 'sale_invoice_format';

    protected $fillable=array('sale_id','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','sub_total','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledger_type extends Model
{
    protected $table = 'ledger_type';

    protected $fillable=array('id');

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Sale_temp extends Model
{
    protected $table = 'sale_temp';

    protected $fillable=array('sale_no','product_id','serial_number','quantity','rate','discount_percentage','discount_amount',
        'amount','sub_total','vat_rate','vat_amount','discount_percentage_after','discount_amount_after','total_amount');


}

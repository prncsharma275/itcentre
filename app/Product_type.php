<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Service;

class Product_type extends Model
{
    protected $table = 'product_types';

    protected $fillable=array('product_name');


    public  function hasManyProduct(){
        return $this->hasMany('App\Product','id');
    }

    public  function hasManyService(){
        return $this->hasMany('App\Service','id');
    }

}

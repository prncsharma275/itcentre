<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Customer;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', '\App\Http\Controllers\Auth\LoginController@login');
Route::get('login', 'Auth\AuthController@getLogin');

Route::get('/404', function () {
    return view('sales/404');
});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//user routes
Route::get('/users',array('as'=>'users','middleware' => ['admin'], 'uses'=>'UsersController@index'));
Route::get('createUsers',array('as'=>'createUsers','middleware' => ['admin'], 'uses'=>'UsersController@register'));
Route::get('/singleUser/{id}',array('as'=>'singleUser','middleware' => ['admin'], 'uses'=>'UsersController@singleUser'));
Route::Post('storeUser',['as'=>'storeUser','middleware' => ['admin'], 'uses'=>'UsersController@storeUser']);
Route::get('/editUser/{id}',array('as'=>'editUser','middleware' => ['admin'], 'uses'=>'UsersController@editUser'));
Route::Post('updateUser/{id}',array('as'=>'editUser','middleware' => ['admin'], 'uses'=>'UsersController@updateUser'));
Route::get('deleteUser/{id}',array('as'=>'deleteUser','middleware' => ['admin'], 'uses'=>'UsersController@deleteUser'));

//product_type routes
Route::get('/product_type',array('as'=>'product_type','middleware' => ['auth'],'uses'=>'Product_typesController@viewProduct_type'));
Route::get('createProduct_type',array('as'=>'createProduct_type','middleware' => ['auth'],'uses'=>'Product_typesController@createProduct_type'));
Route::get('storeProduct_type',array('as'=>'storeProduct_type','middleware' => ['auth'],'uses'=>'Product_typesController@storeProduct_type'));
Route::get('/editProduct_type/{id}',array('as'=>'editProduct_type','middleware' => ['auth'],'uses'=>'Product_typesController@editProduct_type'));
Route::get('updateProdcut_type/{id}',array('as'=>'updateProdcut_type','middleware' => ['auth'],'uses'=>'Product_typesController@updateProdcut_type'));
Route::get('deleteProduct_type/{id}',array('as'=>'deleteProduct_type','middleware' => ['auth'],'uses'=>'Product_typesController@deleteProduct_type'));

//brand routes
Route::get('/brand',array('as'=>'brand','middleware' => ['auth'],'uses'=>'BrandsController@viewBrand'));
Route::get('createBrand',array('as'=>'createBrand','middleware' => ['auth'],'uses'=>'BrandsController@createBrand'));
Route::get('storeBrand',array('as'=>'storeBrand','middleware' => ['auth'],'uses'=>'BrandsController@storeBrand'));
Route::get('/editBrand/{id}',array('as'=>'editBrand','middleware' => ['auth'],'uses'=>'BrandsController@editBrand'));
Route::get('updateBrand/{id}',array('as'=>'updateBrand','middleware' => ['auth'],'uses'=>'BrandsController@updateBrand'));
Route::get('deleteBrand/{id}',array('as'=>'deleteBrand','middleware' => ['auth'],'uses'=>'BrandsController@deleteBrand'));


//Product routes
Route::get('/product',array('as'=>'product','middleware' => ['auth'],'uses'=>'ProductsController@viewProduct'));
Route::get('/singleViewProduct/{id}',array('as'=>'singleViewProduct','uses'=>'ProductsController@singleViewProduct'));
Route::get('/createProduct',array('as'=>'createProduct','uses'=>'ProductsController@createProduct'));
Route::get('storeProduct',array('as'=>'storeProduct','uses'=>'ProductsController@storeProduct'));
Route::get('/editProduct/{id}',array('as'=>'editProduct','uses'=>'ProductsController@editProduct'));
Route::get('updateProduct/{id}',array('as'=>'updateProduct','uses'=>'ProductsController@updateProduct'));
Route::get('deleteProduct/{id}',array('as'=>'deleteProduct','uses'=>'ProductsController@deleteProduct'));

//Product Expiry  routes
Route::get('/productExpiry',array('as'=>'product','middleware' => ['auth'],'uses'=>'ProductsController@productExpiryindex'));
Route::get('/searchProductExpiry',array('as'=>'product','middleware' => ['auth'],'uses'=>'ProductsController@searchProductExpiry'));
Route::get('/singleViewProductExpiry/{id}',array('as'=>'singleViewProduct','middleware' => ['auth'],'uses'=>'ProductsController@singleViewProductExpiry'));
Route::get('/createProductExpiry',array('as'=>'createProduct','middleware' => ['auth'],'uses'=>'ProductsController@createProductExpiry'));
Route::get('storeProductExpiry',array('as'=>'storeProduct','middleware' => ['auth'],'uses'=>'ProductsController@storeProductExpiry'));
Route::get('/editProductExpiry/{id}',array('as'=>'editProduct','middleware' => ['auth'],'uses'=>'ProductsController@editProductExpiry'));
Route::get('updateProductExpiry/{id}',array('as'=>'updateProduct','middleware' => ['auth'],'uses'=>'ProductsController@updateProductExpiry'));
Route::get('deleteProductExpiry/{id}/{product}',array('as'=>'deleteProduct','middleware' => ['auth'],'uses'=>'ProductsController@deleteProductExpiry'));



//customer routes
Route::get('/customers',array('as'=>'customers',
    'middleware' => ['auth'], 'uses'=>'CustomersController@index'));
Route::get('singleCustomer/{id}',array('as'=>'singleCustomer','middleware' => ['auth'],'uses'=>'CustomersController@singleCustomer'));
Route::post('/create_new_ledger_choose',array('as'=>'create_new_ledger_choose','middleware' => ['auth'],'uses'=>'CustomersController@create_new_ledger_choose'));
Route::get('/createCustomer',array('as'=>'createCustomer','middleware' => ['auth'],'uses'=>'CustomersController@createCustomer'));
Route::Post('storeCustomer',['as'=>'storeCustomer','middleware' => ['auth'],'uses'=>'CustomersController@storeCustomer']);
Route::get('editCustomer/{id}',array('as'=>'editCustomer','middleware' => ['auth'],'uses'=>'CustomersController@editCustomer'));
Route::Post('updateCustomer/{id}',array('as'=>'updateCustomer','middleware' => ['auth'],'uses'=>'CustomersController@updateCustomer'));
Route::get('deleteCustomer/{id}',array('as'=>'deleteCustomer','middleware' => ['auth'],'uses'=>'CustomersController@deleteCustomer'));
Route::get('customerLogin',['as'=>'customerLogin','middleware' => ['auth'],'uses'=>'CustomersController@customerLogin']);
Route::Post('customerAuth',['as'=>'customerAuth','middleware' => ['auth'],'uses'=>'CustomersController@customerAuth']);

//ledger link
Route::get('ledgerPanel',array('as'=>'ledgerPanel','middleware' => ['auth'],'uses'=>'CustomersController@ledgerPanel'));
//ledger type routes
Route::get('ledger_type_route',array('as'=>'ledger_type_route','middleware' => ['auth'],'uses'=>'CustomersController@ledger_type_route'));
Route::post('save_new_ledger_type',array('as'=>'save_new_ledger_type','middleware' => ['auth'],'uses'=>'CustomersController@save_new_ledger_type'));
Route::post('update_ledger_type/{id}',array('as'=>'update_ledger_type','middleware' => ['auth'],'uses'=>'CustomersController@update_ledger_type'));



//ledger group routes
Route::get('ledger_group_route',array('as'=>'ledger_group_route','middleware' => ['auth'],'uses'=>'CustomersController@ledger_group_route'));
Route::post('save_new_ledger_group',array('as'=>'save_new_ledger_group','middleware' => ['auth'],'uses'=>'CustomersController@save_new_ledger_group'));
Route::post('update_ledger_group/{id}',array('as'=>'update_ledger_group','middleware' => ['auth'],'uses'=>'CustomersController@update_ledger_group'));


//other party links
Route::get('createOtherLedger',array('as'=>'createOtherLedger','middleware' => ['auth'],'uses'=>'CustomersController@createOtherLedger'));
Route::post('saveOtherLedger',array('as'=>'saveOtherLedger','middleware' => ['auth'],'uses'=>'CustomersController@saveOtherLedger'));
Route::get('viewOtherLedger/{id}',array('as'=>'viewOtherLedger','middleware' => ['auth'],'uses'=>'CustomersController@viewOtherLedger'));
Route::get('editOtherLedger/{id}',array('as'=>'editOtherLedger','middleware' => ['auth'],'uses'=>'CustomersController@editOtherLedger'));
Route::post('updateOtherLedger/{id}',array('as'=>'updateOtherLedger','middleware' => ['auth'],'uses'=>'CustomersController@updateOtherLedger'));



//creditors routes
Route::get('/creditors',array('as'=>'creditors','middleware' => ['auth'],'uses'=>'CreditorsController@index'));
Route::get('singleCreditor/{id}',array('as'=>'singleCreditor','middleware' => ['auth'],'uses'=>'CreditorsController@singleCreditor'));
Route::get('/createCreditors',array('as'=>'createCreditors','middleware' => ['auth'],'uses'=>'CreditorsController@createCreditors'));
Route::Post('storeCreditor',['as'=>'storeCreditor','middleware' => ['auth'],'uses'=>'CreditorsController@storeCreditor']);
Route::get('editCreditor/{id}',array('as'=>'editCreditor','middleware' => ['auth'],'uses'=>'CreditorsController@editCreditor'));
Route::Post('updateCreditor/{id}',array('as'=>'updateCreditor','middleware' => ['auth'],'uses'=>'CreditorsController@updateCreditor'));
Route::get('deleteCreditor/{id}',array('as'=>'deleteCreditor','middleware' => ['auth'],'uses'=>'CreditorsController@deleteCreditor'));

//credit sales route
Route::get('allposts',array('as'=>'allposts','middleware' => ['auth'],'uses'=>'SalesController@allposts'));
Route::get('sale',array('as'=>'sale','middleware' => ['auth'],'uses'=>'SalesController@saleIndex'));
Route::get('sale_type',array('as'=>'sale_type','middleware' => ['auth'],'uses'=>'SalesController@sale_type'));
Route::post('store_tax_type',array('as'=>'store_tax_type','middleware' => ['auth'],'uses'=>'SalesController@store_tax_type'));
Route::get('/createSale/{id}',array('as'=>'createSale','middleware' => ['auth'],'uses'=>'SalesController@createSale'));
Route::get('/singleViewSale/{id}',array('as'=>'singleViewSale','middleware' => ['auth'],'uses'=>'SalesController@singleViewSale'));
Route::get('editSale/{id}',array('as'=>'editSale','middleware' => ['auth'],'uses'=>'SalesController@editSale'));
Route::get('/ajax/{id}',array('as'=>'myform.ajax','middleware' => ['auth'],'uses'=>'SalesController@myformAjax'));
Route::Post('storeSale',['as'=>'storeSale','middleware' => ['auth'],'uses'=>'SalesController@storeSale']);
Route::Post('updateSale/{id}',['as'=>'updateSale','middleware' => ['auth'],'uses'=>'SalesController@updateSale']);
Route::get('printSale/{id}',array('as'=>'printSale','middleware' => ['auth'],'uses'=>'SalesController@printSaleInvoice'));
Route::post('AjaxNewCustomer',array('as'=>'AjaxNewCustomer','uses'=>'SalesController@AjaxNewCustomer'));
Route::post('AjaxNewProduct',array('as'=>'AjaxNewProduct','uses'=>'SalesController@AjaxNewProduct'));
Route::post('AjaxUpdateData',array('as'=>'AjaxUpdateData','uses'=>'SalesController@AjaxUpdateData'));
Route::get('AjaxUpdateDataLoop/{id}',array('as'=>'AjaxUpdateDataLoop','uses'=>'SalesController@AjaxUpdateDataLoop'));
// sales invoice
Route::delete('sale_invoiceAjax/{id}',array('as'=>'sale_invoiceAjax','uses'=>'SalesController@saleInvoiceAjax'));
Route::get('AjaxBatch/{id}',array('as'=>'AjaxBatch','uses'=>'SalesController@AjaxBatch'));
Route::get('AjaxCostGst/{id}/{unique}',array('as'=>'AjaxCostGst','uses'=>'SalesController@AjaxCostGst'));
Route::get('AjaxMfgdate/{id}',array('as'=>'AjaxMfgdate','uses'=>'SalesController@AjaxMfgdate'));
Route::get('deleteSale/{id}',array('as'=>'deleteSale','middleware' => ['auth'],'uses'=>'SalesController@deleteSale'));

//purchase route

Route::get('allpurchase',array('as'=>'allpurchase','middleware' => ['auth'],'uses'=>'PurchasesController@allpurchase'));
Route::get('purchase',array('as'=>'purchase', 'middleware' => ['auth'], 'uses'=>'PurchasesController@purchaseIndex'));
Route::get('purchase_type',array('as'=>'purchase_type', 'middleware' => ['auth'], 'uses'=>'PurchasesController@purchase_type'));
Route::post('store_purchase_tax_type',array('as'=>'store_purchase_tax_type','middleware' => ['auth'], 'uses'=>'PurchasesController@store_purchase_tax_type'));
Route::get('/createPurchase/{id}',array('as'=>'createPurchase','middleware' => ['auth'], 'uses'=>'PurchasesController@createPurchase'));
Route::get('/singleViewPurchase/{id}',array('as'=>'singleViewPurchase','middleware' => ['auth'],'uses'=>'PurchasesController@singleViewPurchase'));
Route::get('/editPurchase/{id}',array('as'=>'editPurchase','middleware' => ['auth'],'uses'=>'PurchasesController@editPurchase'));

Route::get('/ajaxPurchase/{id}',array('as'=>'myform.ajax','uses'=>'PurchasesController@myformAjax'));
Route::Post('storePurchase',['as'=>'storePurchase','middleware' => ['auth'], 'uses'=>'PurchasesController@storePurchase']);
Route::Post('updatePurchase/{id}',['as'=>'updatePurchase','middleware' => ['auth'], 'uses'=>'PurchasesController@updatePurchase']);
Route::get('printPurchase/{id}',array('as'=>'printPurchase','middleware' => ['auth'], 'uses'=>'PurchasesController@printPurchaseInvoice'));
// purchase invoice
Route::delete('purchase_invoiceAjax/{id}',array('as'=>'purchase_invoiceAjax','uses'=>'PurchasesController@purchaseInvoiceAjax'));
Route::get('deletePurchase/{id}',array('as'=>'deletePurchase','uses'=>'PurchasesController@deletePurchase'));
//AjaxNewSupplier
Route::post('AjaxNewSupplier',array('as'=>'AjaxNewSupplier','uses'=>'PurchasesController@AjaxNewSupplier'));
Auth::routes();

//Route::get('/home','middleware' => ['auth'], 'HomeController@index');
Route::get('home',array('as'=>'home', 'middleware' => ['auth'],'uses'=>'HomeController@index'));
Route::get('/CustomerHome', 'CustomerHomeController@index');


//Sales Return routes

Route::get('saleReturn',array('as'=>'saleReturn', 'middleware' => ['auth'], 'uses'=>'Sale_returnsController@saleReturnIndex'));
Route::get('/searchSaleToReturn',array('as'=>'searchSaleToReturn','middleware' => ['auth'], 'uses'=>'Sale_returnsController@searchSaleToReturn'));
Route::get('/singleViewSaleReturn/{id}',array('as'=>'singleViewSaleReturn','middleware' => ['auth'], 'uses'=>'Sale_returnsController@singleViewSaleReturn'));
Route::get('/saleDetailsToReturn/{id}',array('as'=>'saleDetailsToReturn','middleware' => ['auth'], 'uses'=>'Sale_returnsController@saleDetailsToReturn'));
Route::Post('storeSaleReturn',['as'=>'storeSaleReturn','middleware' => ['auth'], 'uses'=>'Sale_returnsController@storeSaleReturn']);
Route::get('printSaleReturn/{id}',array('as'=>'printSaleReturn','middleware' => ['auth'], 'uses'=>'Sale_returnsController@printSaleReturn'));
// sales invoice
Route::delete('saleReturn_invoiceAjax/{id}',array('as'=>'saleReturn_invoiceAjax','uses'=>'Sale_returnsController@saleReturn_invoiceAjax'));
Route::get('deleteSaleReturn/{id}',array('as'=>'deleteSaleReturn','middleware' => ['auth'], 'uses'=>'Sale_returnsController@deleteSaleReturn'));


//purchase return routes

Route::get('purchaseReturn',array('as'=>'purchaseReturn','middleware' => ['auth'], 'uses'=>'Purchase_returnController@purchaseReturnIndex'));
Route::get('/searchPurchaseToReturn',array('as'=>'searchPurchaseToReturn','middleware' => ['auth'],'uses'=>'Purchase_returnController@searchPurchaseToReturn'));
Route::get('/singleViewPurchaseReturn/{id}',array('as'=>'singleViewPurchaseReturn','middleware' => ['auth'],'uses'=>'Purchase_returnController@singleViewPurchaseReturn'));
Route::get('/purchaseDetailsToReturn/{id}',array('as'=>'purchaseDetailsToReturn','middleware' => ['auth'],'uses'=>'Purchase_returnController@purchaseDetailsToReturn'));
Route::Post('storePurchaseReturn',['as'=>'storePurchaseReturn','middleware' => ['auth'],'uses'=>'Purchase_returnController@storePurchaseReturn']);
Route::get('printPurchaseReturn/{id}',array('as'=>'printPurchaseReturn','middleware' => ['auth'],'uses'=>'Purchase_returnController@printPurchaseReturn'));
// sales invoice
Route::delete('purchaseReturn_invoiceAjax/{id}',array('as'=>'purchaseReturn_invoiceAjax','uses'=>'Purchase_returnController@purchaseReturn_invoiceAjax'));
Route::get('deletePurchaseReturn/{id}',array('as'=>'deletePurchaseReturn','uses'=>'Purchase_returnController@deletePurchaseReturn'));

//creditnote routes
Route::get('creditnote',array('as'=>'creditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@creditnoteIndex'));
Route::get('/createCreditnote',array('as'=>'createCreditnote','middleware' => ['auth'], 'uses'=>'CreditnoteController@createCreditnote'));
Route::get('/singleViewCreditnote/{id}',array('as'=>'singleViewCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@singleViewCreditnote'));
Route::get('/ajaxCreditnote/{id}',array('as'=>'ajaxCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@ajaxCreditnote'));
Route::post('storeCreditnote',array('as'=>'storeCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@storeCreditnote'));
Route::get('/editCreditnote/{id}',array('as'=>'editCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@editCreditnote'));
Route::post('updateCreditnote/{id}',array('as'=>'updateCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@updateCreditnote'));
Route::get('deleteCreditnote/{id}',array('as'=>'deleteCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@deleteCreditnote'));
Route::get('printCreditnote/{id}',array('as'=>'printCreditnote','middleware' => ['auth'],'uses'=>'CreditnoteController@printCreditnote'));

//Debitnote Routes
Route::get('debitnote',array('as'=>'debitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@debitnoteIndex'));
Route::get('/createDebitnote',array('as'=>'createDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@createDebitnote'));
Route::get('/singleViewDebitnote/{id}',array('as'=>'singleViewDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@singleViewDebitnote'));
Route::get('/ajaxDebitnote/{id}',array('as'=>'ajaxDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@ajaxDebitnote'));
Route::post('storeDebitnote',array('as'=>'storeDebitnote','uses'=>'DebitnoteController@storeDebitnote'));
Route::get('/editDebitnote/{id}',array('as'=>'editDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@editDebitnote'));
Route::post('updateDebitnote/{id}',array('as'=>'updateDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@updateDebitnote'));
Route::get('deleteDebitnote/{id}',array('as'=>'deleteDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@deleteDebitnote'));
Route::get('printDebitnote/{id}',array('as'=>'printDebitnote','middleware' => ['auth'],'uses'=>'DebitnoteController@printDebitnote'));

//Receipts Routes
Route::get('receipt',array('as'=>'receipt','middleware' => ['auth'],'uses'=>'ReceiptsController@receiptIndex'));
Route::get('/createReceipt',array('as'=>'createReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@createReceipt'));
Route::get('/singleViewReceipt/{id}',array('as'=>'singleViewReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@singleViewReceipt'));
Route::get('/ajaxReceipt/{id}',array('as'=>'ajaxReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@ajaxReceipt'));
Route::post('storeReceipt',array('as'=>'storeReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@storeReceipt'));
Route::get('/editReceipt/{id}',array('as'=>'editReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@editReceipt'));
Route::post('updateReceipt/{id}',array('as'=>'updateReceipt','middleware' => ['auth'],'middleware' => ['auth'],'uses'=>'ReceiptsController@updateReceipt'));
Route::get('deleteReceipt/{id}',array('as'=>'deleteReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@deleteReceipt'));
Route::get('printReceipt/{id}',array('as'=>'printReceipt','middleware' => ['auth'],'uses'=>'ReceiptsController@printReceipt'));

//Payments Routes
Route::get('payment',array('as'=>'payment','middleware' => ['auth'],'uses'=>'PaymentsController@paymentIndex'));
Route::get('/createPayment',array('as'=>'createPayment','middleware' => ['auth'],'uses'=>'PaymentsController@createPayment'));
Route::get('/singleViewPayment/{id}',array('as'=>'singleViewPayment','middleware' => ['auth'],'uses'=>'PaymentsController@singleViewPayment'));
Route::get('/ajaxPayment/{id}',array('as'=>'ajaxPayment','middleware' => ['auth'],'uses'=>'PaymentsController@ajaxPayment'));
Route::post('storePayment',array('as'=>'storePayment','middleware' => ['auth'],'uses'=>'PaymentsController@storePayment'));
Route::get('/editPayment/{id}',array('as'=>'editPayment','middleware' => ['auth'],'uses'=>'PaymentsController@editPayment'));
Route::post('updatePayment/{id}',array('as'=>'updatePayment','middleware' => ['auth'],'uses'=>'PaymentsController@updatePayment'));
Route::get('deletePayment/{id}',array('as'=>'deletePayment','middleware' => ['auth'],'uses'=>'PaymentsController@deletePayment'));
Route::get('printPayment/{id}',array('as'=>'printPayment','middleware' => ['auth'],'uses'=>'PaymentsController@printPayment'));

//reports routes
Route::get('report',array('as'=>'report','middleware' => ['auth'],'uses'=>'ReportsController@reportIndex'));
Route::get('customer_ledger',array('as'=>'customer_ledger','middleware' => ['auth'],'uses'=>'ReportsController@customer_ledger'));
Route::get('supplier_report',array('as'=>'supplier_report','middleware' => ['auth'],'uses'=>'ReportsController@supplier_report'));
Route::get('gst_report',array('as'=>'gst_report','uses'=>'ReportsController@gst_report'));
Route::get('sale_day_book',array('as'=>'sale_day_book','uses'=>'ReportsController@sale_day_book'));
Route::get('purchase_day_book',array('as'=>'purchase_day_book','uses'=>'ReportsController@purchase_day_book'));
Route::get('payment_report',array('as'=>'payment_report','uses'=>'ReportsController@payment_report'));
Route::get('receipt_report',array('as'=>'receipt_report','uses'=>'ReportsController@receipt_report'));
Route::get('sale_return_report',array('as'=>'sale_return_report','uses'=>'ReportsController@sale_return_report'));
Route::get('purchase_return_report',array('as'=>'purchase_return_report','uses'=>'ReportsController@purchase_return_report'));
Route::get('cash_report',array('as'=>'cash_report','uses'=>'ReportsController@cash_report'));
Route::get('bank_report',array('as'=>'bank_report','uses'=>'ReportsController@bank_report'));
Route::get('creditnote_report',array('as'=>'creditnote_report','uses'=>'ReportsController@creditnote_report'));
Route::get('debitnote_report',array('as'=>'debitnote_report','uses'=>'ReportsController@debitnote_report'));
Route::get('stock_report',array('as'=>'stock_report','uses'=>'ReportsController@stock_report'));
Route::get('current_stock_report',array('as'=>'current_stock_report','uses'=>'ReportsController@current_stock_report'));
Route::get('contra_report',array('as'=>'contra_report','uses'=>'ReportsController@contra_report'));
Route::get('day_report',array('as'=>'day_report','uses'=>'ReportsController@day_report'));
Route::post('showDayReport',array('as'=>'showDayReport','uses'=>'ReportsController@showDayReport'));
Route::get('custom_sale_report/{from_date}/{upto_date}',array('as'=>'custom_sale_report','uses'=>'ReportsController@custom_sale_report'));
Route::get('custom_purchase_report/{from_date}/{upto_date}',array('as'=>'custom_purchase_report','uses'=>'ReportsController@custom_purchase_report'));
Route::get('custom_saleReturn_report/{from_date}/{upto_date}',array('as'=>'custom_saleReturn_report','uses'=>'ReportsController@custom_saleReturn_report'));
Route::get('custom_purchaseReturn_report/{from_date}/{upto_date}',array('as'=>'custom_purchaseReturn_report','uses'=>'ReportsController@custom_purchaseReturn_report'));
Route::get('custom_receipt_report/{from_date}/{upto_date}',array('as'=>'custom_receipt_report','uses'=>'ReportsController@custom_receipt_report'));
Route::get('custom_payment_report/{from_date}/{upto_date}',array('as'=>'custom_payment_report','uses'=>'ReportsController@custom_payment_report'));
Route::get('custom_creditNote_report/{from_date}/{upto_date}',array('as'=>'custom_creditNote_report','uses'=>'ReportsController@custom_creditNote_report'));
Route::get('custom_debitNote_report/{from_date}/{upto_date}',array('as'=>'custom_debitNote_report','uses'=>'ReportsController@custom_debitNote_report'));
Route::get('custom_contra_report/{from_date}/{upto_date}',array('as'=>'custom_contra_report','uses'=>'ReportsController@custom_contra_report'));

Route::get('monthly_sale_report/{month}',array('as'=>'monthly_sale_report','uses'=>'ReportsController@monthly_sale_report'));
Route::get('monthly_purchase_report/{month}',array('as'=>'monthly_purchase_report','uses'=>'ReportsController@monthly_purchase_report'));
Route::get('monthly_saleReturn_report/{month}',array('as'=>'monthly_saleReturn_report','uses'=>'ReportsController@monthly_saleReturn_report'));
Route::get('monthly_purchaseReturn_report/{month}',array('as'=>'monthly_purchaseReturn_report','uses'=>'ReportsController@monthly_purchaseReturn_report'));
Route::get('monthly_receipt_report/{month}',array('as'=>'monthly_receipt_report','uses'=>'ReportsController@monthly_receipt_report'));
Route::get('monthly_payment_report/{month}',array('as'=>'monthly_payment_report','uses'=>'ReportsController@monthly_payment_report'));
Route::get('monthly_creditNote_report/{month}',array('as'=>'monthly_creditNote_report','uses'=>'ReportsController@monthly_creditNote_report'));
Route::get('monthly_debitNote_report/{month}',array('as'=>'monthly_debitNote_report','uses'=>'ReportsController@monthly_debitNote_report'));
Route::get('monthly_contra_report/{month}',array('as'=>'monthly_contra_report','uses'=>'ReportsController@monthly_contra_report'));
Route::get('trial_balnace/{from_date}/{upto_date}',array('as'=>'trial_balnace','uses'=>'ReportsController@trial_balnace'));

//trial balance report
//indirect exp
Route::get('indirect_exp_report/{from_date}/{upto_date}',array('as'=>'indirect_exp_report','uses'=>'ReportsController@indirect_exp_report'));
Route::get('detail_indirect_exp/{from_date}/{upto_date}/{id}',array('as'=>'indirect_exp_report','uses'=>'ReportsController@detail_indirect_exp'));

//direct exp
Route::get('direct_exp_report/{from_date}/{upto_date}',array('as'=>'direct_exp_report','uses'=>'ReportsController@direct_exp_report'));
Route::get('detail_direct_exp/{from_date}/{upto_date}/{id}',array('as'=>'detail_direct_exp','uses'=>'ReportsController@detail_direct_exp'));


//Current Assets Report
Route::get('current_assets_report/{from_date}/{upto_date}',array('as'=>'current_assets_report','uses'=>'ReportsController@current_assets_report'));
Route::get('detail_current_assets/{from_date}/{upto_date}/{id}',array('as'=>'detail_current_assets','uses'=>'ReportsController@detail_current_assets'));


//Current Liablities Report
Route::get('current_liablities/{from_date}/{upto_date}',array('as'=>'current_liablities','uses'=>'ReportsController@current_liablities'));
Route::get('detail_direct_exp/{from_date}/{upto_date}/{id}',array('as'=>'detail_direct_exp','uses'=>'ReportsController@detail_direct_exp'));


//Profit and loss A/c
Route::get('pl_account/{from_date}/{upto_date}',array('as'=>'pl_account','uses'=>'ReportsController@pl_account'));




//journal entry links here
Route::get('otherCharge',array('as'=>'otherCharge','middleware' => ['auth'],'uses'=>'Other_ChargesController@otherChargeIndex'));
Route::get('createOtherCharge',array('as'=>'createOtherCharge','middleware' => ['auth'],'uses'=>'Other_ChargesController@createOtherCharge'));
Route::post('storeJournal',array('as'=>'storeJournal','middleware' => ['auth'],'uses'=>'Other_ChargesController@storeJournal'));
Route::get('/singleViewOtherCharge/{id}',array('as'=>'singleViewOtherCharge','middleware' => ['auth'],'uses'=>'Other_ChargesController@singleViewOtherCharge'));
Route::get('printJournal/{id}',array('as'=>'printJournal','middleware' => ['auth'],'uses'=>'Other_ChargesController@printJournal'));
Route::get('/editOtherCharge/{id}',array('as'=>'editOtherCharge','middleware' => ['auth'],'uses'=>'Other_ChargesController@editOtherCharge'));
Route::post('updateOtherCharge/{id}',array('as'=>'updateOtherCharge','middleware' => ['auth'],'uses'=>'Other_ChargesController@updateOtherCharge'));
Route::get('deleteOtherCharge/{id}',array('as'=>'deleteOtherCharge','middleware' => ['auth'],'uses'=>'Other_ChargesController@deleteOtherCharge'));


//company settings
Route::get('company',array('as'=>'company','middleware' => ['auth'],'uses'=>'HomeController@company'));
Route::post('storeCompany/{id}',array('as'=>'storeCompany','middleware' => ['auth'],'uses'=>'HomeController@storeCompany'));

//pdf report link

$newURL=  '/pdf/receipt.php?data=';
Route::get($newURL.'{id}',array('as'=>'myreport'));

//payment routes
$newURL=  '/pdf/payment.php?data=';
Route::get($newURL.'{id}',array('as'=>'payment_print'));

//Debitnote routes
$newURL=  '/pdf/debitnote.php?data=';
Route::get($newURL.'{id}',array('as'=>'debitnote_print'));


//Creditnote routes
$newURL=  '/pdf/creditnote.php?data=';
Route::get($newURL.'{id}',array('as'=>'creditnote_print'));



///Debtors sale invoice
$newURL=  '/pdf/sale.php?data=';
Route::get($newURL.'{id}',array('as'=>'sale_invoice_print'));


$newURL=  '/pdf/sale_retail.php?data=';
Route::get($newURL.'{id}',array('as'=>'sale_invoice_retail_print'));


$newURL=  '/pdf/sale_format.php?data=';
Route::get($newURL.'{id}',array('as'=>'sale_invoice_format_print'));

$newURL=  '/pdf/sale_retail_format.php?data=';
Route::get($newURL.'{id}',array('as'=>'sale_invoice_format_retail_print'));




Route::get('sale_format_index',array('as'=>'sale_format_index','middleware' => ['auth'],'uses'=>'SalesFormatController@saleIndex_format'));
Route::get('sale_format_type',array('as'=>'sale_format_type','middleware' => ['auth'],'uses'=>'SalesFormatController@sale_type_format'));
Route::get('/createSale_format/{id}',array('as'=>'createSale_format','middleware' => ['auth'],'uses'=>'SalesFormatController@createSale_format'));
Route::post('store_tax_type_format',array('as'=>'store_tax_type_format','middleware' => ['auth'],'uses'=>'SalesFormatController@store_tax_type_format'));
Route::Post('storeSale_format',['as'=>'storeSale_format','middleware' => ['auth'],'uses'=>'SalesFormatController@storeSale_format']);
Route::get('editSale_format/{id}',array('as'=>'editSale_format','middleware' => ['auth'],'uses'=>'SalesFormatController@editSale_format'));
Route::get('allposts1',array('as'=>'allposts1','middleware' => ['auth'],'uses'=>'SalesFormatController@allposts1'));
Route::get('deleteSale_format/{id}',array('as'=>'deleteSale_format','middleware' => ['auth'],'uses'=>'SalesFormatController@deleteSale_format'));
Route::Post('updateSale_format/{id}',['as'=>'updateSale_format','middleware' => ['auth'],'uses'=>'SalesFormatController@updateSale_format']);
Route::delete('sale_invoiceAjax_format/{id}',array('as'=>'sale_invoiceAjax_format','uses'=>'SalesFormatController@sale_invoiceAjax_format'));

// challan

Route::get('challan',array('as'=>'challan', 'middleware' => ['auth'], 'uses'=>'ChallanController@challan'));
Route::get('new_challan_entry',array('as'=>'new_challan_entry', 'middleware' => ['auth'], 'uses'=>'ChallanController@new_challan_entry'));
Route::Post('storeChallan',['as'=>'storeChallan','middleware' => ['auth'],'uses'=>'ChallanController@storeChallan']);
Route::get('allposts_challan',array('as'=>'allposts_challan','middleware' => ['auth'],'uses'=>'ChallanController@allposts_challan'));
Route::get('editChallan/{id}',array('as'=>'editChallan','middleware' => ['auth'],'uses'=>'ChallanController@editChallan'));
Route::Post('updateChallan/{id}',['as'=>'updateChallan','middleware' => ['auth'],'uses'=>'ChallanController@updateChallan']);
Route::delete('challan_child_deleteAjax/{id}',array('as'=>'challan_child_deleteAjax','uses'=>'ChallanController@challan_child_deleteAjax'));
Route::get('deleteChallan/{id}',array('as'=>'deleteChallan','middleware' => ['auth'],'uses'=>'ChallanController@deleteChallan'));
Route::get('printChallan/{id}',array('as'=>'printChallan','middleware' => ['auth'],'uses'=>'ChallanController@printChallan'));
Route::get('challan_return',array('as'=>'challan_return', 'middleware' => ['auth'], 'uses'=>'ChallanController@challan_return'));
Route::Post('storeChallanSale',['as'=>'storeChallanSale','middleware' => ['auth'],'uses'=>'ChallanController@storeChallanSale']);
Route::get('saleChallan/{id}',array('as'=>'saleChallan','middleware' => ['auth'],'uses'=>'ChallanController@saleChallan'));

//Challan Return
Route::get('/searchchallanToReturn',array('as'=>'searchchallanToReturn','middleware' => ['auth'],'uses'=>'ChallanController@searchchallanToReturn'));
Route::get('/challanDetailsToReturn/{id}',array('as'=>'challanDetailsToReturn','middleware' => ['auth'],'uses'=>'ChallanController@challanDetailsToReturn'));
Route::post('/challanreturn',array('as'=>'challanreturn','middleware' => ['auth'],'uses'=>'ChallanController@challanreturn'));
Route::get('/editreturn/{id}',array('as'=>'editreturn','middleware' => ['auth'],'uses'=>'ChallanController@editreturn'));
Route::get('/printreturn/{id}',array('as'=>'printreturn','middleware' => ['auth'],'uses'=>'ChallanController@printreturn'));
Route::get('/deletereturn/{id}',array('as'=>'deletereturn','middleware' => ['auth'],'uses'=>'ChallanController@deletereturn'));


//princt Sale Invoice
Route::get('printnewsaleInvoice/{id}',array('as'=>'printnewsaleInvoice','middleware' => ['auth'],'uses'=>'SalesController@printnewsaleInvoice'));




Route::get('challan_date_issue',array('as'=>'challan_date_issue','uses'=>'ReportsController@challan_date_issue'));




//testing
Route::get('changePurchasePartyBillDate',array('as'=>'changePurchasePartyBillDate','uses'=>'PurchasesController@changePurchasePartyBillDate'));